import { Artifact } from 'ai-interface/types/artifact';
import { ArmyType } from 'ai-interface/types/army';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { heroExperienceLevels } from 'ai-interface/constants/experience';
import { EMPTY_MAP } from '../../functions';

export const playerColors = ['#FCFCFC', '#FCEC20', '#FCA000', '#C41C00', '#6FC417', '#4AC7D1', '#005CD0', '#222', '#909090'];

/** Defines default properties of default army types. */
export const allArmyTypes: { [armyTypeId: string]: ArmyType } = {
  ...standardArmyTypes,
  rnd0: {
    id: 'rnd0',
    name: 'Random Level 0 Army',
    strength: 0,
    hitPoints: 2,
    abilities: EMPTY_MAP,
    move: 0,
    upkeep: 0,
    trainingTime: 1,
    buildingCost: 0,
    hero: false,
    neutralStartSize: 1,
    neutralMaxSize : 4
  },
  rnd1: {
    id: 'rnd1',
    name: 'Random Level 1 Army',
    strength: 0,
    hitPoints: 2,
    abilities: EMPTY_MAP,
    move: 0,
    upkeep: 0,
    trainingTime: 1,
    buildingCost: 0,
    hero: false,
    neutralStartSize: 1,
    neutralMaxSize : 4
  },
  rnd2: {
    id: 'rnd2',
    name: 'Random Level 2 Army',
    strength: 0,
    hitPoints: 2,
    abilities: EMPTY_MAP,
    move: 0,
    upkeep: 0,
    trainingTime: 2,
    buildingCost: 0,
    hero: false,
    neutralStartSize: 1,
    neutralMaxSize : 4
  },
  rnd3: {
    id: 'rnd3',
    name: 'Random Level 3 Army',
    strength: 0,
    hitPoints: 3,
    abilities: EMPTY_MAP,
    move: 0,
    upkeep: 0,
    trainingTime: 3,
    buildingCost: 0,
    hero: false,
    neutralStartSize: 1,
    neutralMaxSize : 4
  },
  rnd4: {
    id: 'rnd4',
    name: 'Random Level 4 Army',
    strength: 0,
    hitPoints: 4,
    abilities: EMPTY_MAP,
    move: 0,
    upkeep: 0,
    trainingTime: 4,
    buildingCost: 0,
    hero: false,
    neutralStartSize: 1,
    neutralMaxSize : 3
  }
};

/** This ordering should include non-standard army types such as "Random Level 1 Army". User won't see them. */
export const defaultArmyTypeIdToOrder: { [armyTypeId: string]: number } = {
  scou: 0,
  crow: 1,
  linf: 2,
  orcs: 3,
  lcav: 4,
  rnd0: 5,
  dwar: 6,
  scor: 7,
  hinf: 8,
  elf: 9,
  pike: 10,
  rnd1: 11,
  wolf: 12,
  cata: 13,
  mino: 14,
  hcav: 15,
  gian: 16,
  spid: 17,
  rnd2: 18,
  ghos: 19,
  mamm: 20,
  pega: 21,
  wurm: 22,
  grif: 23,
  wiz: 24,
  medu: 25,
  unic: 26,
  rnd3: 27,
  demn: 28,
  elem: 29,
  devl: 30,
  arch: 31,
  drag: 32,
  rnd4: 33
};
for (let lvl = 1; lvl < heroExperienceLevels.length; lvl++) {
  defaultArmyTypeIdToOrder[`hro${lvl}`] = Object.values(defaultArmyTypeIdToOrder).length;
}

export const allArtifacts: { [artifactId: string]: Artifact } = {
  ...standardArtifacts,
  randomCommonArtifact: {
    id: 'randomCommonArtifact',
    name: 'Random Common Artifact',
    rarity: 1,
    abilities: EMPTY_MAP
  },
  randomMinorArtifact: {
    id: 'randomMinorArtifact',
    name: 'Random Minor Artifact',
    rarity: 2,
    abilities: EMPTY_MAP
  },
  randomMajorArtifact: {
    id: 'randomMajorArtifact',
    name: 'Random Major Artifact',
    rarity: 3,
    abilities: EMPTY_MAP
  },
  randomRelicArtifact: {
    id: 'randomRelicArtifact',
    name: 'Random Relic Artifact',
    rarity: 4,
    abilities: EMPTY_MAP
  }
};
