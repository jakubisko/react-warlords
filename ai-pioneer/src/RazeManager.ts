import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { DONT_BUY } from './main';
import { razeCost } from 'ai-interface/constants/city';
import { ThreatManager } from './ThreatManager';
import { CityData } from 'ai-interface/types/city';
import { SearchPathArgs } from 'ai-interface/types/path';
import { straightMoveCost } from 'ai-interface/constants/path';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { Personality } from './helpers/Personality';

export class RazeManager {

  /** shouldIRazeTheCity is an expensive operation, so cache the results. */
  private readonly cacheCityIdToShouldIRaze: { [cityId: string]: boolean } = {};

  private readonly myPlayerId: number;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly threatManager: ThreatManager,
    private readonly personality: Personality
  ) {
    this.myPlayerId = game.getMyPlayerId();
  }

  public shouldIRazeTheCity(city: CityData, guardPower: number): boolean {
    const cachedResult = this.cacheCityIdToShouldIRaze[city.id];
    if (cachedResult !== undefined) {
      return cachedResult;
    }

    if (DONT_BUY) {
      return false;
    }

    const result = Math.random() < this.personality.razeProbability
      && this.game.getGold() >= razeCost
      && !city.capitol
      && guardPower > 0
      && this.threatManager.getThreatToCityUpToDay(city.id, 0) > 3 * guardPower
      && this.surroundedByEnemies(city)
      && this.land.getAllCityIds()
        .map(cityId => this.game.getCity(cityId))
        .some(city2 => city2.owner === this.myPlayerId && !city2.razed && city.id !== city2.id);

    this.cacheCityIdToShouldIRaze[city.id] = result;
    return result;
  }

  /**
   * Return true if all the nearest cities to this city belong to the enemy.
   * That means that this city was probably grabbed by some lone scout behind the front line. There is no hope of holding it.
   */
  private surroundedByEnemies(city: CityData): boolean {
    const NO_OF_NEAREST_CITIES_TO_CHECK = 3;

    const playersTeam = this.game.getPlayersTeam();

    const cityIdsFound: { [cityId: string]: boolean } = {
      [city.id]: true
    };

    const searchArgs: SearchPathArgs = {
      start: city,
      isDestination: (row, col) => {
        const cityId = this.land.getCityIdAt({ row, col });
        if (cityId !== undefined && !(cityId in cityIdsFound)) {
          const { razed, owner } = this.game.getCity(cityId);
          if (!razed) {
            if (owner === neutralPlayerId || playersTeam[owner] === playersTeam[this.myPlayerId]) {
              return true; // We found a city that doesn't belong to an enemy. We can end - we're not surrounded by enemies.
            } else {
              cityIdsFound[cityId] = true;
              return Object.keys(cityIdsFound).length > NO_OF_NEAREST_CITIES_TO_CHECK;
            }
          }
        }
        return false;
      },
      isToBeAvoided: () => false,
      moveType: {
        abilities: {},
        groupMoveOnLand: 20 * straightMoveCost,
        groupSize: 1
      },
      initialMoveLeft: 20 * straightMoveCost,
      maxDay: 2
    };
    this.game.calculateTimeMatrix(searchArgs);

    return Object.keys(cityIdsFound).length > NO_OF_NEAREST_CITIES_TO_CHECK;
  }

}
