import { Terrain, terrainToName } from './terrain';

/** How many artifacts can one hero take at once. There is no such limit on artifacts laying on the ground. */
export const maxHeroArtifactsCapacity = 8;

/** An artifact has exactly one of these abilities. */
export enum ArtifactAbility {
  StrengthBonus = 'StrengthBonus',
  StrengthBonusOnIce = 'StrengthBonusOnIce',
  StrengthBonusWhenAttacking = 'StrengthBonusWhenAttacking',
  StrengthBonusWhenDefending = 'StrengthBonusWhenDefending',
  LeadershipBonus = 'LeadershipBonus',
  IncomeBonus = 'IncomeBonus',
  MoveBonus = 'MoveBonus',
  ViewRadiusBonus = 'ViewRadiusBonus',
  HitPointsBonus = 'HitPointsBonus',
  GroupFlying = 'GroupFlying',
  FrighteningBonus = 'FrighteningBonus',
  AntiAirBonus = 'AntiAirBonus',
  AmbushBonus = 'AmbushBonus',
  ExperienceBonus = 'ExperienceBonus',
  SummonsArmiesOfLevel = 'SummonsArmiesOfLevel',
  Blessing = 'Blessing',
  NoTerrainPenalty = 'NoTerrainPenalty',
  LowerEnemyAmbush = 'LowerEnemyAmbush',
  IncreaseGroupAmbush = 'IncreaseGroupAmbush',
  CancelEnemyTerrainBonus = 'CancelEnemyTerrainBonus',
  CancelEnemyFortificationBonus = 'CancelEnemyFortificationBonus',
  DecreaseTrainingTime = 'DecreaseTrainingTime',
  GroupMoveBonus = 'GroupMoveBonus',
  CancelsArtifactPenalties = 'CancelsArtifactPenalties'
}

/** Converts ArtifactAbility to the user-friendly description of the artifact. */
export const artifactAbilityToText: { [key in ArtifactAbility]: string } = Object.freeze({
  StrengthBonus: `+{0} to Strength.`,
  StrengthBonusOnIce: `+{0} to Strength when fighting on ${terrainToName[Terrain.Ice]}.`,
  StrengthBonusWhenAttacking: `+{0} to Strength when attacking.`,
  StrengthBonusWhenDefending: `+{0} to Strength when defending.`,
  LeadershipBonus: `+{0} to Leadership.`,
  IncomeBonus: `+{0} to kingdom's daily income.`,
  MoveBonus: `+{0} to hero's Move on land.`,
  ViewRadiusBonus: `+{0} to View radius.`,
  HitPointsBonus: `+{0} to Hit points.`,
  GroupFlying: `Entire group can move as if it had flying.`,
  FrighteningBonus: `Frightening - Each non-hero army in enemy group gets -{0} Strength.`,
  AntiAirBonus: `Anti-air - Each flying army in enemy group gets -{0} Strength.`,
  AmbushBonus: `Ambush - When the hero first enters the combat, he has {0}% chance to kill enemy army.`,
  ExperienceBonus: `+{0} experiences at the start of each day.`,
  SummonsArmiesOfLevel: `Summons random level {0} army at the start of each week.`,
  Blessing: `At the start of each day, blesses each army in hero's group with base Strength {0} or less.`,
  NoTerrainPenalty: `No move penalty on all terrains for entire group.`,
  LowerEnemyAmbush: `Ambush of every enemy army has {0}% chance to fail.`,
  IncreaseGroupAmbush: `Each army with Ambush in group has +{0} to its value.`,
  CancelEnemyTerrainBonus: `Cancels enemy terrain bonuses.`,
  CancelEnemyFortificationBonus: `Cancels enemy Fortification bonus.`,
  DecreaseTrainingTime: `While the hero stands in the city, training of new armies is faster.`,
  GroupMoveBonus: `+{0} to Move on land for entire group.`,
  CancelsArtifactPenalties: `Cancels negative effects of other equipped artifacts.`
});
