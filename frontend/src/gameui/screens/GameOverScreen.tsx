import './GameOverScreen.scss';
import defeat from '../common/images/illustrationDefeat.jpg';
import victory from '../common/images/illustrationVictory.jpg';
import { useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import { ScreenEnum } from '../../model/ui/types';
import { setScreen } from '../../model/ui/actions';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { useDispatch, useSelector } from 'react-redux';
import { getVictoryText } from '../../model/game/selectorsTurnPassing';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { AppDispatch } from '../../model/root/configureStore';
import { EMPTY_ARRAY } from '../../functions';

export default function GameOverScreen() {
  const victoryText = useSelector(getVictoryText);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    soundEffects.play(victoryText !== null ? SoundEffect.ExploringSuccess : SoundEffect.ExploringFailure);
  }, EMPTY_ARRAY);

  // Allow scrolling since the text can get long with a big team of long names.
  return (
    <Modal className="screen game-over-screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>{victoryText !== null ? 'Victory' : 'Defeat'}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <img src={victoryText !== null ? victory : defeat} alt="" draggable={false} />
        <div className="description">{victoryText ?? 'You have been defeated!'}</div>
      </Modal.Body>

      <Modal.Footer>
        <ButtonWithShortcut
          shortcutCode="Enter"
          allowShortcutWhileModalIsOpen={true}
          onClick={() => dispatch(setScreen(ScreenEnum.TimelineScreen))}
        >
          See game timeline
        </ButtonWithShortcut>
      </Modal.Footer>
    </Modal>
  );
}
