import './CityModal.scss';
import { useCallback, useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, ButtonGroup, Modal } from 'react-bootstrap';
import { setCurrentlyTraining, setCaravanDestination } from '../../model/game/actionsPlaying';
import { razeCity, buildTrainingFacility, demolishTrainingFacility } from '../../model/game/actionsPlayingThunks';
import ArmyTypePicker from './subcomponents/ArmyTypePicker';
import { allArmyTypes } from '../../model/game/constants';
import { getNumberOfArmiesPresentInCity } from '../../model/game/selectorsPlaying';
import { setShowingCity, setMapFocus, setErrorMessage } from '../../model/ui/actions';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import ArmyImage from '../common/ArmyImage';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import StrategicMap from '../map/strategic-map/StrategicMap';
import { maxCityTrainingCapacity, maxCaravanCapacity, caravanDelayDays, razeCost, cityIncome, demolishCost } from 'ai-interface/constants/city';
import { EMPTY_ARRAY, createArray, getHighestItem } from '../../functions';
import { Place } from 'ai-interface/types/place';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { IncomeAndFortificationDescription } from '../common/IncomeAndFortificationDescription';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { setSoundSource } from '../../model/sound/actions';
import { IncomingCaravan } from 'ai-interface/types/city';
import { UserError } from '../../model/ui/types';

interface CityModalProps {
  cityId: string;
}

export default function CityModal({ cityId }: CityModalProps) {
  const city = useSelector((state: RootState) => state.game.cities[cityId]);
  const gold = useSelector((state: RootState) => state.game.players[state.game.cities[cityId].owner].gold);
  const numberOfArmiesPresentInCity = useSelector((state: RootState) => getNumberOfArmiesPresentInCity(state, cityId));
  const cities = useSelector((state: RootState) => state.game.cities);
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(0);
  const [isBuildSubmodalShown, setBuildSubmodalShown] = useState(false);
  const [isSettingCaravan, setSettingCaravan] = useState(false);
  const [isShowAllCaravans, setShowAllCaravans] = useState(false);

  function subpageTraining(): JSX.Element {
    const armyType = city.currentlyTrainingArmyTypeId === undefined ? undefined : allArmyTypes[city.currentlyTrainingArmyTypeId];
    return (
      <div className="h-100 d-flex flex-column justify-content-between">
        <IncomeAndFortificationDescription income={cityIncome * (city.capitol ? 2 : 1)} fortification={city.capitol ? 2 : 1} />

        <div>
          <div className="m-1">
            Available training facilities:
          </div>

          <div className="text-nowrap">
            <ArmyTypePicker
              enforceNumberOfSlots={maxCityTrainingCapacity}
              availableArmyTypeIds={city.currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[]}
              selectedArmyTypeIds={(city.currentlyTrainingArmyTypeId === undefined) ? EMPTY_ARRAY as string[] : [city.currentlyTrainingArmyTypeId]}
              changeSelectionButton={(armyTypeId, currentlySelected, close) => (
                <ButtonWithShortcut
                  shortcutCode="Enter"
                  allowShortcutWhileModalIsOpen={true}
                  onClick={() => {
                    dispatch(setCurrentlyTraining(city.id, currentlySelected ? undefined : armyTypeId));
                    close();
                  }}
                >
                  {currentlySelected ? 'Stop training' : 'Start training'}
                </ButtonWithShortcut>
              )}
            />
          </div>

          <div className="m-1">
            {armyType === undefined
              ? 'Currently training nothing'
              : `Next army trained in ${city.turnsTrainingLeft} ${(city.turnsTrainingLeft ?? 0) > 1 ? 'days' : 'day'}`
            }
          </div>
        </div>

        <Button onClick={() => setBuildSubmodalShown(true)}>Build / demolish</Button>

        {isBuildSubmodalShown && submodalBuilding()}
      </div>
    );
  }

  function submodalBuilding(): JSX.Element {
    const atMaxCapacity = (city.currentlyTrainableArmyTypeIds?.length ?? 0) >= maxCityTrainingCapacity;
    return (
      <Modal className="submodal-building " show centered onHide={() => setBuildSubmodalShown(false)}>
        <Modal.Header>
          <Modal.Title>
            Build or demolish
          </Modal.Title>
        </Modal.Header>

        <Modal.Body className="text-center">
          <ArmyTypePicker
            availableArmyTypeIds={Object.keys(standardArmyTypes).filter(armyTypeId => !standardArmyTypes[armyTypeId].hero)}
            selectedArmyTypeIds={city.currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[]}
            grayedArmyTypeIds={Object.keys(allArmyTypes).filter(armyTypeId =>
              (city.currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY).includes(armyTypeId)
                ? (gold < demolishCost)
                : (gold < allArmyTypes[armyTypeId].buildingCost))}
            changeSelectionButton={(armyTypeId, currentlySelected, close) => (
              <ButtonWithShortcut
                shortcutCode="Enter"
                allowShortcutWhileModalIsOpen={true}
                disabled={(!currentlySelected && atMaxCapacity)
                  || (!currentlySelected && gold < allArmyTypes[armyTypeId].buildingCost)
                  || (currentlySelected && gold < demolishCost)}
                onClick={() => {
                  if (currentlySelected) {
                    dispatch(demolishTrainingFacility(city.id, armyTypeId));
                    setBuildSubmodalShown(false);
                    soundEffects.play(SoundEffect.DemolishTrainingFacility);
                  } else {
                    dispatch(buildTrainingFacility(city.id, armyTypeId));
                    dispatch(setCurrentlyTraining(city.id, armyTypeId));
                    setBuildSubmodalShown(false);
                    soundEffects.play(SoundEffect.BuildTrainingFacility);
                  }
                  close();
                }}
              >
                {currentlySelected
                  ? (
                    `Demolish cost ${demolishCost} gold`
                  )
                  : (
                    atMaxCapacity
                      ? `All ${maxCityTrainingCapacity} slots taken`
                      : `Build cost ${allArmyTypes[armyTypeId].buildingCost} gold`
                  )}
              </ButtonWithShortcut>
            )}
          />
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={() => setBuildSubmodalShown(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function subpageCaravan() {
    const titles = ['Armies arriving next turn:', 'Armies arriving turn after:'];
    const dayToIncomingArmyTypeIds: (string | null)[][] = createArray(caravanDelayDays,
      dayMinus1 => (city.incomingCaravans ?? EMPTY_ARRAY as IncomingCaravan[])
        .filter(item => item.days === dayMinus1 + 1)
        .map(({ armyTypeId }) => armyTypeId));

    for (const incomingArmyTypeIds of dayToIncomingArmyTypeIds) {
      while (incomingArmyTypeIds.length < maxCaravanCapacity) {
        incomingArmyTypeIds.push(null);
      }
    }

    return (
      <div className="h-100 d-flex flex-column justify-content-between">
        {dayToIncomingArmyTypeIds.map((incomingArmyTypeIds, dayMinus1) => (
          <div key={dayMinus1}>
            <div>{titles[dayMinus1]}</div>
            {incomingArmyTypeIds.map((armyTypeId, index) => (
              <span className="m-1" key={index}>
                <ArmyImage
                  armyTypeId={armyTypeId}
                  selectable={true}
                  onLeftClick="INFO"
                  onRightClick="INFO"
                />
              </span>
            ))}
          </div>
        ))}

        <ButtonGroup vertical>
          <Button
            onClick={() => setSettingCaravan(!isSettingCaravan)}
            active={isSettingCaravan}
          >
            {isSettingCaravan ? 'Click destination city' : 'Set destination'}
          </Button>
          <Button
            onClick={() => setShowAllCaravans(!isShowAllCaravans)}
            active={isShowAllCaravans}
          >
            Show all caravans
          </Button>
        </ButtonGroup>
      </div>
    );
  }

  const subpageRaze = () => (
    <>
      <p>Razing will permanently destroy the city.</p>
      <p>Presence of an army in the city to start fires is required.</p>
      <Button
        className="w-100"
        disabled={(gold < razeCost) || numberOfArmiesPresentInCity === 0}
        onClick={() => {
          dispatch(setShowingCity(null));
          dispatch(razeCity(city.id));
          soundEffects.play(SoundEffect.Raze);
        }}
      >
        {numberOfArmiesPresentInCity > 0
          ? `Raze for ${razeCost} gold`
          : `Requires army presence`
        }
      </Button>
    </>
  );

  const handleStrategicMapClick = useCallback((place: Place) => {
    // First we need to find which of our cities was clicked.
    const pRow = place.row - 1; // Fix for the fact that city is 2x2 tiles large.
    const pCol = place.col - 1;

    const nearestCity = getHighestItem(
      Object.values(cities).filter(city2 => !city2.razed && city2.owner === city.owner),
      ({ row, col }) => - ((row - pRow) ** 2 + (col - pCol) ** 2)
    );

    if (nearestCity !== undefined) {
      if (isSettingCaravan) {
        if (nearestCity.id !== city.id &&
          nearestCity.id !== city.caravanToCityId &&
          Object.values(cities)
            .filter(({ owner, caravanToCityId }) => owner === city.owner && caravanToCityId === nearestCity.id)
            .length >= maxCaravanCapacity) {
          const reason = `No more than ${maxCaravanCapacity} cities may set their destination to the same city at any one time.`;
          dispatch(setErrorMessage(`Can't set up caravan`, new UserError(reason)));
        } else {
          dispatch(setCaravanDestination(city.id, city.id === nearestCity.id ? undefined : nearestCity.id));
        }
        setSettingCaravan(false);

      } else {
        dispatch(setMapFocus(nearestCity.row + 1, nearestCity.col + 1));
        dispatch(setSoundSource(nearestCity.row + 1, nearestCity.col + 1));
        dispatch(setShowingCity(nearestCity.id));
      }
    }
  }, [cities, city.caravanToCityId, city.id, city.owner, isSettingCaravan]);

  function handleSetSubpage(newSubpage: number) {
    setSubpage(newSubpage);
    setSettingCaravan(false);
  }

  let subpageEl: JSX.Element;
  switch (subpage) {
    default: subpageEl = subpageTraining(); break;
    case 1: subpageEl = subpageCaravan(); break;
    case 2: subpageEl = subpageRaze(); break;
  }

  // Draw caravan lines on StrategicMap
  const lines: { from: Place, to: Place, bidirectional: boolean }[] = [];
  for (const cityId in cities) {
    const { id, owner, row, col, caravanToCityId } = cities[cityId];
    if (caravanToCityId !== undefined && owner === city.owner) {
      if (isShowAllCaravans || city.id === id || city.id === caravanToCityId) {
        const toCity = cities[caravanToCityId];
        lines.push({
          from: { row: row + 0.5, col: col + 0.5 },
          to: { row: toCity.row + 0.5, col: toCity.col + 0.5 },
          bidirectional: toCity.caravanToCityId === id
        });
      }
    }
  }

  return (
    <Modal className="city-modal overflow-hidden" show centered onHide={() => dispatch(setShowingCity(null))}>
      <Modal.Header>
        <ModalTitleWithFlags owner={city.owner} caption={city.name} />
      </Modal.Header>

      <Modal.Body>
        <div className="map-part">
          <StrategicMap
            drawCircle={{ row: city.row + 0.5, col: city.col + 0.5 }}
            drawLines={lines}
            onClick={handleStrategicMapClick}
          />
        </div>

        <div className="content-part">
          {subpageEl}
        </div>
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <ButtonGroup>
          <Button active={subpage === 0} onClick={() => handleSetSubpage(0)}>Training</Button>
          <Button active={subpage === 1} onClick={() => handleSetSubpage(1)}>Caravan</Button>
          <Button active={subpage === 2} onClick={() => handleSetSubpage(2)}>Raze</Button>
        </ButtonGroup>

        <Button onClick={() => dispatch(setShowingCity(null))}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
