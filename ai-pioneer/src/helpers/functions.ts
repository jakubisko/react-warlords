export function shuffle<T>(a: T[]): void {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor((i + 1) * Math.random());
    [a[i], a[j]] = [a[j], a[i]];
  }
}

export function clamp(min: number, max: number, value: number): number {
  if (value > max) {
    return max;
  }
  return value < min ? min : value;
}
