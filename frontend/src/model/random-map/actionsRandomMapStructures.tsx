import { AppDispatch, RootState } from '../root/configureStore';
import { createAndPlaceStructure, setRuinLevel } from '../game/actionsEditor';
import { getRandom, shuffle, getRandomItem, createArray } from '../../functions';
import { Structure } from 'ai-interface/constants/structure';
import { canPlaceStructure } from '../editor/selectors';
import { Terrain } from 'ai-interface/constants/terrain';
import { getAllWaterAreas, isCityTooRemote } from './selectors';
import { Place } from 'ai-interface/types/place';
import { createArmyAtOrAroundPlace, buildTrainingFacility, changePlaceOwner } from '../game/actionsPlayingThunks';
import { neutralPlayerId, maxPlayers } from 'ai-interface/constants/player';
import { experienceForRuin } from 'ai-interface/constants/experience';
import { RandomMapSettings } from '../editor/types';

/** Describes the level of random army types guarding given structure. One of the sublists will be randomly picked and used. */
const armiesGuardingStructure: { [structure: string]: number[][] } = {
  [Structure.Temple]: [[3, 3], [2, 2, 2]],
  [Structure.Village]: [[1], [0, 0]],
  [Structure.Watchtower]: [[0]],
  [Structure.SpyDen]: [[1, 1], [0, 0, 0]]
};

/**
 * Given a list of Places, this returns a subset of given size such that the places returned are farthest away from one another.
 * Warning: O(N ^ 3). But it produces nicer results than the O(N ^ 2) that was used before.
 */
function placesWithGreatestSpacing<T extends Place>(potentialPlaces: T[], count: number): T[] {
  const unpickedPlaces = shuffle([...potentialPlaces]);
  if (unpickedPlaces.length <= count) {
    return unpickedPlaces;
  }

  const acceptedPlaces: T[] = [];
  for (let i = 0; i < count; i++) {
    let bestPlace = 0;
    let largestDistFromNearest = 0;

    for (let p = 0; p < unpickedPlaces.length; p++) {
      let distFromNearest = 1E6;
      const { row: row1, col: col1 } = unpickedPlaces[p];

      for (const { row: row2, col: col2 } of acceptedPlaces) {
        const dist = (row1 - row2) ** 2 + (col1 - col2) ** 2;
        if (dist < distFromNearest) {
          distFromNearest = dist;
        }
      }

      if (distFromNearest > largestDistFromNearest) {
        largestDistFromNearest = distFromNearest;
        bestPlace = p;
      }
    }

    acceptedPlaces.push(unpickedPlaces[bestPlace]);
    unpickedPlaces.splice(bestPlace, 1);
  }

  return acceptedPlaces;
}

const placeStructures = (structure: Structure, count: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size, terrain } = getState().game;
  const size1 = size - (structure === Structure.City ? 3 : 2);

  // Most structures check for 1 free tile around them because it looks bad when structures are next to each another.
  // City is an exception; we allow zero border because:
  // 1. Cities are placed first so they won't collide with anything.
  // 2. This places cities in "cooler" locations such as between rivers etc.
  // 3. Ruins and villages can be placed on water, so most of the time this doesn't limit their positions anyway.
  const border = structure === Structure.City ? 0 : 1;

  const potentialPlaces: Place[] = [];

  // Choose some potential places randomly. Choose more than required, then we will pick only the best of them.
  for (let i = 0; potentialPlaces.length < 2 * count && i < 10 * count; i++) {
    const row = 1 + getRandom(size1);
    const col = 1 + getRandom(size1);

    if (canPlaceStructure(getState(), { row, col, structure, border })) {
      // We don't like when structures on water touch the land by side, because they look like they could be entered from the land.
      if (terrain[row][col] === Terrain.Water && (
        terrain[row - 1][col] !== Terrain.Water || terrain[row + 1][col] !== Terrain.Water ||
        terrain[row][col - 1] !== Terrain.Water || terrain[row][col + 1] !== Terrain.Water)) {
        continue;
      }

      potentialPlaces.push({
        row,
        col,
      });
    }
  }

  for (const { row, col } of placesWithGreatestSpacing(potentialPlaces, count)) {
    if (canPlaceStructure(getState(), { row, col, structure })) {
      dispatch(createAndPlaceStructure(row, col, structure));

      const guardians = armiesGuardingStructure[structure];
      if (guardians !== undefined) {
        for (const level of getRandomItem(guardians)) {
          dispatch(createArmyAtOrAroundPlace(row, col, `rnd${level}`, neutralPlayerId));
        }
      }
    }
  }
};

const setLevelForRuins = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { ruins } = getState().game;

  Object.keys(ruins).forEach((ruinId, index) => {
    let level = 1;
    for (let c = 2 ** (experienceForRuin.length - 2); c > 1; c /= 2) {
      if (index % c === c - 1) {
        level++;
      }
    }
    dispatch(setRuinLevel(ruinId, level));
  });
};

const placePorts = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { structures } = getState().game;

  for (const { structuresOnWater, adjacentContinents } of getAllWaterAreas(getState())) {
    for (const { structuresOnLand, shore } of adjacentContinents) {
      const emptyShore = shore.filter(({ row, col }) => structures[row][col] === Structure.Nothing);

      // Each continent adjacent to this water area should have a port if the water area contains structures.
      const noOfPorts = (structuresOnLand > 0 && structuresOnWater > 0 ? 1 : 0) + Math.floor(emptyShore.length / (24 + getRandom(12)));
      if (noOfPorts > 1 || (noOfPorts === 1 && structuresOnLand > 0 && structuresOnWater > 0)) {
        const potentialPlaces = shuffle(emptyShore).splice(0, 3 * noOfPorts);

        for (const place of placesWithGreatestSpacing(potentialPlaces, noOfPorts)) {
          const { row, col } = place;
          dispatch(createAndPlaceStructure(row, col, Structure.Port));
        }
      }
    }
  }
};

const pickStartingCities = (players: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { cities } = state.game;

  const nonIslandCities = Object.values(cities)
    .filter(city => !isCityTooRemote(state, city));
  const startingCities = placesWithGreatestSpacing(nonIslandCities, players);
  const chosenPlayerIds = shuffle(createArray(maxPlayers, id => id));

  for (let i = 0; i < players && i < startingCities.length; i++) {
    const playerId = chosenPlayerIds[i];
    const { id, row, col } = startingCities[i];

    dispatch(changePlaceOwner(row, col, playerId));
    dispatch(createArmyAtOrAroundPlace(row, col, 'hro1', playerId));
    dispatch(createArmyAtOrAroundPlace(row, col, 'mino', playerId));
    dispatch(createArmyAtOrAroundPlace(row, col, 'linf', playerId));
    dispatch(createArmyAtOrAroundPlace(row, col, 'linf', playerId));
    dispatch(buildTrainingFacility(id, 'scou'));
    dispatch(buildTrainingFacility(id, 'linf'));
  }
};

const setProductionForNeutralCities = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;
  for (const cityId in cities) {
    const { owner } = cities[cityId];
    if (owner === neutralPlayerId) {
      dispatch(buildTrainingFacility(cityId, `rnd0`));
    }
  }
};

export const createRandomMapStructures = (settings: RandomMapSettings) => (dispatch: AppDispatch) => {
  const { size, cities, ruins, players } = settings;

  dispatch(placeStructures(Structure.City, Math.round((0.7 + 0.6 * cities) * size ** 1.5 / 23)));
  dispatch(placeStructures(Structure.Ruin, Math.round(ruins * size ** 1.5 / 23)));
  dispatch(placeStructures(Structure.SageTower, Math.round(size ** 1.5 / 100)));
  // Ruins provide gold. If there are less ruins on the map, add extra villages to compensate this.
  dispatch(placeStructures(Structure.Village, Math.round(size ** 1.5 / 46 + (1 - ruins) * size ** 1.5 / 90)));
  dispatch(placeStructures(Structure.Watchtower, getRandom(Math.ceil(size ** 1.5 / 140))));
  dispatch(placeStructures(Structure.Encampment, getRandom(Math.ceil(size ** 1.5 / 125))));
  dispatch(placeStructures(Structure.Grove, getRandom(Math.ceil(size ** 1.5 / 125))));
  dispatch(placeStructures(Structure.SpyDen, getRandom(Math.ceil(size ** 1.5 / 350))));
  dispatch(placeStructures(Structure.Temple, getRandom(Math.ceil(size ** 1.5 / 175))));

  dispatch(setLevelForRuins());
  dispatch(placePorts());
  dispatch(pickStartingCities(players));
  dispatch(setProductionForNeutralCities());
};
