import { MapDisplaySettings } from './types';
import { Place } from 'ai-interface/types/place';
import { WarReportItem } from 'ai-interface/types/warReport';
import { IncomingCaravan } from 'ai-interface/types/city';

export enum ActionTypes {
  // Fog of war and unexplored
  REVEAL_FOG_OF_WAR_AT_PLACES = '[Game] REVEAL_FOG_OF_WAR_AT_PLACES',
  REVEAL_UNEXPLORED_AT_PLACES = '[Game] REVEAL_UNEXPLORED_AT_PLACES',
  SET_UNEXPLORED_BY_TEAM = '[Game] SET_UNEXPLORED_BY_TEAM',

  // Player
  SET_PLAYER_ALIVE = '[Game] SET_PLAYER_ALIVE',
  SET_SAGE_VISITED_THIS_WEEK = '[Game] SET_SAGE_VISITED_THIS_WEEK',
  SET_ARMY_TYPE_ID_TO_ORDER = '[Game] SET_ARMY_TYPE_ID_TO_ORDER',
  SET_MAP_DISPLAY_SETTINGS = '[Game] SET_MAP_DISPLAY_SETTINGS',
  RESET_WAR_REPORTS = '[Game] RESET_WAR_REPORTS',
  ADD_WAR_REPORT_ITEM = '[Game] ADD_WAR_REPORT_ITEM',
  ADD_KILLS_LOSSES = '[Game] ADD_KILLS_LOSSES',
  ADD_GOLD = '[Game] ADD_GOLD',

  // City
  SET_CITY_CAPITOL = '[Game] SET_CITY_CAPITOL',
  SET_CITY_RAZED = '[Game] SET_CITY_RAZED',
  SET_CURRENTLY_TRAINING = '[Game] SET_CURRENTLY_TRAINING',
  SET_TURNS_TRAINING_LEFT = '[Game] SET_TURNS_TRAINING_LEFT',
  SET_CURRENTLY_TRAINABLE_ARMY_TYPE_IDS = '[Game] SET_CURRENTLY_TRAINABLE_ARMY_TYPE_IDS',
  SET_CITY_OWNER = '[Game] SET_CITY_OWNER',
  SET_FLAGGABLE_OWNER = '[Game] SET_FLAGGABLE_OWNER',
  SET_CARAVAN_DESTINATION = '[Game] SET_CARAVAN_DESTINATION',
  SET_INCOMING_CARAVANS = '[Game] SET_INCOMING_CARAVANS',

  // Group
  CREATE_ARMY = '[Game] CREATE_ARMY',
  DESTROY_ARMY = '[Game] DESTROY_ARMY',
  TRANSFER_ARMY_BETWEEN_GROUPS = '[Game] TRANSFER_ARMY_BETWEEN_GROUPS',
  GROUP_JUMP_TO_PLACE = '[Game] GROUP_JUMP_TO_PLACE',
  SET_ARMY_MOVE_LEFT = '[Game] SET_ARMY_MOVE_LEFT',
  DECREASE_GROUP_MOVE_LEFT = '[Game] DECREASE_GROUP_MOVE_LEFT',
  SET_GROUP_DESTINATION = '[Game] SET_GROUP_DESTINATION',
  SET_GROUP_WAITING = '[Game] SET_GROUP_WAITING',
  SET_GROUP_OWNER = '[Game] SET_GROUP_OWNER',
  SET_ARMY_BLESSED = '[Game] SET_ARMY_BLESSED',
}

export interface RevealFogOfWarAtPlaces {
  type: ActionTypes.REVEAL_FOG_OF_WAR_AT_PLACES;
  payload: {
    places: Place[]; // Places must be ordered by row number!
  }
}

export interface RevealUnexploredAtPlaces {
  type: ActionTypes.REVEAL_UNEXPLORED_AT_PLACES;
  payload: {
    places: Place[]; // Places must be ordered by row number!
  }
}

export interface SetUnexploredByTeam {
  type: ActionTypes.SET_UNEXPLORED_BY_TEAM;
  payload: {
    team: number;
    unexplored: boolean[][] | null;
  }
}

export interface SetPlayerAlive {
  type: ActionTypes.SET_PLAYER_ALIVE;
  payload: {
    playerId: number;
    alive: boolean;
  }
}

export interface SetSageVisitedThisWeek {
  type: ActionTypes.SET_SAGE_VISITED_THIS_WEEK;
  payload: {
    playerId: number;
    sageVisitedThisWeek: boolean;
  }
}

export interface SetArmyTypeIdToOrder {
  type: ActionTypes.SET_ARMY_TYPE_ID_TO_ORDER;
  payload: {
    playerId: number;
    armyTypeIdToOrder: { [armyTypeId: string]: number };
  }
}

export interface SetMapDisplaySettings {
  type: ActionTypes.SET_MAP_DISPLAY_SETTINGS;
  payload: {
    playerId: number;
    mapDisplaySettings: MapDisplaySettings;
  }
}

export interface ResetWarReports {
  type: ActionTypes.RESET_WAR_REPORTS;
  payload: {
    playerId: number;
  }
}

export interface AddWarReportItem {
  type: ActionTypes.ADD_WAR_REPORT_ITEM;
  payload: {
    playerId: number;
    reportItem: WarReportItem;
  }
}

export interface AddKillsLosses {
  type: ActionTypes.ADD_KILLS_LOSSES;
  payload: {
    playerId: number;
    kills: number;
    losses: number;
  }
}

export interface AddGold {
  type: ActionTypes.ADD_GOLD;
  payload: {
    playerId: number;
    amount: number;
  }
}

export interface SetCityCapitol {
  type: ActionTypes.SET_CITY_CAPITOL;
  payload: {
    cityId: string;
    capitol: boolean;
  }
}

export interface SetCityRazed {
  type: ActionTypes.SET_CITY_RAZED;
  payload: {
    cityId: string;
    razed: boolean;
  }
}

export interface SetCurrentlyTraining {
  type: ActionTypes.SET_CURRENTLY_TRAINING;
  payload: {
    cityId: string;
    armyTypeId?: string; // undefined to stop city production
  }
}

export interface SetTurnsTrainingLeft {
  type: ActionTypes.SET_TURNS_TRAINING_LEFT;
  payload: {
    cityId: string;
    turnsTrainingLeft?: number;
  }
}

export interface SetCurrentlyTrainableArmyTypeIds {
  type: ActionTypes.SET_CURRENTLY_TRAINABLE_ARMY_TYPE_IDS;
  payload: {
    cityId: string;
    currentlyTrainableArmyTypeIds?: string[];
  }
}

export interface SetCityOwner {
  type: ActionTypes.SET_CITY_OWNER;
  payload: {
    cityId: string;
    owner: number;
  }
}

export interface SetFlaggableOwner {
  type: ActionTypes.SET_FLAGGABLE_OWNER;
  payload: {
    flaggableId: string;
    owner: number;
  }
}

export interface SetCaravanDestination {
  type: ActionTypes.SET_CARAVAN_DESTINATION;
  payload: {
    fromCityId: string;
    toCityId?: string;
  }
}

export interface SetIncomingCaravans {
  type: ActionTypes.SET_INCOMING_CARAVANS;
  payload: {
    cityId: string;
    incomingCaravans?: IncomingCaravan[];
  }
}

export interface CreateArmy {
  type: ActionTypes.CREATE_ARMY;
  payload: {
    row: number;
    col: number;
    armyTypeId: string;
    owner: number;
    heroPortraitId?: number;
  }
}

export interface DestroyArmy {
  type: ActionTypes.DESTROY_ARMY;
  payload: {
    groupId: string;
    armyId: string;
  }
}

export interface TransferArmyBetweenGroups {
  type: ActionTypes.TRANSFER_ARMY_BETWEEN_GROUPS;
  payload: {
    armyId: string;
    srcGroupId: string;
    destGroupId: string | null; // null when you want to split to a new group
  }
}

export interface GroupJumpToPlace {
  type: ActionTypes.GROUP_JUMP_TO_PLACE;
  payload: {
    groupId: string;
    destRow: number;
    destCol: number;
  }
}

export interface SetArmyMoveLeft {
  type: ActionTypes.SET_ARMY_MOVE_LEFT;
  payload: {
    groupId: string;
    armyId: string;
    moveLeft: number;
  }
}

export interface DecreaseGroupMoveLeft {
  type: ActionTypes.DECREASE_GROUP_MOVE_LEFT;
  payload: {
    groupId: string;
    moveCost: number;
    isBoardingDeboardingShip: boolean; // When true, all moveLeft except 1 after this step will be lost. Negative will remain.
  }
}

export interface SetGroupDestination {
  type: ActionTypes.SET_GROUP_DESTINATION;
  payload: {
    groupId: string;
    destRow?: number; // undefined to unset the destination
    destCol?: number;
  }
}

export interface SetGroupWaiting {
  type: ActionTypes.SET_GROUP_WAITING;
  payload: {
    groupId: string;
    waiting: boolean;
  }
}

export interface SetGroupOwner {
  type: ActionTypes.SET_GROUP_OWNER;
  payload: {
    groupId: string;
    owner: number;
  }
}

export interface SetArmyBlessed {
  type: ActionTypes.SET_ARMY_BLESSED;
  payload: {
    groupId: string;
    armyId: string;
    blessed: boolean;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

/** Places must be ordered by row number. */
export const revealFogOfWarAtPlaces = (places: Place[]): RevealFogOfWarAtPlaces => ({
  type: ActionTypes.REVEAL_FOG_OF_WAR_AT_PLACES,
  payload: {
    places
  }
});

/** Places must be ordered by row number. */
export const revealUnexploredAtPlaces = (places: Place[]): RevealUnexploredAtPlaces => ({
  type: ActionTypes.REVEAL_UNEXPLORED_AT_PLACES,
  payload: {
    places
  }
});

export const setUnexploredByTeam = (team: number, unexplored: boolean[][] | null): SetUnexploredByTeam => ({
  type: ActionTypes.SET_UNEXPLORED_BY_TEAM,
  payload: {
    team,
    unexplored: unexplored === null ? null : unexplored.map(row => [...row])
  }
});

/** Low-level action. Use killAndReleasePlayer(). */
export const _setPlayerAlive = (playerId: number, alive: boolean): SetPlayerAlive => ({
  type: ActionTypes.SET_PLAYER_ALIVE,
  payload: {
    playerId,
    alive
  }
});

export const setSageVisitedThisWeek = (playerId: number, sageVisitedThisWeek: boolean): SetSageVisitedThisWeek => ({
  type: ActionTypes.SET_SAGE_VISITED_THIS_WEEK,
  payload: {
    playerId,
    sageVisitedThisWeek
  }
});

/** Don't include non-standard army types such as "Random Level 1 Army". */
export const setStandardArmyTypeIdToOrder = (playerId: number, standardArmyTypeIdToOrder: { [armyTypeId: string]: number }): SetArmyTypeIdToOrder => ({
  type: ActionTypes.SET_ARMY_TYPE_ID_TO_ORDER,
  payload: {
    playerId,
    armyTypeIdToOrder: {
      ...standardArmyTypeIdToOrder
    }
  }
});

export const setMapDisplaySettings = (playerId: number, mapDisplaySettings: MapDisplaySettings): SetMapDisplaySettings => ({
  type: ActionTypes.SET_MAP_DISPLAY_SETTINGS,
  payload: {
    playerId,
    mapDisplaySettings: { ...mapDisplaySettings }
  }
});

export const addGold = (playerId: number, amount: number): AddGold => ({
  type: ActionTypes.ADD_GOLD,
  payload: {
    playerId,
    amount
  }
});

export const resetWarReports = (playerId: number): ResetWarReports => ({
  type: ActionTypes.RESET_WAR_REPORTS,
  payload: {
    playerId
  }
});

export const addWarReportItem = (playerId: number, reportItem: WarReportItem): AddWarReportItem => ({
  type: ActionTypes.ADD_WAR_REPORT_ITEM,
  payload: {
    playerId,
    reportItem: { ...reportItem }
  }
});

export const addKillsLosses = (playerId: number, kills: number, losses: number): AddKillsLosses => ({
  type: ActionTypes.ADD_KILLS_LOSSES,
  payload: {
    playerId,
    kills,
    losses
  }
});

export const setCityCapitol = (cityId: string, capitol: boolean): SetCityCapitol => ({
  type: ActionTypes.SET_CITY_CAPITOL,
  payload: {
    cityId,
    capitol
  }
});

/** Low-level action. Use razeCity(). */
export const _setCityRazed = (cityId: string, razed: boolean): SetCityRazed => ({
  type: ActionTypes.SET_CITY_RAZED,
  payload: {
    cityId,
    razed
  }
});

export const setCurrentlyTraining = (cityId: string, armyTypeId?: string): SetCurrentlyTraining => ({
  type: ActionTypes.SET_CURRENTLY_TRAINING,
  payload: {
    cityId,
    armyTypeId
  }
});

export const setTurnsTrainingLeft = (cityId: string, turnsTrainingLeft?: number): SetTurnsTrainingLeft => ({
  type: ActionTypes.SET_TURNS_TRAINING_LEFT,
  payload: {
    cityId,
    turnsTrainingLeft
  }
});

/** Low-level action. Use buildTrainingFacility() and demolishTrainingFacility(). */
export const _setCurrentlyTrainableArmyTypeIds = (cityId: string, currentlyTrainableArmyTypeIds?: string[]): SetCurrentlyTrainableArmyTypeIds => ({
  type: ActionTypes.SET_CURRENTLY_TRAINABLE_ARMY_TYPE_IDS,
  payload: {
    cityId,
    currentlyTrainableArmyTypeIds: currentlyTrainableArmyTypeIds === undefined || currentlyTrainableArmyTypeIds.length === 0
      ? undefined : [...currentlyTrainableArmyTypeIds]
  }
});

/** Low-level action. Use changeCityOwner(), or changePlaceOwner(). */
export const _setCityOwner = (cityId: string, owner: number): SetCityOwner => ({
  type: ActionTypes.SET_CITY_OWNER,
  payload: {
    cityId,
    owner
  }
});

export const setFlaggableOwner = (flaggableId: string, owner: number): SetFlaggableOwner => ({
  type: ActionTypes.SET_FLAGGABLE_OWNER,
  payload: {
    flaggableId,
    owner
  }
});

export const setCaravanDestination = (fromCityId: string, toCityId?: string): SetCaravanDestination => ({
  type: ActionTypes.SET_CARAVAN_DESTINATION,
  payload: {
    fromCityId,
    toCityId
  }
});

export const setIncomingCaravans = (cityId: string, incomingCaravans?: IncomingCaravan[]): SetIncomingCaravans => ({
  type: ActionTypes.SET_INCOMING_CARAVANS,
  payload: {
    cityId,
    incomingCaravans: incomingCaravans === undefined ? undefined : incomingCaravans.map(ic => ({ ...ic }))
  }
});

/** Low-level action. Use createArmyAtOrAroundPlace(). */
export const _createArmy = (row: number, col: number, armyTypeId: string, owner: number, heroPortraitId?: number): CreateArmy => ({
  type: ActionTypes.CREATE_ARMY,
  payload: {
    row,
    col,
    armyTypeId,
    owner,
    heroPortraitId
  }
});

/** Low-level action. Use destroyArmy(). */
export const _destroyArmy = (groupId: string, armyId: string): DestroyArmy => ({
  type: ActionTypes.DESTROY_ARMY,
  payload: {
    groupId,
    armyId
  }
});

/**
 * Assigns one army to another group. It stops being part of the previous group.
 * It is assumed that they are on the same place and owned by same player.
 * Provide an existing destination group id, or null if you want to split to a new group.
 * If last army is transferred away from source group, source group will be removed.
 */
export const transferArmyBetweenGroups = (armyId: string, srcGroupId: string, destGroupId: string | null): TransferArmyBetweenGroups => ({
  type: ActionTypes.TRANSFER_ARMY_BETWEEN_GROUPS,
  payload: {
    armyId,
    srcGroupId,
    destGroupId
  }
});

export const groupJumpToPlace = (groupId: string, destRow: number, destCol: number): GroupJumpToPlace => ({
  type: ActionTypes.GROUP_JUMP_TO_PLACE,
  payload: {
    groupId,
    destRow,
    destCol
  }
});

export const setArmyMoveLeft = (groupId: string, armyId: string, moveLeft: number): SetArmyMoveLeft => ({
  type: ActionTypes.SET_ARMY_MOVE_LEFT,
  payload: {
    groupId,
    armyId,
    moveLeft
  }
});

export const decreaseGroupMoveLeft = (groupId: string, moveCost: number, isBoardingDeboardingShip: boolean): DecreaseGroupMoveLeft => ({
  type: ActionTypes.DECREASE_GROUP_MOVE_LEFT,
  payload: {
    groupId,
    moveCost,
    isBoardingDeboardingShip
  }
});

export const setGroupDestination = (groupId: string, destRow?: number, destCol?: number): SetGroupDestination => ({
  type: ActionTypes.SET_GROUP_DESTINATION,
  payload: {
    groupId,
    destRow,
    destCol
  }
});

export const setGroupWaiting = (groupId: string, waiting: boolean): SetGroupWaiting => ({
  type: ActionTypes.SET_GROUP_WAITING,
  payload: {
    groupId,
    waiting
  }
});

export const setGroupOwner = (groupId: string, owner: number): SetGroupOwner => ({
  type: ActionTypes.SET_GROUP_OWNER,
  payload: {
    groupId,
    owner
  }
});

export const setArmyBlessed = (groupId: string, armyId: string, blessed: boolean): SetArmyBlessed => ({
  type: ActionTypes.SET_ARMY_BLESSED,
  payload: {
    groupId,
    armyId,
    blessed
  }
});

export type PlayingAction = RevealFogOfWarAtPlaces
  | RevealUnexploredAtPlaces
  | SetUnexploredByTeam
  | SetPlayerAlive
  | SetSageVisitedThisWeek
  | SetArmyTypeIdToOrder
  | SetMapDisplaySettings
  | ResetWarReports
  | AddWarReportItem
  | AddKillsLosses
  | AddGold
  | SetCityCapitol
  | SetCityRazed
  | SetCurrentlyTraining
  | SetTurnsTrainingLeft
  | SetCurrentlyTrainableArmyTypeIds
  | SetCityOwner
  | SetFlaggableOwner
  | SetCaravanDestination
  | SetIncomingCaravans
  | CreateArmy
  | DestroyArmy
  | TransferArmyBetweenGroups
  | GroupJumpToPlace
  | SetArmyMoveLeft
  | DecreaseGroupMoveLeft
  | SetGroupDestination
  | SetGroupWaiting
  | SetGroupOwner
  | SetArmyBlessed;
