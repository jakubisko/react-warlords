import { MapData, GameState, TimelineItem } from './types';
import { calculateBackgroundSprites } from '../editor/tiling';
import { createGrid, createArray, getRandomItem, EMPTY_MAP, EMPTY_ARRAY } from '../../functions';
import { defaultArmyTypeIdToOrder } from './constants';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { ArmyType } from 'ai-interface/types/army';
import { defaultPlayerNames, maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';
import { WinCondition } from 'ai-interface/constants/winCondition';
import { WarReportItem } from 'ai-interface/types/warReport';

/** Since backgroundSprites is deterministically calculated from other fields of GameState, we don't need it when saving. */
export type SaveGameData = Omit<GameState, 'backgroundSprites'>;

/** Returned object will contain only map data. Everything else will be thrown away. */
export function extractMapDataFromGameState(gameState: GameState): MapData {
  const { size, terrain, structures, tiles, cities, flaggables, ruins, groups, heroes, lastIds } = gameState;
  return { size, terrain, structures, tiles, cities, flaggables, ruins, groups, heroes, lastIds };
}

/** Returned object will contain entire GameState. Data that were missing in MapData will be replaced by default values. */
export function constructGameStateFromMapData(mapData: MapData): GameState {
  const { size, terrain, structures, cities } = mapData;

  const unrazedCities = Object.keys(cities)
    .map(cityId => cities[cityId])
    .filter(city => city.owner !== neutralPlayerId && !city.razed);
  let teamId = 0;
  const teams = createArray(maxPlayers + 1,
    playerId => unrazedCities.some(city => city.owner === playerId)
      ? teamId++
      : neutralPlayerId
  );

  return {
    ...mapData,
    winCondition: WinCondition.CaptureAllEnemyCities,
    backgroundSprites: calculateBackgroundSprites(terrain, structures),
    fogOfWar: createGrid(size, () => false),
    unexploredByTeam: createArray(maxPlayers + 1, () => null),
    players: createArray(maxPlayers + 1, playerId => ({
      team: teams[playerId],
      name: defaultPlayerNames[playerId],
      alive: teams[playerId] !== neutralPlayerId,
      gold: 100,
      armyTypeIdToOrder: defaultArmyTypeIdToOrder,
      mapDisplaySettings: teams[playerId] === neutralPlayerId && playerId !== neutralPlayerId ? EMPTY_MAP : {
        showTerrain: true,
        showRoads: true,
        showRuins: true,
        showCities: true,
        showFlaggables: true,
        showArmies: true,
        showArtifacts: true,
        showFogOfWar: true,
        showWarReport: true
      },
      savedMapFocus: { row: 0, col: 0 },
      warReports: EMPTY_ARRAY as WarReportItem[],
      totalKills: 0,
      totalLosses: 0
    })),
    currentPlayerId: neutralPlayerId, // Both Editor and "New Game" thumbnail require neutral player to see everything.
    turnNumber: 0, // Game starts with call to passTurnToNextPlayer, which will move it to first turn.
    timeline: EMPTY_ARRAY as TimelineItem[],
    heroOffer: null,
    aiOutputVisible: false,
    aiStates: createArray(maxPlayers + 1, () => ''),
    aiAdvantage: 0,
    aiOutput: EMPTY_MAP
  };
}

/**
 * This will directly modify given GameState object, replacing placeholders for random items and armies by actual items and armies.
 * For example, "Random Minor Artifact" will be replaced by "Thunder Mace". Call this right after constructing the GameState from MapData,
 * before assigning it to redux. Don't call this when loading map in editor.
 */
export function replaceRandomObjectsInGameState(gameState: GameState) {
  const { size, tiles, cities } = gameState;

  const randomArtifactSubstitutes: { [artifactId: string]: string[] } = {
    randomCommonArtifact: Object.keys(standardArtifacts).filter(artifactId => standardArtifacts[artifactId].rarity === 1),
    randomMinorArtifact: Object.keys(standardArtifacts).filter(artifactId => standardArtifacts[artifactId].rarity === 2),
    randomMajorArtifact: Object.keys(standardArtifacts).filter(artifactId => standardArtifacts[artifactId].rarity === 3),
    randomRelicArtifact: Object.keys(standardArtifacts).filter(artifactId => standardArtifacts[artifactId].rarity === 4)
  };
  const armyTypeIdsMatching = (predicate: (armyType: ArmyType) => boolean) => Object.keys(standardArmyTypes).filter(armyTypeId => predicate(standardArmyTypes[armyTypeId]));
  const randomArmyTypeSubstitutes: { [armyTypeId: string]: string[] } = {
    rnd0: armyTypeIdsMatching(armyType => armyType.trainingTime === 1 && armyType.strength <= 2),
    rnd1: armyTypeIdsMatching(armyType => armyType.trainingTime === 1 && armyType.strength >= 3),
    rnd2: armyTypeIdsMatching(armyType => armyType.trainingTime === 2),
    rnd3: armyTypeIdsMatching(armyType => armyType.trainingTime === 3),
    rnd4: armyTypeIdsMatching(armyType => armyType.trainingTime >= 4),
  };

  // Random trainable army types in cities
  for (const cityId in cities) {
    const { currentlyTrainableArmyTypeIds } = cities[cityId];
    if (currentlyTrainableArmyTypeIds !== undefined) {
      for (let i = 0; i < currentlyTrainableArmyTypeIds.length; i++) {
        let possible = randomArmyTypeSubstitutes[currentlyTrainableArmyTypeIds[i]];
        if (possible !== undefined) {
          // Randomly pick an army type of the appropriate level, but other than those already present.
          possible = possible.filter(armyTypeId => !currentlyTrainableArmyTypeIds.includes(armyTypeId));
          currentlyTrainableArmyTypeIds[i] = getRandomItem(possible);
        }
      }
    }
  }

  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      const { artifactIds, armies, cityId } = tiles[row][col];

      // Random artifacts on ground
      if (artifactIds) {
        for (let i = 0; i < artifactIds.length; i++) {
          const possible = randomArtifactSubstitutes[artifactIds[i]];
          if (possible !== undefined) {
            artifactIds[i] = getRandomItem(possible);
          }
        }
      }

      // Random armies on land
      if (armies) {
        for (const { armyTypeId } of armies) {
          let possible = randomArmyTypeSubstitutes[armyTypeId];

          if (possible !== undefined) {
            // If the armies are in a city, use one of the armies trainable in that city if possible.
            if (cityId !== undefined) {
              const { currentlyTrainableArmyTypeIds } = cities[cityId];
              const intersection = currentlyTrainableArmyTypeIds === undefined
                ? EMPTY_ARRAY as string[]
                : currentlyTrainableArmyTypeIds.filter(armyTypeId => possible.includes(armyTypeId));
              if (intersection.length > 0) {
                possible = intersection;
              }
            }

            // We want all random armies of one level to be replaced by the same army type.
            const newArmyTypeId = getRandomItem(possible);

            for (let i = 0; i < armies.length; i++) {
              if (armies[i].armyTypeId === armyTypeId) {
                armies[i] = {
                  ...armies[i],
                  armyTypeId: newArmyTypeId
                };
              }
            }
          }
        }
      }
    }
  }
}

/** Generates a new MapData representing an empty map of given size. */
export function getEmptyMap(size: number): MapData {
  return {
    size,
    terrain: createGrid(size, () => Terrain.Open),
    structures: createGrid(size, () => Structure.Nothing),
    tiles: createGrid(size, () => EMPTY_MAP),
    cities: EMPTY_MAP,
    flaggables: EMPTY_MAP,
    ruins: EMPTY_MAP,
    groups: EMPTY_MAP,
    heroes: EMPTY_MAP,
    lastIds: createArray(maxPlayers + 1, () => 0)
  };
}
