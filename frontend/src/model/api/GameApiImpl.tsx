import { setStandardArmyTypeIdToOrder, transferArmyBetweenGroups, setCurrentlyTraining, setCaravanDestination } from '../game/actionsPlaying';
import { buildTrainingFacility, demolishTrainingFacility, razeCity, destroyGroup, acceptHeroOffer, destroyEverythingOwnedByPlayer, destroyArmy } from '../game/actionsPlayingThunks';
import { GameApi } from 'ai-interface/apis/GameApi';
import { AppDispatch, RootState } from '../root/configureStore';
import { GameApiObstacle } from 'ai-interface/apis/GameApiObstacle';
import {
  getVisibleArmiesDefendingPlace, getAllArmiesOfPlayer, getIncome, getUpkeepOfExistingArmies,
  getUpkeepOfArmiesCreatedTomorrow, getLastGeneratedIds, getArmiesAtPlaceOfGroupOrderedByGroups, getSpyReport, getOwnerOfPlace
} from '../game/selectorsPlaying';
import { defaultArmyTypeIdToOrder } from '../game/constants';
import { Place, PlaceWithTime } from 'ai-interface/types/place';
import { createArtifactForHero, destroyArtifactOnGround, destroyArtifactForHero, createArtifactOnGround } from '../game/actionsHero';
import { getHerosStrength, getHerosHitPoints, getHerosMove, getHerosAbilities } from '../game/selectorsHero';
import { exploreRuin, exploreSageTower, applyExploringResult } from '../exploring/actions';
import { getMoveTypeOfGroup, getMoveLeftOfGroup } from '../path/selectors';
import { calculateTimeMatrix, calculatePathToPlaceFromTimeMatrix } from '../path/dijkstrasAlgorithm';
import { moveGroupOneStepNoUi } from '../path/actions';
import { applyCombatResult, attackTile, predictCombat } from '../combat/actions';
import { getLastCombatInputsAndResult } from '../combat/selectors';
import { createBasicAvoidingFunction } from '../path/basicAvoidingFunction';
import { MoveCostCalculatorImplUnchecked, calculateMoveTypeOfArmyTypes } from '../path/MoveCostCalculatorImplUnchecked';
import { CombatInputsAndResult, CombatPrediction } from 'ai-interface/types/combat';
import { WarReportItem } from 'ai-interface/types/warReport';
import { Army } from 'ai-interface/types/army';
import { MoveType, SearchPathArgs, TimeMatrix, BasicAvoidingStrategy } from 'ai-interface/types/path';
import { ExploringResult, RuinData, SageTowerChoice } from 'ai-interface/types/exploring';
import { CityData } from 'ai-interface/types/city';
import { FlaggableData } from 'ai-interface/types/flaggable';
import { HeroData } from 'ai-interface/types/hero';
import { heroExperienceLevels } from 'ai-interface/constants/experience';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { straightMoveCost, diagonalMoveCost } from 'ai-interface/constants/path';
import { createGrid, sortByMultiple } from '../../functions';
import { HeroOffer } from 'ai-interface/types/heroOffer';
import { SpyReport } from 'ai-interface/types/spyReport';
import { setPlayerAiState } from '../game/actionsTurnPassing';
import heroNames from '../../predefined-data/hero-names.json';
import { getNumberOfCitiesByWhichPlayerIsBehindBestEnemy } from '../exploring/selectors';
import { Obstacle } from 'ai-interface/types/obstacle';
import { WinCondition } from 'ai-interface/constants/winCondition';

export class GameApiImpl implements GameApi {

  constructor(
    private myPlayerId: number,
    private dispatch: AppDispatch,
    private getState: () => RootState,
    private obstacle: GameApiObstacle) {
  }

  private assertNone(obstacleObject: Obstacle | null) {
    if (obstacleObject !== null) {
      throw obstacleObject;
    }
  }

  //*********** Global ***********/

  saveState(state: string): void {
    this.assertNone(this.obstacle.saveState(state));
    this.dispatch(setPlayerAiState(this.myPlayerId, state));
  }

  getSavedState(): string {
    return this.getState().game.aiStates[this.myPlayerId];
  }

  getMyPlayerId(): number {
    return this.myPlayerId;
  }

  getTurnNumber(): number {
    return this.getState().game.turnNumber;
  }

  getPlayerAlive(): boolean[] {
    return this.getState().game.players.map(player => player.alive);
  }

  getPlayersTeam(): number[] {
    return this.getState().game.players.map(player => player.team);
  }

  setMyOrdering(armyTypeIds: string[]): void {
    this.assertNone(this.obstacle.setMyOrdering(armyTypeIds));
    const armyTypeIdToOrder: { [armyTypeId: string]: number } = {};
    for (let i = 0; i < armyTypeIds.length; i++) {
      const armyTypeId = armyTypeIds[i];
      armyTypeIdToOrder[armyTypeId] = i;
    }
    this.dispatch(setStandardArmyTypeIdToOrder(this.myPlayerId, armyTypeIdToOrder));
  }

  getMyOrdering(): string[] {
    const { armyTypeIdToOrder } = this.getState().game.players[this.myPlayerId];
    return Object.keys(standardArmyTypes).sort((id1, id2) => armyTypeIdToOrder[id1] - armyTypeIdToOrder[id2]);
  }

  getOwnerOfPlace(place: Place): number | null {
    this.assertNone(this.obstacle.getOwnerOfPlace(place));
    return getOwnerOfPlace(this.getState(), place);
  }

  getPreviousTurnWarReportItems(): WarReportItem[] {
    const { warReports } = this.getState().game.players[this.myPlayerId];
    return warReports.map(WarReportItem => ({ ...WarReportItem }));
  }

  getSpyReport(): SpyReport {
    return getSpyReport(this.getState(), this.myPlayerId);
  }

  getWinCondition(): WinCondition {
    return this.getState().game.winCondition;
  }

  resign(): void {
    this.dispatch(destroyEverythingOwnedByPlayer(this.myPlayerId));
  }

  //*********** Group move ***********/

  getGold(): number {
    return this.getState().game.players[this.myPlayerId].gold;
  }

  getIncome(): number {
    return getIncome(this.getState(), this.myPlayerId);
  }

  getUpkeepOfExistingArmies(): number {
    return getUpkeepOfExistingArmies(this.getState(), this.myPlayerId);
  }

  getUpkeepOfArmiesCreatedTomorrow(): number {
    return getUpkeepOfArmiesCreatedTomorrow(this.getState(), this.myPlayerId);
  }

  //*********** Cities ***********/

  getCity(cityId: string): CityData {
    this.assertNone(this.obstacle.getCity(cityId));
    const city = this.getState().game.cities[cityId];
    return {
      id: city.id,
      owner: city.owner,
      name: city.name,
      row: city.row,
      col: city.col,
      capitol: city.capitol === true,
      razed: city.razed === true,
      currentlyTrainableArmyTypeIds: city.currentlyTrainableArmyTypeIds === undefined ? []
        : [...city.currentlyTrainableArmyTypeIds].sort((id1, id2) => defaultArmyTypeIdToOrder[id1] - defaultArmyTypeIdToOrder[id2]),
      currentlyTrainingArmyTypeId: city.currentlyTrainingArmyTypeId,
      turnsTrainingLeft: city.turnsTrainingLeft,
      caravanToCityId: city.caravanToCityId,
      incomingCaravans: city.incomingCaravans === undefined ? [] : [...city.incomingCaravans],
    };
  }

  setTrainingInCity(cityId: string, armyTypeId?: string): void {
    this.assertNone(this.obstacle.setTrainingInCity(cityId, armyTypeId));
    this.dispatch(setCurrentlyTraining(cityId, armyTypeId));
  }

  buildTrainingFacility(cityId: string, armyTypeId: string): void {
    this.assertNone(this.obstacle.buildTrainingFacility(cityId, armyTypeId));
    this.dispatch(buildTrainingFacility(cityId, armyTypeId));
  }

  demolishTrainingFacility(cityId: string, armyTypeId: string): void {
    this.assertNone(this.obstacle.demolishTrainingFacility(cityId, armyTypeId));
    this.dispatch(demolishTrainingFacility(cityId, armyTypeId));
  }

  setCaravanFromCityToCity(fromCityId: string, toCityId?: string): void {
    this.assertNone(this.obstacle.setCaravanFromCityToCity(fromCityId, toCityId));
    this.dispatch(setCaravanDestination(fromCityId, toCityId));
  }

  razeCity(cityId: string): void {
    this.assertNone(this.obstacle.razeCity(cityId));
    this.dispatch(razeCity(cityId));
  }

  //*********** Flaggable structures ***********/

  getFlaggable(flaggableId: string): FlaggableData {
    this.assertNone(this.obstacle.getFlaggable(flaggableId));
    const { structures, flaggables } = this.getState().game;
    const { id, owner, row, col } = flaggables[flaggableId];
    return {
      id,
      structure: structures[row][col],
      owner,
      row,
      col
    };
  }

  //*********** Ruins, Sage Tower ***********/

  getRuin(ruinId: string): RuinData {
    this.assertNone(this.obstacle.getRuin(ruinId));
    const ruin = this.getState().game.ruins[ruinId];
    return {
      id: ruin.id,
      level: ruin.level,
      explored: ruin.explored === true,
      row: ruin.row,
      col: ruin.col
    };
  }

  exploreRuin(heroId: string): ExploringResult {
    this.assertNone(this.obstacle.exploreRuin(heroId));
    this.dispatch(exploreRuin(heroId));
    this.dispatch(applyExploringResult());

    const { exploringResult } = this.getState().exploring;
    return {
      ...exploringResult,
      message: [...exploringResult.message],
      artifactIdsFound: [...exploringResult.artifactIdsFound],
      armyTypeIdsFound: [...exploringResult.armyTypeIdsFound],
      mapExplored: { ...exploringResult.mapExplored }
    };
  }

  exploreSageTower(heroId: string, choice: SageTowerChoice): ExploringResult {
    this.assertNone(this.obstacle.exploreSageTower(heroId, choice));
    this.dispatch(exploreSageTower(heroId, choice));
    this.dispatch(applyExploringResult());

    const { exploringResult } = this.getState().exploring;
    return {
      ...exploringResult,
      message: [...exploringResult.message],
      artifactIdsFound: [...exploringResult.artifactIdsFound],
      armyTypeIdsFound: [...exploringResult.armyTypeIdsFound],
      mapExplored: { ...exploringResult.mapExplored }
    };
  }

  getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(playerId: number): number {
    this.assertNone(this.obstacle.getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(playerId));
    return getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(this.getState(), playerId);
  }

  wasSageVisitedThisWeek(): boolean {
    const { players, currentPlayerId } = this.getState().game;
    return !!players[currentPlayerId].sageVisitedThisWeek;
  }

  //*********** Groups ***********/

  getAllMyGroupIds(): string[] {
    const { groups } = this.getState().game;
    return Object.keys(groups).filter(group => groups[group].owner === this.myPlayerId);
  }

  groupExists(groupId: string): boolean {
    const group = this.getState().game.groups[groupId];
    return group !== undefined && group.owner === this.myPlayerId;
  }

  getPlaceOfGroup(groupId: string): Place {
    this.assertNone(this.obstacle.getPlaceOfGroup(groupId));
    const { row, col } = this.getState().game.groups[groupId];
    return { row, col };
  }

  dismissGroup(groupId: string): void {
    this.assertNone(this.obstacle.dismissGroup(groupId));
    this.dispatch(destroyGroup(groupId));
  }

  //*********** Armies ***********/

  getAllMyArmies(): Army[] {
    return getAllArmiesOfPlayer(this.getState(), this.myPlayerId).map(army => ({ ...army }));
  }

  getArmiesInGroup(groupId: string): Army[] {
    this.assertNone(this.obstacle.getArmiesInGroup(groupId));
    const { groups, tiles } = this.getState().game;
    const { row, col } = groups[groupId];
    return tiles[row][col].armies!.filter(army => army.groupId === groupId).map(army => ({ ...army }));
  }

  transferArmyFromGroupToOtherGroup(armyId: string, srcGroupId: string, destGroupId: string): void {
    this.assertNone(this.obstacle.transferArmyFromGroupToOtherGroup(armyId, srcGroupId, destGroupId));
    this.dispatch(transferArmyBetweenGroups(armyId, srcGroupId, destGroupId));
  }

  transferArmyFromGroupToNewGroup(armyId: string, srcGroupId: string): string {
    this.assertNone(this.obstacle.transferArmyFromGroupToNewGroup(armyId, srcGroupId));
    this.dispatch(transferArmyBetweenGroups(armyId, srcGroupId, null));
    return getLastGeneratedIds(this.getState()).groupId;
  }

  dismissArmy(groupId: string, armyId: string): void {
    this.assertNone(this.obstacle.dismissArmy(groupId, armyId));
    this.dispatch(destroyArmy(groupId, armyId));
  }

  getMyArmiesAt(place: Place): Army[] {
    this.assertNone(this.obstacle.getMyArmiesAt(place));

    const { tiles, groups, currentPlayerId, players, heroes } = this.getState().game;
    const { row, col } = place;
    const { armies } = tiles[row][col];
    if (!armies || groups[armies[0].groupId].owner !== currentPlayerId) {
      return [];
    }

    const { armyTypeIdToOrder } = players[currentPlayerId];

    return sortByMultiple(armies.map(army => ({ ...army })),
      army => armyTypeIdToOrder[army.armyTypeId],
      army => standardArmyTypes[army.armyTypeId].hero ? heroes[army.id].experience : 0,
      army => army.blessed ? 1 : 0,
      army => army.moveLeft
    );
  }

  getArmyTypeIdsAt(place: Place): string[] {
    this.assertNone(this.obstacle.getArmyTypeIdsAt(place));

    const { row, col } = place;
    const { tiles } = this.getState().game;
    const { armies } = tiles[row][col];
    return (armies === undefined) ? [] : armies.map(army => army.armyTypeId);
  }

  //*********** Heroes ***********/

  getHeroOffer(): HeroOffer | null {
    const { heroOffer } = this.getState().game;
    return heroOffer === null ? null : { ...heroOffer };
  }

  acceptHeroOffer(): void {
    this.assertNone(this.obstacle.acceptHeroOffer());
    this.dispatch(acceptHeroOffer());
  }

  getHero(heroId: string): HeroData {
    this.assertNone(this.obstacle.getHero(heroId));
    const hero = this.getState().game.heroes[heroId];

    return {
      id: heroId,
      groupId: hero.groupId,
      name: heroNames[hero.portraitId],
      level: hero.level,
      experience: hero.experience,
      experienceRemainingToNextLevel: (hero.level >= heroExperienceLevels.length - 1) ? 0 : (heroExperienceLevels[hero.level + 1] - hero.experience),
      strength: getHerosStrength(hero),
      move: getHerosMove(hero),
      hitPoints: getHerosHitPoints(hero),
      abilities: getHerosAbilities(hero),
      equippedArtifactIds: (hero.artifactIds === undefined) ? [] : [...hero.artifactIds]
    };
  }

  getArtifactIdsOnGround(place: Place): string[] {
    this.assertNone(this.obstacle.getArtifactIdsOnGround(place));
    const { tiles } = this.getState().game;
    const { row, col } = place;
    const { artifactIds } = tiles[row][col];
    return (artifactIds === undefined) ? [] : [...artifactIds];
  }

  pickArtifactFromGround(heroId: string, artifactId: string): void {
    this.assertNone(this.obstacle.pickArtifactFromGround(heroId, artifactId));
    const { groups, heroes } = this.getState().game;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];
    this.dispatch(createArtifactForHero(heroId, artifactId));
    this.dispatch(destroyArtifactOnGround(row, col, artifactId));
  }

  dropArtifactOntoGround(heroId: string, artifactId: string): void {
    this.assertNone(this.obstacle.dropArtifactOntoGround(heroId, artifactId));
    const { groups, heroes } = this.getState().game;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];
    this.dispatch(destroyArtifactForHero(heroId, artifactId));
    this.dispatch(createArtifactOnGround(row, col, artifactId));
  }

  //*********** Group move ***********/

  getMoveTypeOfGroup(groupId: string): MoveType {
    this.assertNone(this.obstacle.getMoveTypeOfGroup(groupId));
    return getMoveTypeOfGroup(this.getState(), groupId);
  }

  getMoveTypeOfArmyTypes(armyTypeIds: string[]): MoveType {
    this.assertNone(this.obstacle.getMoveTypeOfArmyTypes(armyTypeIds));
    return calculateMoveTypeOfArmyTypes(armyTypeIds);
  }

  createBasicAvoidingFunction(groupSize: number, strategy: BasicAvoidingStrategy): (row: number, col: number) => boolean {
    this.assertNone(this.obstacle.createBasicAvoidingFunction(groupSize, strategy));
    return createBasicAvoidingFunction(this.getState().game, groupSize, strategy);
  }

  calculateTimeMatrix(searchPathArgs: SearchPathArgs): TimeMatrix {
    this.assertNone(this.obstacle.calculateTimeMatrix(searchPathArgs));
    return calculateTimeMatrix(this.getState().game, searchPathArgs);
  }

  calculatePathToPlaceFromTimeMatrix(timeMatrix: TimeMatrix, destination: Place): PlaceWithTime[] {
    this.assertNone(this.obstacle.calculatePathToPlaceFromTimeMatrix(timeMatrix, destination));
    const { row, col } = destination;
    return calculatePathToPlaceFromTimeMatrix(timeMatrix, row, col);
  }

  calculateTimesOnPath(path: Place[], moveType: MoveType, initialMoveLeft: number): PlaceWithTime[] {
    this.assertNone(this.obstacle.calculateTimesOnPath(path, moveType, initialMoveLeft));

    const { game } = this.getState();
    const { abilities } = moveType;
    const moveCostCalculator = new MoveCostCalculatorImplUnchecked(game, abilities);
    let day = 0;
    let moveLeft = initialMoveLeft;

    let { row: srcRow, col: srcCol } = path[0];
    const resultPath: PlaceWithTime[] = [{
      row: path[0].row,
      col: path[0].col,
      day,
      moveLeft
    }];

    for (let i = 1; i < path.length; i++) {
      while (moveLeft < 0) {
        moveLeft += moveType.groupMoveOnLand;
        day++;
      }

      const { row, col } = path[i];
      const directionCost = (row !== srcRow && col !== srcCol) ? diagonalMoveCost : straightMoveCost;
      const moveCost = directionCost * moveCostCalculator.getMoveCostAt(row, col);
      const boardingDeboardingShip = moveCostCalculator.isBoardingDeboardingShipAt(row, col, srcRow, srcCol);
      moveLeft = boardingDeboardingShip ? Math.min(moveLeft - moveCost, 0) : (moveLeft - moveCost);

      resultPath.push({
        row,
        col,
        day,
        moveLeft
      });

      srcRow = row;
      srcCol = col;
    }

    return resultPath;
  }

  getMoveLeftOfGroup(groupId: string): number {
    this.assertNone(this.obstacle.getMoveLeftOfGroup(groupId));
    return getMoveLeftOfGroup(this.getState(), groupId);
  }

  moveGroupOneStep(groupId: string, destination: Place): void {
    this.assertNone(this.obstacle.moveGroupOneStep(groupId, destination));

    const { row, col } = destination;

    this.dispatch(attackTile(groupId, row, col));
    this.dispatch(moveGroupOneStepNoUi(groupId, row, col));
    if (this.getState().combat.combatResult !== null) {
      this.dispatch(applyCombatResult());
    }
  }

  //*********** Combat ***********/

  getLastCombat(): CombatInputsAndResult | null {
    return getLastCombatInputsAndResult(this.getState());
  }

  predictCombatIfGroupAttacks(attackingGroupId: string, defendingPlace: Place): CombatPrediction {
    this.assertNone(this.obstacle.predictCombatIfGroupAttacks(attackingGroupId, defendingPlace));

    const state = this.getState();
    const attackingArmies = getArmiesAtPlaceOfGroupOrderedByGroups(state, attackingGroupId).filter(army => army.groupId === attackingGroupId);
    return predictCombat(state, attackingArmies, defendingPlace);
  }

  predictCombatIfAllArmiesAtPlaceAttack(attackingPlace: Place, defendingPlace: Place): CombatPrediction {
    this.assertNone(this.obstacle.predictCombatIfAllArmiesAtPlaceAttack(attackingPlace, defendingPlace));

    const { row, col } = defendingPlace;

    const state = this.getState();
    const { groups } = state.game;
    const attackingArmies = getVisibleArmiesDefendingPlace(state, attackingPlace)
      .filter(army => groups[army.groupId].row === row && groups[army.groupId].col === col);
    return predictCombat(state, attackingArmies, defendingPlace);
  }

  //*********** Unexplored ***********/

  getUnexploredGrid(): boolean[][] {
    const { unexploredByTeam, players, size } = this.getState().game;
    const unexplored = unexploredByTeam[players[this.myPlayerId].team] ?? createGrid(size, () => false);
    if (!Object.isFrozen(unexplored)) {
      Object.freeze(unexplored);
      unexplored.forEach(Object.freeze);
    }
    return unexplored;
  }

  /** Use this if you want to help your ally with exploring the map. AI is unaffected by "unexplored shroud". */
  isPlaceUnexplored(place: Place): boolean {
    this.assertNone(this.obstacle.isPlaceUnexplored(place));

    const { row, col } = place;
    const { unexploredByTeam, players } = this.getState().game;
    const unexplored = unexploredByTeam[players[this.myPlayerId].team];

    return unexplored !== null && unexplored[row][col];
  }

}
