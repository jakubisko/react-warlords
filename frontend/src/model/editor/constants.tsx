import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import {
  SpriteSheet, spriteSheetWater, spriteSheetOpen, spriteSheetSwamp, spriteSheetForest, spriteSheetHill, spriteSheetMountain,
  spriteSheetDesert, spriteSheetVolcanic, spriteSheetIce
} from '../../gameui/map/tactical-map/spriteMetaData';

export type IsTerrainPredicate = (row: number, col: number, consideredTerrain: Terrain | TempTerrain) => boolean;

// Special Terrain acting as a temporal Terrain for the tiling purposes.
export enum TempTerrain {
  ForestClearing = 'FC'
}

export const structureAllowedTerrains: { [structure: string]: Terrain[] } = {
  [Structure.Nothing]: [Terrain.Water, Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Mountain, Terrain.Desert, Terrain.Volcanic, Terrain.Ice],
  [Structure.Road]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Mountain, Terrain.Desert, Terrain.Volcanic, Terrain.Ice],
  [Structure.Signpost]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Ridge]: [Terrain.Water, Terrain.Open, Terrain.Desert, Terrain.Ice],
  [Structure.Ruin]: [Terrain.Water, Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.SageTower]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Temple]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Port]: [Terrain.Water],
  [Structure.Encampment]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Grove]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.City]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Village]: [Terrain.Water, Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.Watchtower]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
  [Structure.SpyDen]: [Terrain.Open, Terrain.Swamp, Terrain.Forest, Terrain.Hill, Terrain.Desert, Terrain.Ice],
};

/** True for terrains that have "corner" tiles 41, 42, 51, 52, 61, 62, 71, 72. There is an exception for Forest, so it can't be automated. */
export const hasCornerTiles: { [terrain: string]: boolean } = {
  [Terrain.Swamp]: true,
  [Terrain.Hill]: true,
  [Terrain.Mountain]: true,
  [Terrain.Desert]: true,
  [Terrain.Volcanic]: true,
  [Terrain.Ice]: true
};

export const terrainToSpriteSheet: { [terrain: string]: SpriteSheet } = {
  [Terrain.Water]: spriteSheetWater,
  [Terrain.Open]: spriteSheetOpen,
  [Terrain.Swamp]: spriteSheetSwamp,
  [Terrain.Forest]: spriteSheetForest,
  [Terrain.Hill]: spriteSheetHill,
  [Terrain.Mountain]: spriteSheetMountain,
  [Terrain.Desert]: spriteSheetDesert,
  [Terrain.Volcanic]: spriteSheetVolcanic,
  [Terrain.Ice]: spriteSheetIce,
  [TempTerrain.ForestClearing]: { 20: spriteSheetForest[99] }
};
