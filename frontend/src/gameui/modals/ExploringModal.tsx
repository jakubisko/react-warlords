import './ExploringModal.scss';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Modal } from 'react-bootstrap';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { applyExploringResult } from '../../model/exploring/actions';
import { setShowingExploring } from '../../model/ui/actions';
import ArtifactPicker from './subcomponents/ArtifactPicker';
import ArmyImage from '../common/ArmyImage';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { EMPTY_ARRAY } from '../../functions';

export default function ExploringModal() {
  const isShip = useSelector((state: RootState) => {
    const { game: { terrain, groups }, ui: { activeGroupId } } = state;
    const { row, col } = groups[activeGroupId!];
    return terrain[row][col] === Terrain.Water;
  });
  const isSage = useSelector((state: RootState) => {
    const { game: { structures, groups }, ui: { activeGroupId } } = state;
    const { row, col } = groups[activeGroupId!];
    return structures[row][col] === Structure.SageTower;
  });
  const { message, artifactIdsFound, heroDies } = useSelector((state: RootState) => state.exploring.exploringResult);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    soundEffects.play(isSage ? SoundEffect.SageTower : SoundEffect.ExploringRuin);
    if (!isSage) {
      setTimeout(() => {
        soundEffects.play(heroDies ? SoundEffect.ExploringFailure : SoundEffect.ExploringSuccess);
      }, 4000); // Same value as the third line of the text has in the SCSS.
    }
  }, EMPTY_ARRAY);

  function handleEndOfExploring() {
    dispatch(applyExploringResult());
    dispatch(setShowingExploring(false));
  }

  return (
    <Modal className="exploring-modal" show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Exploring</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className={'exploring-image mx-auto' + (isShip ? ' ship' : '') + (!isShip && isSage ? ' sage' : '')} />

        <div className="message mx-3">
          {message.map((line, i) => (
            <p key={i} className={'line-' + i}>
              {line}
            </p>
          ))}
        </div>

        <div className={'line-image' + (artifactIdsFound.length > 0 ? '' : ' d-invisible')}>
          <div className="text-center line-3">
            <ArtifactPicker
              artifactIds={artifactIdsFound}
              actionButton={() => <div />}
            />
            {/* We have to always show something; otherwise user could guess the result by the size of the modal. */}
            {heroDies && <ArmyImage armyTypeId={null} dead={true} />}
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={handleEndOfExploring}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
