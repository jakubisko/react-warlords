import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { Place } from 'ai-interface/types/place';
import { EMPTY_MAP, createGrid, getHighestItem } from '../../functions';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { City } from '../game/types';
import { StructureAtPlace } from '../editor/types';
import { canPlaceStructure } from '../editor/selectors';
import { SearchPathArgs } from 'ai-interface/types/path';
import { calculateTimeMatrix } from '../path/dijkstrasAlgorithm';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { neutralPlayerId } from 'ai-interface/constants/player';

const getGameState = (state: RootState) => state.game;

/** Water area is a continuous area of water that can be traveled by ship. Thus waterfalls and cliffs act as obstacles. */
export interface WaterArea {
  readonly structuresOnWater: number;

  /** Continent is a continuous area of land that is travelable on foot. Thus Mountains and Volcanic act as obstacles. */
  readonly adjacentContinents: {
    /** Shore is a list of Water tiles touching a land by one or more sides. Edge of the map is not a land. */
    readonly shore: Place[];
    readonly structuresOnLand: number;
  }[];
}

/**
 * Scans entire map and returns a list of every water area.
 * For each water area, returns the number of visitable structures on it, and a list of continents it touches.
 * For each continent the water area touches, returns the shore tiles and the number of visitable structures on it.
 */
export const getAllWaterAreas = createSelector(
  getGameState,
  ({ size, terrain, structures }) => {
    const steps = [{ dRow: -1, dCol: 0 }, { dRow: 1, dCol: 0 }, { dRow: 0, dCol: -1 }, { dRow: 0, dCol: 1 }];

    const waterAreas: WaterArea[] = [];
    // continent[][] : 0 = not done; -1 = done but not walkable (water, mountain, volcanic, ridge); 1+ = walkable land done, continentId
    const continent = createGrid(size, (row, col) => terrain[row][col] === Terrain.Mountain || terrain[row][col] === Terrain.Volcanic
      || structures[row][col] === Structure.Ridge ? -1 : 0 as number);
    const continentToStructures: { [continentId: number]: number } = {};

    // Given a place on land, this traverses the enclosing continent.
    // This will set the "continent[][]" to a new continentId, and will mark the continent's area to the "continentToArea" map.
    function traverseOneContinent(row: number, col: number) {
      const continentId = Object.keys(continentToStructures).length + 1;
      let structuresOnLand = 0;
      const stack: Place[] = [{ row, col }];
      continent[row][col] = continentId;

      while (true) {
        const place = stack.pop();
        if (place === undefined) {
          break;
        }
        const { row, col } = place;

        for (const { dRow, dCol } of steps) {
          const row2 = row + dRow;
          const col2 = col + dCol;
          if (row2 >= 0 && row2 < size && col2 >= 0 && col2 < size) {
            if (continent[row2][col2] === 0 && terrain[row2][col2] !== Terrain.Water) {
              continent[row2][col2] = continentId;
              stack.push({ row: row2, col: col2 });
            }
          }
        }

        if (structures[row][col] !== Structure.Nothing) {
          structuresOnLand++;
        }
      }
      continentToStructures[continentId] = structuresOnLand;
    }


    // Given a place on water, this traverses the enclosing water area.
    // This will set the "continent[][]" to a -1.
    // For each shore (border with a continent) collects all tiles of the water area that are bordering with that continent.
    function traverseOneWaterArea(row: number, col: number) {
      let structuresOnWater = 0;
      const adjacentShores: { [continentId: number]: Place[] } = {};
      const stack: Place[] = [{ row, col }];
      continent[row][col] = -1;

      while (true) {
        const place = stack.pop();
        if (place === undefined) {
          break;
        }
        const { row, col } = place;

        const nextToContinent: { [continentId: number]: boolean } = {};
        for (const { dRow, dCol } of steps) {
          const row2 = row + dRow;
          const col2 = col + dCol;
          if (row2 >= 0 && row2 < size && col2 >= 0 && col2 < size) {
            const continentId = continent[row2][col2];
            if (continentId === 0) { // No need to check the terrain - entire land was traversed by this point.
              continent[row2][col2] = -1;
              stack.push({ row: row2, col: col2 });
            } else if (continentId > 0) {
              nextToContinent[continentId] = true;
            }
          }
        }

        if (structures[row][col] !== Structure.Nothing) {
          structuresOnWater++;
        }
        for (const continentId in nextToContinent) {
          if (!(continentId in adjacentShores)) {
            adjacentShores[continentId] = [];
          }
          adjacentShores[continentId].push(place);
        }
      }

      waterAreas.push({
        structuresOnWater,
        adjacentContinents: Object.keys(adjacentShores).map(Number).map(continentId => ({
          shore: adjacentShores[continentId],
          structuresOnLand: continentToStructures[continentId]
        }))
      });
    }

    for (let row = 0; row < size; row++) {
      for (let col = 0; col < size; col++) {
        if (continent[row][col] === 0 && terrain[row][col] !== Terrain.Water) {
          traverseOneContinent(row, col);
        }
      }
    }

    for (let row = 0; row < size; row++) {
      for (let col = 0; col < size; col++) {
        if (continent[row][col] === 0) { // No need to check the terrain - entire land was traversed by this point.
          traverseOneWaterArea(row, col);
        }
      }
    }

    return waterAreas;
  }
);


/**
 * Given a place, this returns the nearest city to it that is not in the "ignoreCities" list. Direct distance is used.
 * Returns undefined when all cities are to be ignored.
 */
export const getNearestCity = createSelector(
  getGameState,
  (rootState: RootState, place: Place) => place,
  (rootState: RootState, place: Place, ignoreCities: City[]) => ignoreCities,
  ({ cities }, place, ignoreCities) => {
    const { row: row1, col: col1 } = place;
    const citiesToIgnore = new Set(ignoreCities);

    return getHighestItem(
      Object.values(cities).filter(city => !citiesToIgnore.has(city)),
      ({ row: row2, col: col2 }) => - ((row1 - row2) ** 2 + (col1 - col2) ** 2)
    );
  });


export const getSomeNearFreeTileForStructure = createSelector(
  (rootState: RootState) => rootState, getGameState, (rootState: RootState, structureAtPlace: StructureAtPlace) => structureAtPlace,
  (state, { size }, structureAtPlace) => {
    const { row: row0, col: col0, structure, border = 0 } = structureAtPlace;

    let a = 2 * Math.PI * Math.random();
    for (let dist = 2.85; dist < 10; dist += 0.1, a += 1) {
      const row = Math.round(row0 + dist * Math.sin(a));
      const col = Math.round(col0 + dist * Math.cos(a));
      const structureAtPlace: StructureAtPlace = { row, col, structure, border };
      if (row >= 0 && col >= 0 && row < size && col < size && canPlaceStructure(state, structureAtPlace)) {
        return { row, col };
      }
    }
    return undefined;
  });


/**
 * This will return true for a city that has less than given number of cities within some walking distance.
 * Such city is not good as a starting location.
 */
export const isCityTooRemote = createSelector(
  getGameState, (rootState: RootState, city: City) => city,
  (gameState, city) => {
    const { tiles, terrain } = gameState;

    const cityIdsFound: { [cityId: string]: boolean } = {};

    const searchPathArgs: SearchPathArgs = {
      start: city,
      isDestination: (row, col) => {
        const { cityId } = tiles[row][col];
        if (cityId !== undefined) {
          cityIdsFound[cityId] = true;
        }
        return false;
      },
      isToBeAvoided: (row, col) => terrain[row][col] === Terrain.Water,
      moveType: {
        abilities: EMPTY_MAP,
        groupMoveOnLand: 2 * 1.5 * standardArmyTypes.mino.move, // 2 days on road
        groupSize: 1
      },
      initialMoveLeft: 2 * 1.5 * standardArmyTypes.mino.move,
      maxDay: 0
    };

    calculateTimeMatrix(gameState, searchPathArgs);

    // A city is considered remote when there are less than 3 cities (including this one) within walking distance of 35 on road.
    return Object.keys(cityIdsFound).length < 3;
  });


/**
 * Given a list of places, this returns a place from that list that is the farthest from any city controlled by any player and from
 * the edge of the map.
 */
export const pickPlaceForAlternativeWinCondition = createSelector(
  getGameState, (rootState: RootState, places: Place[]) => places,
  ({ size, cities }, places) => {
    const keepAway: Place[] = [
      ...Object.values(cities).filter(city => city.owner !== neutralPlayerId),
      { row: 0, col: 0 },
      { row: 0, col: size - 1 },
      { row: size - 1, col: 0 },
      { row: size - 1, col: size - 1 },
      { row: 0, col: size / 2 },
      { row: size / 2, col: 0 },
      { row: size - 1, col: size / 2 },
      { row: size / 2, col: size - 1 },
    ];
    return getHighestItem(places, ({ row: row1, col: col1 }) => Math.min(...keepAway.map(
      ({ row: row2, col: col2 }) => (row1 - row2) ** 2 + (col1 - col2) ** 2
    )));
  });
