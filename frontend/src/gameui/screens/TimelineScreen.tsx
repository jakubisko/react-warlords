import './TimelineScreen.scss';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, ButtonGroup } from 'react-bootstrap';
import { ScreenEnum } from '../../model/ui/types';
import { setScreen } from '../../model/ui/actions';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { PlayerFlag } from '../common/PlayerFlag';
import { playerColors } from '../../model/game/constants';
import { EMPTY_ARRAY } from '../../functions';

export default function TimelineScreen() {
  const players = useSelector((state: RootState) => state.game.players);
  const timeline = useSelector((state: RootState) => state.game.timeline);
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(0);

  const parentRef = useRef<HTMLDivElement>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    function handleResize() {
      const parentDiv = parentRef.current!;
      const canvas = canvasRef.current!;

      canvas.width = parentDiv.clientWidth;
      canvas.height = parentDiv.clientHeight;

      updateCanvas();
    }

    // Initial call so that the canvas has correct size and is drawn.
    handleResize();

    // And then resize and redraw the canvas whenever the window is resized.
    window.addEventListener('resize', handleResize, true);
    return () => window.removeEventListener('resize', handleResize, true);
  });

  function updateCanvas() {
    if (subpage >= 2) {
      return;
    }

    const canvas = canvasRef.current!;
    const ctx = canvas.getContext('2d')!;
    const { width, height } = canvas;

    const data = subpage === 0
      ? timeline.map(item => item.cities)
      : timeline.map(item => item.armyValue);

    const maxTurn = data.length - 1;
    const maxValue = Math.max(...data.flatMap(item => item)) * 1.05;
    if (data.length === 0) {
      return;
    }

    ctx.strokeStyle = '#433';
    ctx.lineWidth = 2;

    // Draws the vertical lines
    ctx.setLineDash([1, 2]);
    ctx.beginPath();
    for (let turn = 7; turn < maxTurn; turn += 7) {
      ctx.moveTo(width * turn / maxTurn, 0);
      ctx.lineTo(width * turn / maxTurn, height);
    }
    ctx.stroke();
    ctx.setLineDash(EMPTY_ARRAY as number[]);

    // Draws a graph for each player
    ctx.lineWidth = 3;
    for (let playerId = 0; playerId < maxPlayers; playerId++) {
      ctx.strokeStyle = playerColors[playerId];

      const value = data[0][playerId];
      if (value === 0) {
        continue;
      }

      ctx.beginPath();
      ctx.moveTo(0, height - height * value / maxValue);

      for (let turn = 1; turn <= maxTurn; turn++) {
        const value = data[turn][playerId];
        ctx.lineTo(width * turn / maxTurn, height - height * value / maxValue);
      }

      ctx.stroke();
    }

    // Draws the rectangle
    ctx.strokeStyle = '#433';
    ctx.strokeRect(0, 0, width, height);
  }

  function handleSetSubpage(newSubpage: number) {
    setSubpage(newSubpage);
    updateCanvas();
  }

  const killsLossesSubpage = () => (
    <div className="container">
      <div className="row">
        <div className="col-6">
          Value of all armies
        </div>
        <div className="col-3 text-right pe-0">
          Killed
        </div>
        <div className="col-3 text-right pe-4">
          Lost
        </div>
      </div>
      {
        players.map((x, playerId) => playerId).filter(playerId => players[playerId].team !== neutralPlayerId).map(playerId => (
          <div
            className="row"
            key={playerId}
          >
            <div className="col-1 px-0 text-center">
              {players[playerId].team + 1}
            </div>
            <div className="col-3 prevent-cutting">
              {players[playerId].name}
            </div>
            <div className="col-3">
              <PlayerFlag owner={playerId} />
            </div>
            <div className="col-3 prevent-cutting text-right ps-3">
              {players[playerId].totalKills}
            </div>
            <div className="col-3 prevent-cutting text-right ps-3">
              {players[playerId].totalLosses}
            </div>
          </div>
        ))
      }
    </div>
  );

  return (
    <Modal className="screen timeline-screen overflow-hidden" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Game timeline</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div ref={parentRef} className="h-100">
          <canvas ref={canvasRef} className={subpage === 2 ? 'd-none' : ''} />
          {subpage === 2 && killsLossesSubpage()}
        </div>
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <ButtonGroup>
          <Button active={subpage === 0} onClick={() => handleSetSubpage(0)}>Cities</Button>
          <Button active={subpage === 1} onClick={() => handleSetSubpage(1)}>Armies</Button>
          <Button active={subpage === 2} onClick={() => handleSetSubpage(2)}>Kills/Losses</Button>
        </ButtonGroup>

        <Button onClick={() => dispatch(setScreen(ScreenEnum.MainMenuScreen))}>Exit</Button>
      </Modal.Footer>
    </Modal>
  );
}
