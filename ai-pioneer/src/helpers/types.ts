import { Place } from 'ai-interface/types/place';

export interface Task {
  utility: number; // Expected utility; higher is better.
  dailyMultiplier?: number; // Utility naturally decreases with a distance. However, if you want to speed it up, set this to less than 1.
  place?: Place; // Final place where the action ends. For example, "visit a ruin" has the ruin's place.
  action: () => void; // Executes the action. For example moves the group and explores a ruin.
}

export interface BuildTask {
  utility: number; // Expected utility; higher is better.
  price: number; // In gold. This may also include price to demolish a building if necessary.
  action: () => void; // Executes the action (Builds some army type in a city).
  description: string; // Text displayed when debugging is on.
}

export interface TrainingTask {
  utility: number; // Expected utility; higher is better.
  cityId: string;
  armyTypeId?: string;
}

export const NOP = () => { };
