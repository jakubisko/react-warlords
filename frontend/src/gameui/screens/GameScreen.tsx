import './GameScreen.scss';
import { useCallback } from 'react';
import TacticalMap from '../map/tactical-map/TacticalMap';
import StrategicMap from '../map/strategic-map/StrategicMap';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { useDispatch, useSelector } from 'react-redux';
import CombatModal from '../modals/CombatModal';
import InfoArea from '../side-areas/InfoArea';
import ActionArea from '../side-areas/ActionArea';
import EditorMenuModal from '../modals/EditorMenuModal';
import { ProgramMode, ScreenEnum } from '../../model/ui/types';
import GameMenuModal from '../modals/GameMenuModal';
import { setScreen, setShowingToastMessage, setShowingArmyType, setMapFocus } from '../../model/ui/actions';
import { interruptMoving } from '../../model/path/actions';
import CityModal from '../modals/CityModal';
import PlaceInfoModal from '../modals/PlaceInfoModal';
import HeroModal from '../modals/HeroModal';
import { getPlaceOfActiveGroup } from '../../model/game/selectorsPlaying';
import ExploringModal from '../modals/ExploringModal';
import WarReportModal from '../modals/WarReportModal';
import SpyReportModal from '../modals/SpyReportModal';
import { Toast } from 'react-bootstrap';
import ArmyTypeModal from '../modals/ArmyTypeModal';
import HeroOfferModal from '../modals/HeroOfferModal';
import { Place } from 'ai-interface/types/place';
import ManualModal from '../modals/ManualModal';
import { EMPTY_ARRAY } from '../../functions';

export default function GameScreen() {
  const animationOverlay = useSelector((state: RootState) => state.path.movingAnimation);
  const programMode = useSelector((state: RootState) => state.ui.programMode);
  const showingMenu = useSelector((state: RootState) => state.ui.showingMenu);
  const showingCityId = useSelector((state: RootState) => state.ui.showingCityId);
  const showingPlaceInfo = useSelector((state: RootState) => state.ui.showingPlaceInfo);
  const showingHeroId = useSelector((state: RootState) => state.ui.showingHeroId);
  const showingWarReport = useSelector((state: RootState) => state.ui.showingWarReport);
  const showingSpyReport = useSelector((state: RootState) => state.ui.showingSpyReport);
  const showingHeroOffer = useSelector((state: RootState) => state.ui.showingHeroOffer);
  const showingCombat = useSelector((state: RootState) => state.ui.showingCombat);
  const showingRuinExploring = useSelector((state: RootState) => state.ui.showingRuinExploring);
  const showingManual = useSelector((state: RootState) => state.ui.showingManual);
  const showingArmyTypeId = useSelector((state: RootState) => state.ui.showingArmyTypeId);
  const showingToastMessage = useSelector((state: RootState) => state.ui.showingToastMessage);
  const placeOfActiveGroup = useSelector(getPlaceOfActiveGroup);

  const dispatch: AppDispatch = useDispatch();

  const handleStrategicMapClick = useCallback(({ row, col }: Place) => {
    dispatch(setMapFocus(row, col));
  }, EMPTY_ARRAY);

  const handleStrategicMapDoubleClick = useCallback(() => {
    dispatch(setScreen(ScreenEnum.ZoomedMapScreen));
  }, EMPTY_ARRAY);

  return (
    <div className="game-screen screen">
      {animationOverlay && (
        <div
          className="animation-overlay"
          onClick={() => dispatch(interruptMoving())}
          onContextMenu={() => dispatch(interruptMoving())}
        />
      )}

      {showingMenu && (programMode === ProgramMode.MapEditor ? <EditorMenuModal /> : <GameMenuModal />)}

      {showingCityId !== null && <CityModal cityId={showingCityId} />}

      {showingPlaceInfo !== null && <PlaceInfoModal place={showingPlaceInfo} />}

      {showingHeroId !== null && <HeroModal heroId={showingHeroId} place={placeOfActiveGroup!} />}

      {/**
       * Order of these modals is important! It determines which will be on top if they are displayed at once.
       * Order them so that the smallest by size is first and as such on the bottom, covered by bigger modals.
       */}
      {showingWarReport && <WarReportModal />}

      {showingHeroOffer && <HeroOfferModal />}

      {showingSpyReport && <SpyReportModal />}

      {showingCombat && <CombatModal />}

      {showingRuinExploring && <ExploringModal />}

      {showingManual && <ManualModal />}

      {showingArmyTypeId !== null && (
        <ArmyTypeModal
          armyTypeId={showingArmyTypeId}
          tooltipLike={true}
          onClose={() => dispatch(setShowingArmyType(null))}
        />
      )}

      <div className="side-menu">
        <InfoArea />
      </div>

      <div className="center-area">
        <TacticalMap />
      </div>

      <div className="side-menu right-area">
        <div className="right-top-area">
          <StrategicMap
            showFocusRectangle={true}
            onClick={handleStrategicMapClick}
            onDrag={handleStrategicMapClick}
            onDoubleClick={handleStrategicMapDoubleClick}
          />
        </div>

        <div className="right-bottom-area">
          <ActionArea />
        </div>
      </div>

      {showingToastMessage && (
        <Toast
          className="modal-dialog py-2"
          autohide
          onClose={() => dispatch(setShowingToastMessage(null))}
        >
          <Toast.Body className="modal-content" onClick={() => dispatch(setShowingToastMessage(null))}>
            <div className="modal-body">
              {showingToastMessage}
            </div>
          </Toast.Body>
        </Toast>
      )}
    </div>
  );
}
