import { Abilities } from './army';
import { Place } from './place';

/** Describes how a group travels on various terrains. */
export interface MoveType {

  /** Whether the group Flies, WalksOnVolcanic, NoPenaltyInSwamp etc. */
  readonly abilities: Abilities;

  /** How many armies are in the group. */
  readonly groupSize: number;

  /** Move value of the slowest army in the group. Doesn't apply on water - all ships have same speed. */
  readonly groupMoveOnLand: number;
}


/** Describes parameters for Dijkstra's algorithm, which searches for the fastest path from given place to some other place. */
export interface SearchPathArgs {

  /** Starting place of path. */
  readonly start: Place;

  /**
   * Function which determines whether we found the destination. It is guaranteed that this is called in ascending order of travel time.
   * Search will terminate after this function returns true for the first time.
   * 
   * IMPORTANT!
   * 1. This function must not call any actions on state. Doing so will result in arbitrary behavior.
   * 2. This function should be fast, since it will be called for each tile. If required, precalculate any values which you'll be comparing.
   */
  readonly isDestination: (row: number, col: number) => boolean;

  /**
   * Function which determines whether to avoid given tile. As an example, you can use this to avoid enemy armies.
   * 
   * IMPORTANT!
   * 1. This function must not call any actions on state. Doing so will result in an arbitrary behavior.
   * 2. This function should be fast, since it will be called for each tile. If required, precalculate any values which you'll be comparing.
   */
  readonly isToBeAvoided: (row: number, col: number) => boolean;

  /** Whether the path is for flying army, or the army has some native terrains. */
  readonly moveType: MoveType;

  /** Set to the lowest moveLeft among armies in the group. */
  readonly initialMoveLeft: number;

  /**
   * Optional maximum number of days after which the search is cut off. This is useful for example when you want to see what can be reached
   * within 10 days. 0 is today, 1 at most tomorrow etc. By default, entire map is searched.
   */
  readonly maxDay?: number;
}

export interface Direction {
  readonly rowOff: number;
  readonly colOff: number;
  readonly cost: number;
}

/**
 * This is a two-dimensional matrix of in-game travel times from one place to other places. Its size is equal to land size.
 * It is possible that the travel time calculation was terminated before covering entire map. Check this with "isCalculated" method.
 */
export interface TimeMatrix {
  /** Move type of the group which is traveling from source to destination. */
  readonly moveType: MoveType;

  /** The place from which the times are calculated. */
  readonly start: Place;

  /** The nearest place that passed the "isDestination" predicate. Undefined when no such place can be reached. */
  readonly destination?: Place;

  /**
   * Places with travel time higher than this travel time contain no relevant data - the calculation was terminated when it reached the
   * destination. Use method isCalculated(row, col) to check this.
   */
  readonly maxCalculatedDays: number;

  /**
   * If a place has calculated "days" exactly equal to the maxCalculatedDays, then values with "moveLeft" lower than this value contain no
   * relevant data - the calculation was terminated when it reached the destination. Use method isCalculated(row, col) to check this.
   */
  readonly minCalculatedMoveLeft: number;

  /**
   * Returns true if the place is no farther than the destination and it's distance is calculated. Otherwise returns false - for such places
   * ignore returned travel times, they will contain an arbitrary larger values.
   */
  isCalculated(row: number, col: number): boolean;

  /**
   * Holds the number of days to reach given tile and what move will be left for the slowest army in the group.
   * When comparing, lower number of days wins. Then, in case of same values for days, higher movesLeft wins.
   * If days[row][col] is infinite and the maxCalculatedDays is infinite, then the tile is unreachable.
   */
  readonly days: number[][];
  readonly movesLeft: number[][];

  /**
   * For each tile, remembers which direction was used to reach it. Used to reconstruct the entire path backwards from the destination.
   * To calculate the previous tile, subtract the direction's offsets from coordinates of current tile.
   */
  readonly directions: Direction[][];
}

/** When using createBasicAvoidingFunction, decides whether to avoid all other players, just allies, or attack anybody. */
export type BasicAvoidingStrategy = 'GO_AROUND_ALLIES_AND_ENEMIES' | 'GO_AROUND_ALLIES' | 'GO_THROUGH_ALLIES_AND_ENEMIES';