import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { Place } from 'ai-interface/types/place';
import { CombatConditions, CombatResult, CombatInputsAndResult, CombatInputs } from 'ai-interface/types/combat';

const getGameState = (state: RootState) => state.game;
const getCombatState = (state: RootState) => state.combat;

export const getCombatConditions = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  ({ terrain, tiles, cities }, { row, col }) => {
    const { cityId } = tiles[row][col];

    const combatConditions: CombatConditions = {
      row,
      col,
      terrain: terrain[row][col],
      city: cityId !== undefined,
      fortification: (cityId !== undefined && !cities[cityId].razed) ? (cities[cityId].capitol ? 2 : 1) : 0
    };

    return combatConditions;
  }
);

/**
 * This method is used by API only. Returns an object holding both combatInputs and combatResult. The returned object will be frozen.
 * Ids not visible to current player will be replaced by new, so that user can't access info normally hidden from him.
 */
export const getLastCombatInputsAndResult = createSelector(
  getCombatState,
  ({ combatInputs, combatResult }) => {
    if (combatInputs === null || combatResult === null) {
      return null;
    }

    const { conditions, attackingArmies, defendingArmies, attackingPlayerId, defendingPlayerId } = combatInputs;

    // Ids of enemy armies need to be replaced, so that user can't access hidden info such as whether it's same army as ten turns before.
    // Ids of current player's armies won't be changed.
    const transformId = (armyId: string) => {
      const enemyIndex = defendingArmies.findIndex(army => army.id === armyId);
      return enemyIndex < 0 ? armyId : (armyId.charAt(0) + '-TEMP-' + enemyIndex);
    };

    const combatInputs2: CombatInputs = {
      conditions: { ...conditions },
      attackingArmies: attackingArmies.map(army => ({ ...army })),
      defendingArmies: defendingArmies.map(army => ({
        ...army,
        id: transformId(army.id),
        groupId: '',
        moveLeft: 0
      })),
      attackingPlayerId,
      defendingPlayerId
    };

    const { attackingAbilities, defendingAbilities, effectiveStrength, combatEvents, experiences, attackerWon } = combatResult;

    const combatResult2: CombatResult = {
      attackingAbilities: { ...attackingAbilities },
      defendingAbilities: { ...defendingAbilities },
      effectiveStrength: Object.keys(effectiveStrength).reduce((result, armyId) => {
        result[transformId(armyId)] = effectiveStrength[armyId];
        return result;
      }, {} as { [armyId: string]: number }),
      combatEvents: combatEvents.map(combatEvent => ({
        type: combatEvent.type,
        losingArmyId: transformId(combatEvent.losingArmyId),
        losingGroupId: defendingArmies.some(army => army.id === combatEvent.losingArmyId) ? '' : combatEvent.losingGroupId,
        winningArmyId: transformId(combatEvent.winningArmyId),
        winningGroupId: defendingArmies.some(army => army.id === combatEvent.winningArmyId) ? '' : combatEvent.winningGroupId
      })),
      experiences,
      attackerWon
    };

    const result: CombatInputsAndResult = {
      combatInputs: combatInputs2,
      combatResult: combatResult2
    };

    return result;
  }
);