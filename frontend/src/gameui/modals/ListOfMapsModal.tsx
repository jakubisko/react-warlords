import './ListOfMapsModal.scss';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setErrorMessage } from '../../model/ui/actions';
import { Modal, Button } from 'react-bootstrap';
import { default as maps } from '../../predefined-data/maps.json';
import PageableList from '../common/PageableList';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMale } from '@fortawesome/free-solid-svg-icons';
import { AppDispatch } from '../../model/root/configureStore';

interface ListOfMapsModalProps {
  onClose: () => void;
  handleMapLoad: (fileData: ArrayBuffer, author: string) => void;
}

export default function ListOfMapsModal({ onClose, handleMapLoad }: ListOfMapsModalProps) {
  const [working, setWorking] = useState(false);
  const [players, setPlayers] = useState(0);
  const [size, setSize] = useState(0);
  const dispatch: AppDispatch = useDispatch();

  async function mapPicked({ filename, author }: { filename: string, author: string }) {
    setWorking(true);

    try {
      const res = await fetch(`maps/${filename}.wmap`);
      const fileData = await res.arrayBuffer();
      handleMapLoad(fileData, author);
      onClose();
    } catch (e) {
      setWorking(false);
      dispatch(setErrorMessage(`Sorry, failed to load the map.`, e));
    }
  }

  const mapsFiltered = maps
    .filter(map => players === 0 || players === map.players)
    .filter(map => size === 0 || size === map.size);

  return (
    <Modal className="list-of-maps-submodal overflow-hidden" show centered onHide={onClose}>
      <Modal.Header>
        <Modal.Title>Pick a map</Modal.Title>
      </Modal.Header>

      <Modal.Body className="py-0">

        <div className="container">
          <div className="row p-1">

            <div className="col-2 d-flex">
              <span className="d-inline-flex align-items-center">
                Players:
              </span>
            </div>
            <div className="col-4">
              <select
                className="form-control"
                value={players}
                onChange={e => setPlayers(parseInt(e.target.value))}
              >
                <option key={0} value={0}>Any</option>
                {[2, 3, 4, 5, 6, 7, 8].map(i => (
                  <option key={i} value={i}>
                    {i}
                  </option>
                ))}
              </select>
            </div>

            <div className="col-2 d-flex">
              <span className="d-inline-flex align-items-center">
                Size:
              </span>
            </div>
            <div className="col-4">
              <select
                className="form-control"
                value={size}
                onChange={e => setSize(parseInt(e.target.value))}
              >
                <option key={0} value={0}>Any</option>
                {maps.map(m => m.size).filter((elem, pos, arr) => arr.indexOf(elem) === pos).sort((a, b) => a - b).map(i => (
                  <option key={i} value={i}>
                    {i}
                  </option>
                ))}
              </select>
            </div>

          </div>

          <PageableList
            list={mapsFiltered.map(map =>
              <div className="row" key={map.filename}>
                <div className="col-7 py-1 text-left text-truncate">
                  <span className="link" onClick={() => !working && mapPicked(map)}>
                    {map.filename.replace(/-/g, ' ')}
                  </span>
                </div>
                <div className="col-2 px-0 py-1 text-center">
                  {map.players} <FontAwesomeIcon icon={faMale} className="me-2" />
                </div>
                <div className="col-3 px-0 py-1 text-center">
                  {map.size}x{map.size}
                </div>
              </div>
            )}
            working={working}
            maxItemsPerPage={20}
            itemHeightPx={28}
            restOfHeightPx={180}
            footer={(
              <Button
                disabled={working}
                onClick={onClose}
              >
                Back
              </Button>
            )}
          />

        </div>
      </Modal.Body>
    </Modal>
  );
}