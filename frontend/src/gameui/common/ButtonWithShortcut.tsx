import { ReactNode, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'react-bootstrap';
import { interruptMoving } from '../../model/path/actions';
import { AppDispatch } from '../../model/root/configureStore';

interface ButtonWithShortcutProps {
  children: ReactNode;
  disabled?: boolean; // Whether the button is disabled. Shortcuts don't work for disabled button. Default false.
  shortcutCode: string;
  allowShortcutWhileModalIsOpen?: boolean; // By default, shortcuts don't work when some modal is open. This enables that.

  onClick: () => void;
}

export default function ButtonWithShortcut(props: ButtonWithShortcutProps) {
  const { children, disabled, shortcutCode, allowShortcutWhileModalIsOpen, onClick } = props;
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown, true);
    return () => window.removeEventListener('keydown', handleKeyDown, true);
  });

  function handleKeyDown(event: KeyboardEvent) {
    const { repeat, code, ctrlKey } = event;

    // These keys by default press the last focused button. We don't want that.
    if (code !== shortcutCode && (code === 'Space' || code === 'Enter')) {
      event.preventDefault();
    }

    if (!disabled && code === shortcutCode && !ctrlKey && !repeat
      && (allowShortcutWhileModalIsOpen || !document.body.classList.contains('modal-open'))) {
      dispatch(interruptMoving());
      onClick();
      event.preventDefault();
    }
  }

  return (
    <Button
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </Button>
  );
}
