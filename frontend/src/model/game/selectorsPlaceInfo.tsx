import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { Place } from 'ai-interface/types/place';
import { allArmyTypes } from './constants';
import { MoveCostCalculatorImplUnchecked } from '../path/MoveCostCalculatorImplUnchecked';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain, terrainToName } from 'ai-interface/constants/terrain';
import { Abilities } from 'ai-interface/types/army';
import { Ability } from 'ai-interface/constants/ability';
import { villageIncome } from 'ai-interface/constants/flaggable';
import { getChanceToDieWhenExploringRuin } from 'ai-interface/constants/exploring';
import { maxStrengthOnShip } from 'ai-interface/constants/combat';
import { getUnexploredForReadingOnly } from './selectorsPlaying';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { cityIncome } from 'ai-interface/constants/city';
import { experienceForGrove } from 'ai-interface/constants/experience';
import { ProgramMode } from '../ui/types';
import { EMPTY_ARRAY, EMPTY_MAP } from '../../functions';

const getUiState = (state: RootState) => state.ui;
const getGameState = (state: RootState) => state.game;

export const isUnexplored = createSelector(
  (rootState: RootState) => rootState, (rootState: RootState, place: Place) => place,
  (rootState, { row, col }) => {
    const unexplored = getUnexploredForReadingOnly(rootState);
    return unexplored !== null && unexplored[row][col];
  }
);

export const getStructureName = createSelector(
  getGameState, getUiState, (rootState: RootState, place: Place) => place,
  (gameState, { programMode }, { row, col }) => {
    const { terrain, tiles, structures, fogOfWar, ruins, cities, flaggables, groups, players, currentPlayerId } = gameState;
    const structure = structures[row][col];

    function getAlliedFlaggableSuffix() {
      const { flaggableId } = tiles[row][col];
      const { owner } = flaggables[flaggableId!];
      const ownerTeam = players[owner].team;
      const currentTeamId = players[currentPlayerId].team;
      return currentPlayerId !== owner && ownerTeam === currentTeamId && programMode === ProgramMode.Playing ? ' (Allied)' : '';
    }

    switch (structure) {
      case Structure.Ruin: {
        const isWater = terrain[row][col] === Terrain.Water;
        const { ruinId } = tiles[row][col];
        const { level } = ruins[ruinId!];
        switch (level) {
          case 1: return isWater ? 'Derelict barge' : 'Cathedral';
          case 2: return isWater ? 'Sinking schooner' : 'Ziggurat';
          case 3: return isWater ? 'Beached shipwreck' : 'Mausoleum';
          case 4: return isWater ? 'Ghost ship' : 'Tomb';
          default: return 'Ruin'; // Fallthrough prevention
        }
      }

      case Structure.Signpost:
        return 'Signpost';

      case Structure.Temple:
        return 'Temple';

      case Structure.Port:
        return 'Port';

      case Structure.Encampment:
        return 'Encampment';

      case Structure.SageTower:
        return `Sage Tower`;

      case Structure.Grove:
        return 'Grove';

      case Structure.City: {
        const { cityId } = tiles[row][col];
        const { owner, name, razed } = cities[cityId!];
        const ownerTeam = players[owner].team;
        const currentTeamId = players[currentPlayerId].team;

        return name + (currentPlayerId !== owner && ownerTeam === currentTeamId && programMode === ProgramMode.Playing && !razed ? ' (Allied)' : '');
      }

      case Structure.Village:
        switch (terrain[row][col]) {
          case Terrain.Water: return 'Fishing hut' + getAlliedFlaggableSuffix();
          case Terrain.Open: return 'Village' + getAlliedFlaggableSuffix();
          case Terrain.Swamp: return 'Extractor' + getAlliedFlaggableSuffix();
          case Terrain.Forest: return 'Sawmill' + getAlliedFlaggableSuffix();
          case Terrain.Hill: return 'Iron mine' + getAlliedFlaggableSuffix();
          case Terrain.Desert: return 'Gold mine' + getAlliedFlaggableSuffix();
          case Terrain.Ice: return 'Coal mine' + getAlliedFlaggableSuffix();
        }
        break;

      case Structure.Watchtower:
        return 'Watchtower' + getAlliedFlaggableSuffix();

      case Structure.SpyDen:
        return 'Spy den' + getAlliedFlaggableSuffix();
    }

    // Movable objects are visible only if there is not fog of war.
    if (!fogOfWar[row][col]) {

      // Only if there is no structure name to be lost can we display this text
      const { armies, artifactIds } = tiles[row][col];
      if (armies) {
        const { owner } = groups[armies[0].groupId];
        const { team, name } = players[owner];
        const currentTeamId = players[currentPlayerId].team;

        if (owner === currentPlayerId && programMode === ProgramMode.Playing) {
          return 'Your army';
        } else if (team === currentTeamId && programMode === ProgramMode.Playing) {
          return `${name} army (Allied)`;
        } else {
          return `${name} army`;
        }
      }

      if (artifactIds) {
        return 'Bag with Artifacts';
      }
    }

    return '';
  }
);

export const getPlaceIncomeAndFortification = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, { row, col }) => {
    const { tiles, cities, structures, players, currentPlayerId } = gameState;
    const { cityId } = tiles[row][col];
    const currentTeamId = players[currentPlayerId].team;

    if (cityId !== undefined) {
      const { razed, lastSeenCapitol } = cities[cityId];
      if (!razed) {
        return lastSeenCapitol[currentTeamId]
          ? { income: 2 * cityIncome, fortification: 2 }
          : { income: cityIncome, fortification: 1 };
      }
    }

    if (structures[row][col] === Structure.Village) {
      return { income: villageIncome };
    }

    return EMPTY_MAP as { income?: number, fortification?: number };
  }
);

export const getStructureDescription = createSelector(
  getGameState, getUiState, (rootState: RootState, place: Place) => place,
  (gameState, { programMode }, { row, col }) => {
    const { structures, tiles, ruins, cities, players, currentPlayerId } = gameState;
    const structure = structures[row][col];
    const { ruinId, cityId, signpost } = tiles[row][col];
    const currentTeamId = players[currentPlayerId].team;

    switch (structure) {
      case Structure.Ruin: {
        const { level, lastSeenExplored } = ruins[ruinId!];

        if (lastSeenExplored[currentTeamId]) {
          return 'This ruin was already explored.';
        }

        return `Level ${level} ruin. Monster guards ${150 * level} ± 50 gold and a level ${level} artifact here. `
          + `Only a group led by a hero can explore the ruin.`;
      }

      case Structure.Signpost:
        return signpost ?? '';

      case Structure.Temple:
        return 'Blesses visiting armies at the start of the turn, giving them a permanent +1 Strength bonus.';

      case Structure.Port:
        return 'Non-flying groups board and unboard ships here.';

      case Structure.City: {
        const lastSeenRazed = cities[cityId!].lastSeenRazed[currentTeamId];
        return lastSeenRazed ? 'Razed city.' : '';
      }

      case Structure.Watchtower:
        return `Provides large view radius to the owner.`;

      case Structure.Encampment:
        return `Player without a hero will receive a free hero if his group stays here overnight.`;

      case Structure.SageTower:
        return `Provides help to weak players. Only a hero can visit the sage. Each player may visit one sage per week. ` +
          (programMode === ProgramMode.Playing ?
            (players[currentPlayerId].sageVisitedThisWeek ? `You already visited a sage this week.` : `You haven't yet visited a sage this week.`)
            : '');

      case Structure.Grove:
        return `Level 1 hero resting here overnight will receive ${experienceForGrove} experience.`;

      case Structure.SpyDen:
        return `Provides its owner full spy report on other players.`;
    }

    return '';
  }
);

/** Some structures have an expandable element with additional rules details. */
export const getStructureDescriptionExpanded = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, { row, col }) => {
    const { structures, tiles, ruins, players, currentPlayerId } = gameState;
    const structure = structures[row][col];
    const { ruinId } = tiles[row][col];
    const currentTeamId = players[currentPlayerId].team;

    switch (structure) {

      case Structure.Ruin: {
        const { level, lastSeenExplored } = ruins[ruinId!];
        if (lastSeenExplored[currentTeamId]) {
          return null;
        }

        const probabilityToDie = getChanceToDieWhenExploringRuin(level, 1, EMPTY_ARRAY as string[]);
        return `Level 1 hero has ${probabilityToDie}% probability of getting slaughtered here. This is decreased by 20 for each additional`
          + ` hero level, and by twice the total strength of all other armies present. Strength bonuses are ignored.`;
      }

      case Structure.SageTower: {
        return `Player weakness is determined by the number of cities he is behind the enemy with the most cities. Difference above 10 is ignored.`;
      }
    }

    return null;
  }
);


export const getTerrainNameAndDescription = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, { row, col }) => {
    const { terrain: terrains, structures } = gameState;
    const structure = structures[row][col];
    const terrain = terrains[row][col];

    const abilities: Abilities = {
      [Ability.Flies]: (terrain === Terrain.Mountain && structure === Structure.Nothing) || structure === Structure.Ridge ? 1 : 0,
      [Ability.WalksOnVolcanic]: 1
    };
    const moveCostCalculator = new MoveCostCalculatorImplUnchecked(gameState, abilities);
    const moveCost = moveCostCalculator.getMoveCostAt(row, col);
    const moveText = `Move cost ${moveCost} per tile.`;

    switch (structure) {
      case Structure.City: return `City. ${moveText}`;
      case Structure.Road: return `Road in ${terrainToName[terrain]}. ${moveText}`;
      case Structure.Ridge: return (terrain === Terrain.Water ? `Impassable water` : `Ridge`) + `. Flying armies only. ${moveText}`;
      default: return `Structure in ${terrainToName[terrain]}. ${moveText}`;
      case Structure.Nothing:
    }

    switch (terrain) {
      case Terrain.Water: return `${terrainToName[terrain]}. Ships and flying armies only. ${moveText} Non-flying armies have Strength capped at ${maxStrengthOnShip}.`;
      case Terrain.Mountain: return `${terrainToName[terrain]}. Flying armies and ${allArmyTypes.dwar.name} only. ${moveText}`;
      case Terrain.Volcanic: return `${terrainToName[terrain]}. ${allArmyTypes.elem.name} only. ${moveText} Impassable to all other armies, including flying ones.`;
      default: return `${terrainToName[terrain]}. ${moveText}`;
    }
  }
);

/**
 * If the given place is visible, returns the current owner of the city, flaggable or group standing there.
 * If the given place is in the fog of war, returns the last seen owner of the city or flaggable standing there.
 * Otherwise, returns null.
 */
export const getLastSeenOwnerOfPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, { row, col }) => {
    const { tiles, cities, flaggables, groups, fogOfWar, players, currentPlayerId } = gameState;
    const { cityId, flaggableId, armies } = tiles[row][col];
    const currentTeamId = players[currentPlayerId].team;

    // Movable objects are visible only if there is not fog of war.
    if (armies && !fogOfWar[row][col]) {
      return groups[armies[0].groupId].owner;
    }

    // Check city ownership only after you checked there are no armies. Razed cities don't change ownership to preserve the graphics!
    if (cityId !== undefined) {
      if (cities[cityId].lastSeenRazed[currentTeamId]) {
        return neutralPlayerId;
      }
      return cities[cityId].lastSeenOwner[currentTeamId];
    }

    if (flaggableId !== undefined) {
      return flaggables[flaggableId].lastSeenOwner[currentTeamId];
    }

    return null;
  }
);
