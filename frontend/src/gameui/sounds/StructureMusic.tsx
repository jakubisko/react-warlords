import cathedralSfx from '../sounds/structures/cathedral.mp3';
import city1Sfx from '../sounds/structures/city1.mp3';
import city2Sfx from '../sounds/structures/city2.mp3';
import city3Sfx from '../sounds/structures/city3.mp3';
import city4Sfx from '../sounds/structures/city4.mp3';
import encampmentSfx from '../sounds/structures/encampment.mp3';
import extractorSfx from '../sounds/structures/extractor.mp3';
import goldMineSfx from '../sounds/structures/goldMine.mp3';
import groveSfx from '../sounds/structures/grove.mp3';
import ironAndCoalMineSfx from '../sounds/structures/ironAndCoalMine.mp3';
import mausoleumAndTombSfx from '../sounds/structures/mausoleumAndTomb.mp3';
import oilPlatformSfx from '../sounds/structures/oilPlatform.mp3';
import portSfx from '../sounds/structures/port.mp3';
import razedCitySfx from '../sounds/structures/razedCity.mp3';
import ruinOnWaterSfx from '../sounds/structures/ruinOnWater.mp3';
import sageTowerSfx from '../sounds/structures/sageTower.mp3';
import sawmillSfx from '../sounds/structures/sawmill.mp3';
import spyDenSfx from '../sounds/structures/spyDen.mp3';
import templeSfx from '../sounds/structures/temple.mp3';
import villageSfx from '../sounds/structures/village.mp3';
import watchtowerSfx from '../sounds/structures/watchtower.mp3';
import zigguratSfx from '../sounds/structures/ziggurat.mp3';

import { useCallback, useEffect, useState } from 'react';
import useHowl from './useHowl';
import { StructureSound } from '../../model/sound/types';
import { EMPTY_ARRAY } from '../../functions';

/** How long fading between silence and full volume lasts. Smaller volume differences take appropriately shorter time. */
const fadeDuration = 2000;

const spriteNameToSfx: { [key in StructureSound]: string } = {
  [StructureSound.Cathedral]: cathedralSfx,
  [StructureSound.City1]: city1Sfx,
  [StructureSound.City2]: city2Sfx,
  [StructureSound.City3]: city3Sfx,
  [StructureSound.City4]: city4Sfx,
  [StructureSound.Encampment]: encampmentSfx,
  [StructureSound.Extractor]: extractorSfx,
  [StructureSound.Grove]: groveSfx,
  [StructureSound.GoldMine]: goldMineSfx,
  [StructureSound.IronAndCoalMine]: ironAndCoalMineSfx,
  [StructureSound.MausoleumAndTomb]: mausoleumAndTombSfx,
  [StructureSound.OilPlatform]: oilPlatformSfx,
  [StructureSound.Port]: portSfx,
  [StructureSound.RazedCity]: razedCitySfx,
  [StructureSound.RuinOnWater]: ruinOnWaterSfx,
  [StructureSound.SageTower]: sageTowerSfx,
  [StructureSound.Sawmill]: sawmillSfx,
  [StructureSound.SpyDen]: spyDenSfx,
  [StructureSound.Temple]: templeSfx,
  [StructureSound.Village]: villageSfx,
  [StructureSound.Watchtower]: watchtowerSfx,
  [StructureSound.Ziggurat]: zigguratSfx,
};

interface StructureMusicProps {
  volumeOfStructures: { [name: string]: number };
}

export const StructureMusic = ({ volumeOfStructures }: StructureMusicProps) => (
  <>
    {Object.entries(spriteNameToSfx).map(([name, sfx]) => (
      <OneSound key={name} sfx={sfx} volumeRequested={volumeOfStructures[name] ?? 0} />
    ))}
  </>
);

interface OneSoundProps {
  sfx: string;
  volumeRequested: number;
}

function OneSound({ sfx, volumeRequested }: OneSoundProps) {
  const [fading, setFading] = useState<'STOPPED' | 'FADING' | 'FIXED'>('STOPPED');

  // Workaround: Instead of handling the event in the event handler, we modify the state and handle it in the useEffect.
  // We can't access the sound in the "onfade" callback because that callback is an argument to the constructor of the sound.
  // Reading the state in the callback also doesn't work.
  const [fadingEnded, setFadingEnded] = useState(false);

  const howl = useHowl({
    src: sfx,
    loop: true,
    onfade: useCallback(() => setFadingEnded(true), EMPTY_ARRAY)
  });

  useEffect(() => {
    if (howl !== null) {
      if (fadingEnded) {
        setFadingEnded(false);
        if (howl.volume() === 0) {
          setFading('STOPPED');
          howl.stop();
        } else {
          setFading('FIXED');
        }
      } else if (fading === 'STOPPED' && volumeRequested > 0) {
        setFading('FADING');
        howl.play();
        howl.fade(0, volumeRequested, fadeDuration * volumeRequested);
      } else if (fading === 'FIXED' && volumeRequested !== howl.volume()) {
        setFading('FADING');
        howl.fade(howl.volume(), volumeRequested, fadeDuration * Math.abs(volumeRequested - howl.volume()));
      }
    }
  }, [howl, fadingEnded, fading, volumeRequested]);

  return <></>;
}
