import { Component, ReactNode } from 'react';
import FatalErrorScreen from './screens/FatalErrorScreen';

interface ErrorBoundaryProps {
  children: ReactNode;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

/**
 * NOTE:
 * During development, React displays an extra error overlay with the stacktrace when an error is thrown.
 * It won't be visible during production. Press "X" in the top right corner to remove it.
 * Under it you'll see what will be displayed in the production.
 */
export default class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {

  constructor(props: ErrorBoundaryProps) {
    super(props);

    this.state = {
      hasError: false
    };
  }

  static getDerivedStateFromError(): ErrorBoundaryState {
    return {
      hasError: true
    };
  }

  render() {
    return this.state.hasError ? <FatalErrorScreen /> : this.props.children;
  }

}
