import './ArmyTypeModal.scss';
import type { JSX } from 'react';
import ArmyImage from '../common/ArmyImage';
import { allArmyTypes } from '../../model/game/constants';
import { Button, Modal } from 'react-bootstrap';
import { AbilitiesIcons } from '../common/AbilitiesIcons';
import { IconImage, OtherIcon } from '../common/IconImage';
import { straightMoveCost } from 'ai-interface/constants/path';

interface ArmyTypeModalProps {
  armyTypeId: string;
  tooltipLike?: boolean; // If true, buttons will be hidden and clicking anywhere on screen will close the modal.

  onClose: () => void;
  changeSelectionButton?: (armyTypeId: string, close: () => void) => JSX.Element;
}

export default function ArmyTypeModal({ armyTypeId, tooltipLike, onClose, changeSelectionButton }: ArmyTypeModalProps) {
  const armyType = allArmyTypes[armyTypeId];

  return (
    <Modal
      className="army-type-modal overflow-hidden"
      show centered
      onHide={onClose}
      onClick={() => tooltipLike && onClose()}
    >
      <Modal.Header>
        <Modal.Title className="d-inline-flex align-items-center">
          {armyTypeId.indexOf('rnd') === 0 ? armyType.name : (
            <>
              <ArmyImage armyTypeId={armyTypeId} />
              &nbsp;
              {armyType.name}
              &nbsp;
              <ArmyImage armyTypeId={armyTypeId} />
            </>
          )}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="container px-0">
          <div className="row d-flex align-items-center">
            <div className="col-2"><IconImage icon={OtherIcon.Strength} tooltip="Strength" /></div>
            <div className="col-6">Strength</div>
            <div className="col-4">{armyType.strength}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-2"><IconImage icon={OtherIcon.HitPoints} tooltip="Hit points" /></div>
            <div className="col-6">Hit points</div>
            <div className="col-4">{armyType.hitPoints}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-2"><IconImage icon={OtherIcon.Move} tooltip="Move" /></div>
            <div className="col-6">Move</div>
            <div className="col-4">{armyType.move / straightMoveCost}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-2"><IconImage icon={OtherIcon.Price} tooltip="Daily upkeep. Paid at start of turn, even by new armies." /></div>
            <div className="col-6">Daily upkeep</div>
            <div className="col-4">{armyType.upkeep} gold</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-2"><IconImage icon={OtherIcon.Time} tooltip="Training time" /></div>
            <div className="col-6">Training time</div>
            <div className="col-4">{armyType.trainingTime} {armyType.trainingTime > 1 ? 'days' : 'day'}</div>
          </div>
          <div className="row pt-2">
            <div className="col">
              <AbilitiesIcons abilities={armyType.abilities} />
            </div>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        {!tooltipLike && (
          <>
            {changeSelectionButton !== undefined && changeSelectionButton(armyType.id, onClose)}

            <Button onClick={onClose}>
              Close
            </Button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
}
