import { SuchArmyTypeDoesntExist, ValueOutOfRange } from '../types/obstacle';
import { standardArmyTypes } from './standardArmyTypes';
import { experienceForRuin, heroExperienceLevels } from './experience';

/**
 * Returns a percentual chance that a hero will die when exploring a ruin. This method may return values lower than 0 or higher than 100.
 * This is equal to 40% per ruin level.
 * Each hero level beyond first decreases the chance by 20.
 * Each other army present decreases the chance by twice its base power.
 * Artifacts, Leadership, army abilities are ignored.
 */
export function getChanceToDieWhenExploringRuin(ruinLevel: number, heroLevel: number, otherArmyTypeIds: string[]): number {
  if (ruinLevel % 1 !== 0 || ruinLevel < 1 || ruinLevel >= experienceForRuin.length) {
    throw new ValueOutOfRange(`Ruin level '${ruinLevel}' doesn't exist, only values from 1 to ${experienceForRuin.length - 1} are allowed`);
  } else if (heroLevel % 1 !== 0 || heroLevel < 1 || heroLevel >= heroExperienceLevels.length) {
    throw new ValueOutOfRange(`Hero level '${heroLevel}' doesn't exist, only values from 1 to ${heroExperienceLevels.length - 1} are allowed`);
  }
  otherArmyTypeIds.forEach(assertArmyTypeExists);

  return 40 * ruinLevel - 20 * (heroLevel - 1) - 2 * otherArmyTypeIds.reduce((sum, armyTypeId) => sum + standardArmyTypes[armyTypeId].strength, 0);
}

function assertArmyTypeExists(armyTypeId: string) {
  if (!(armyTypeId in standardArmyTypes)) {
    throw new SuchArmyTypeDoesntExist(`Army type '${armyTypeId}' doesn't exist`);
  }
}
