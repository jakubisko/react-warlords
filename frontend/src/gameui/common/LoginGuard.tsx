import { ReactNode } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { setScreen } from '../../model/ui/actions';
import { ScreenEnum } from '../../model/ui/types';
import { WaitScreen } from '../screens/WaitScreen';

export default function LoginGuard({ children }: { children: ReactNode }) {
  const dispatch: AppDispatch = useDispatch();

  const loggedInUser = useSelector((state: RootState) => state.online.loggedInUser);
  if (loggedInUser === null) {
    dispatch(setScreen(ScreenEnum.OnlineLoginScreen));
    return <WaitScreen />;
  }

  return <>{children}</>;
}
