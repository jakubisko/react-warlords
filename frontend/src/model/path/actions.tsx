import { AppDispatch, RootState } from '../root/configureStore';
import { getMoveTypeOfGroup, getGroupIdsAffectedByMoveAll, wasSomethingSpotted, numberOfArmiesOfCurrentPlayerAtPlace, getMoveLeftOfGroup } from './selectors';
import { groupJumpToPlace, addWarReportItem, decreaseGroupMoveLeft, setFlaggableOwner } from '../game/actionsPlaying';
import { getGroupsDataCalculator, getViewRadiusOfGroup, getUnexploredForReadingOnly } from '../game/selectorsPlaying';
import { calculateMoveTypeOfGroup, MoveCostCalculatorImplUnchecked } from './MoveCostCalculatorImplUnchecked';
import { setMapFocus, setShowingCombat, activateGroupAndCalculatePath, setShowingToastMessage, setShowingCity } from '../ui/actions';
import { attackTile, applyCombatResult } from '../combat/actions';
import { Place, PlaceWithTime } from 'ai-interface/types/place';
import { calculateTimeMatrix, calculatePathToPlaceFromTimeMatrix } from './dijkstrasAlgorithm';
import { createBasicAvoidingFunction } from './basicAvoidingFunction';
import { WarReportType } from 'ai-interface/types/warReport';
import { SearchPathArgs } from 'ai-interface/types/path';
import { maxTileCapacity, diagonalMoveCost, straightMoveCost } from 'ai-interface/constants/path';
import { changeCityOwner, revealFogOfWar } from '../game/actionsPlayingThunks';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { SoundEffect } from '../sound/types';
import { soundEffects } from '../../gameui/sounds/soundEffects';
import { ScreenEnum } from '../ui/types';
import { setSoundSource } from '../sound/actions';
import { EMPTY_ARRAY } from '../../functions';

/** This many milliseconds will take for a group to move by one tile. */
const animationTimeToTravelOneTile = 1000 / 5;

export enum ActionTypes {
  SET_PATH = '[Path] SET_PATH',
  SET_MOVING_ANIMATION = '[Path] SET_MOVING_ANIMATION',
  SET_MOVE_ALL_GROUP_IDS = '[Path] SET_MOVE_ALL_GROUP_IDS',
  INTERRUPT_MOVING = '[Path] INTERRUPT_MOVING',
  SET_LAST_REVEALED_PLACES = '[Path] SET_LAST_REVEALED_PLACES'
}

export interface SetPath {
  type: ActionTypes.SET_PATH;
  payload: {
    path: PlaceWithTime[];
  }
}

export interface SetMovingAnimation {
  type: ActionTypes.SET_MOVING_ANIMATION;
  payload: {
    movingAnimation: boolean;
  }
}

export interface SetMoveAllGroupIds {
  type: ActionTypes.SET_MOVE_ALL_GROUP_IDS;
  payload: {
    moveAllGroupIds: string[];
  }
}

export interface InterruptMoving {
  type: ActionTypes.INTERRUPT_MOVING;
}

export interface SetLastRevealedPlaces {
  type: ActionTypes.SET_LAST_REVEALED_PLACES;
  payload: {
    lastRevealedPlaces: Place[];
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

/** Calculates the path to the given place and displays it on the screen. */
export const calculateAndDisplayShortestPathToPlaceForGroup = (groupId: string, destRow: number, destCol: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {

    const rootState = getState();
    const gameState = rootState.game;
    const { row, col } = gameState.groups[groupId];

    const moveType = getMoveTypeOfGroup(rootState, groupId);
    const initialMoveLeft = getMoveLeftOfGroup(rootState, groupId);

    const unexplored = getUnexploredForReadingOnly(rootState);
    const avoidingFunction0 = createBasicAvoidingFunction(gameState, moveType.groupSize, 'GO_AROUND_ALLIES_AND_ENEMIES');
    const avoidingFunction1 = (row: number, col: number) => avoidingFunction0(row, col) && (row !== destRow || col !== destCol);
    const avoidingFunction2 = (unexplored !== null)
      ? (row: number, col: number) => unexplored[row][col] || avoidingFunction1(row, col)
      : avoidingFunction1;

    const searchPathArgs: SearchPathArgs = {
      start: { row, col },
      isDestination: (row, col) => row === destRow && col === destCol,
      isToBeAvoided: avoidingFunction2,
      moveType,
      initialMoveLeft
    };

    const timeMatrix = calculateTimeMatrix(gameState, searchPathArgs);
    let path = calculatePathToPlaceFromTimeMatrix(timeMatrix, destRow, destCol);
    if (path.length === 0) {
      // On screen, we still want to draw selection rectangle around the group even when the destination is unreachable.
      path = [{
        row,
        col,
        day: 0,
        moveLeft: initialMoveLeft
      }];
    }

    dispatch(setPath(path));
  };

/** Moves each group of current player that has some move left and a destination set. */
export const moveAllGroups = () => (dispatch: AppDispatch, getState: () => RootState) => {
  if (!getState().path.movingAnimation) {
    dispatch(setMovingAnimation(true));
    dispatch(setShowingToastMessage(null));

    const groupIds: string[] = getGroupIdsAffectedByMoveAll(getState());
    dispatch(setMoveAllGroupIds(groupIds));

    if (groupIds.length > 0) {
      dispatch(moveNextGroup());
    } else {
      dispatch(setMovingAnimation(false));
    }
  }
};

/** If there are other groups to move, pick one, recalculate path and continue with animation. If not, end the animation. */
const moveNextGroup = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { moveAllGroupIds } = getState().path;

  if (moveAllGroupIds.length > 0) {
    const groupId = moveAllGroupIds[0];
    dispatch(setMoveAllGroupIds(moveAllGroupIds.slice(1)));
    dispatch(activateGroupAndCalculatePath(groupId, true));

    setTimeout(() => dispatch(moveActiveGroupAlongPath(true)), animationTimeToTravelOneTile);
  } else {
    if (getMoveLeftOfGroup(getState(), getState().ui.activeGroupId!) <= 0) {
      dispatch(activateGroupAndCalculatePath(null));
    }
    dispatch(setMovingAnimation(false));
  }
};

/**
 * Moves the active group along the previously calculated path. Calling this second time during move will be ignored.
 * Move includes animation and delays. Clicking on the screen during move will interrupt it (done by full-screen overlay).
 * If the group would step on enemy army, this will start the combat.
 * Move of this group is automatically finished after combat, when enemy is spotted, or if the move would be illegal.
 * After the move ends, next group will move if there is any in the "moveAllIds" list.
 */
export const moveActiveGroupAlongPath = (lockAlreadyAcquired = false) => (dispatch: AppDispatch, getState: () => RootState) => {
  if (!lockAlreadyAcquired) {
    if (getState().path.movingAnimation) {
      return;
    } else {
      dispatch(setMovingAnimation(true));
      dispatch(setShowingToastMessage(null));
    }
  }

  const activeGroupId = getState().ui.activeGroupId!;

  const moveType = calculateMoveTypeOfGroup(getState().game, activeGroupId);
  const moveCostCalculator = new MoveCostCalculatorImplUnchecked(getState().game, moveType.abilities);

  const activeGroup = getState().game.groups[activeGroupId];
  const groupsDataCalculator = getGroupsDataCalculator(getState());
  const { highestArmyTypeId, isShip } = groupsDataCalculator(activeGroup)!;
  const moveSound = 'Move_' + (isShip ? 'ship' : highestArmyTypeId.replace(/[0-9]/g, '')); // In case of a hero, remove the level number.
  let stepCount = 0;

  const oneStep = () => {
    const timeBefore = Date.now();

    /** User manually wishes to interrupt move. */
    if (getState().path.interruptMoving) {
      dispatch(setMoveAllGroupIds(EMPTY_ARRAY as string[])); // Cancel move all command
      return dispatch(setMovingAnimation(false));
    }

    /** If there is no more path, end the move. */
    const path = [...getState().path.path];
    if (path.length <= 1) {
      return dispatch(moveNextGroup());
    }

    const { row: srcRow, col: srcCol } = path[0];
    const { row: destRow, col: destCol } = path[1];

    if (getMoveLeftOfGroup(getState(), activeGroupId) <= 0) { // There is no move left for the army
      return dispatch(moveNextGroup());
    }

    if (moveType.groupSize + numberOfArmiesOfCurrentPlayerAtPlace(getState(), path[1]) > maxTileCapacity // There are armies of same owner, but together they would exceed maximum tile capacity
      || moveCostCalculator.getMoveCostAt(destRow, destCol) === Infinity // The group can't be placed at the place
      || moveCostCalculator.isCoastCrossingForbiddenAt(srcRow, srcCol, destRow, destCol) // The group doesn't fly and it would cross the coast outside of port
    ) {
      soundEffects.play(SoundEffect.ImpossibleMove);
      return dispatch(moveNextGroup());
    }

    /** Try to attack the tile. If this results in a combat, end the move animation and start the combat animation. Combat is asynchronous. */
    dispatch(attackTile(activeGroupId, destRow, destCol));
    if (getState().combat.combatResult !== null) {
      dispatch(setShowingCombat(true));
      dispatch(setMovingAnimation(false));
      return; // Do not select next group - that will be done when combat animation ends.
    }

    /** Do the step. Remove the step taken from path. */
    dispatch(moveGroupOneStepNoUi(activeGroupId, destRow, destCol));
    path.shift();
    dispatch(setPath(path));

    dispatch(setSoundSource(destRow, destCol));
    if (stepCount++ % 2 === 0) { // Sound effects are longer than the animation time, so don't play sound on each step.
      soundEffects.play(moveSound);
    }

    /** If something relevant was revealed from fog of war, end the move. */
    dispatch(setMapFocus(destRow + 0.5, destCol + 0.5));
    const toastMessage = wasSomethingSpotted(getState());
    if (toastMessage !== null) {
      soundEffects.play(SoundEffect.SomethingSpotted);
      dispatch(setShowingToastMessage(toastMessage));
      return dispatch(moveNextGroup());
    }

    /** Do next step. */
    const timePassed = Date.now() - timeBefore;
    const delay = Math.max(animationTimeToTravelOneTile - timePassed, 0);
    setTimeout(oneStep, delay);
  };

  /** Do the first step. */
  oneStep();
};

/** Call this after animation of combat ends to move the group and apply combat results. */
export const afterCombatAnimationEnded = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { combat: { combatInputs, combatResult }, path: { moveAllGroupIds } } = getState();
  const { attackingArmies, conditions: { row, col } } = combatInputs!;
  const { attackerWon } = combatResult!;
  const attackingGroupId = attackingArmies[0].groupId;

  dispatch(moveGroupOneStepNoUi(attackingGroupId, row, col));
  dispatch(applyCombatResult());

  dispatch(setShowingCombat(false));

  if (moveAllGroupIds.length > 0) {
    dispatch(moveAllGroups());
  } else {
    // If the group died, we need to unselect it. If the group lives, its movement type may have changed.
    dispatch(activateGroupAndCalculatePath(attackerWon ? attackingGroupId : null));
  }
};

/**
 * Moves a group to a place that is adjacent to its current place, reveals fog of war.
 * Unless the moved army was defeated in combat, changes ownership of captured city or flaggable.
 * Doesn't update the UI, skips animation and delay.
 *
 * This DOESN'T check whether the move is legal (volcanic, enemy armies etc.). You must check that yourself.
 */
export const moveGroupOneStepNoUi = (groupId: string, row: number, col: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { row: srcRow, col: srcCol } = getState().game.groups[groupId];

    dispatch(groupJumpToPlace(groupId, row, col));

    const moveType = calculateMoveTypeOfGroup(getState().game, groupId);
    const moveCostCalculator = new MoveCostCalculatorImplUnchecked(getState().game, moveType.abilities);
    const directionCost = (row !== srcRow && col !== srcCol) ? diagonalMoveCost : straightMoveCost;
    const moveCost = directionCost * moveCostCalculator.getMoveCostAt(row, col);
    const isBoardingDeboardingShip = moveCostCalculator.isBoardingDeboardingShipAt(row, col, srcRow, srcCol);
    dispatch(decreaseGroupMoveLeft(groupId, moveCost, isBoardingDeboardingShip));

    const { combatResult } = getState().combat;
    if (combatResult === null || combatResult.attackerWon) {
      dispatch(captureCityOrFlaggable(groupId, row, col));

      const viewRadius = getViewRadiusOfGroup(getState(), groupId);
      dispatch(revealFogOfWar(row, col, viewRadius));
    }
  };

/**
 * Change ownership of city or flaggable. Add capturing to report.
 * Note that the group should already stand on the given place, but place's ownership has not yet been changed.
 */
const captureCityOrFlaggable = (groupId: string, row: number, col: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { game: { size, tiles, cities, flaggables, currentPlayerId, players }, path: { moveAllGroupIds }, ui: { screen } } = getState();
    const { cityId, flaggableId } = tiles[row][col];

    if (cityId !== undefined) {
      const { row, col, owner, razed } = cities[cityId];
      if (!razed && owner !== currentPlayerId) {
        dispatch(changeCityOwner(cityId, currentPlayerId));

        dispatch(addWarReportItem(owner, {
          row,
          col,
          reportType: WarReportType.CityLost,
          value: 1,
          playerId: currentPlayerId
        }));

        if (screen === ScreenEnum.LandScreen) {
          // If our team controls every other non-neutral city, reveal entire map.
          const myTeam = players[currentPlayerId].team;
          if (Object.values(cities)
            .filter(city => city.id !== cityId && !city.razed && city.owner !== neutralPlayerId)
            .every(city => players[city.owner].team === myTeam)) {
            dispatch(revealFogOfWar(0, 0, 2 * size));
            dispatch(setShowingToastMessage(`Last enemy city captured.`));
            soundEffects.play(SoundEffect.LastEnemyCityCaptured);
          } else {
            soundEffects.play(SoundEffect.CaptureFlaggable);
            if (moveAllGroupIds.length === 0) {
              dispatch(setShowingCity(cityId));
            }
          }
        }
      }
    } else if (flaggableId !== undefined) {
      const { row, col, owner } = flaggables[flaggableId];
      if (owner !== currentPlayerId) {
        dispatch(setFlaggableOwner(flaggableId, currentPlayerId));

        dispatch(addWarReportItem(owner, {
          row,
          col,
          reportType: WarReportType.FlaggableLost,
          value: 1,
          playerId: currentPlayerId
        }));

        if (screen === ScreenEnum.LandScreen) {
          soundEffects.play(SoundEffect.CaptureFlaggable);
        }
      }
    }
  };

/** Sets the visible path symbols. */
export const setPath = (path: PlaceWithTime[]): SetPath => ({
  type: ActionTypes.SET_PATH,
  payload: {
    path: [...path]
  }
});

const setMovingAnimation = (movingAnimation: boolean): SetMovingAnimation => ({
  type: ActionTypes.SET_MOVING_ANIMATION,
  payload: {
    movingAnimation
  }
});

const setMoveAllGroupIds = (moveAllGroupIds: string[]): SetMoveAllGroupIds => ({
  type: ActionTypes.SET_MOVE_ALL_GROUP_IDS,
  payload: {
    moveAllGroupIds: [...moveAllGroupIds]
  }
});

export const setLastRevealedPlaces = (lastRevealedPlaces: Place[]): SetLastRevealedPlaces => ({
  type: ActionTypes.SET_LAST_REVEALED_PLACES,
  payload: {
    lastRevealedPlaces: [...lastRevealedPlaces]
  }
});

export const interruptMoving = (): InterruptMoving => ({
  type: ActionTypes.INTERRUPT_MOVING
});

export type PathAction = SetPath
  | SetMovingAnimation
  | SetMoveAllGroupIds
  | InterruptMoving
  | SetLastRevealedPlaces;
