import { LandApi } from './apis/LandApi';
import { GameApi } from './apis/GameApi';
import { GameApiObstacle } from './apis/GameApiObstacle';
import { DrawApi } from './apis/DrawApi';

/**
 * Your AI is a function that will be called to execute one turn.
 * Use arguments passed to the function to retrieve all values you'll need and to execute all operations you want.
 * 
 * Use LandApi methods to retrieve values about land that don't change during the entire game, such as the size of the land and the terrain.
 * Use GameApi methods to retrieve part of the game state that changes, and to issue commands. A command that would break game rules throws an error.
 * Use GameApiObstacle to check whether a method of GameApi would throw an error without executing the method.
 * Use DrawApi methods to draw debug text on the screen.
 * 
 * Please don't break the engine - don't change global objects such as window, call external services, create timers, access DOM, access
 * hidden information by reflections etc.
 * 
 * If your code throws an uncaught error, your program will be disqualified.
 */
export type AI = (
  land: LandApi,
  game: GameApi,
  obstacle: GameApiObstacle,
  draw: DrawApi
) => void;
