import './ArtifactImage.scss';
import type { JSX } from 'react';
import { allArtifacts } from '../../model/game/constants';
import { artifactAbilityToText, ArtifactAbility } from 'ai-interface/constants/artifactAbility';
import { EMPTY_ARRAY } from '../../functions';

const artifactImageOffsets: { [artifactId: string]: number } = {
  thunderMace: 6,
  giantFlail: 5,
  deathbringerLance: 42,
  defendersBuckler: 63,
  iciclePike: 59,
  murderersAxe: 50,
  armoredGauntlet: 14,
  lightningRod: 21,
  rentedMedal: 39,
  martyrsHand: 54,
  holyPendant: 53,
  berserkersAmulet: 58,
  sharpshootersBow: 38,
  snakeRing: 33,
  nomadBoots: 9,
  telescope: 10,
  tomeOfKnowledge: 11,
  scholarsVolume: 40,
  astrolabe: 55,
  sandsOfTime: 34,
  cloverOfFortune: 56,
  unwieldyOreCart: 57,
  mobilizationDecree: 60,
  endlessPurseOfGold: 18,
  randomCommonArtifact: 1,
  swordBreaker: 12,
  skullMace: 41,
  backstabber: 30,
  weightyBreastplate: 37,
  medalOfCourage: 15,
  ringOfDeath: 49,
  hideousMask: 16,
  heavyCrossbow: 8,
  catapultAmmo: 7,
  orbOfNegation: 52,
  summonersPipe: 44,
  bootsOfPolarity: 35,
  allSeeingEye: 17,
  endlessBagOfGold: 25,
  randomMinorArtifact: 2,
  heartPiercerJavelin: 19,
  goldenHammer: 13,
  spikedShield: 36,
  defenderHelm: 20,
  wandOfWizardry: 47,
  veteransMedal: 51,
  peacemakersRibbon: 62,
  cloakOfProtection: 48,
  conjurersBroach: 45,
  prepaidStagecoach: 22,
  trueCompass: 24,
  endlessSackOfGold: 43,
  randomMajorArtifact: 3,
  titansGladius: 26,
  ultimateStaff: 23,
  crownOfEnlightenment: 27,
  skeletalHelm: 28,
  ankh: 29,
  ringOfWishes: 46,
  angelsFeather: 31,
  luckyHorseshoe: 61,
  goldenGoose: 32,
  randomRelicArtifact: 4
};

interface ArtifactImageProps {
  artifactId: string | null; // For null id, empty space image is displayed.
  selectable?: boolean; // Whether the artifact can be selected. Default false.
  includeText?: boolean; // Whether the text describing the artifact should be included. Default false.
  alwaysLarge?: boolean; // Whether the image should be large even on mobile resolution. Default false. 
  onClick?: () => void; // Optional on-click event handler.
}

export default function ArtifactImage(props: ArtifactImageProps) {
  const { artifactId, selectable, includeText, alwaysLarge, onClick } = props;
  const offset = artifactId !== null ? artifactImageOffsets[artifactId] : 0;
  const abilities = (includeText && artifactId !== null) ? Object.entries(allArtifacts[artifactId].abilities)
    .map(([ability, value]) => (
      <p key={ability}>
        {artifactAbilityToText[ability as ArtifactAbility]
          .replace('{0}', '' + value)
          .replace('+', (value ?? 0) > 0 ? '+' : '')}
      </p>
    )) : EMPTY_ARRAY as JSX.Element[];

  return (
    <span className="artifact-image">
      <div
        className={'image-border' + (selectable ? ' selectable' : '')}
        onClick={() => onClick !== undefined && onClick()}
      >
        <div
          className={'image-content' + (alwaysLarge ? ' always-large' : '')}
          style={{
            backgroundPositionX: -64 * (offset % 10),
            backgroundPositionY: -64 * Math.floor(offset / 10)
          }}
        />
      </div>

      {abilities}
    </span>
  );
}
