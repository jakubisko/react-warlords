import { AppDispatch, RootState } from '../root/configureStore';
import { addGold, revealUnexploredAtPlaces, setSageVisitedThisWeek } from '../game/actionsPlaying';
import { addExperiences, createArtifactForHero, setRuinExplored, createArtifactOnGround } from '../game/actionsHero';
import { activateGroupAndCalculatePath } from '../ui/actions';
import { EMPTY_ARRAY, clamp, getRandom, getRandomItem, hashCoords } from '../../functions';
import { ExploringResult, SageTowerChoice } from 'ai-interface/types/exploring';
import { experienceForRuin } from 'ai-interface/constants/experience';
import { maxHeroArtifactsCapacity } from 'ai-interface/constants/artifactAbility';
import { getChanceToDieWhenExploringRuin } from 'ai-interface/constants/exploring';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';
import { createArmyAtOrAroundPlace, destroyArmy } from '../game/actionsPlayingThunks';
import heroNames from '../../predefined-data/hero-names.json';
import { getNumberOfCitiesByWhichPlayerIsBehindBestEnemy, getPlacesToExploreBySage } from './selectors';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { Place } from 'ai-interface/types/place';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { getPossibleRuinMonsterNames } from '../../predefined-data/predefinedDataGetters';

export enum ActionTypes {
  SET_EXPLORING_RESULT = '[Ruin] SET_EXPLORING_RESULT'
}

export interface SetExploringResult {
  type: ActionTypes.SET_EXPLORING_RESULT;
  payload: {
    exploringResult: ExploringResult;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

const setExploringResult = (exploringResult: ExploringResult): SetExploringResult => ({
  type: ActionTypes.SET_EXPLORING_RESULT,
  payload: {
    exploringResult: {
      ...exploringResult,
      message: [...exploringResult.message],
      artifactIdsFound: [...exploringResult.artifactIdsFound],
      armyTypeIdsFound: [...exploringResult.armyTypeIdsFound],
      mapExplored: { ...exploringResult.mapExplored }
    }
  }
});

export type ExploringAction = SetExploringResult;

// Thunks /////////////////////////////////////////////////////////////////////////////////////////

/**
 * Executes the ruin exploring in-memory; without animation and applying the results in-game. To get entire ruin exploring,
 * 1. Call exploreRuin() to calculate result of exploring.
 * 2. Call setShowingExploring() to display animation in ExploringModal.
 * 3. After animation ends, call applyExploringResult() to apply result on game state. Don't do it before - killed hero would
 *    disappear from the map before animation would even start.
 */
export const exploreRuin = (heroId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  /** Collect data about armies entering ruin etc. */
  const { terrain, tiles, ruins, heroes, groups } = getState().game;
  const hero = heroes[heroId];
  const { row, col } = groups[hero.groupId];
  const { armies, ruinId } = tiles[row][col];
  const { explored, level } = ruins[ruinId!];
  const otherArmyTypeIds = armies!.filter(army => army.id !== heroId).map(army => army.armyTypeId);

  /** Calculate entire ruin exploring without animation. */
  const message: string[] = [];
  let becomesExplored = false;
  let heroDies = false;
  let experiences = 0;
  let gold = 0;
  let artifactIdsFound = EMPTY_ARRAY as string[];

  if (explored) {
    message.push(`${heroNames[hero.portraitId]} searches the place...`, `and finds nothing!`, `This place was ransacked long ago.`);
  } else {
    const isShip = terrain[row][col] === Terrain.Water;
    const possibleMonsters = getPossibleRuinMonsterNames(level, isShip);
    const monsterName = possibleMonsters[hashCoords(row, col, possibleMonsters.length)];

    message.push(`${heroNames[hero.portraitId]} encounters a ${monsterName}...`);

    const probabilityToDie = getChanceToDieWhenExploringRuin(level, hero.level, otherArmyTypeIds);
    heroDies = getRandom(100) < probabilityToDie;

    if (heroDies) {
      message.push(`and is slaughtered!`, `Another hero will have to kill the ${monsterName}`, `and claim the guarded treasures.`);
      heroDies = true;
    } else {
      becomesExplored = true;
      experiences = experienceForRuin[level];
      gold = 150 * level + getRandom(100) - 50;

      const artifact = getRandomItem(Object.keys(standardArtifacts)
        .map(artifactId => standardArtifacts[artifactId])
        .filter(artifact => artifact.rarity === level));
      artifactIdsFound = [artifact.id];

      message.push(`and is victorious!`, `Treasure worth ${gold} gold`, `and the ${artifact.name} were found.`);
    }
  }

  const exploringResult: ExploringResult = {
    heroId,
    message,
    becomesExplored,
    heroDies,
    experiences,
    gold,
    artifactIdsFound,
    armyTypeIdsFound: EMPTY_ARRAY as string[],
    mapExplored: { minRow: 1, minCol: 1, maxRow: 0, maxCol: 0 }
  };
  dispatch(setExploringResult(exploringResult));
};

/** See exploreRuin() above. This is Sage Tower equivalent instead of a Ruin. */
export const exploreSageTower = (heroId: string, choicePicked: SageTowerChoice) => (dispatch: AppDispatch, getState: () => RootState) => {
  /** Collect the number of cities for each player etc. */
  const { heroes, groups, players, currentPlayerId } = getState().game;
  const hero = heroes[heroId];

  const { sageVisitedThisWeek } = players[currentPlayerId];

  const power = sageVisitedThisWeek ? 0 : clamp(0, 10, getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(getState(), currentPlayerId));

  /** Calculate reward without animation. */
  const message: string[] = [`${heroNames[hero.portraitId]} asks to see the sage...`];
  const becomesExplored = power > 0;
  let experiences = 0;
  let gold = 0;
  const armyTypeIdsFound: string[] = [];
  let mapExplored = { minRow: 1, minCol: 1, maxRow: 0, maxCol: 0 };

  if (sageVisitedThisWeek) {
    message.push(`but is denied,`, `because he has already visited a sage this week.`, `You have to wait until the next week.`);
  } else if (power === 0) {
    message.push(`but the sage can help you no longer!`, `You are already stronger than your enemies.`, `Seek the sage when you need the help.`);
  } else {
    message.push(`and is admitted!`);
    switch (choicePicked) {
      case SageTowerChoice.Armies: {
        // Only offer armies that don't give global bonuses such as Morale. These armies are meant to fight and die in long game.
        // Sage Tower shouldn't be abused to collect a perfect group of 1 Pegasi, 1 Dragon, 1 Catapult etc.
        let credit = Math.round((50 + 160 * power) * (0.9 + 0.2 * Math.random()));
        const potential = ['linf', 'orcs', 'lcav', 'dwar', 'hinf', 'pike', 'scor', 'elf', 'wolf', 'mino', 'hcav', 'gian', 'spid', 'grif']
          .filter(armyTypeId => standardArmyTypes[armyTypeId].buildingCost <= credit);

        while (credit > 0) {
          const armyTypeId = getRandomItem(potential);
          armyTypeIdsFound.push(armyTypeId);
          credit -= standardArmyTypes[armyTypeId].buildingCost;
        }
        message.push(`${armyTypeIdsFound.length} armies join your cause.`);
        break;
      }

      case SageTowerChoice.Gold: {
        gold = Math.round(80 * power * (0.9 + 0.2 * Math.random()));
        message.push(`He gives you a gem worth ${gold} gold.`);
        break;
      }

      case SageTowerChoice.Maps: {
        mapExplored = getPlacesToExploreBySage(getState(), groups[hero.groupId], 150 + 500 * power);
        message.push(`He gives you a map of nearby lands.`);
        break;
      }

      case SageTowerChoice.Experience: {
        experiences = Math.round(333 * power * (0.9 + 0.2 * Math.random()));
        message.push(`He teaches you lore worth ${experiences} experience.`);
        break;
      }
    }
    message.push(`Seek the sage again next week.`);
  }

  const exploringResult: ExploringResult = {
    heroId,
    message,
    becomesExplored,
    heroDies: false,
    experiences,
    gold,
    artifactIdsFound: EMPTY_ARRAY as string[],
    armyTypeIdsFound,
    mapExplored
  };
  dispatch(setExploringResult(exploringResult));
};

/**
 * Apply ruin exploring results on game data model. Call this only after animation ends. Otherwise, the defeated hero would disappear from
 * the map before animation even starts.
 */
export const applyExploringResult = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game: { tiles, structures, groups, heroes, unexploredByTeam, players, currentPlayerId }, exploring: { exploringResult } } = getState();
  const { heroId, becomesExplored, heroDies, experiences, gold, artifactIdsFound, armyTypeIdsFound, mapExplored } = exploringResult;
  const { groupId } = heroes[heroId];
  const { owner, row, col } = groups[groupId];
  const { team } = players[currentPlayerId];
  const unexplored = unexploredByTeam[team];

  if (heroDies) {
    dispatch(activateGroupAndCalculatePath(null));
    dispatch(destroyArmy(groupId, heroId));
  } else if (experiences !== 0) { // So that no message is displayed when no experiences are awarded.
    dispatch(addExperiences(heroId, experiences));
  }

  for (const artifactId of artifactIdsFound) {
    if (heroDies || (heroes[heroId].artifactIds?.length ?? 0) >= maxHeroArtifactsCapacity) {
      dispatch(createArtifactOnGround(row, col, artifactId));
    } else {
      dispatch(createArtifactForHero(heroId, artifactId));
    }
  }

  for (const armyTypeId of armyTypeIdsFound) {
    dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, owner));
  }

  if (becomesExplored) {
    const { ruinId } = tiles[row][col];
    if (ruinId !== undefined) {
      dispatch(setRuinExplored(ruinId, true));
    } else if (structures[row][col] === Structure.SageTower) {
      dispatch(setSageVisitedThisWeek(owner, true));
    }
  }

  if (unexplored !== null) {
    const { minRow, maxRow, minCol, maxCol } = mapExplored;
    const placesExplored: Place[] = [];
    for (let row = minRow; row <= maxRow; row++) {
      for (let col = minCol; col <= maxCol; col++) {
        if (unexplored[row][col]) {
          placesExplored.push({ row, col });
        }
      }
    }
    if (placesExplored.length > 0) {
      dispatch(revealUnexploredAtPlaces(placesExplored));
    }
  }

  dispatch(addGold(owner, gold));
};
