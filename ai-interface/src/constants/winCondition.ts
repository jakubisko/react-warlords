/**
 * Win condition.
 * Applies to both humans and computer players.
 * Standard loss condition of losing all cities always applies.
 */
export enum WinCondition {
  CaptureAllEnemyCities = 'Capture all enemy cities',
  Capture75PercentOfAllCities = 'Capture 75% of all cities',
  DestroyAllEnemyCapitols = 'Destroy all enemy capitols',
  ObtainALevel4Artifact = 'Obtain a level 4 artifact',
  CaptureTheCityUtopia = 'Capture the city Utopia',
}

/** The name of the city to capture when playing the Capture the Utopia. */
export const nameOfUtopia = 'Utopia';
