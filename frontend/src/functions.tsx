/** Helper functions. */

///////// Reusable constants to save React rerendering //////////
export const EMPTY_ARRAY = Object.freeze([] as unknown[]);
export const EMPTY_MAP = Object.freeze({} as unknown);

///////// Types and objects //////////

/** Make all properties in T non-readonly. Reverse of the standard utility type Readonly. */
export type Mutable<T> = {
  -readonly [P in keyof T]: T[P]
};


/** Given an object, this returns a copy of that object that will be missing properties that had undefined as a value in the original. */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function withoutUndefinedProperties<T extends Record<string, any>>(value: T) {
  return Object.fromEntries(Object.entries(value).filter(([, v]) => v !== undefined)) as T;
}

export function shallowEqualObjects<T>(objA: T, objB: T): boolean {
  if (objA === objB) {
    return true;
  }

  if (!objA || !objB) {
    return false;
  }

  const aKeys = Object.keys(objA);
  const bKeys = Object.keys(objB);
  const len = aKeys.length;

  if (bKeys.length !== len) {
    return false;
  }

  for (const key in objA) {
    if (objA[key] !== objB[key] || !Object.prototype.hasOwnProperty.call(objB, key)) {
      return false;
    }
  }

  return true;
}

///////// Random //////////

/** Returns a random whole number from 0 to max-1. */
export function getRandom(max: number): number {
  return Math.floor(max * Math.random());
}

/** Returns a random item from an array. */
export function getRandomItem<T>(array: T[]): T {
  return array[getRandom(array.length)];
}

/** Return a pseudo random integer based on given coordinates. Always returns the same value for same input parameters. */
export function hashCoords(row: number, col: number, upperBound: number): number {
  let h = 1 + row * 374761393 + col * 668265263;
  h = (h ^ (h >> 13)) * 1274126177;
  h = h ^ (h >> 16);
  return h % upperBound;
}

/** Randomly shuffles given array in place. Returns the reference to the array. */
export function shuffle<T>(a: T[]): T[] {
  for (let i = a.length - 1; i > 0; i--) {
    const j = getRandom(i + 1);
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

///////// Functions and numbers //////////

export function clamp(min: number, max: number, value: number): number {
  if (value > max) {
    return max;
  }
  return value < min ? min : value;
}

///////// Arrays //////////

/** Sorts given array in place by multiple fields. Returns the array. */
export function sortByMultiple<T>(array: T[], ...fieldGetters: ((item: T) => string | number)[]): T[] {
  return array.sort((o1, o2) => {
    for (const getter of fieldGetters) {
      const val1 = getter(o1);
      const val2 = getter(o2);

      if (val1 < val2) {
        return 1;
      } else if (val1 > val2) {
        return -1;
      }
    }

    return 0;
  });
}

/** Given an array of items and a function, this returns the item for which the function returned the highest value. */
export function getHighestItem<T>(array: T[], func: (item: T) => number): T | undefined {
  if (array.length === 0) {
    return undefined;
  }

  let bestVal = -Infinity;
  let bestItem: T = array[0];
  for (const item of array) {
    const val = func(item);
    if (val > bestVal) {
      bestVal = val;
      bestItem = item;
    }
  }

  return bestItem;
}

/** Creates a new two-dimensional array of given type with items based on the coordinates in the array. */
export function createGrid<T>(size: number, provider: (row: number, col: number) => T): T[][] {
  const array = new Array<T[]>(size);
  for (let row = 0; row < size; row++) {
    const newRow = new Array<T>(size);
    array[row] = newRow;
    for (let col = 0; col < size; col++) {
      newRow[col] = provider(row, col);
    }
  }

  return array;
}

/** Returns a copy of two-dimensional array with one item replaced. Old array will not be modified. */
export function updateGrid<T>(array: T[][], row: number, col: number, getNewItem: (oldItem: T) => T): T[][] {
  const newRow = [...array[row]];
  newRow[col] = getNewItem(newRow[col]);
  const newArray = [...array];
  newArray[row] = newRow;
  return newArray;
}

/** Creates an array of given length by calling given item provider. */
export function createArray<T>(length: number, itemProvider: (index: number) => T): T[] {
  const array = new Array<T>(length);
  for (let i = 0; i < length; i++) {
    array[i] = itemProvider(i);
  }
  return array;
}
