import { Component, ReactNode } from 'react';

interface UnmountableComponentProps {
  children: ReactNode;

  onComponentWillUnmount: () => void;
}

/**
 * This is a workaround.
 * Function components in React don't have a state and as such they don't know when they are unmounted.
 * Most of the time useEffect hook can replace componentWillUnmount(), but it's impossible to replicate with it the behavior of
 * "when this component is removed from screen and not just redrawn."
 */
export default class UnmountableComponent extends Component<UnmountableComponentProps> {

  componentWillUnmount() {
    this.props.onComponentWillUnmount();
  }

  render() {
    return this.props.children;
  }

}
