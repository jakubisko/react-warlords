export const localStorageKeyVolume = 'REACT_WARLORDS_VOLUME';

export const localStorageKeyAutosave = 'REACT_WARLORDS_AUTOSAVE';

export const sessionStorageKeyOnlineLogin = 'REACT_WARLORDS_ONLINE_LOGIN';
