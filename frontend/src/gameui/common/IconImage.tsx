import './IconImage.scss';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Ability } from 'ai-interface/constants/ability';
import { Terrain } from 'ai-interface/constants/terrain';

export enum OtherIcon {
  HitPoints = 'HitPoints',
  Brush1 = 'Brush1',
  Brush3 = 'Brush3',
  Brush5 = 'Brush5',
  Brush9 = 'Brush9',
  Price = 'Price',
  Strength = 'Strength',
  Move = 'Move',
  Time = 'Time',
  Level = 'Level'
}

const imageOffset: { [key in Terrain | Ability | OtherIcon]: number } = {
  [Terrain.Water]: 0,
  [Terrain.Open]: 1,
  [Terrain.Swamp]: 2,
  [Terrain.Forest]: 3,
  [Terrain.Hill]: 4,
  [Terrain.Mountain]: 5,
  [Terrain.Desert]: 6,
  [Terrain.Volcanic]: 7,
  [Terrain.Ice]: 8,

  NoPenaltyInSwamp: 2,
  NoPenaltyInForest: 3,
  NoPenaltyInHills: 4,
  WalksOverMountains: 5,
  NoPenaltyInDesert: 6,
  WalksOnVolcanic: 7,
  NoPenaltyOnIce: 8,
  Flies: 26,
  CanBeCarriedByFlier: 43,
  StrengthBonusInOpen: 29,
  StrengthBonusInSwamp: 30,
  StrengthBonusInForest: 31,
  StrengthBonusInHillsAndMountains: 32,
  StrengthBonusInDesert: 33,
  StrengthBonusOnIce: 34,
  StrengthBonusInCity: 35,
  StrengthBonusWhenAttacking: 14,
  StrengthBonusWhenDefending: 15,
  Ambush: 16,
  MoraleBonus: 17,
  FrighteningBonus: 18,
  AntiAirBonus: 36,
  CancelEnemyTerrainBonus: 20,
  CancelEnemyFortificationBonus: 21,
  CancelEnemyMoraleBonus: 23,
  CancelEnemyFrighteningBonus: 24,
  LowerEnemyAmbush: 22,
  LowerEnemyLeadership: 39,
  IncreaseGroupAmbush: 42,
  Leadership: 37,
  FortificationBonus: 40,
  ViewRadiusBonus: 25,
  GroupMoveBonus: 44,

  HitPoints: 9,
  Brush1: 10,
  Brush3: 11,
  Brush5: 12,
  Brush9: 13,
  Price: 19,
  Strength: 27,
  Move: 28,
  Time: 38,
  Level: 41
};

interface IconImageProps {
  icon: Terrain | Ability | OtherIcon;
  tooltip: string; // Text that will be displayed when mouse is over or when the icon is clicked on mobile.
  tooltipPlacement?: 'left' | 'right' | 'top' | 'bottom';
}

export const IconImage = ({ icon, tooltip, tooltipPlacement }: IconImageProps) => (
  <OverlayTrigger placement={tooltipPlacement} overlay={
    <Tooltip id="icon-image">{tooltip}</Tooltip>}
  >
    <div className="icon-image">
      <div
        style={{
          backgroundPositionX: -26 * (imageOffset[icon] % 10),
          backgroundPositionY: -26 * (Math.floor(imageOffset[icon] / 10))
        }}
      />
    </div>
  </OverlayTrigger>
);
