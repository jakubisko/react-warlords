import { ActionTypes } from './actionsTurnPassing';
import { AiOutputForGroup, GameState } from './types';
import { EMPTY_ARRAY, EMPTY_MAP, createGrid, withoutUndefinedProperties } from '../../functions';
import { AppAction } from '../root/configureStore';

export function reducer(state: GameState, action: AppAction): GameState {
  switch (action.type) {

    case ActionTypes.SET_GAME_STATE: {
      return JSON.parse(JSON.stringify(action.payload.gameState));
    }

    case ActionTypes.SET_PLAYER_TEAM: {
      const { playerId, team } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          team
        })
      };
    }

    case ActionTypes.SET_PLAYER_NAME: {
      const { playerId, name } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          name
        })
      };
    }

    case ActionTypes.SET_PLAYER_ONLINE_UID: {
      const { playerId, onlineUid } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : withoutUndefinedProperties({
          ...player,
          onlineUid
        }))
      };
    }

    case ActionTypes.SET_PLAYER_AI_NAME: {
      const { playerId, aiName } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : withoutUndefinedProperties({
          ...player,
          aiName
        }))
      };
    }

    case ActionTypes.SET_PLAYER_AI_STATE: {
      const { playerId, aiState } = action.payload;
      return {
        ...state,
        aiStates: state.aiStates.map((oldValue, id) => id !== playerId ? oldValue : aiState)
      };
    }

    case ActionTypes.SET_AI_ADVANTAGE: {
      const { aiAdvantage } = action.payload;
      return {
        ...state,
        aiAdvantage
      };
    }

    case ActionTypes.SET_AI_OUTPUT_VISIBLE: {
      const { aiOutputVisible } = action.payload;
      return {
        ...state,
        aiOutputVisible
      };
    }

    case ActionTypes.RESET_AI_OUTPUT: {
      return {
        ...state,
        aiOutput: EMPTY_MAP
      };
    }

    case ActionTypes.ADD_TO_AI_OUTPUT: {
      const { text, groupId, place } = action.payload;

      const { sticky, atPlace } = state.aiOutput[groupId] ?? { sticky: EMPTY_ARRAY, atPlace: EMPTY_ARRAY };
      const output: AiOutputForGroup = (place === undefined)
        ? {
          sticky: [...sticky, text],
          atPlace
        } : {
          sticky,
          atPlace: [...atPlace, { row: place.row, col: place.col, text }]
        };

      return {
        ...state,
        aiOutput: {
          ...state.aiOutput,
          [groupId]: output
        }
      };
    }

    case ActionTypes.SET_WIN_LOSS_CONDITION: {
      const { winCondition } = action.payload;
      return {
        ...state,
        winCondition
      };
    }

    case ActionTypes.SET_CURRENT_PLAYER: {
      const { playerId } = action.payload;
      return {
        ...state,
        currentPlayerId: playerId
      };
    }

    case ActionTypes.SET_TURN_NUMBER: {
      const { turnNumber } = action.payload;
      return {
        ...state,
        turnNumber
      };
    }

    case ActionTypes.RESET_FOG_OF_WAR: {
      return {
        ...state,
        fogOfWar: createGrid(state.size, () => true)
      };
    }

    case ActionTypes.SET_PLAYER_SAVED_MAP_FOCUS: {
      const { playerId, mapFocus } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          savedMapFocus: mapFocus
        })
      };
    }

    case ActionTypes.ADD_TIMELINE_ITEM: {
      return {
        ...state,
        timeline: [...state.timeline, action.payload.item]
      };
    }

    case ActionTypes.SET_HERO_OFFER: {
      const { heroOffer } = action.payload;
      return {
        ...state,
        heroOffer
      };
    }

    default:
      return state;
  }

}
