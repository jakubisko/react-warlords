
/** Types holding meta data about sprites, such as position of each sprite on the sheet. */

/** Size of one tile on sprite sheet in pixels. All sprite sheets use same value. */
export const sheetResolution = 64;

/** An image with size equal to a whole number of tiles. */
export interface TileSprite {
  readonly N: string; // Name of the sprite sheet without file extension.
  readonly L: number; // Position of the left of the sprite on the sprite sheet image in pixels.
  readonly T: number; // Position of the top of the sprite on the sprite sheet image in pixels.
  readonly W: number; // Width of the sprite in tiles.
  readonly H: number; // Height of the sprite in tiles.
}

/** An image with size not equal to a whole number of tiles. */
export interface FreeformSprite {
  readonly N: string; // Name of the sprite sheet without file extension.
  readonly L: number; // Position of the left of the sprite on the sprite sheet image in pixels.
  readonly T: number; // Position of the top of the sprite on the sprite sheet image in pixels.
  readonly W: number; // Width of the sprite in pixels.
  readonly H: number; // Height of the sprite in pixels.
  readonly X: number; // Offset of the control point of the sprite from the left of the sprite in pixels.
  readonly Y: number; // Offset of the control point of the sprite from the top of the sprite in pixels.
  readonly F?: { // Optional offset of the flag from the control point for flaggable structures.
    readonly x: number;
    readonly y: number;
  };
}

export type Sheet<T> = { [name: string]: T };
