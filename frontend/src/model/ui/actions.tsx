import { ScreenEnum, ProgramMode, UserError } from './types';
import { AppDispatch, RootState } from '../root/configureStore';
import { setPath, calculateAndDisplayShortestPathToPlaceForGroup, moveActiveGroupAlongPath } from '../path/actions';
import { Place, PlaceWithTime } from 'ai-interface/types/place';
import { getSavedMapFocusOfCurrentPlayer } from '../game/selectorsTurnPassing';
import { setGroupDestination } from '../game/actionsPlaying';
import { getGroupsDataCalculator } from '../game/selectorsPlaying';
import { getMoveLeftOfGroup } from '../path/selectors';
import { soundEffects } from '../../gameui/sounds/soundEffects';
import { SoundEffect } from '../sound/types';
import { setSoundSource } from '../sound/actions';
import { getNearestPlaceSafeForListening } from '../sound/selectors';
import { localStorageKeyAutosave } from '../root/constantsStorage';
import { EMPTY_ARRAY } from '../../functions';

export enum ActionTypes {
  // Screen routing
  SET_ERROR_MESSAGE = '[UI] SET_ERROR_MESSAGE',
  SET_PROGRAM_MODE = '[UI] SET_PROGRAM_MODE',
  SET_SCREEN = '[UI] SET_SCREEN',

  // Modals
  SET_SHOWING_MENU = '[UI] SET_SHOWING_MENU',
  SET_SHOWING_CITY = '[UI] SET_SHOWING_CITY',
  SET_SHOWING_PLACE_INFO = '[UI] SET_SHOWING_PLACE_INFO',
  SET_SHOWING_HERO = '[UI] SET_SHOWING_HERO',
  SET_SHOWING_WAR_REPORT = '[UI] SET_SHOWING_WAR_REPORT',
  SET_SHOWING_SPY_REPORT = '[UI] SET_SHOWING_SPY_REPORT',
  SET_SHOWING_RECRUITMENT_OFFER = '[UI] SET_SHOWING_RECRUITMENT_OFFER',
  SET_SHOWING_COMBAT = '[UI] SET_SHOWING_COMBAT',
  SET_SHOWING_EXPLORING = '[UI] SET_SHOWING_EXPLORING',
  SET_SHOWING_MANUAL = '[UI] SET_SHOWING_MANUAL',
  SET_SHOWING_ARMY_TYPE = '[UI] SET_SHOWING_ARMY_TYPE',
  SET_SHOWING_TOAST_MESSAGE = '[UI] SET_SHOWING_TOAST_MESSAGE',

  // Tactical map
  SET_TACTICAL_MAP_SIZE = '[UI] SET_TACTICAL_MAP_SIZE',
  SET_MAP_FOCUS = '[UI] SET_MAP_FOCUS',
  SET_ACTIVE_GROUP = '[UI] SET_ACTIVE_GROUP',
  SET_LAST_ACTIVE_GROUP = '[UI] SET_LAST_ACTIVE_GROUP',
}

export interface SetErrorMessage {
  type: ActionTypes.SET_ERROR_MESSAGE;
  payload: {
    errorTitle: string | null;
    errorText: string | null;
  }
}

export interface SetProgramMode {
  type: ActionTypes.SET_PROGRAM_MODE;
  payload: {
    programMode: ProgramMode;
  }
}

export interface SetScreen {
  type: ActionTypes.SET_SCREEN;
  payload: {
    screen: ScreenEnum;
  }
}

export interface SetShowingMenu {
  type: ActionTypes.SET_SHOWING_MENU;
  payload: {
    showingMenu: boolean;
  }
}

export interface SetShowingCity {
  type: ActionTypes.SET_SHOWING_CITY;
  payload: {
    showingCityId: string | null;
  }
}

export interface SetShowingPlaceInfo {
  type: ActionTypes.SET_SHOWING_PLACE_INFO;
  payload: {
    showingPlaceInfo: Place | null;
  }
}

export interface SetShowingHero {
  type: ActionTypes.SET_SHOWING_HERO;
  payload: {
    showingHeroId: string | null; // hero.id = army.id
  }
}

export interface SetShowingWarReport {
  type: ActionTypes.SET_SHOWING_WAR_REPORT;
  payload: {
    showingWarReport: boolean;
  }
}

export interface SetShowingSpyReport {
  type: ActionTypes.SET_SHOWING_SPY_REPORT;
  payload: {
    showingSpyReport: boolean;
  }
}

export interface SetShowingHeroOffer {
  type: ActionTypes.SET_SHOWING_RECRUITMENT_OFFER;
  payload: {
    showingHeroOffer: boolean;
  }
}

export interface SetShowingCombatModal {
  type: ActionTypes.SET_SHOWING_COMBAT;
  payload: {
    showingCombat: boolean;
  }
}

export interface SetShowingExploring {
  type: ActionTypes.SET_SHOWING_EXPLORING;
  payload: {
    showingExploring: boolean;
  }
}

export interface SetShowingManual {
  type: ActionTypes.SET_SHOWING_MANUAL;
  payload: {
    showingManual: boolean;
  }
}

export interface SetShowingArmyType {
  type: ActionTypes.SET_SHOWING_ARMY_TYPE;
  payload: {
    showingArmyTypeId: string | null;
  }
}

export interface SetShowingToastMessage {
  type: ActionTypes.SET_SHOWING_TOAST_MESSAGE;
  payload: {
    showingToastMessage: string | null;
  }
}

export interface SetTacticalMapSize {
  type: ActionTypes.SET_TACTICAL_MAP_SIZE;
  payload: {
    widthInTiles: number;
    heightInTiles: number;
    pixelsPerTile: number;
  }
}

export interface SetMapFocus {
  type: ActionTypes.SET_MAP_FOCUS;
  payload: {
    focusedRow: number;
    focusedCol: number;
  }
}

export interface SetActiveGroup {
  type: ActionTypes.SET_ACTIVE_GROUP;
  payload: {
    groupId: string | null;
  }
}

export interface SetLastActiveGroup {
  type: ActionTypes.SET_LAST_ACTIVE_GROUP;
  payload: {
    groupId: string;
  }
}

// Thunk action creators //////////////////////////////////////////////////////////////////////////

/**
* If given place is a city owned by current player that is not razed, calls showCityModal(). Otherwise, calls setShowingPlaceInfo().
* If null is given, closes both modals.
*/
export const showCityModalOrPlaceInfoModal = (place: Place | null) => (dispatch: AppDispatch, getState: () => RootState) => {
  if (place === null) {
    dispatch(setShowingCity(null));
    dispatch(setShowingPlaceInfo(null));
    return;
  }

  const { ui, game } = getState();
  const { programMode } = ui;
  const { tiles, cities, currentPlayerId } = game;
  const { row, col } = place;
  const city = cities[tiles[row][col].cityId!];

  if (programMode === ProgramMode.Playing && city !== undefined && city.owner === currentPlayerId && !city.razed) {
    dispatch(setShowingCity(city.id));
    dispatch(setSoundSource(city.row, city.col));
  } else {
    dispatch(setShowingPlaceInfo(place));
  }
};

/** Set the active group, then recalculates its path. Optionally also moves the map focus to that group. */
export const activateGroupAndCalculatePath = (groupId: string | null, alsoSetMapFocus = false) =>
  (dispatch: AppDispatch, getState: () => RootState) => {

    dispatch(_setActiveGroup(groupId));
    if (groupId === null) {
      dispatch(setPath(EMPTY_ARRAY as PlaceWithTime[]));
    } else {
      const { row, col, destRow, destCol } = getState().game.groups[groupId];

      dispatch(setSoundSource(row, col));
      if (alsoSetMapFocus) {
        dispatch(setMapFocus(row + 0.5, col + 0.5));
      }

      if (destRow === undefined || destCol === undefined) {
        dispatch(setPath([{
          row,
          col,
          day: 0,
          moveLeft: getMoveLeftOfGroup(getState(), groupId)
        }]));
      } else {
        dispatch(calculateAndDisplayShortestPathToPlaceForGroup(groupId, destRow, destCol));
      }
    }
  };

/**
 * Cleans UI state before user looks onto map, so that he doesn't see what group was selected and what part of map was focused by
 * previous player. Displays land. Displays report modal if something happened between last and current turn.
 */
export const cleanUiAndShowLand = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game: { currentPlayerId, players, turnNumber }, online: { onlineGameId } } = getState();

  const mapFocus = getSavedMapFocusOfCurrentPlayer(getState());
  dispatch(setMapFocus(mapFocus.row, mapFocus.col));
  const soundSource = getNearestPlaceSafeForListening(getState(), mapFocus);
  dispatch(setSoundSource(soundSource.row, soundSource.col));

  dispatch(activateGroupAndCalculatePath(null));
  dispatch(setLastActiveGroup(''));
  dispatch(setShowingToastMessage(null));
  dispatch(setShowingMenu(false));
  dispatch(setScreen(ScreenEnum.LandScreen));
  dispatch(setShowingManual(onlineGameId == null && window.localStorage.getItem(localStorageKeyAutosave) === null));

  if (players[currentPlayerId].aiName === undefined) {
    dispatch(showReports('WAR', 'OFFER'));
  }
  soundEffects.play(turnNumber % 7 === 1 ? SoundEffect.NewWeek : SoundEffect.NewDay);
};

export const showReports = (...reports: ('WAR' | 'SPY' | 'OFFER')[]) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { currentPlayerId, players, heroOffer } = getState().game;

  if (reports.includes('WAR') && players[currentPlayerId].warReports.length > 0) {
    dispatch(setShowingWarReport(true));
  }

  if (reports.includes('OFFER') && heroOffer !== null) {
    dispatch(setShowingHeroOffer(true));
  }

  if (reports.includes('SPY')) {
    dispatch(setShowingSpyReport(true));
  }
};

/**
 * Left clicking on TacticalMap during play:
 * a) selects current player's group
 * b) calculates path for active group
 * c) moves active group
 * d) opens CityModal
 * f) opens PlaceInfoModal - fallback if nothing else
 */
export const tacticalMapLeftClickPlaying = (clickRow: number, clickCol: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const state = getState();
    const { game: { groups, tiles, currentPlayerId }, ui: { activeGroupId } } = state;

    if (activeGroupId === null) {
      const { armies } = tiles[clickRow][clickCol];
      const place: Place = { row: clickRow, col: clickCol };

      // If a tile with armies of current player was clicked, activate the Group containing the highest order army - the one displayed.
      if (armies && groups[armies[0].groupId].owner === currentPlayerId) {
        const groupsDataCalculator = getGroupsDataCalculator(state);
        const groupsData = groupsDataCalculator(place);
        dispatch(activateGroupAndCalculatePath(groupsData!.groupIdWithHighestArmyType));

      } else {
        // If city of current player was clicked, display CityModal. Otherwise, show info about clicked Place.
        dispatch(showCityModalOrPlaceInfoModal(place));
      }
    } else {
      const { destRow, destCol } = groups[activeGroupId];

      // If group has a destination set and that destination was clicked again, start moving.
      if (clickRow === destRow && clickCol === destCol) {
        dispatch(moveActiveGroupAlongPath());
      } else {

        dispatch(calculateAndDisplayShortestPathToPlaceForGroup(activeGroupId, clickRow, clickCol));

        // If we found the path for the group to reach the destination, save it. Otherwise, drop it so that Move All button disables.
        if (getState().path.path.length < 2) {
          dispatch(setGroupDestination(activeGroupId, undefined, undefined));
        } else {
          dispatch(setGroupDestination(activeGroupId, clickRow, clickCol));
        }
      }
    }
  };

/** Right clicking on TacticalMap deselects active group if one is selected, and opens PlaceInfoModal if none is selected. */
export const tacticalMapRightClick = (clickRow: number, clickCol: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { activeGroupId } = getState().ui;

  if (activeGroupId === null) {
    const place: Place = { row: clickRow, col: clickCol };
    dispatch(showCityModalOrPlaceInfoModal(place));
  } else {
    dispatch(activateGroupAndCalculatePath(null));
  }
};

// Basic action creators //////////////////////////////////////////////////////////////////////////

/**
 * Displays a dialog with an error message.
 * Also writes the error message to the console as a warning.
 * This can be called from any state and the dialog will be displayed on any screen.
 */
export function setErrorMessage(errorTitle: string | null, error: unknown): SetErrorMessage {
  if (errorTitle !== null || error !== null) {
    if (errorTitle !== null) {
      console.error(errorTitle);
    }
    if (error instanceof Error && !(error instanceof UserError)) {
      console.error(error);
    }
    soundEffects.play(SoundEffect.Modal);
  }

  return {
    type: ActionTypes.SET_ERROR_MESSAGE,
    payload: {
      errorTitle,
      errorText: (error instanceof UserError) ? error.message : null
    }
  };
}

/** When setting mode to Map Editor, you should also set current player to neutral player. */
export const setProgramMode = (programMode: ProgramMode): SetProgramMode => ({
  type: ActionTypes.SET_PROGRAM_MODE,
  payload: {
    programMode
  }
});

export const setScreen = (screen: ScreenEnum): SetScreen => ({
  type: ActionTypes.SET_SCREEN,
  payload: {
    screen
  }
});

export const setShowingMenu = (showingMenu: boolean): SetShowingMenu => ({
  type: ActionTypes.SET_SHOWING_MENU,
  payload: {
    showingMenu
  }
});

export const setShowingCity = (showingCityId: string | null): SetShowingCity => ({
  type: ActionTypes.SET_SHOWING_CITY,
  payload: {
    showingCityId
  }
});

export const setShowingPlaceInfo = (showingPlaceInfo: Place | null): SetShowingPlaceInfo => ({
  type: ActionTypes.SET_SHOWING_PLACE_INFO,
  payload: {
    showingPlaceInfo
  }
});

export const setShowingHero = (showingHeroId: string | null): SetShowingHero => ({
  type: ActionTypes.SET_SHOWING_HERO,
  payload: {
    showingHeroId
  }
});

export const setShowingWarReport = (showingWarReport: boolean): SetShowingWarReport => ({
  type: ActionTypes.SET_SHOWING_WAR_REPORT,
  payload: {
    showingWarReport
  }
});

export const setShowingSpyReport = (showingSpyReport: boolean): SetShowingSpyReport => ({
  type: ActionTypes.SET_SHOWING_SPY_REPORT,
  payload: {
    showingSpyReport
  }
});

export const setShowingHeroOffer = (showingHeroOffer: boolean): SetShowingHeroOffer => ({
  type: ActionTypes.SET_SHOWING_RECRUITMENT_OFFER,
  payload: {
    showingHeroOffer
  }
});

/** Display the modal with animation. The animation is asynchronous; this function will return before the animation ends. */
export const setShowingCombat = (showingCombat: boolean): SetShowingCombatModal => ({
  type: ActionTypes.SET_SHOWING_COMBAT,
  payload: {
    showingCombat
  }
});

/** Display the modal with animation. The animation is asynchronous; this function will return before the animation ends. */
export const setShowingExploring = (showingExploring: boolean): SetShowingExploring => ({
  type: ActionTypes.SET_SHOWING_EXPLORING,
  payload: {
    showingExploring
  }
});

export const setShowingManual = (showingManual: boolean): SetShowingManual => ({
  type: ActionTypes.SET_SHOWING_MANUAL,
  payload: {
    showingManual
  }
});

export const setShowingArmyType = (showingArmyTypeId: string | null): SetShowingArmyType => ({
  type: ActionTypes.SET_SHOWING_ARMY_TYPE,
  payload: {
    showingArmyTypeId
  }
});

export const setShowingToastMessage = (showingToastMessage: string | null): SetShowingToastMessage => ({
  type: ActionTypes.SET_SHOWING_TOAST_MESSAGE,
  payload: {
    showingToastMessage
  }
});

export const setTacticalMapSize = (widthInTiles: number, heightInTiles: number, pixelsPerTile: number): SetTacticalMapSize => ({
  type: ActionTypes.SET_TACTICAL_MAP_SIZE,
  payload: {
    widthInTiles,
    heightInTiles,
    pixelsPerTile
  }
});

export const setMapFocus = (focusedRow: number, focusedCol: number): SetMapFocus => ({
  type: ActionTypes.SET_MAP_FOCUS,
  payload: {
    focusedRow,
    focusedCol
  }
});

/** Low-level action. Use activateGroupAndCalculatePath(). */
const _setActiveGroup = (groupId: string | null): SetActiveGroup => ({
  type: ActionTypes.SET_ACTIVE_GROUP,
  payload: {
    groupId
  }
});

export const setLastActiveGroup = (groupId: string): SetLastActiveGroup => ({
  type: ActionTypes.SET_LAST_ACTIVE_GROUP,
  payload: {
    groupId
  }
});

export type UiAction = SetErrorMessage
  | SetProgramMode
  | SetScreen
  | SetShowingMenu
  | SetShowingCity
  | SetShowingPlaceInfo
  | SetShowingHero
  | SetShowingWarReport
  | SetShowingSpyReport
  | SetShowingHeroOffer
  | SetShowingCombatModal
  | SetShowingExploring
  | SetShowingManual
  | SetShowingArmyType
  | SetShowingToastMessage
  | SetTacticalMapSize
  | SetMapFocus
  | SetActiveGroup
  | SetLastActiveGroup;
