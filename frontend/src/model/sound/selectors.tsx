import { createSelector } from '@reduxjs/toolkit';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { Place } from 'ai-interface/types/place';
import { RootState } from '../root/configureStore';
import { ProgramMode, ScreenEnum } from '../ui/types';
import { OtherMusic, StructureSound } from './types';

export const getBackgroundMusic = createSelector(
  (state: RootState) => state.game,
  // We use multiple selectors instead of 1 so that this doesn't recalculate whenever a map is dragged 1 pixel.
  (state: RootState) => state.ui.programMode,
  (state: RootState) => state.ui.screen,
  (state: RootState) => state.sound.soundRow,
  (state: RootState) => state.sound.soundCol,
  ({ size, terrain }, programMode, screen, row, col) => {

    if (programMode === ProgramMode.MapEditor || (screen !== ScreenEnum.LandScreen && screen !== ScreenEnum.ZoomedMapScreen)) {
      return null;
    }
    const result = terrain[row][col];

    if (result === Terrain.Water &&
      ((row > 0 && col > 0 && terrain[row - 1][col - 1] !== Terrain.Water) ||
        (row > 0 && terrain[row - 1][col] !== Terrain.Water) ||
        (col > 0 && terrain[row][col - 1] !== Terrain.Water)
      ) && (
        (row > 0 && col < size - 1 && terrain[row - 1][col + 1] !== Terrain.Water) ||
        (row > 0 && terrain[row - 1][col] !== Terrain.Water) ||
        (col < size - 1 && terrain[row][col + 1] !== Terrain.Water)
      ) && (
        (row < size - 1 && col > 0 && terrain[row + 1][col - 1] !== Terrain.Water) ||
        (row < size - 1 && terrain[row + 1][col] !== Terrain.Water) ||
        (col > 0 && terrain[row][col - 1] !== Terrain.Water)
      ) && (
        (row < size - 1 && col < size - 1 && terrain[row + 1][col + 1] !== Terrain.Water) ||
        (row < size - 1 && terrain[row + 1][col] !== Terrain.Water) ||
        (col < size - 1 && terrain[row][col + 1] !== Terrain.Water)
      )
    ) {
      return OtherMusic.River;
    }

    return result;
  }
);

// Maximum distance from which the user can hear a sound of a nearby structure.
const radius = 3;

/** Sorted from the farthest to the nearest. That way if there are two structures with the same sound, the nearer (louder) will be used. */
const offsetsNear: { row: number, col: number, volume: number }[] = [];
for (let row = -radius; row <= radius; row++) {
  for (let col = -radius; col <= radius; col++) {
    const dist = (row ** 2 + col ** 2) ** 0.5;
    if (dist <= radius) {
      const volume = 0.1 - (1 - 0.1) * (dist - radius) / radius;
      offsetsNear.push({ row, col, volume });
    }
  }
}
offsetsNear.sort((a, b) => a.volume - b.volume);

/** Returns a map that determines the volume of a sound playing for each structure near given location. */
export const getVolumeOfStructures = createSelector(
  (state: RootState) => state.game,
  // We use multiple selectors instead of 1 so that this doesn't recalculate whenever a map is dragged 1 pixel.
  (state: RootState) => state.ui.programMode,
  (state: RootState) => state.ui.screen,
  (state: RootState) => state.ui.showingCombat,
  (state: RootState) => state.ui.showingCityId,
  (state: RootState) => state.sound.soundRow,
  (state: RootState) => state.sound.soundCol,
  ({ size, terrain, tiles, structures, cities, ruins }, programMode, screen, showingCombat, showingCityId, row0, col0) => {

    const result: { [name: string]: number } = {};
    if (programMode === ProgramMode.MapEditor || (screen !== ScreenEnum.LandScreen && screen !== ScreenEnum.ZoomedMapScreen)) {
      return result;
    }

    for (const offset of offsetsNear) {
      const row = row0 + offset.row;
      const col = col0 + offset.col;
      if (row < 0 || row >= size || col < 0 || col >= size) {
        continue;
      }

      let name = '';
      let volume = offset.volume * (showingCombat ? 0.5 : 1);

      switch (structures[row][col]) {
        case Structure.Village:
          switch (terrain[row][col]) {
            case Terrain.Water: name = StructureSound.OilPlatform; break;
            case Terrain.Open: name = StructureSound.Village; break;
            case Terrain.Swamp: name = StructureSound.Extractor; break;
            case Terrain.Forest: name = StructureSound.Sawmill; break;
            case Terrain.Hill: name = StructureSound.IronAndCoalMine; break;
            case Terrain.Desert: name = StructureSound.GoldMine; break;
            case Terrain.Ice: name = StructureSound.IronAndCoalMine; break;
          }
          break;

        case Structure.Ruin:
          if (terrain[row][col] === Terrain.Water) {
            name = StructureSound.RuinOnWater;
          } else {
            const { ruinId } = tiles[row][col];
            switch (ruins[ruinId!].level) {
              case 1: name = StructureSound.Cathedral; break;
              case 2: name = StructureSound.Ziggurat; break;
              case 3: name = StructureSound.MausoleumAndTomb; break;
              case 4: name = StructureSound.MausoleumAndTomb; break;
            }
          }
          break;

        case Structure.City: {
          const { cityId } = tiles[row][col];
          const city = cities[cityId!];
          if (city.razed) {
            name = StructureSound.RazedCity;
          } else {
            const sounds = [StructureSound.City1, StructureSound.City2, StructureSound.City3, StructureSound.City4];
            name = sounds[(city.row + city.col) % 4];
            volume *= showingCityId === null ? 0.5 : 1;
          }
          break;
        }

        case Structure.Encampment: name = StructureSound.Encampment; break;
        case Structure.SpyDen: name = StructureSound.SpyDen; break;
        case Structure.SageTower: name = StructureSound.SageTower; break;
        case Structure.Grove: name = StructureSound.Grove; break;
        case Structure.Watchtower: name = StructureSound.Watchtower; break;
        case Structure.Port: name = StructureSound.Port; break;
        case Structure.Temple: name = StructureSound.Temple; break;
      }

      if (name !== '') {
        result[name] = volume;
      }
    }

    return result;
  }
);

/**
 * Given a place, this searches for the nearest place that is guaranteed to be outside of the fog of war.
 * Use this so you can ensure that the user is not trying to listen to sounds in the fog of war.
 */
export const getNearestPlaceSafeForListening = createSelector(
  (state: RootState) => state.game,
  (rootState: RootState, place: Place) => place,
  ({ cities, groups, players, currentPlayerId }, place) => {
    const { row: row1, col: col1 } = place;
    const isAllied = players.map(player => player.alive && player.team === players[currentPlayerId].team);

    const potentialPlaces: Place[] = [
      ...Object.values(cities).filter(city => isAllied[city.owner] && !city.razed),
      ...Object.values(groups).filter(group => isAllied[group.owner])
    ];

    let nearestDist = Infinity;
    let nearestPlace: Place = { row: 0, col: 0 };
    for (const place2 of potentialPlaces) {
      const { row: row2, col: col2 } = place2;
      const dist = (row1 - row2) ** 2 + (col1 - col2) ** 2;
      if (dist < nearestDist) {
        nearestDist = dist;
        nearestPlace = place2;
      }
    }

    return nearestPlace;
  }
);
