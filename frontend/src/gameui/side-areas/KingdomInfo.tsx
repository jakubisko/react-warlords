import './KingdomInfo.scss';
import { memo } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import { getNumberOfCities, getIncome, getNumberOfFlaggables, getUpkeepOfArmiesCreatedTomorrow, getUpkeepOfExistingArmies } from '../../model/game/selectorsPlaying';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { ProgramMode } from '../../model/ui/types';
import { getNumberOfRuinsByLevel } from '../../model/game/selectorsEditor';
import { faCoins, faLandmarkFlag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconCity } from '../common/svg/IconCity';
import { IconIncome } from '../common/svg/IconIncome';
import { IconRuin } from '../common/svg/IconRuin';
import { IconExpenses } from '../common/svg/IconExpenses';

function KingdomInfo() {
  const programMode = useSelector((state: RootState) => state.ui.programMode);
  const noOfCities = useSelector((state: RootState) => getNumberOfCities(state, state.game.currentPlayerId));
  const noOfFlaggables = useSelector((state: RootState) => getNumberOfFlaggables(state, state.game.currentPlayerId));
  const gold = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].gold);
  const income = useSelector((state: RootState) => getIncome(state, state.game.currentPlayerId));
  const upkeepOfExistingArmies = useSelector((state: RootState) => getUpkeepOfExistingArmies(state, state.game.currentPlayerId));
  const upkeepOfArmiesCreatedTomorrow = useSelector((state: RootState) => getUpkeepOfArmiesCreatedTomorrow(state, state.game.currentPlayerId));
  const numberOfRuinsByLevel = useSelector((state: RootState) => getNumberOfRuinsByLevel(state));

  return (
    <div className="kingdom-info text-center">

      <div className="item">
        <OverlayTrigger placement="right" overlay={
          <Tooltip id="ki-cities">Cities</Tooltip>}
        >
          <div>
            <div className="icon"><IconCity /></div>
            <div>{noOfCities}</div>
          </div>
        </OverlayTrigger>
      </div>

      <div className="item">
        <OverlayTrigger placement="right" overlay={
          <Tooltip id="ki-cities">Other flaggable structures</Tooltip>}
        >
          <div>
            <div className="icon"><FontAwesomeIcon icon={faLandmarkFlag} /></div>
            <div>{noOfFlaggables}</div>
          </div>
        </OverlayTrigger>
      </div>

      {programMode === ProgramMode.Playing
        ?
        <>
          <div className="item">
            <OverlayTrigger placement="right" overlay={
              <Tooltip id="ki-gold">Gold</Tooltip>}
            >
              <div>
                <div className="icon"> <FontAwesomeIcon icon={faCoins} /></div>
                <div>{gold}</div>
              </div>
            </OverlayTrigger>
          </div>

          <div className="item">
            <OverlayTrigger placement="right" overlay={
              <Tooltip id="ki-income">Daily income</Tooltip>}
            >
              <div>
                <div className="icon"><IconIncome /></div>
                <div>+{income}</div>
              </div>
            </OverlayTrigger>
          </div>

          <div className="item">
            <OverlayTrigger placement="right"
              overlay={<Tooltip id="ki-expenses">Daily upkeep of current armies / Additional daily upkeep of armies created next turn</Tooltip>}
            >
              <div>
                <div className="icon"><IconExpenses /></div>
                <div>-{upkeepOfExistingArmies}/-{upkeepOfArmiesCreatedTomorrow}</div>
              </div>
            </OverlayTrigger>
          </div>
        </>
        :
        <div className="item">
          <OverlayTrigger placement="right"
            overlay={<Tooltip id="ki-expenses">Number of ruins of level<br />1, 2, 3 and 4</Tooltip>}
          >
            <div>
              <div className="icon"><IconRuin /></div>
              <div>{numberOfRuinsByLevel.slice(1).join(', ')}</div>
            </div>
          </OverlayTrigger>
        </div>
      }
    </div>
  );
}

// When you open some modal, GameScreen redraws all its children. "memo" prevents redraw of this component if it hasn't changed.
export default memo(KingdomInfo);
