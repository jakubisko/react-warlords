import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import { ScreenEnum, ProgramMode } from '../../model/ui/types';
import { setScreen, setProgramMode, setShowingMenu } from '../../model/ui/actions';
import { createEmptyMap } from '../../model/game/actionsEditor';
import { setCurrentPlayer } from '../../model/game/actionsTurnPassing';
import { neutralPlayerId } from 'ai-interface/constants/player';
import FullscreenButton from '../common/FullscreenButton';
import LoadButton from '../common/LoadButton';
import LoadFromAutosaveButton from '../common/LoadFromAutosaveButton';
import { buildVersion, decompressObject } from '../../model/game/serialization';
import { loadGame } from '../../model/game/actionsTurnPassingThunks';
import VolumeButtons from '../common/VolumeButtons';
import { sessionStorageKeyOnlineLogin } from '../../model/root/constantsStorage';
import { AppDispatch } from '../../model/root/configureStore';
import { EMPTY_ARRAY } from '../../functions';
import { setOnlineGameId } from '../../model/online/actions';

export default function MainMenuScreen() {
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    if (sessionStorage.getItem(sessionStorageKeyOnlineLogin)) {
      sessionStorage.removeItem(sessionStorageKeyOnlineLogin);
      handleOnlinePlay();
    }
  }, EMPTY_ARRAY);

  function handleNewGame() {
    dispatch(setOnlineGameId(null));
    dispatch(createEmptyMap(0));
    dispatch(setScreen(ScreenEnum.NewGameMapScreen));
  }

  async function handleLoadGame(fileData: string | ArrayBuffer) {
    dispatch(setOnlineGameId(null));
    const saveGameData = await decompressObject(fileData, 'game');
    dispatch(loadGame(saveGameData));
    dispatch(setShowingMenu(false));
  }

  function handleOnlinePlay() {
    dispatch(setOnlineGameId(''));
    dispatch(setScreen(ScreenEnum.OnlineLoginScreen));
  }

  function handleMapEditor() {
    dispatch(createEmptyMap(40));
    dispatch(setCurrentPlayer(neutralPlayerId));
    dispatch(setScreen(ScreenEnum.LandScreen));
    dispatch(setProgramMode(ProgramMode.MapEditor));
    dispatch(setShowingMenu(true));
  }

  return (
    <Modal className="screen background-main-menu overflow-hidden" show centered backdrop={false} animation={false}>
      <Modal.Header>
        <Modal.Title>Warlords</Modal.Title>
        <VolumeButtons />
        <FullscreenButton />
      </Modal.Header>

      <Modal.Body className="text-center">
        <ButtonGroup vertical>
          <Button onClick={handleNewGame}>New game</Button>
          <LoadButton
            caption="Load game from file"
            fileExtension="wsav"
            onUpload={handleLoadGame}
          />
          <LoadFromAutosaveButton
            onUpload={handleLoadGame}
          />
          <Button onClick={handleOnlinePlay}>Online play</Button>
          <Button onClick={handleMapEditor}>Map editor</Button>
        </ButtonGroup>
      </Modal.Body>

      <Modal.Footer>
        <span className="fs-6">
          Created by Jakub {buildVersion}
        </span>
      </Modal.Footer>
    </Modal>
  );
}
