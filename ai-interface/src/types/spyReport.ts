/** Spy info about other players. What data is available depends on the number of cities flagged by current player. */
export interface SpyReport {

  /** To get the percent of map explored by player 3, red mapExplored[3]. */
  mapExplored: (number | undefined)[];

  /** To get the number of heroes player 3 has, read heroes[3]. */
  heroes: (number | undefined)[];

  /** To get the level of the best hero player 3 has, read bestHeroLevel[3]. This will be undefined when he has no heroes. */
  bestHeroLevel: (number | undefined)[];

  /** To get the number of artifacts player 3 has, read heroes[3]. */
  artifacts: (number | undefined)[];

  /** To get the armyTypeId of the strongest army player 3 has, read bestArmyTypeId[3]. This will be undefined when he has no armies. */
  bestArmyTypeId: (string | undefined)[];

  /** To get the number of cities player 3 has, read cities[3]. */
  cities: (number | undefined)[];

  /** To get the current gold player 3 has, read gold[3]. */
  gold: (number | undefined)[];

  /** To get the daily income of player 3, read income[3]. */
  income: (number | undefined)[];

  /** To get the total daily upkeep for armies of player 3, read armiesUpkeep[3]. */
  armiesUpkeep: (number | undefined)[];
}
