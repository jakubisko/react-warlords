import { MapData, GameState } from '../game/types';
import { EditorTool, RandomMapSettings, StructureAtPlace } from './types';
import { AppDispatch, RootState } from '../root/configureStore';
import { setBackgroundSprites, eraseTile, drawWithTerrainBrushEraseIllegalTiles, createAndPlaceStructure } from '../game/actionsEditor';
import { calculateBackgroundSprites } from './tiling';
import { canPlaceStructure, getArmyTypePlaceableAtPlace, canEditorPlaceArmyAtPlace, canEditorPlaceArtifactAtPlace } from './selectors';
import { createArmyAtOrAroundPlace } from '../game/actionsPlayingThunks';
import { Place } from 'ai-interface/types/place';
import { setShowingPlaceInfo } from '../ui/actions';
import { allArtifacts } from '../game/constants';
import { createArtifactOnGround } from '../game/actionsHero';
import { setGameState } from '../game/actionsTurnPassing';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { getOwnerOfPlace } from '../game/selectorsPlaying';
import { extractMapDataFromGameState } from '../game/saveLoad';

/** How many actions in editor can be undone. Remember that this is an array of MapState, so it should be low. */
const maxUndoQueueLength = 20;

export enum ActionTypes {
  SET_EDITOR_TOOL = '[Editor] SET_EDITOR_TOOL',
  SET_TERRAIN_BRUSH = '[Editor] SET_TERRAIN_BRUSH',
  SET_TERRAIN_BRUSH_SIZE = '[Editor] SET_TERRAIN_BRUSH_SIZE',
  SET_STRUCTURE_TYPE = '[Editor] SET_STRUCTURE_TYPE',
  SET_UNDO_QUEUE = '[Editor] UPDATE_UNDO_QUEUE',
  SET_LAST_RANDOM_MAP_SETTINGS = '[Editor] SET_LAST_RANDOM_MAP_SETTINGS'
}

export interface SetEditorTool {
  type: ActionTypes.SET_EDITOR_TOOL;
  payload: {
    tool: EditorTool;
  }
}

export interface SetTerrainBrush {
  type: ActionTypes.SET_TERRAIN_BRUSH;
  payload: {
    terrainBrush: Terrain;
  }
}

export interface SetTerrainBrushSize {
  type: ActionTypes.SET_TERRAIN_BRUSH_SIZE;
  payload: {
    terrainBrushSize: number;
  }
}

export interface SetStructure {
  type: ActionTypes.SET_STRUCTURE_TYPE;
  payload: {
    structure: Structure;
  }
}

export interface SetUndoQueue {
  type: ActionTypes.SET_UNDO_QUEUE;
  payload: {
    undoQueue: MapData[];
    undoQueuePosition: number;
  }
}

export interface SetLastRandomMapSettings {
  type: ActionTypes.SET_LAST_RANDOM_MAP_SETTINGS;
  payload: {
    randomMapSettings: RandomMapSettings;
  }
}


// Action creators ////////////////////////////////////////////////////////////////////////////////
export const tacticalMapLeftClickMapEditor = (row: number, col: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const place: Place = { row, col };
  const { terrain: oldTerrain, structures: oldStructures } = getState().game;

  switch (getState().editor.tool) {

    case EditorTool.PlaceInfo:
      dispatch(setShowingPlaceInfo(place));
      return;

    case EditorTool.TerrainBrush: {
      const { terrainBrush, terrainBrushSize } = getState().editor;
      dispatch(drawWithTerrainBrushEraseIllegalTiles(row, col, terrainBrush, terrainBrushSize));
      break;
    }

    case EditorTool.StructurePlacing: {
      const { structure } = getState().editor;
      const structureAtPlace: StructureAtPlace = { row, col, structure };
      if (!canPlaceStructure(getState(), structureAtPlace)) {
        return;
      }
      dispatch(createAndPlaceStructure(row, col, structure));
      if (structure === Structure.City || structure === Structure.Ruin || structure === Structure.Signpost) {
        dispatch(setShowingPlaceInfo(place));
      }

      break;
    }

    case EditorTool.ArmyPlacing: {
      const state = getState();
      if (canEditorPlaceArmyAtPlace(state, place)) {
        const armyType = getArmyTypePlaceableAtPlace(state, place);
        const playerId = getOwnerOfPlace(state, place);
        dispatch(createArmyAtOrAroundPlace(row, col, armyType.id, playerId === null ? neutralPlayerId : playerId));
        dispatch(setShowingPlaceInfo(place));
      } else {
        dispatch(setShowingPlaceInfo(place));
        return;
      }

      break;
    }

    case EditorTool.ArtifactPlacing: {
      const state = getState();
      if (canEditorPlaceArtifactAtPlace(state, place)) {
        dispatch(createArtifactOnGround(row, col, allArtifacts.randomCommonArtifact.id));
        dispatch(setShowingPlaceInfo(place));
      } else {
        dispatch(setShowingPlaceInfo(place));
        return;
      }

      break;
    }

    case EditorTool.Erase: {
      dispatch(eraseTile(row, col));
      break;
    }
  }


  const { terrain, structures } = getState().game;
  if (terrain !== oldTerrain || structures !== oldStructures) {
    const backgroundSprites = calculateBackgroundSprites(terrain, structures);
    dispatch(setBackgroundSprites(backgroundSprites));
  }

  dispatch(pushToUndoQueue());
};

export const undo = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { editor: { undoQueue, undoQueuePosition }, game } = getState();

  if (undoQueuePosition > 0) {
    const mapData = undoQueue[undoQueuePosition - 1];
    const { terrain, structures } = mapData;
    const backgroundSprites = calculateBackgroundSprites(terrain, structures);
    const gameState: GameState = {
      ...game,
      ...mapData,
      backgroundSprites
    };

    dispatch(setGameState(gameState));
    dispatch(setUndoQueue(undoQueue, undoQueuePosition - 1));
  }
};

export const redo = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { editor: { undoQueue, undoQueuePosition }, game } = getState();

  if (undoQueuePosition < undoQueue.length - 1) {
    const mapData = undoQueue[undoQueuePosition + 1];
    const { terrain, structures } = mapData;
    const backgroundSprites = calculateBackgroundSprites(terrain, structures);
    const gameState: GameState = {
      ...game,
      ...mapData,
      backgroundSprites
    };

    dispatch(setGameState(gameState));
    dispatch(setUndoQueue(undoQueue, undoQueuePosition + 1));
  }
};

/**
 * Call this after any action in editor that changes MapData. If multiple actions happen at once, call this only after the last one, and
 * they will be threated as one for the purposes of undo/redo.
 * Failure to call this will result in the action to be threated as part of the next action, and undo/redo will apply to them as one.
 */
export const pushToUndoQueue = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game, editor } = getState();
  const { undoQueue, undoQueuePosition } = editor;

  const newUndoQueue = [
    ...undoQueue.slice(undoQueuePosition === maxUndoQueueLength - 1 ? 1 : 0, undoQueuePosition + 1),
    extractMapDataFromGameState(game)
  ];
  dispatch(setUndoQueue(newUndoQueue, Math.min(undoQueuePosition + 1, maxUndoQueueLength - 1)));
};


export const setEditorTool = (tool: EditorTool): SetEditorTool => ({
  type: ActionTypes.SET_EDITOR_TOOL,
  payload: {
    tool
  }
});

export const setTerrainBrush = (terrainBrush: Terrain): SetTerrainBrush => ({
  type: ActionTypes.SET_TERRAIN_BRUSH,
  payload: {
    terrainBrush
  }
});

/** Use only odd numbers for brush size. */
export const setTerrainBrushSize = (terrainBrushSize: number): SetTerrainBrushSize => ({
  type: ActionTypes.SET_TERRAIN_BRUSH_SIZE,
  payload: {
    terrainBrushSize
  }
});

export const setStructure = (structure: Structure): SetStructure => ({
  type: ActionTypes.SET_STRUCTURE_TYPE,
  payload: {
    structure
  }
});

export const setUndoQueue = (undoQueue: MapData[], undoQueuePosition: number): SetUndoQueue => ({
  type: ActionTypes.SET_UNDO_QUEUE,
  payload: {
    undoQueue: [...undoQueue],
    undoQueuePosition
  }
});

export const setLastRandomMapSettings = (randomMapSettings: RandomMapSettings): SetLastRandomMapSettings => ({
  type: ActionTypes.SET_LAST_RANDOM_MAP_SETTINGS,
  payload: {
    randomMapSettings: {
      ...randomMapSettings
    }
  }
});

export type EditorUiAction = SetEditorTool
  | SetTerrainBrush
  | SetTerrainBrushSize
  | SetStructure
  | SetUndoQueue
  | SetLastRandomMapSettings;
