import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { clamp, shuffle } from './helpers/functions';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { CityData } from 'ai-interface/types/city';
import { TerrainManager } from './TerrainManager';
import { DRAW_DEBUG_INFO } from './main';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { maxCityTrainingCapacity, demolishCost } from 'ai-interface/constants/city';
import { BuildTask, NOP, TrainingTask } from './helpers/types';
import { Ability } from 'ai-interface/constants/ability';
import { Terrain } from 'ai-interface/constants/terrain';
import { GuardManager } from './GuardManager';
import { PathingManager } from './PathingManager';
import { straightMoveCost } from 'ai-interface/constants/path';
import { ObstacleType } from 'ai-interface/types/obstacle';
import { ThreatManager } from './ThreatManager';
import { Personality } from './helpers/Personality';
import { nameOfUtopia, WinCondition } from 'ai-interface/constants/winCondition';

export class CityManager {

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly terrainManager: TerrainManager,
    private readonly guardManager: GuardManager,
    private readonly pathingManager: PathingManager,
    private readonly threatManager: ThreatManager,
    private readonly personality: Personality
  ) {
    guardManager.setCityManager(this);
  }

  private getMyCities(): CityData[] {
    const myPlayerId = this.game.getMyPlayerId();
    const myCities = this.land.getAllCityIds()
      .map(cityId => this.game.getCity(cityId))
      .filter(city => city.owner === myPlayerId && !city.razed);
    shuffle(myCities);
    return myCities;
  }

  public getCityUtility(city: CityData): number {
    if (city.razed) {
      return 0;
    }

    if (city.name === nameOfUtopia && this.game.getWinCondition() === WinCondition.CaptureTheCityUtopia) {
      return 1750;
    }

    return (city.capitol ? (this.game.getWinCondition() === WinCondition.DestroyAllEnemyCapitols ? 700 : 500) : 250)
      + city.incomingCaravans.reduce((sum, { armyTypeId }) => sum + this.threatManager.armyTypeIdToGuardPower[armyTypeId]
        + this.threatManager.armyTypeIdToAttackPower[armyTypeId], 0)
      + Math.max(
        ...city.currentlyTrainableArmyTypeIds.map(armyTypeId => this.utilityOfArmyTypeProductionAt(armyTypeId, city)),
        0
      ) / 2;
  }

  private utilityOfArmyTypeProductionAt(armyTypeId: string, city: CityData): number {
    const { abilities } = standardArmyTypes[armyTypeId];
    let utility = this.personality.utilityOfTrainingArmyTypeId[armyTypeId];

    if (armyTypeId === 'linf') { // Good, reliable, cheap army
      utility *= 1.5;
    }

    // Terrain
    const terrainWeight = this.terrainManager.getTerrainWeightsForCity(city.id);
    let bonus = 0;
    for (const ability in abilities) {
      switch (ability) {
        case Ability.NoPenaltyInSwamp: bonus += 0.001 + 1.5 * (terrainWeight[Terrain.Swamp] ?? 0); break;
        case Ability.NoPenaltyInForest: bonus += 0.001 + 1.0 * (terrainWeight[Terrain.Forest] ?? 0); break;
        case Ability.NoPenaltyInHills: bonus += 0.001 + 2.0 * (terrainWeight[Terrain.Hill] ?? 0); break;
        case Ability.NoPenaltyInDesert: bonus += 0.001 + 0.5 * (terrainWeight[Terrain.Desert] ?? 0); break;
        case Ability.NoPenaltyOnIce: bonus += 0.001 + 0.5 * (terrainWeight[Terrain.Ice] ?? 0); break;
        case Ability.WalksOnVolcanic: bonus += 0.001 + 4.0 * (terrainWeight[Terrain.Volcanic] ?? 0) ** 1.5; break;
        case Ability.WalksOverMountains: bonus += 0.001 + 4.0 * (terrainWeight[Terrain.Mountain] ?? 0) ** 1.5; break;
        case Ability.Flies:
          bonus += Math.max(-0.3, -0.7
            + 2.0 * (terrainWeight[Terrain.Mountain] ?? 0) + 2.0 * (terrainWeight[Terrain.Water] ?? 0)
            + 1.5 * (terrainWeight[Terrain.Swamp] ?? 0)
            + 1.0 * (terrainWeight[Terrain.Forest] ?? 0)
            + 2.0 * (terrainWeight[Terrain.Hill] ?? 0)
            + 0.5 * (terrainWeight[Terrain.Desert] ?? 0)
            + 0.5 * (terrainWeight[Terrain.Ice] ?? 0));
          break;
        case Ability.StrengthBonusInOpen: bonus += 0.001 + (terrainWeight[Terrain.Open] ?? 0); break;
        case Ability.StrengthBonusInSwamp: bonus += 0.001 + (terrainWeight[Terrain.Swamp] ?? 0); break;
        case Ability.StrengthBonusInForest: bonus += 0.001 + (terrainWeight[Terrain.Forest] ?? 0); break;
        case Ability.StrengthBonusInHillsAndMountains: bonus += 0.001 + (terrainWeight[Terrain.Hill] ?? 0); break;
        case Ability.StrengthBonusInDesert: bonus += 0.001 + (terrainWeight[Terrain.Desert] ?? 0); break;
        case Ability.StrengthBonusOnIce: bonus += 0.001 + (terrainWeight[Terrain.Ice] ?? 0); break;
        case Ability.MoraleBonus: bonus += 0.001; break;
        case Ability.FrighteningBonus: bonus += 0.001; break;
      }
    }
    if (bonus !== 0) {
      utility *= 0.8 + bonus;
      utility **= 1.1;
    }

    // TODO - if opponent has a lot of flyers (around this city or on entire map), anti-fliers should have higher utility. Same for other cancel abilities.

    return utility;
  }

  // We don't want to switch the training while midway of training, because we would lost the investment already spent.
  private midwayInTraining(city: CityData): boolean {
    const { currentlyTrainingArmyTypeId, turnsTrainingLeft } = city;
    return currentlyTrainingArmyTypeId !== undefined && turnsTrainingLeft !== undefined
      && turnsTrainingLeft < standardArmyTypes[currentlyTrainingArmyTypeId].trainingTime;
  }

  private generatePossibleBuildTasksForCity(city: CityData, utilityMultiplier: number): BuildTask[] {
    const { id, currentlyTrainableArmyTypeIds } = city;
    const armyTypeIdToDemolish = currentlyTrainableArmyTypeIds.length === maxCityTrainingCapacity
      ? currentlyTrainableArmyTypeIds[0] : null;

    // Utility of building something new is decreased by the utility of already existing building.
    // This is because it is better to increase from utility 2 to 6 rather than from 5 to 6.
    const currentUtility = Math.max(0, ...currentlyTrainableArmyTypeIds.map(armyTypeId => this.utilityOfArmyTypeProductionAt(armyTypeId, city)));
    const text = 'Current utility:' + (' ' + currentUtility).substring(0, 4);
    this.draw.drawTextWhenCityIsSelected(text, id);

    const tasks: BuildTask[] = [];
    for (const armyTypeId in standardArmyTypes) {
      if (!standardArmyTypes[armyTypeId].hero && !currentlyTrainableArmyTypeIds.includes(armyTypeId)) {
        let price = standardArmyTypes[armyTypeId].buildingCost;
        let action = () => this.game.buildTrainingFacility(id, armyTypeId);
        let description = `Build ${standardArmyTypes[armyTypeId].name}`;

        if (armyTypeIdToDemolish !== null) {
          price += demolishCost;
          description += ` and demolish ${standardArmyTypes[armyTypeIdToDemolish].name}`
          action = () => {
            this.game.demolishTrainingFacility(id, armyTypeIdToDemolish);
            this.game.buildTrainingFacility(id, armyTypeId);
          };
        }

        let utility = this.utilityOfArmyTypeProductionAt(armyTypeId, city) - currentUtility;
        if (utility > 0) {
          utility *= utilityMultiplier;
        }

        description = Math.round(100 * utility / price) + '%; '
          + (utility > 0 ? '+' : '') + ('' + utility).substring(0, 4) + '; '
          + price + 'g; '
          + description
          + ` in ${city.name}`;
        tasks.push({ utility, price, action, description });
      }
    }

    return tasks;
  }

  /** For simplicity, we build at most one building per turn. It is unlikely that we suddenly have so much gold to build multiple anyway. */
  public findBuildTaskWithHighestUtility(goldAvailable: number): BuildTask {
    let bestTask: BuildTask = {
      utility: 0,
      price: 1, // To prevent division by zero
      action: NOP,
      description: 'Build nothing'
    };

    // For each city, calculate all possible build tasks.
    for (const city of this.getMyCities()) {
      const utilityMultiplier = clamp(0, 1, 1 - this.guardManager.getAdditionalGuardPowerNeededForCity(city) / 10);
      const tasks = this.generatePossibleBuildTasksForCity(city, utilityMultiplier);

      // We don't choose the task with the highest utility gain; which would most of the time mean some level 4 army.
      // We choose the best ratio of utility to price; we want a lot of fun for as little money as possible.
      tasks.sort((t1, t2) => t2.utility / t2.price - t1.utility / t1.price);

      if (DRAW_DEBUG_INFO) {
        this.draw.drawTextWhenCityIsSelected(`Build task utility multiplier based on enemy threat: ${Math.round(10 * utilityMultiplier) / 10}`, city.id);
        for (const { description } of tasks) {
          this.draw.drawTextWhenCityIsSelected(description, city.id);
        }
      }

      if (!this.midwayInTraining(city)) {
        for (const task of tasks) {
          if (task.price <= goldAvailable && task.utility / task.price > bestTask.utility / bestTask.price) {
            bestTask = task;
          }
        }
      }
    }

    return bestTask;
  }

  private generateBestTrainingTaskForCity(city: CityData): TrainingTask {
    const { id, currentlyTrainableArmyTypeIds, currentlyTrainingArmyTypeId, caravanToCityId } = city;
    let destinationCity = city;
    if (caravanToCityId !== undefined) {
      destinationCity = this.game.getCity(caravanToCityId);
    }

    const additionalGuardPowerNeeded = this.guardManager.getAdditionalGuardPowerNeededForCity(destinationCity);

    let bestUtility = 0;
    let bestArmyTypeId: string | undefined = undefined;

    for (const armyTypeId of currentlyTrainableArmyTypeIds) {
      let utility = this.utilityOfArmyTypeProductionAt(armyTypeId, destinationCity);
      if (armyTypeId === currentlyTrainingArmyTypeId && this.midwayInTraining(city)) {
        utility *= 3;
      }

      // Don't produce scout armies to defend a threatened city - they don't really help with guarding, only cost gold.
      if (additionalGuardPowerNeeded > 2 * this.threatManager.armyTypeIdToGuardPower[armyTypeId]
        && this.threatManager.armyTypeIdToGuardPower[armyTypeId] < 4
        && this.game.getMyArmiesAt(destinationCity).length > 0 // But if the city is empty, create at least one, so we can raze it.
      ) {
        utility = 0;
      }

      if (utility > bestUtility) {
        bestUtility = utility;
        bestArmyTypeId = armyTypeId;
      }
    }

    return {
      utility: bestUtility,
      cityId: id,
      armyTypeId: bestArmyTypeId
    };
  }

  public trainArmyTypesWithHighestUtility(): void {
    const tasks = this.getMyCities()
      .map(city => this.generateBestTrainingTaskForCity(city))
      .sort((t1, t2) => t2.utility - t1.utility);

    // TODO - currently we're training everywhere. But he should be able to deliberately turn off production when he wants to save gold
    for (const { cityId, armyTypeId } of tasks) {
      this.game.setTrainingInCity(cityId, armyTypeId);
    }
  }

  /**
   * Set up caravans. Some heuristics are used:
   * 1. Don't send from a city that is badly guarded; we need armies there.
   * 2. Send production from the cities farthest away to the cities nearest to the enemy.
   * 3. Late in the game we usually have more production than gold. As such, we don't care when we lose caravan because they were send to a threatened city.
   * 4. Don't send between cities that are so near that going on foot is faster than using caravan.
   * As a note, it is completely safe to modify all caravans whenever and however you like - it doesn't cancel training or lose produced armies.
   */
  public updateCaravans() {
    const myCitiesWithDist = this.getMyCities().map(city => {
      this.game.setCaravanFromCityToCity(city.id, undefined);

      const dist = this.pathingManager.calculateFlyMovesToNearestEnemy(city) / straightMoveCost;
      const text = '' + Math.floor(dist);
      this.draw.drawTextWhenNothingIsSelected(text, { row: city.row + 1.5, col: city.col + 1.5 });

      return { city, dist };
    });

    // We want to make caravans from the cities that are at the beginning of the array to cities that are at the end of the array.
    myCitiesWithDist.sort((c1, c2) => c2.dist - c1.dist);

    for (const { city: cityFrom, dist: distFrom } of myCitiesWithDist) {
      if (this.guardManager.getAdditionalGuardPowerNeededForCity(cityFrom) > 0) {
        continue;
      }

      for (let i = myCitiesWithDist.length - 1; i > 0; i--) {
        const { city: cityTo, dist: distTo } = myCitiesWithDist[i];

        if (distFrom - distTo < 20) {
          break;
        }

        if ((cityFrom.row - cityTo.row) ** 2 + (cityFrom.col - cityTo.col) ** 2 < 30 * 30) {
          continue;
        }

        try {
          this.game.setCaravanFromCityToCity(cityFrom.id, cityTo.id);
          break;
        } catch (e: any) {
          if (e.type !== ObstacleType.CITY_CANT_HAVE_MORE_INCOMING_CARAVANS) {
            throw e;
          }
        }
      }
    }
  }

}
