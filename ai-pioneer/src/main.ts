import { AI } from 'ai-interface/AI';
import { GameApi } from 'ai-interface/apis/GameApi';
import { GameApiObstacle } from 'ai-interface/apis/GameApiObstacle';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { TransferManager } from './TransferManager';
import { GroupManager } from './GroupManager';
import { CityManager } from './CityManager';
import { ArtifactManager } from './ArtifactManager';
import { CombatManager } from './CombatManager';
import { RuinManager } from './RuinManager';
import { TerrainManager } from './TerrainManager';
import { SpendingManager } from './SpendingManager';
import { Personality, generatePersonality } from './helpers/Personality';
import { ThreatManager } from './ThreatManager';
import { PathingManager } from './PathingManager';
import { GuardManager } from './GuardManager';
import { RazeManager } from './RazeManager';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { AggressionManager } from './AggressionManager';

/** Debugging. For production, set all to false. */
export const DRAW_DEBUG_INFO = true;
export const DONT_MOVE_ARMIES = false;
export const DONT_DISMISS_ARMIES = false; // False => Dismiss armies that are too far away from enemies. Distance from enemy is written next to an army.
export const DONT_BUY = false; // True => No heroes, buildings or razing will be bought. Armies will still be trained, since that isn't immediate spending.
export const IGNORE_DISTANCE = false; // True => Utility decreases with distance. False => Utility stays the same no matter the distance.
export const MOVE_EACH_GROUP_JUST_ONCE = false;
export const THREAT_ALL_PLAYERS_EQUALLY = false; // False => Attack stronger players more than weak players.
export const CONSTANT_PERSONALITY = false; // True => Magical constants will be always the same. False => there will be some random used.
export const PRETEND_ITS_TURN_NUMBER: number | null = null; // If non-null value is given, this turn number will be used instead of the real turn number.

export const takeTurn: AI = (land: LandApi, game: GameApi, obstacle: GameApiObstacle, draw: DrawApi) => {
  // Load personality from state if there is any
  let personality: Personality;
  try {
    personality = JSON.parse(game.getSavedState());
  } catch (e) {
    personality = generatePersonality(!CONSTANT_PERSONALITY);
  }

  // Construct all objects 
  const artifactManager = new ArtifactManager(land, game, draw);
  const aggressionManager = new AggressionManager(land, game, draw, personality);
  const combatManager = new CombatManager(land, game, draw);
  const ruinManager = new RuinManager(land, game, draw, artifactManager);
  const terrainManager = new TerrainManager(land, game, draw);
  const threatManager = new ThreatManager(land, game, draw);
  const razeManager = new RazeManager(land, game, draw, threatManager, personality);
  const guardManager = new GuardManager(land, game, draw, threatManager, razeManager);
  const transferManager = new TransferManager(land, game, draw, personality);
  const pathingManager = new PathingManager(land, game, draw, artifactManager, aggressionManager, combatManager, threatManager);
  const cityManager = new CityManager(land, game, draw, terrainManager, guardManager, pathingManager, threatManager, personality);
  const spendingManager = new SpendingManager(land, game, draw, threatManager, guardManager, pathingManager, cityManager, personality);
  const groupManager = new GroupManager(land, game, draw, artifactManager, aggressionManager, combatManager, ruinManager, threatManager,
    guardManager, transferManager, pathingManager, cityManager, spendingManager);
  
  // On turn 1, set ordering.
  if ((PRETEND_ITS_TURN_NUMBER ?? game.getTurnNumber()) === 1) {
    transferManager.joinAllArmiesStandingTogether();

    const toOrder = (armyTypeId: string) => {
      // In a group of Light Infantry and Scouts, it's better to protect the Scouts because of the movement bonus.
      if (armyTypeId === 'linf') {
        return 0;
      }
      if (standardArmyTypes[armyTypeId].hero) {
        return 100000 + standardArmyTypes[armyTypeId].buildingCost;
      }
      return personality.utilityOfTrainingArmyTypeId[armyTypeId];
    };

    const armyTypeIds = game.getMyOrdering();
    armyTypeIds.sort((id1, id2) => toOrder(id1) - toOrder(id2));
    game.setMyOrdering(armyTypeIds);
  }

  artifactManager.pickArtifactsWithHeroesStandingOnBags();
  threatManager.calculateThreatToEachCity();
  // This should be after the calculateThreatToEachCity(), because preferred guard depends on threat;
  // and before generateAndExecuteBestTasks(), because groups need to know where to move for guarding.
  guardManager.calculatePreferredGuardPowerForEachCity(spendingManager.getMaximalUpkeepForGuarding());
  aggressionManager.calculateGlobalAggressionBasedOnSpending(spendingManager.getMaximalDesiredUpkeepForArmies());
  groupManager.generateAndExecuteBestTasks();
  spendingManager.checkHeroOffer();
  spendingManager.buyBuilding();
  cityManager.updateCaravans();
  cityManager.trainArmyTypesWithHighestUtility();
  spendingManager.dismissUselessArmiesUntilUpkeepRemains(spendingManager.getMaximalDesiredUpkeepForArmies());

  if (DRAW_DEBUG_INFO) {
    threatManager.drawThreatToEachCity();
    guardManager.drawGuardPowerForEachCity();
    pathingManager.drawPathsTraveled();
  }

  // Save personality to state
  game.saveState(JSON.stringify(personality));
};
