import './FullscreenButton.scss';
import { useEffect, useState } from 'react';
import { faCompress, faExpand } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { EMPTY_ARRAY } from '../../functions';

export default function FullscreenButton() {
  const { isFullscreen, setFullscreen } = useFullscreenStatus();
  return (
    <FontAwesomeIcon
      className="fullscreen-button"
      size="lg"
      icon={isFullscreen ? faCompress : faExpand}
      onClick={() => setFullscreen(!isFullscreen)}
    />
  );
}

/** NOTE: It's impossible to use browser API to detect or escape a fullscreen that was initiated by pressing F11. */
export function useFullscreenStatus() {
  const [isFullscreen, setIsFullscreen] = useState(document.fullscreenElement !== null);

  const setFullscreen = (goToFullscreen: boolean) => {
    if (goToFullscreen) {
      if (document.documentElement !== null) {
        document.documentElement
          .requestFullscreen()
          .then(() => setIsFullscreen(document.fullscreenElement !== null))
          .catch(() => setIsFullscreen(false));
      }
    } else {
      document
        .exitFullscreen()
        .then(() => setIsFullscreen(document.fullscreenElement !== null))
        .catch(() => setIsFullscreen(true));
    }
  };

  useEffect(() => {
    function handleFullscreenChange() {
      setIsFullscreen(document.fullscreenElement !== null);
    }

    document.addEventListener('fullscreenchange', handleFullscreenChange);
    return () => document.removeEventListener('fullscreenchange', handleFullscreenChange);
  }, EMPTY_ARRAY);

  return { isFullscreen, setFullscreen };
}
