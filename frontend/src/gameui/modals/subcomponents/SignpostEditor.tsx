import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import { setSignpostMessage } from '../../../model/game/actionsEditor';
import { Place } from 'ai-interface/types/place';
import { pushToUndoQueue } from '../../../model/editor/actions';
import UnmountableComponent from '../../common/UnmountableComponent';

interface SignpostEditorProps {
  place: Place;
}

export default function SignpostEditor({ place: { row, col } }: SignpostEditorProps) {
  const originalValue = useSelector((state: RootState) => state.game.tiles[row][col].signpost);
  const dispatch: AppDispatch = useDispatch();

  const [newValue, setNewValue] = useState(originalValue);

  function saveSignpostValue() {
    if (originalValue !== newValue) {
      dispatch(setSignpostMessage(row, col, newValue));
      pushToUndoQueue();
    }
  }

  return (
    <UnmountableComponent onComponentWillUnmount={saveSignpostValue}>
      <textarea
        className="w-100 overflow-hidden"
        rows={2}
        value={newValue}
        maxLength={200}
        placeholder="Signpost message"
        onChange={e => setNewValue(e.target.value)}
      />
    </UnmountableComponent>
  );
}
