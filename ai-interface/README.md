# AI Interface

Use this project as a dependency for your AI plugin. The only thing your plugin has to provide is an implementation of the function `takeTurn`.

### npm i

Downloads and installs all dependencies for this project into folder `node_modules`.

### npm run build

Builds this library to the `lib` folder.

### npm run clean

Removes folders `node_modules` and `lib`.
