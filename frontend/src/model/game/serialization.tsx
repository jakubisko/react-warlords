import JSZip from 'jszip';
import { UserError } from '../ui/types';
import { MapData } from './types';
import { SaveGameData } from './saveLoad';

/** User won't be able to load a map, load a game save, or join an online game of a different version. */
export const buildVersion = '20. 1. 2025';

/** Manually exported from JZip. */
interface InputByType {
  base64: string;
  string: string;
  text: string;
  binarystring: string;
  array: number[];
  uint8array: Uint8Array;
  arraybuffer: ArrayBuffer;
  blob: Blob;
  stream: NodeJS.ReadableStream;
}

/** Manually exported from JSZip. */
interface OutputByType {
  base64: string;
  string: string;
  text: string;
  binarystring: string;
  array: number[];
  uint8array: Uint8Array;
  arraybuffer: ArrayBuffer;
  blob: Blob;
  nodebuffer: Buffer;
}

/** Manually exported from JZip. */
export type InputFileFormat = InputByType[keyof InputByType] | Promise<InputByType[keyof InputByType]>;

/**
 * Compresses given object into a zip.
 * The zip will be returned in a desired type (string, blob, uint8array etc).
 * Use decompressObject() for reversing.
 */
export async function compressObject<O extends JSZip.OutputType>(object: object, type: 'map' | 'game', outputType: O): Promise<OutputByType[O]> {
  // 1. Add type of file.
  const envelope = {
    o: object,
    t: type,
    v: buildVersion
  };

  // 2. Turn object to string.
  const jsonString = JSON.stringify(envelope);

  // 3. Compress the string into a desired type.
  const zip = new JSZip();
  zip.file('data.json', jsonString);

  return zip.generateAsync({
    type: outputType,
    compression: 'DEFLATE',
    compressionOptions: {
      level: 9 // Highest compression level 
    }
  });
}

type DecompressionOutputByType = {
  map: MapData;
  game: SaveGameData;
}

/**
 * Reverse function to compressObject().
 * Given an input of desired type (string, blob, uint8array etc), converts it into a zip file.
 * Then extracts and returns an object written in it.
 * Checks whether the expected file type matches the actual file type.
 * In case of failure, this throws an error which can be displayed to the user, so use try..catch.
 */
export async function decompressObject<T extends keyof DecompressionOutputByType>
  (zipData: InputFileFormat, type: T, checkVersion = true): Promise<DecompressionOutputByType[T]> {
  let jsonString;

  // 3. Decompress the input and extract the string.
  try {
    const zip = await JSZip.loadAsync(zipData);
    const dataFile = zip.file('data.json');

    if (dataFile === null) {
      throw new UserError('No data found in the file.');
    }
    jsonString = await dataFile.async('string');
  } catch (err) {
    throw new UserError('Failed to decompress the file.', err);
  }

  // 2. Turn string to object.
  let envelope;
  try {
    envelope = JSON.parse(jsonString);
  } catch (err) {
    throw new UserError('The file is not valid.', err);
  }

  // 1a. Check whether the JSON contains all required fields. If not, this is not a Warlords file, just an unrelated JSON file.
  if (envelope.o === undefined || envelope.t === undefined || envelope.v === undefined) {
    throw new UserError('The file is not a Warlords file.');
  }

  // 1b. Check type. User may be trying to open a save game instead of a map or other way around.
  if (envelope.t !== type) {
    throw new UserError(`Please choose a ${type} file. Given file is a ${envelope.t} file.`);
  }

  // 1c. Check version. User may be trying to open a save game made by the older version of the game.
  if (checkVersion && envelope.v !== buildVersion) {
    throw new UserError(`Given file has a version '${envelope.v}' which differs from the version of your game '${buildVersion}'.`);
  }

  // Return the object
  return envelope.o;
}
