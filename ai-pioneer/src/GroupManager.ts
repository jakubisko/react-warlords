import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { SearchPathArgs } from 'ai-interface/types/path';
import { Place } from 'ai-interface/types/place';
import { Task, NOP } from './helpers/types';
import { Structure } from 'ai-interface/constants/structure';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { IGNORE_DISTANCE, DONT_MOVE_ARMIES, DRAW_DEBUG_INFO, MOVE_EACH_GROUP_JUST_ONCE } from './main';
import { ArtifactManager } from './ArtifactManager';
import { CombatManager, ExpectedUtilityOfCombat } from './CombatManager';
import { RuinManager } from './RuinManager';
import { TransferManager } from './TransferManager';
import { CityManager } from './CityManager';
import { PathingManager } from './PathingManager';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { ThreatManager } from './ThreatManager';
import { GuardManager } from './GuardManager';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { AggressionManager } from './AggressionManager';
import { experienceForGrove, heroExperienceLevels } from 'ai-interface/constants/experience';
import { SageTowerChoice } from 'ai-interface/types/exploring';
import { SpendingManager } from './SpendingManager';
import { clamp } from './helpers/functions';

/**
 * Stops searching for more tasks for a group once this number is reached. Once the group is full and can't join new armies, transfer
 * tasks won't be generated. As a result, algorithm will automatically search for combat farther away.
 */
const MAX_TASKS = 85;

export class GroupManager {

  private readonly myPlayerId: number;

  // Lazy loaded - we'll count them when it's called first time.
  private doWeHaveHero?: boolean = undefined;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly artifactManager: ArtifactManager,
    private readonly aggressionManager: AggressionManager,
    private readonly combatManager: CombatManager,
    private readonly ruinManager: RuinManager,
    private readonly threatManager: ThreatManager,
    private readonly guardManager: GuardManager,
    private readonly transferManager: TransferManager,
    private readonly pathingManager: PathingManager,
    private readonly cityManager: CityManager,
    private readonly spendingManager: SpendingManager
  ) {
    this.myPlayerId = game.getMyPlayerId();
  }

  private createTaskGeneratorForGroup(groupId: string, neededForGuarding: boolean): (row: number, col: number) => Task {
    const { game, land, myPlayerId, artifactManager, combatManager, ruinManager, transferManager } = this;
    const structures = land.getStructures();

    const armiesInGroup = game.getArmiesInGroup(groupId);
    const heroPresent = armiesInGroup.some(army => standardArmyTypes[army.armyTypeId].hero);
    const aggression = this.aggressionManager.getAggressionOfGroup(groupId, armiesInGroup, heroPresent, neededForGuarding);

    return (row: number, col: number) => {
      const place: Place = { row, col };
      let utility = 0;
      let dailyMultiplier = 1 - aggression / 2; // Aggressive groups don't want to travel far for a combat.
      let action = NOP;

      // Basic avoiding function guarantees that this won't be called for place owned by an ally.
      const owner = game.getOwnerOfPlace(place);
      const enemyOrNeutral = owner !== null && owner !== myPlayerId;

      switch (structures[row][col]) {

        case Structure.City:
          const cityId = land.getCityIdAt(place)!;
          const city = game.getCity(cityId);
          if (!city.razed) {
            if (enemyOrNeutral) {
              utility += this.cityManager.getCityUtility(city);

              const { row, col } = city;
              if (!game.getArmyTypeIdsAt(city) && !game.getArmyTypeIdsAt({ row: row + 1, col: col + 1 })
                && !game.getArmyTypeIdsAt({ row: row + 1, col }) && !game.getArmyTypeIdsAt({ row, col: col + 1 })) {
                // AI used to chase empty cities from a too large distance. We slightly discourage that.
                dailyMultiplier *= (100 - 2 * maxTileCapacity + 2 * armiesInGroup.length) / 100;
              }
            } else {
              // Is our city threatened and missing armies for optimal defense?
              const additionalPowerNeeded = this.guardManager.getAdditionalGuardPowerNeededForCity(city);
              if (additionalPowerNeeded > 0) {
                const groupPower = armiesInGroup.reduce((sum, army) => sum + this.threatManager.armyTypeIdToGuardPower[army.armyTypeId], 0);
                utility += Math.min(groupPower, additionalPowerNeeded)
                  * this.cityManager.getCityUtility(city) / 40;
              }
            }
          }

          break;

        case Structure.Temple:
          const unblessedArmies = armiesInGroup.filter(army => !army.blessed);
          utility += 40 * unblessedArmies.length; // CombatManager.calculateSituationalUtility awards extra utility to nearby enemies
          break;

        case Structure.Ruin:
          if (heroPresent) {
            const task = ruinManager.visitRuinTask(groupId, place);
            if (task.utility > 0) {
              utility += task.utility;
              action = task.action;
            }
          }
          break;

        case Structure.SageTower:
          if (heroPresent && !game.wasSageVisitedThisWeek()) {
            const power = clamp(0, 10, this.game.getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(myPlayerId));

            if (power > 0) {
              utility += 125 * power;

              action = () => {
                // It could happen that we reached the place without the hero. We need to check whether we have a hero with us.
                const armiesInGroup = this.game.getArmiesInGroup(groupId);
                const hero = armiesInGroup.find(army => standardArmyTypes[army.armyTypeId].hero);

                if (hero !== undefined) {
                  const choices: SageTowerChoice[] = [];
                  if (armiesInGroup.length > maxTileCapacity / 2) {
                    choices.push(SageTowerChoice.Gold);

                    if (game.getHero(hero.id).level < heroExperienceLevels.length - 1) {
                      choices.push(SageTowerChoice.Experience);
                    }
                  }
                  if (armiesInGroup.length < maxTileCapacity - 1) {
                    choices.push(SageTowerChoice.Armies);
                  }

                  const choice = choices[Math.floor(choices.length * Math.random())];
                  this.game.exploreSageTower(hero.id, choice);

                  // Transfer all new armies to hero's group. If we don't do this, hero will instead join them.
                  // And because we're unaware of the new armies, they wouldn't move for the rest of the turn.
                  if (choice === SageTowerChoice.Armies) {
                    for (const army of this.game.getMyArmiesAt(place)) {
                      if (army.groupId !== groupId) {
                        this.game.transferArmyFromGroupToOtherGroup(army.id, army.groupId, groupId);
                      }
                    }
                  }

                  // If we received gold, we don't need to dismiss. If we received armies, we don't want to dismiss.
                  if (choice === SageTowerChoice.Gold || choice === SageTowerChoice.Armies) {
                    this.spendingManager.setIgnoreDismissingToday(true);
                  }
                }
              };
            }
          }
          break;

        case Structure.Grove:
          if (heroPresent) {
            utility += armiesInGroup.filter(army => army.armyTypeId === 'hro1').length * (experienceForGrove / 5);
          }
          break;

        case Structure.Watchtower:
        case Structure.SpyDen:
          utility += enemyOrNeutral ? 50 : 0;
          break;

        case Structure.Village:
          utility += enemyOrNeutral ? 190 : 0; // CombatManager.calculateSituationalUtility awards extra utility to nearby enemies
          break;

        case Structure.Encampment:
          if (this.doWeHaveHero === undefined) {
            this.doWeHaveHero = this.game.getAllMyArmies().some(army => standardArmyTypes[army.armyTypeId].hero);
          }
          utility += this.doWeHaveHero ? 0 : 250; // CombatManager.calculateSituationalUtility awards extra utility to nearby enemies
          break;
      }

      if (heroPresent) {
        const task = artifactManager.pickArtifactsIfThereAreAnyTask(groupId, place);
        if (task.utility > 0) {
          utility += task.utility;
          // Actual picking of artifacts is in the PathingManager and is called each time a group with a hero makes a step.
          // This is because we want to pick artifacts that we walk over without planning.; such as an artifact dropped by a killed hero.
        }
      }

      let expectedUtilityOfCombat: ExpectedUtilityOfCombat | null = null;
      if (enemyOrNeutral) {
        expectedUtilityOfCombat = combatManager.predictExpectedUtilityOfCombat(groupId, place, owner!);
      } else if (owner === myPlayerId && !neededForGuarding) { // If we're needed for guarding, don't transfer to other groups!
        // If there are my armies, we may decide to join the groups.
        const task = transferManager.transferFromThisGroup(groupId, place);
        if (task.utility > 0) {
          utility += task.utility;
          dailyMultiplier *= 0.75; // We don't want to spend time chasing another group for joining if we are far away.
          action = task.action;
        }
      }

      if (expectedUtilityOfCombat === null) {
        // High aggression leads to decrease of utility of non-combat tasks.
        utility *= 1 - aggression;
      } else {
        // Combat is it's own reward for an aggressive group regardless of the outcome.
        utility = this.combatManager.utilityUpdatedWithCombat(utility + 50 * aggression, expectedUtilityOfCombat)
          + 50 * aggression;

        // If we're needed for guarding, only engage in combat that is a sure win.
        if (neededForGuarding && expectedUtilityOfCombat.loseChance > 0.04) {
          utility = 0;
        }
      }

      return { utility, place, dailyMultiplier, action };
    };
  }

  private generateReachableTasksForGroup(groupId: string): Task[] {
    const start = this.game.getPlaceOfGroup(groupId);

    // Is the group free to leave or does it need to guard?
    // This method will transfer away from our group armies needed to guard. Armies that are still in the group are free to move away.
    const neededForGuarding = this.guardManager.entireGroupIsNeededForGuardingCurrentPlace(groupId, start);

    const tasks: Task[] = [];

    const moveType = this.game.getMoveTypeOfGroup(groupId);
    const { groupMoveOnLand, groupSize } = moveType;

    const generateTaskAtPlace = this.createTaskGeneratorForGroup(groupId, neededForGuarding);

    const searchArgs: SearchPathArgs = {
      start,
      isDestination: (row, col) => {
        const task = generateTaskAtPlace(row, col);
        if (task.utility > 0) {
          tasks.push(task);
          return tasks.length >= MAX_TASKS;
        }
        return false;
      },
      // IMPORTANT: This avoiding function goes through the enemies. But actual movement will try to avoid enemies if the detour is small.
      isToBeAvoided: this.game.createBasicAvoidingFunction(groupSize, 'GO_AROUND_ALLIES'),
      moveType,
      // If the group is needed for guarding, we allow it to travel only so far that it can return back to the city.
      initialMoveLeft: Math.floor(this.game.getMoveLeftOfGroup(groupId) * (neededForGuarding ? 0.5 : 1)),
      maxDay: neededForGuarding ? 0 : undefined
    };
    const { days, movesLeft } = this.game.calculateTimeMatrix(searchArgs);

    for (const task of tasks) {
      const { row, col } = task.place!;
      const day = days[row][col];

      if (!IGNORE_DISTANCE) {
        // Utility of the task decreases with travel time.
        const moveLeft = movesLeft[row][col];
        task.utility *= 0.666666667 / (1 + day) + (0.666666667 / (1 + day) - 0.666666667 / (2 + day)) * moveLeft / groupMoveOnLand;
        if (task.dailyMultiplier !== undefined) {
          task.utility *= task.dailyMultiplier ** day;
        }
      }

      if (task.action === NOP) {
        task.action = () => this.pathingManager.sendGroupTo(groupId, moveType, task.place!, day);
      } else {
        const { action, place } = task;
        task.action = () => {
          this.pathingManager.sendGroupTo(groupId, moveType, place!, day);
          if (this.game.groupExists(groupId)) {
            const { row, col } = this.game.getPlaceOfGroup(groupId);
            if (row === place!.row && col === place!.col) {
              action();
            }
          }
        };
      }

      // If the group is needed for guarding, we need it to return back to its original position.
      if (neededForGuarding) {
        const { action } = task;
        task.action = () => {
          action();
          if (this.game.groupExists(groupId)) {
            this.pathingManager.sendGroupTo(groupId, moveType, start, 0);
          }
        };
      }
    }

    return tasks;
  }

  private findBestTaskForGroup(groupId: string): Task {
    const tasks = this.generateReachableTasksForGroup(groupId);

    if (DRAW_DEBUG_INFO) {
      for (const { utility, place } of tasks) {
        const text = ('' + utility).substring(0, 4);
        this.draw.drawTextWhenGroupIsSelected(text, groupId, place);
      }
    }

    let bestTask: Task = {
      utility: 0,
      dailyMultiplier: 0,
      place: { row: 0, col: 0 },
      action: NOP
    };

    // Find the action with the highest utility.
    if (tasks.length > 0 && !DONT_MOVE_ARMIES) {
      for (const task of tasks) {
        if (task.utility > bestTask.utility) {
          bestTask = task;
        }
      }
    }

    return bestTask;
  }

  private getMyGroupIdsSortedBySize(): string[] {
    // There is a known problem that sometimes a small group moves to join a big group. However, the big group then can't move anymore,
    // because the new member has no move left. This wastes more total movement than when big group moves to join a small group.
    // We use simple heuristics to improve on this: First move with bigger groups, and only then move the smaller groups.
    return this.game.getAllMyGroupIds()
      .map(groupId => ({
        groupId,
        order: this.game.getArmiesInGroup(groupId).length + Math.random() // Groups with same size will be shuffled
      }))
      .sort((a, b) => b.order - a.order)
      .map(item => item.groupId);
  }

  public generateAndExecuteBestTasks() {
    // 1. Collect all groups you control.
    // 2. For each group in the list:
    // 2.1. Find all possible tasks for the group.
    // 2.2. Execute the task with the highest utility of the tasks for the group.
    // 3. Repeat step 2 until everyone is out of movement or didn't move twice in a row.

    // Currently groups act independently from one another.
    // The algorithm when implemented correctly should collect all tasks for each group and then find
    // the combination of mutually non-exclusive tasks with the highest sum of expected utilities.
    // I don't know how to implement that, so this will have to be enough.

    // For each group, gather how much movement it has left.
    let groupIds = this.getMyGroupIdsSortedBySize();
    const groupIdMoveLeft: { [groupId: string]: number } = {};
    for (const groupId of groupIds) {
      groupIdMoveLeft[groupId] = this.game.getMoveLeftOfGroup(groupId);
    }
    // These groups have not moved in the last action. If this happens twice in a row, skip the group for the rest of the turn.
    const groupIdStands: { [groupId: string]: boolean } = {};

    while (groupIds.length > 0) {
      const nextGroupIds: string[] = [];

      for (const groupId of groupIds) {
        const { action } = this.findBestTaskForGroup(groupId);
        action();
        if (!this.game.groupExists(groupId)) {
          continue;
        }

        const moveLeftAfter = this.game.getMoveLeftOfGroup(groupId);
        if (moveLeftAfter <= 0) {
          continue;
        }

        if (groupIdMoveLeft[groupId] > moveLeftAfter) {
          groupIdStands[groupId] = false;
          groupIdMoveLeft[groupId] = moveLeftAfter;
        } else if (groupIdStands[groupId]) {
          continue;
        } else {
          groupIdStands[groupId] = true;
        }

        nextGroupIds.push(groupId);
      }

      groupIds = nextGroupIds;

      if (MOVE_EACH_GROUP_JUST_ONCE) {
        break;
      }
    }
  }

}