import { Hero } from '../game/types';
import { getHerosStrength, getHerosHitPoints, getHerosValueOfAbility, getHerosAbilities } from '../game/selectorsHero';
import { EMPTY_MAP, clamp, createArray } from '../../functions';
import { CombatEventType, CombatEvent, CombatConditions } from 'ai-interface/types/combat';
import { Army, Abilities } from 'ai-interface/types/army';
import { Ability } from 'ai-interface/constants/ability';
import { Terrain } from 'ai-interface/constants/terrain';
import { maxStrengthOnShip, maxStrengthOverall, getChanceForAtoHitB } from 'ai-interface/constants/combat';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';

const NO_ONE = -1;
const ATT = 0;
const DEF = 1;

interface EffectiveValues {
  readonly attackingAbilities: Abilities;
  readonly defendingAbilities: Abilities;
  readonly effectiveStrength: { [armyId: string]: number };
  readonly effectiveHitPoints: { [armyId: string]: number };
  readonly effectiveAmbush: { [armyId: string]: number };
}

export function calculateEffectiveValues(attackingArmies: Army[], defendingArmies: Army[], conditions: CombatConditions, heroes: { [armyId: string]: Hero; }): EffectiveValues {
  const armies = [attackingArmies, defendingArmies];

  const abilities = createArray(2, i => calculateAbilitiesForOneGroup(armies[i], armies[1 - i], conditions, heroes, i === ATT));
  return {
    attackingAbilities: abilities[ATT],
    defendingAbilities: abilities[DEF],
    effectiveStrength: createArray(2, i => calculateEffectiveStrengthForOneGroup(armies[i], abilities[i], abilities[1 - i], conditions, heroes, i === ATT))
      .reduce((result, value) => ({ ...result, ...value }), EMPTY_MAP),
    effectiveHitPoints: createArray(2, i => calculateEffectiveHitPointsForOneGroup(armies[i], heroes))
      .reduce((result, value) => ({ ...result, ...value }), EMPTY_MAP),
    effectiveAmbush: createArray(2, i => calculateEffectiveAmbushForOneGroup(armies[i], abilities[i], abilities[1 - i], heroes))
      .reduce((result, value) => ({ ...result, ...value }), EMPTY_MAP)
  };
}

export function calculateCombatEvents(attackingArmies: Army[], defendingArmies: Army[], effectiveValues: EffectiveValues, random: () => number): CombatEvent[] {
  const { effectiveStrength, effectiveHitPoints, effectiveAmbush } = effectiveValues;
  const armies = [attackingArmies, defendingArmies];
  const index = createArray(2, i => armies[i].length - 1);
  const army = createArray(2, i => armies[i][index[i]]);
  const strength = createArray(2, i => effectiveStrength[army[i].id]);
  const hitPoints = createArray(2, i => effectiveHitPoints[army[i].id]);
  const ambush0 = createArray(2, i => effectiveAmbush[army[i].id]);
  const ambush = [ambush0[ATT] - Math.min(...ambush0), ambush0[DEF] - Math.min(...ambush0)];

  const combatEvents: CombatEvent[] = [];
  while (true) {
    let loser = NO_ONE;
    let ambushed = false;

    // An army with ambush has a chance to one-kill an enemy.
    const ambusher = (ambush[ATT] > 0) ? ATT : (ambush[DEF] > 0 ? DEF : NO_ONE);
    if (ambusher !== NO_ONE) {
      ambushed = 100 * random() < ambush[ambusher];
      if (ambushed) {
        loser = 1 - ambusher;
        hitPoints[loser] = 0;
      }
      ambush[ambusher] = 0; // Ambush only works once for each army.
    }

    // If ambush fails, normal combat follows
    if (loser === NO_ONE) {
      loser = (100 * random() < getChanceForAtoHitB(strength[ATT], strength[DEF])) ? DEF : ATT;
    }

    combatEvents.push({
      type: --hitPoints[loser] > 0 ? CombatEventType.Damage : (ambushed ? CombatEventType.Ambush : CombatEventType.Kill),
      losingArmyId: army[loser].id,
      losingGroupId: army[loser].groupId,
      winningArmyId: army[1 - loser].id,
      winningGroupId: army[1 - loser].groupId
    });

    if (hitPoints[loser] <= 0) {
      // Move to next combatant if there is some
      if (--index[loser] < 0) {
        return combatEvents;
      } else {
        army[loser] = armies[loser][index[loser]];
        strength[loser] = effectiveStrength[army[loser].id];
        hitPoints[loser] = effectiveHitPoints[army[loser].id];
        ambush[loser] = effectiveAmbush[army[loser].id];
      }
    }
  }
}

function calculateAbilitiesForOneGroup(armies: Army[], enemyArmies: Army[], conditions: CombatConditions, heroes: { [armyId: string]: Hero },
  attacking: boolean): Abilities {
  const { fortification } = conditions;

  const maxAbility = (armies: Army[], ability: Ability): number =>
    Math.max(0, ...armies.map(army => standardArmyTypes[army.armyTypeId].hero
      ? getHerosValueOfAbility(heroes[army.id], ability)
      : standardArmyTypes[army.armyTypeId].abilities[ability] ?? 0));

  const abilities: Abilities = {
    [Ability.CancelEnemyFortificationBonus]: fortification && attacking ? maxAbility(armies, Ability.CancelEnemyFortificationBonus) : 0,
    [Ability.FortificationBonus]: !maxAbility(enemyArmies, Ability.CancelEnemyFortificationBonus) && !attacking ? fortification : 0,

    [Ability.CancelEnemyMoraleBonus]: maxAbility(armies, Ability.CancelEnemyMoraleBonus),
    [Ability.MoraleBonus]: !maxAbility(enemyArmies, Ability.CancelEnemyMoraleBonus) ? maxAbility(armies, Ability.MoraleBonus) : 0,

    [Ability.Leadership]: maxAbility(armies, Ability.Leadership) - maxAbility(enemyArmies, Ability.LowerEnemyLeadership),

    [Ability.AntiAirBonus]: maxAbility(armies, Ability.AntiAirBonus),

    [Ability.CancelEnemyFrighteningBonus]: maxAbility(armies, Ability.CancelEnemyFrighteningBonus),
    [Ability.FrighteningBonus]: maxAbility(enemyArmies, Ability.CancelEnemyFrighteningBonus) ? 0 : maxAbility(armies, Ability.FrighteningBonus),

    [Ability.CancelEnemyTerrainBonus]: maxAbility(armies, Ability.CancelEnemyTerrainBonus),

    [Ability.LowerEnemyAmbush]: Math.min(100,
      maxAbility(armies.filter(army => standardArmyTypes[army.armyTypeId].hero), Ability.LowerEnemyAmbush) +
      maxAbility(armies.filter(army => !standardArmyTypes[army.armyTypeId].hero), Ability.LowerEnemyAmbush)
    ),

    [Ability.IncreaseGroupAmbush]: Math.min(100,
      maxAbility(armies.filter(army => standardArmyTypes[army.armyTypeId].hero), Ability.IncreaseGroupAmbush) +
      maxAbility(armies.filter(army => !standardArmyTypes[army.armyTypeId].hero), Ability.IncreaseGroupAmbush)
    )
  };

  return Object.fromEntries(Object.entries(abilities).filter(([, value]) => value !== undefined && value > 0));
}

/** Applies abilities such as morale, terrain etc. Abilities do not change during combat, even when some armies die. */
function calculateEffectiveStrengthForOneGroup(armies: Army[], groupAbilities: Abilities, enemyGroupAbilities: Abilities,
  conditions: CombatConditions, heroes: { [armyId: string]: Hero }, attacking: boolean): { [armyId: string]: number } {

  const { terrain, city } = conditions;

  const effectiveStrength: { [armyId: string]: number } = {};

  for (const army of armies) {
    const { id, armyTypeId, blessed } = army;
    const { hero } = standardArmyTypes[armyTypeId];
    const abilities = hero ? getHerosAbilities(heroes[id]) : standardArmyTypes[armyTypeId].abilities;
    let strength = hero ? getHerosStrength(heroes[id]) : standardArmyTypes[armyTypeId].strength;

    // Bonus for terrain abilities
    if (!enemyGroupAbilities[Ability.CancelEnemyTerrainBonus]) {
      if (city) {
        strength += (abilities[Ability.StrengthBonusInCity] ?? 0);
      } else {
        strength += (terrain === Terrain.Open ? (abilities[Ability.StrengthBonusInOpen] ?? 0) : 0)
          + (terrain === Terrain.Forest ? (abilities[Ability.StrengthBonusInForest] ?? 0) : 0)
          + (terrain === Terrain.Swamp ? (abilities[Ability.StrengthBonusInSwamp] ?? 0) : 0)
          + ((terrain === Terrain.Hill || terrain === Terrain.Mountain) ? (abilities[Ability.StrengthBonusInHillsAndMountains] ?? 0) : 0)
          + (terrain === Terrain.Desert ? (abilities[Ability.StrengthBonusInDesert] ?? 0) : 0)
          + (terrain === Terrain.Ice ? (abilities[Ability.StrengthBonusOnIce] ?? 0) : 0);
      }
    }

    // Blessed bonus
    strength += blessed ? 1 : 0;

    // Attacking / defending bonus
    if (attacking) {
      strength += abilities[Ability.StrengthBonusWhenAttacking] ?? 0;
    } else {
      strength += abilities[Ability.StrengthBonusWhenDefending] ?? 0;
    }

    // Group bonuses
    strength = strength
      + (groupAbilities[Ability.FortificationBonus] ?? 0)
      + (groupAbilities[Ability.MoraleBonus] ?? 0)
      + (hero ? 0 : (groupAbilities[Ability.Leadership] ?? 0))
      - (hero ? 0 : (enemyGroupAbilities[Ability.FrighteningBonus] ?? 0))
      - (abilities[Ability.Flies] ? (enemyGroupAbilities[Ability.AntiAirBonus] ?? 0) : 0);

    // Strength limitation on ship
    if (terrain === Terrain.Water && !abilities[Ability.Flies] && strength > maxStrengthOnShip) {
      strength = maxStrengthOnShip;
    }

    // Minimal an maximal limit
    effectiveStrength[id] = clamp(1, maxStrengthOverall, strength);
  }

  return effectiveStrength;
}

function calculateEffectiveAmbushForOneGroup(armies: Army[], groupAbilities: Abilities, enemyGroupAbilities: Abilities,
  heroes: { [armyId: string]: Hero }): { [armyId: string]: number } {

  const effectiveAmbush: { [armyId: string]: number } = {};
  for (const { id, armyTypeId } of armies) {
    let chance = standardArmyTypes[armyTypeId].hero
      ? getHerosValueOfAbility(heroes[id], Ability.Ambush)
      : (standardArmyTypes[armyTypeId].abilities[Ability.Ambush] ?? 0);

    if (chance > 0) {
      chance += (groupAbilities[Ability.IncreaseGroupAmbush] ?? 0);
      chance *= Math.floor(100 - (enemyGroupAbilities[Ability.LowerEnemyAmbush] ?? 0)) / 100;
    }

    effectiveAmbush[id] = chance;
  }
  return effectiveAmbush;
}

function calculateEffectiveHitPointsForOneGroup(armies: Army[], heroes: { [armyId: string]: Hero }): { [armyId: string]: number } {

  const effectiveHitPoints: { [armyId: string]: number } = {};
  for (const army of armies) {
    effectiveHitPoints[army.id] = standardArmyTypes[army.armyTypeId].hero
      ? getHerosHitPoints(heroes[army.id])
      : standardArmyTypes[army.armyTypeId].hitPoints;
  }
  return effectiveHitPoints;
}

export function calculateExperiencesForKillingArmies(armies: Army[]): number {
  return armies
    .map(army => standardArmyTypes[army.armyTypeId].buildingCost)
    .reduce((sum, value) => sum + value, 0);
}
