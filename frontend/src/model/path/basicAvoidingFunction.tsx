import { GameState } from '../game/types';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { BasicAvoidingStrategy } from 'ai-interface/types/path';
import { createArray } from '../../functions';
import { maxPlayers } from 'ai-interface/constants/player';

export function createBasicAvoidingFunction(gameState: GameState, groupSize: number, strategy: BasicAvoidingStrategy): (row: number, col: number) => boolean {
  const { tiles, fogOfWar, groups, cities, flaggables, currentPlayerId, players } = gameState;
  const maxAlliedGroupSize = maxTileCapacity - groupSize;
  const currentTeamId = players[currentPlayerId].team;

  const avoidPlayer = createArray(maxPlayers + 1, playerId =>
    (strategy === 'GO_AROUND_ALLIES_AND_ENEMIES' && playerId !== currentPlayerId) ||
    (strategy === 'GO_AROUND_ALLIES' && playerId !== currentPlayerId && players[playerId].team === currentTeamId)
  );

  return (row: number, col: number) => {
    const { armies, flaggableId, cityId } = tiles[row][col];

    if (armies) {
      const { owner } = groups[armies[0].groupId];

      // Avoid your armies only if total army size exceeds maximum tile capacity
      if (owner === currentPlayerId) {
        return armies.length > maxAlliedGroupSize;
      }

      // Avoid other player's armies only if they are visible
      if (avoidPlayer[owner] && !fogOfWar[row][col]) {
        return true;
      }
    }

    // Avoid other player's cities if they are not razed
    if (cityId !== undefined) {
      const { owner, lastSeenRazed } = cities[cityId];
      return avoidPlayer[owner] && !lastSeenRazed[currentTeamId];
    }

    // Avoid other player's flaggables
    if (flaggableId !== undefined) {
      const { owner } = flaggables[flaggableId];
      return avoidPlayer[owner];
    }

    return false;
  };
}
