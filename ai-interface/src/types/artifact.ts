import { ArtifactAbility } from '../constants/artifactAbility';

/**
 * Class Artifact acts as an enum. There is only one instance of each existing Artifact. You should refer to them only by their ids.
 * However, your hero can have same artifact multiple times.
 */
export interface Artifact {

  /** Unique identifier of the artifact type. Two artifacts with same id are interchangeable. */
  readonly id: string;

  /** Name of the artifact. Not relevant for gameplay. */
  readonly name: string;

  /** 1 - Common, 2 - Minor, 3 - Major, 4 - Relic */
  readonly rarity: number;

  /** What bonuses and penalties this artifact bestows. */
  readonly abilities: Partial<{ [key in ArtifactAbility]: number }>;
}
