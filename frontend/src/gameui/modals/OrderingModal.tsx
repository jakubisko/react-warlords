import './OrderingModal.scss';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal } from 'react-bootstrap';
import { defaultArmyTypeIdToOrder } from '../../model/game/constants';
import ArmyImage from '../common/ArmyImage';
import { setStandardArmyTypeIdToOrder } from '../../model/game/actionsPlaying';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import QuestionModal from './QuestionModal';

interface OrderingModalProps {
  onClose: () => void;
}

export default function OrderingModal({ onClose }: OrderingModalProps) {
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const armyTypeIdToOrder = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].armyTypeIdToOrder);
  const dispatch: AppDispatch = useDispatch();

  const [items, setItems] = useState(Object.keys(standardArmyTypes)
    .map(armyTypeId => standardArmyTypes[armyTypeId])
    .filter(armyType => !armyType.hero || armyType.id === 'hro1')
    .sort((a, b) => armyTypeIdToOrder[a.id] - armyTypeIdToOrder[b.id]) // Sort by the player's ordering.
  );
  const [selectedIndex, setSelectedIndex] = useState<number | undefined>(undefined);

  const [showingDiscardQuestion, setShowingDiscardQuestion] = useState(false);

  function handleResetReordering() {
    setItems(Object.keys(standardArmyTypes)
      .map(armyTypeId => standardArmyTypes[armyTypeId])
      .filter(armyType => !armyType.hero || armyType.id === 'hro1')
      .sort((a, b) => defaultArmyTypeIdToOrder[a.id] - defaultArmyTypeIdToOrder[b.id])
    );
  }

  function handleSaveReordering() {
    const items2 = [...items];
    items2.splice(
      items.findIndex(armyType => armyType.id === 'hro1'),
      1,
      ...Object.values(standardArmyTypes).filter(armyType => armyType.hero).sort()
    );

    const ordering = items2.reduce((res, { id }, index) => { res[id] = index; return res; }, {} as { [armyTypeId: string]: number });
    dispatch(setStandardArmyTypeIdToOrder(currentPlayerId, ordering));
  }

  /** Returns a new array with the item moved to the new position. */
  function arrayMove<T>(array: T[], from: number, to: number): T[] {
    array = [...array];
    array.splice(to < 0 ? array.length + to : to, 0, array.splice(from, 1)[0]);
    return array;
  }

  function handleMoveArmyType(fromIndex: number, toIndex: number) {
    setItems(arrayMove(items, fromIndex, toIndex));
    setSelectedIndex(undefined);
  }

  function handleClose() {
    if (items.every((_, i) => i === 0 || armyTypeIdToOrder[items[i - 1].id] < armyTypeIdToOrder[items[i].id])) {
      onClose();
    } else {
      setShowingDiscardQuestion(true);
    }
  }

  return (
    <Modal className="ordering-modal overflow-hidden" show centered onHide={onClose}>
      <Modal.Header>
        <Modal.Title>Change ordering of your armies</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {items.map(({ id }, index) => (
          <span key={id} className="m-1 position-relative">
            <ArmyImage
              armyTypeId={id}
              selectable={true}
              selected={index === selectedIndex}
              onLeftClick={() => {
                if (selectedIndex === undefined) {
                  setSelectedIndex(index);
                } else {
                  handleMoveArmyType(selectedIndex, index);
                }
              }}
              onRightClick="INFO"
            />
          </span>
        ))}

        {showingDiscardQuestion &&
          <QuestionModal
            confirmString="Save changes"
            rejectString="Discard changes"
            onConfirm={() => { handleSaveReordering(); onClose(); }}
            onReject={onClose}
          >
            Would you like to save your changes before leaving?
          </QuestionModal>
        }
      </Modal.Body>

      <Modal.Footer>
        <Button
          disabled={items.every((_, i) => i === 0 || defaultArmyTypeIdToOrder[items[i - 1].id] < defaultArmyTypeIdToOrder[items[i].id])}
          onClick={handleResetReordering}
        >
          Reset
        </Button>
        <Button
          disabled={items.every((_, i) => i === 0 || armyTypeIdToOrder[items[i - 1].id] < armyTypeIdToOrder[items[i].id])}
          onClick={handleSaveReordering}
        >
          Save changes
        </Button>
        <Button onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
