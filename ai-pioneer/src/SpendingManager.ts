import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { CityManager } from './CityManager';
import { Personality } from './helpers/Personality';
import { DONT_BUY, DONT_DISMISS_ARMIES, PRETEND_ITS_TURN_NUMBER } from './main';
import { ThreatManager } from './ThreatManager';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { straightMoveCost } from 'ai-interface/constants/path';
import { GuardManager } from './GuardManager';
import { Army } from 'ai-interface/types/army';
import { Place } from 'ai-interface/types/place';
import { PathingManager } from './PathingManager';
import { cityIncome } from 'ai-interface/constants/city';
import { clamp } from './helpers/functions';

export class SpendingManager {

  private ignoreDismissingToday = false;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly threatManager: ThreatManager,
    private readonly guardManager: GuardManager,
    private readonly pathingManager: PathingManager,
    private readonly cityManager: CityManager,
    private readonly personality: Personality
  ) { }

  public setIgnoreDismissingToday(ignoreDismissingToday: boolean) {
    this.ignoreDismissingToday = ignoreDismissingToday;
  }

  public getMaximalUpkeepForGuarding(): number {
    const turnNumber = PRETEND_ITS_TURN_NUMBER ?? this.game.getTurnNumber();
    return this.personality.partOfIncomeSpentOnArmiesGuarding
      * Math.min(1, (turnNumber + 2) / 14) // At the start of the game the income is higher than armies produced, so guarding according to the income is bad
      * this.game.getIncome();
  }

  public getMaximalDesiredUpkeepForArmies(): number {
    const { partOfIncomeSpentOnArmies } = this.personality;
    const income = this.game.getIncome();
    const maximalDesiredUpkeep = Math.floor((income - this.game.getUpkeepOfArmiesCreatedTomorrow()) * partOfIncomeSpentOnArmies);

    // When we have a small income (start of the game or forced to defend), we're willing to buy any army we possibly can.
    if (maximalDesiredUpkeep <= 2.5 * cityIncome) {
      return income;
    }

    return maximalDesiredUpkeep;
  }

  public checkHeroOffer() {
    if (DONT_BUY) {
      return;
    }

    const heroOffer = this.game.getHeroOffer();
    if (heroOffer === null) {
      this.draw.drawTextWhenNothingIsSelected('No hero offer this turn');
      return;
    }

    const { cost, cityId, armyTypeIds } = heroOffer;
    const myArmies = this.game.getAllMyArmies();

    // We'll buy a hero only to a city where he won't immediately die.
    const city = this.game.getCity(cityId);
    if (this.guardManager.getAdditionalGuardPowerNeededForCity(city) > 0) {
      return;
    }

    // Since having multiple heroes is detrimental, don't buy additional hero unless we need additional spaces for picking artifacts
    // or have high level heroes. Usually this will lead to playing on one hero, which is fine.
    if (myArmies.filter(army => standardArmyTypes[army.armyTypeId].hero)
      .map(army => this.game.getHero(army.id))
      .some(({ level, equippedArtifactIds }) => level + equippedArtifactIds.length < 8)) { // TODO - magical constant which could go to personality
      return;
    }

    const { name, row, col } = this.game.getCity(cityId);
    const text = armyTypeIds.map(armyTypeId => standardArmyTypes[armyTypeId].name).join(', ') + ` for ${cost} gold in ${name} [${row},${col}].`;
    try {
      this.game.acceptHeroOffer();
      this.draw.drawTextWhenNothingIsSelected(`Recruited ${text}`);
    } catch (e) {
      // It seems that we can't accept the offer. Maybe the city is full...
      this.draw.drawTextWhenNothingIsSelected(`Was interested in recruiting but failed to recruit ${text}`);
    }
  }

  /** Buys a new building if there is some gold left and the utility of the building is good. */
  public buyBuilding() {
    const goldForBuilding = clamp(0, this.game.getGold(),
      this.game.getGold()
      + this.game.getIncome() / 2
      - this.game.getUpkeepOfExistingArmies()
      - this.game.getUpkeepOfArmiesCreatedTomorrow()
    );
    this.draw.drawTextWhenNothingIsSelected('Gold allocated for building: ' + goldForBuilding + 'g');

    if (goldForBuilding > 0) {
      const bestTask = this.cityManager.findBuildTaskWithHighestUtility(goldForBuilding);

      if (!DONT_BUY) {
        const { utility, price, action, description } = bestTask;
        if (utility / price >= 1) { // TODO - magical constant; and a shitty strategy - he tried to build 18% on turn 3
          action();
          this.draw.drawTextWhenNothingIsSelected('Today we built: ' + description);
        }
      }
    }
  }

  /**
   * It's better to dismiss armies manually and decide which we want rather than leaving it to the game mechanisms.
   * Given how much we we want to spend, this will dismiss armies to meet that budget.
   * Groups are dismissed based on their distance to the enemy - the most distant are dismissed.
   * 1. When dismissing armies in a threatened city, dismiss only armies that are over the required guard size.
   * 2. Dismiss groups with heroes only as the last resort.
   */
  public dismissUselessArmiesUntilUpkeepRemains(targetUpkeep: number) {
    let currentUpkeep = this.game.getUpkeepOfExistingArmies();
    if (DONT_DISMISS_ARMIES || this.ignoreDismissingToday || currentUpkeep <= targetUpkeep) {
      return;
    }

    this.personality.upkeepOfArmiesDismissedLastTurn = 0;

    const groupsWithDist: {
      groupId: string;
      armies: Army[];
      place: Place;
      expendable: number; // Higher is worse
    }[] = [];

    for (const groupId of this.game.getAllMyGroupIds()) {
      const armies = this.game.getArmiesInGroup(groupId);
      const place = this.game.getPlaceOfGroup(groupId);
      let expendable = (armies.every(army => !standardArmyTypes[army.armyTypeId].hero))
        ? (this.pathingManager.calculateFlyMovesToNearestEnemy(place) / straightMoveCost)
        : 0;
      groupsWithDist.push({ groupId, armies, place, expendable });
    }

    groupsWithDist.sort((g1, g2) => g2.expendable - g1.expendable);

    let count = 0;

    for (const { groupId, armies, place, expendable } of groupsWithDist) {
      const text = '' + Math.floor(expendable);
      this.draw.drawTextWhenNothingIsSelected(text, { row: place.row + 0.5, col: place.col + 0.5 });

      let guardPowerNeeded = this.guardManager.getPreferredGuardPowerForPlace(place);

      const countBefore = count;
      for (const { id, armyTypeId } of armies) {
        if (currentUpkeep - this.personality.upkeepOfArmiesDismissedLastTurn > targetUpkeep) {
          if (guardPowerNeeded > 0) {
            guardPowerNeeded -= this.threatManager.armyTypeIdToGuardPower[armyTypeId];
          } else {
            this.game.dismissArmy(groupId, id);
            this.personality.upkeepOfArmiesDismissedLastTurn += standardArmyTypes[armyTypeId].upkeep;
            count++;
          }
        }
      }

      if (countBefore !== count) {
        this.draw.drawTextWhenNothingIsSelected('Dismiss', place);
      }
    }

    this.draw.drawTextWhenNothingIsSelected(`Armies dismissed to bring upkeep down to ${targetUpkeep} gold: ${count}`);
  }

}
