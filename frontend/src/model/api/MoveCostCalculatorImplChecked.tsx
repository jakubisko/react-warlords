import { MoveCostCalculator } from 'ai-interface/apis/MoveCostCalculator';
import { MoveCostCalculatorImplUnchecked } from '../path/MoveCostCalculatorImplUnchecked';
import { GameState } from '../game/types';
import { IllegalPlace } from 'ai-interface/types/obstacle';
import { Abilities } from 'ai-interface/types/army';

export class MoveCostCalculatorImplChecked implements MoveCostCalculator {

  private readonly moveCostCalculator: MoveCostCalculatorImplUnchecked;
  private readonly size: number;

  constructor(gameState: GameState, abilities: Abilities) {
    this.moveCostCalculator = new MoveCostCalculatorImplUnchecked(gameState, { ...abilities });
    this.size = gameState.size;
  }

  private assertLegalPlace(row: number, col: number) {
    if (row % 1 !== 0 || col % 1 !== 0) {
      throw new IllegalPlace(`Place [${row}, ${col}] has coordinates that are not integers`);
    } else if (row < 0 || row >= this.size || col < 0 || col >= this.size) {
      throw new IllegalPlace(`Place [${row}, ${col}] is outside of the land boundaries`);
    }
  }

  getMoveCostAt(row: number, col: number): number {
    this.assertLegalPlace(row, col);
    return this.moveCostCalculator.getMoveCostAt(row, col);
  }

  isCoastCrossingForbiddenAt(row1: number, col1: number, row2: number, col2: number): boolean {
    this.assertLegalPlace(row1, col1);
    this.assertLegalPlace(row2, col2);
    return this.moveCostCalculator.isCoastCrossingForbiddenAt(row1, col1, row2, col2);
  }

  isBoardingDeboardingShipAt(row1: number, col1: number, row2: number, col2: number): boolean {
    this.assertLegalPlace(row1, col1);
    this.assertLegalPlace(row2, col2);
    return this.moveCostCalculator.isBoardingDeboardingShipAt(row1, col1, row2, col2);
  }

  isShipRequiredAt(row: number, col: number): boolean {
    this.assertLegalPlace(row, col);
    return this.moveCostCalculator.isShipRequiredAt(row, col);
  }

}
