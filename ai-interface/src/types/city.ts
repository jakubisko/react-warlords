export interface CityData {

  /** Unique id. */
  readonly id: string;

  /** Index of the player owning the city. Equals to "neutralPlayerId" for neutral city. */
  readonly owner: number;

  /** City's name. Not relevant for gameplay. */
  readonly name: string;

  /** Position of the top left corner of the city. */
  readonly row: number;

  /** Position of the top left corner of the city. */
  readonly col: number;

  /** Capitol has double city's income and double fortification bonus. */
  readonly capitol: boolean;

  /** Whether the city is razed. */
  readonly razed: boolean;

  /** List of ArmyTypes that can be currently trained without additional building. Ordered from the weakest to the strongest. */
  readonly currentlyTrainableArmyTypeIds: string[];

  /** Id of ArmyType that is currently being trained. Undefined if nothing is being trained. */
  readonly currentlyTrainingArmyTypeId?: string;

  /** How many turns have to pass until the next army is trained. Undefined if nothing is being trained. */
  readonly turnsTrainingLeft?: number;

  /** Owner may redirect army training to another city. If undefined, armies trained will be created in this city. */
  readonly caravanToCityId?: string;

  /** List of armies that will soon arrive in this city with caravan. If the owner of the city changes, these armies will be lost. */
  readonly incomingCaravans: IncomingCaravan[];
}

/** An army that travels using the caravan and will be created in a few days. */
export interface IncomingCaravan {

  /** Army's type defines strength, abilities etc. */
  readonly armyTypeId: string;

  /** How many extra days for this army to arrive. Minimal value is 1; such army will arrive at the start of the owner's next turn. */
  readonly days: number;
}
