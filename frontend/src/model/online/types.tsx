import { Timestamp } from 'firebase/firestore';

/**
 * This state contains only data that are related to online play.
 * None of it matters to AI. None of it is saved when land or game is saved.
 */
export interface OnlineState {
  readonly onlineGameId: string | null; // Determines whether you are playing offline or online. Guaranteed to be null when playing offline.
  //                                    // Identifies the game in the Firebase's database when playing online.
  readonly onlineTimesLoaded: number;   // See OnlineGame.timesLoaded. Prevents loading the game in two tabs when playing online.
  readonly onlineToken: string;         // See OnlineGame.token. Prevents saving from two tabs when playing online.
  readonly loggedInUser: OnlineUser | null; // User logged in for online play.
}

export interface OnlineGame {
  readonly id: string; // OnlineGame uses automatically generated id. Same id is used for serialized state.
  readonly createdAt: Timestamp; // Timestamp when the game was created.
  readonly buildVersion: string; // Player won't be able to join the game if their build version differs.
  readonly title: string; // User readable title of the game.
  readonly gameOver: boolean;
  readonly currentPlayerId: number;
  readonly timesLoaded: number; // See UiState.onlineTimesLoaded.
  //                            // How many times a client downloaded this particular game state from server. Resets to 0 each turn.
  //                            // Prevents loading the game in two tabs at once. Prevents cheating by refreshing a browser and then starting the turn again.
  //                            // Only the client that loaded first will be allowed to play.
  readonly token: string; // See UiState.onlineToken.
  //                      // Prevents submitting to the server from two tabs; for example by opening DuplicateStartOfTurnScreen and ending the turn twice.
  //                      // When a client tries to push a new game state to the server, he must check whether the current token on
  //                      // the server matches the one when the game was loaded.
  readonly userUids: string[]; // Uid key to "users". Empty string when that player is not a human user. Length is equal to the maxPlayers.
  readonly aiNames: string[]; // User displayable aiName. Empty string when that player is not a computer. Length is equal to the maxPlayers.
  readonly alive: boolean[]; // Whether the player is alive or not. Length is equal to the maxPlayers. Length is equal to the maxPlayers.
}

/** Deliberately not part of the OnlineGame to decrease the network usage when requesting list of all games. */
export interface OnlineGameState {
  readonly stateSerialized: string;
}

export interface OnlineUser {
  readonly uid: string;
  readonly photoURL: string;
  readonly displayName: string;
  readonly email: string;
}
