import { AppDispatch, RootState } from '../root/configureStore';
import { getVisibleArmiesDefendingPlace, getArmiesAtPlaceOfGroupOrderedByGroups } from '../game/selectorsPlaying';
import { Place } from 'ai-interface/types/place';
import { addKillsLosses, addWarReportItem } from '../game/actionsPlaying';
import { getCombatConditions } from './selectors';
import { addExperiences } from '../game/actionsHero';
import { CombatResult, CombatEventType, CombatInputs, CombatPrediction } from 'ai-interface/types/combat';
import { experiencePercentForAttackingCity } from 'ai-interface/constants/experience';
import { WarReportType } from 'ai-interface/types/warReport';
import { calculateEffectiveValues, calculateCombatEvents, calculateExperiencesForKillingArmies } from './combatCalculation';
import { Army } from 'ai-interface/types/army';
import { destroyArmy } from '../game/actionsPlayingThunks';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';

export enum ActionTypes {
  SET_COMBAT_INPUTS = '[Combat] SET_COMBAT_INPUTS',
  SET_COMBAT_RESULT = '[Combat] SET_COMBAT_RESULT'
}

export interface SetCombatInputs {
  type: ActionTypes.SET_COMBAT_INPUTS;
  payload: {
    combatInputs: CombatInputs | null;
  }
}

export interface SetCombatResult {
  type: ActionTypes.SET_COMBAT_RESULT;
  payload: {
    combatResult: CombatResult | null;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

/**
 * Calculates probable result of combat based on multiple simulations.
 * All relevant bonuses, such as artifacts and fortification, are included in the calculation.
 */
export function predictCombat(state: RootState, attackingArmies: Army[], defendingPlace: Place): CombatPrediction {
  const { heroes } = state.game;

  const defendingArmies = getVisibleArmiesDefendingPlace(state, defendingPlace);

  // For combat prediction, use pseudo-random so that the result is consistent on multiple calls.
  let randomSeed = 1;
  const pseudoRandom = () => {
    randomSeed = randomSeed * 16807 % 2147483647;
    return (randomSeed % 1000) / 1000;
  };

  // Simulate combat multiple times
  const times = 500;

  const survives: { [armyId: string]: number } = {};
  [...attackingArmies, ...defendingArmies].forEach(army => survives[army.id] = times);

  const conditions = getCombatConditions(state, defendingPlace);
  const effectiveValues = calculateEffectiveValues(attackingArmies, defendingArmies, conditions, heroes);

  for (let i = 0; i < times; i++) {
    const combatEvents = calculateCombatEvents(attackingArmies, defendingArmies, effectiveValues, pseudoRandom);
    for (const { type, losingArmyId } of combatEvents) {
      if (type === CombatEventType.Ambush || type === CombatEventType.Kill) {
        survives[losingArmyId]--;
      }
    }
  }

  return {
    attackingArmies: attackingArmies.map(army => ({
      armyTypeId: army.armyTypeId,
      probabilityOfSurvival: survives[army.id] / times
    })),
    defendingArmies: defendingArmies.map(army => ({
      armyTypeId: army.armyTypeId,
      probabilityOfSurvival: survives[army.id] / times
    }))
  };
}

/**
 * Executes the combat in-memory; without animation and applying the results in-game. To get entire combat,
 * 1. Call attackTile() to calculate result of combat.
 * 2. Call showCombatModal() to display animation in CombatModal.
 * 3. After animation ends, call applyCombatResult() to apply combat on game state. Don't do it before - defeated army would disappear from
 *    the map before animation would even start.
 * 
 * You CAN call this to attack a place which contains no armies. If so, this will set combatInputs and combatResult to null.
 */
export const attackTile = (attackingGroupId: string, defendingRow: number, defendingCol: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {

    // Collect combatInputs - data about armies entering combat, terrain etc.
    const state = getState();
    const { groups, heroes, currentPlayerId, players } = state.game;

    const place: Place = { row: defendingRow, col: defendingCol };
    const defendingArmies = getVisibleArmiesDefendingPlace(state, place, currentPlayerId);
    if (defendingArmies.length === 0) {
      dispatch(setCombatInputs(null));
      dispatch(setCombatResult(null));
      return;
    }

    const conditions = getCombatConditions(state, place);
    const attackingArmies = getArmiesAtPlaceOfGroupOrderedByGroups(state, attackingGroupId).filter(army => army.groupId === attackingGroupId);
    const combatInputs: CombatInputs = {
      conditions: getCombatConditions(state, place),
      attackingArmies,
      defendingArmies,
      attackingPlayerId: groups[attackingArmies[0].groupId].owner,
      defendingPlayerId: groups[defendingArmies[0].groupId].owner
    };
    dispatch(setCombatInputs(combatInputs));

    // For true combat, use best random generator available - crypto. It's little slower, but Math.random() produces many bizarre combats.
    const random = () => {
      const typedArray = new Uint32Array(1);
      const crypto = typeof window !== 'undefined' ? window.crypto : self.crypto; // window doesn't exist in web worker
      crypto.getRandomValues(typedArray);
      return typedArray[0] / (0xffffffff + 1);
    };

    // Collect combatResult - values of abilities, effective strength etc.
    const effectiveValues = calculateEffectiveValues(attackingArmies, defendingArmies, conditions, heroes);
    const combatEvents = calculateCombatEvents(attackingArmies, defendingArmies, effectiveValues, random);
    const lastKilledArmyId = combatEvents[combatEvents.length - 1].losingArmyId;
    const attackerWon = defendingArmies.some(army => army.id === lastKilledArmyId);
    const experiences = players[combatInputs.attackingPlayerId].team === players[combatInputs.defendingPlayerId].team ? 0 : Math.floor(
      calculateExperiencesForKillingArmies(attackerWon ? defendingArmies : attackingArmies)
      * (attackerWon ? (100 + experiencePercentForAttackingCity * conditions.fortification) : 100) / 100
    );

    const combatResult = {
      attackingAbilities: effectiveValues.attackingAbilities,
      defendingAbilities: effectiveValues.defendingAbilities,
      effectiveStrength: effectiveValues.effectiveStrength,
      combatEvents,
      experiences,
      attackerWon
    };

    dispatch(setCombatResult(combatResult));
  };

/**
 * Apply combat results on the game data model (destroys armies, awards experiences).
 * Call this AFTER moving the attacking army to the place of combat. That way the defeated attacker drops artifacts on the place of combat.
 * Call this AFTER the animation ends. Otherwise, the defeated army would disappear from the map before animation even starts.
 */
export const applyCombatResult = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { combatInputs, combatResult } = getState().combat;
  const { combatEvents, experiences, attackerWon } = combatResult!;
  const { attackingPlayerId, defendingPlayerId, attackingArmies, defendingArmies, conditions } = combatInputs!;

  let attackingArmiesLost = 0;
  let defendingArmiesLost = 0;
  let attackingCostLost = 0;
  let defendingCostLost = 0;

  for (const combatEvent of combatEvents) {
    const { type, losingGroupId, losingArmyId } = combatEvent;
    if (type === CombatEventType.Ambush || type === CombatEventType.Kill) {
      let armyLost = attackingArmies.find(army => army.id === losingArmyId);
      if (armyLost !== undefined) {
        attackingArmiesLost++;
        attackingCostLost += standardArmyTypes[armyLost.armyTypeId].buildingCost;
      } else {
        armyLost = defendingArmies.find(army => army.id === losingArmyId)!;
        defendingArmiesLost++;
        defendingCostLost += standardArmyTypes[armyLost.armyTypeId].buildingCost;
      }

      dispatch(destroyArmy(losingGroupId, losingArmyId));
    }
  }

  const { heroes } = getState().game; // This needs to be called after armies are destroyed
  const livingHeroes = (attackerWon ? attackingArmies : defendingArmies).filter(army => army.id in heroes);
  if (livingHeroes.length > 0) {
    const portion = Math.floor(experiences / livingHeroes.length);
    livingHeroes.forEach(army => dispatch(addExperiences(army.id, portion)));
  }

  let { row, col } = conditions;
  const { tiles, cities } = getState().game;
  const { cityId } = tiles[row][col];
  if (cityId !== undefined) {
    row = cities[cityId].row;
    col = cities[cityId].col;
  }

  dispatch(addWarReportItem(defendingPlayerId, {
    row,
    col,
    reportType: WarReportType.ArmiesLostInCombat,
    value: defendingArmiesLost,
    playerId: attackingPlayerId
  }));
  dispatch(addWarReportItem(defendingPlayerId, {
    row,
    col,
    reportType: WarReportType.EnemyArmiesKilled,
    value: attackingArmiesLost,
    playerId: attackingPlayerId
  }));
  dispatch(addKillsLosses(attackingPlayerId, defendingCostLost, attackingCostLost));
  dispatch(addKillsLosses(defendingPlayerId, attackingCostLost, defendingCostLost));
};

const setCombatInputs = (combatInputs: CombatInputs | null): SetCombatInputs => ({
  type: ActionTypes.SET_COMBAT_INPUTS,
  payload: {
    combatInputs
  }
});

const setCombatResult = (combatResult: CombatResult | null): SetCombatResult => ({
  type: ActionTypes.SET_COMBAT_RESULT,
  payload: {
    combatResult
  }
});

export type CombatAction = SetCombatInputs | SetCombatResult;
