import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { combatReducer } from '../combat/reducers';
import { editorReducer } from '../editor/reducers';
import { exploringReducer } from '../exploring/reducers';
import { gameReducer } from '../game/reducersPlaying';
import { onlineReducer } from '../online/reducers';
import { pathReducer } from '../path/reducers';
import { soundReducer } from '../sound/reducers';
import { uiReducer } from '../ui/reducers';
import { CombatAction } from '../combat/actions';
import { EditorUiAction } from '../editor/actions';
import { ExploringAction } from '../exploring/actions';
import { HeroAction } from '../game/actionsHero';
import { TurnPassingAction } from '../game/actionsTurnPassing';
import { LandAction } from '../game/actionsEditor';
import { OnlineAction } from '../online/actions';
import { PathAction } from '../path/actions';
import { SoundAction } from '../sound/actions';
import { UiAction } from '../ui/actions';
import { PlayingAction } from '../game/actionsPlaying';

const rootReducer = combineReducers({
  combat: combatReducer,
  editor: editorReducer,
  exploring: exploringReducer,
  game: gameReducer,
  online: onlineReducer,
  path: pathReducer,
  sound: soundReducer,
  ui: uiReducer
});
export type AppAction = CombatAction
  | EditorUiAction
  | HeroAction
  | TurnPassingAction
  | LandAction
  | ExploringAction
  | OnlineAction
  | PathAction
  | PlayingAction
  | SoundAction
  | UiAction;

export const storeWithUi = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => getDefaultMiddleware({
    thunk: true,
    immutableCheck: false,
    serializableCheck: false,
    actionCreatorCheck: false,
  }),
  devTools: import.meta.env.NODE_ENV !== 'production',
});
export const storeWithoutUi = storeWithUi; // TODO

// export type RootState = ReturnType<typeof rootReducer>;
export type RootState = ReturnType<typeof storeWithUi.getState>;
export type AppDispatch = typeof storeWithUi.dispatch;
