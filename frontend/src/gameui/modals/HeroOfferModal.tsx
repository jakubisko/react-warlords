import './HeroOfferModal.scss';

import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Modal, Button } from 'react-bootstrap';
import { setShowingHeroOffer, setMapFocus, setShowingWarReport } from '../../model/ui/actions';
import { acceptHeroOffer } from '../../model/game/actionsPlayingThunks';
import StrategicMap from '../map/strategic-map/StrategicMap';
import { getNumberOfArmiesPresentInCity } from '../../model/game/selectorsPlaying';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { HeroPortrait } from '../common/HeroPortrait';
import heroNames from '../../predefined-data/hero-names.json';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { setSoundSource } from '../../model/sound/actions';
import { EMPTY_ARRAY } from '../../functions';

export default function HeroOfferModal() {
  const { armyTypeIds, portraitId, cost } = useSelector((state: RootState) => state.game.heroOffer!);
  const city = useSelector((state: RootState) => state.game.cities[state.game.heroOffer!.cityId]);
  const numberOfArmiesPresentInCity = useSelector((state: RootState) => getNumberOfArmiesPresentInCity(state, state.game.heroOffer!.cityId));
  const gold = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].gold);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    soundEffects.play(SoundEffect.HeroOffer);
  }, EMPTY_ARRAY);

  function showCity() {
    const { row, col } = city;

    dispatch(setMapFocus(row + 1, col + 1));
    dispatch(setSoundSource(row + 1, col + 1));
    dispatch(setShowingHeroOffer(false));
    dispatch(setShowingWarReport(false)); // Multiple reports may be open at the start of the turn. We need to close them all.
  }

  function acceptOffer() {
    const { row, col } = city;

    dispatch(setMapFocus(row + 0.5, col + 0.5)); // Focus the top left tile of the city, not the middle of the city.
    dispatch(setSoundSource(row, col));
    dispatch(acceptHeroOffer());
    dispatch(setShowingHeroOffer(false));
    dispatch(setShowingWarReport(false)); // Multiple reports may be open at the start of the turn. We need to close them all.
  }

  const description = `${heroNames[portraitId]}, a level ${armyTypeIds[0].charAt(3)} hero`
    + `${armyTypeIds.length > 1 ? ` with ${armyTypeIds.length - 1} ${standardArmyTypes[armyTypeIds[1]].name}` : ''}, offers to join you in ${city.name}.`;

  return (
    <Modal className="hero-offer-modal overflow-hidden" animation={false} show centered onHide={() => dispatch(setShowingHeroOffer(false))}>
      <Modal.Header>
        <Modal.Title>
          A Hero!
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="d-flex flex-row justify-content-between mb-2">
          <div className="map-border">
            <StrategicMap
              drawCircle={{ row: city.row + 0.5, col: city.col + 0.5 }}
              onClick={showCity}
            />
          </div>

          <div>
            <div className="portrait-border">
              <HeroPortrait imageId={portraitId} />
            </div>
          </div>
        </div>

        <div>
          {description}
        </div>
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button
          disabled={gold < cost || city.razed || numberOfArmiesPresentInCity + armyTypeIds.length > 4 * maxTileCapacity}
          onClick={acceptOffer}
        >
          {city.razed ? `The city was razed.` : (
            numberOfArmiesPresentInCity + armyTypeIds.length > 4 * maxTileCapacity
              ? `No space available in city`
              : `Recruit for ${cost} gold`
          )}
        </Button>

        <Button onClick={() => dispatch(setShowingHeroOffer(false))}>
          Later
        </Button>
      </Modal.Footer>
    </Modal >
  );
}
