import { Abilities } from './army';

export interface HeroData {

  /** Unique id. */
  readonly id: string;

  /** Id of the hero's current group. */
  readonly groupId: string;

  /** Hero's name. Not relevant for gameplay. */
  readonly name: string;

  /** Hero's current level. */
  readonly level: number;

  /** Hero's current experiences. */
  readonly experience: number;

  /** Experiences required to gain a level. Returns 0 on max level. */
  readonly experienceRemainingToNextLevel: number;

  /** Hero's strength, with artifacts applied. */
  readonly strength: number;

  /** Hero's daily movement, with artifacts applied. */
  readonly move: number;

  /** How many hits it takes to kill this hero in combat, with artifacts applied. */
  readonly hitPoints: number;

  /** Values of other abilities that common armies can have, such as Leadership, AntiAir, NoPenaltyInSwamp etc., with artifacts applied. */
  readonly abilities: Abilities;

  /** List of equipped artifacts. */
  readonly equippedArtifactIds: string[];
}
