import './ScreenOrientationReminder.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUndo } from '@fortawesome/free-solid-svg-icons';

export const ScreenOrientationReminder = () => (
  <div className="screen-orientation-reminder">
    <div>
      <p>Rotate the screen</p>
      <FontAwesomeIcon icon={faUndo} />
    </div>
  </div>
);
