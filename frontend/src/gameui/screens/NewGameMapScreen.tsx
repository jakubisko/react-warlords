import './NewGameMapScreen.scss';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import { ScreenEnum, ProgramMode } from '../../model/ui/types';
import { setScreen } from '../../model/ui/actions';
import LoadButton from '../common/LoadButton';
import { decompressObject } from '../../model/game/serialization';
import { loadMap } from '../../model/game/actionsEditor';
import StrategicMap from '../map/strategic-map/StrategicMap';
import ListOfMapsModal from '../modals/ListOfMapsModal';
import CreateRandomMapModal from '../modals/CreateRandomMapModal';
import { maxPlayers } from 'ai-interface/constants/player';
import { setPlayerAiName, setPlayerOnlineUid } from '../../model/game/actionsTurnPassing';
import { aiFunctions } from '../../listOfAIs';

export default function NewGameMapScreen() {
  const size = useSelector((state: RootState) => state.game.size);
  const players = useSelector((state: RootState) => state.game.players);
  const uidIfOnline = useSelector((state: RootState) => state.online.onlineGameId === null ? null : state.online.loggedInUser?.uid ?? null);
  const dispatch: AppDispatch = useDispatch();

  const [showStrategicMap, setShowStrategicMap] = useState(false);
  const [showListOfMaps, setShowListOfMaps] = useState(false);
  const [showCreateRandomMap, setShowCreateRandomMap] = useState(false);
  const [author, setAuthor] = useState<string>();

  async function handleMapLoad(fileData: ArrayBuffer, author?: string) {
    const mapData = await decompressObject(fileData, 'map');
    dispatch(loadMap(mapData, ProgramMode.Playing));

    setShowStrategicMap(false);
    setAuthor(author);
  }

  function handleNext() {
    // When playing online, we can't start with players set to "Human". Choose some AI for all players.
    if (uidIfOnline !== null) {
      const nameOfSomeAi = Object.keys(aiFunctions)[0];

      for (let playerId = 0; playerId < maxPlayers; playerId++) {
        const { alive, aiName, onlineUid } = players[playerId];
        if (alive && aiName === undefined && onlineUid === undefined) {
          dispatch(setPlayerAiName(playerId, nameOfSomeAi));
          dispatch(setPlayerOnlineUid(playerId, undefined));
        }
      }
    }

    dispatch(setScreen(ScreenEnum.NewGamePlayersScreen));
  }

  return (
    <Modal className="new-game-screen screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>New Game</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="map">
          {size > 0 && (
            showStrategicMap ? <StrategicMap /> : (
              <div
                className="map-hidden w-100 h-100 d-flex align-items-center justify-content-center"
                onClick={() => setShowStrategicMap(true)}
              >
                <div>Click to reveal the map</div>
              </div>
            )
          )}
        </div>

        <div className="right">
          <ButtonGroup vertical>
            <Button onClick={() => setShowListOfMaps(true)}>
              Pick map from list
            </Button>

            <Button onClick={() => setShowCreateRandomMap(true)}>
              Create random map
            </Button>

            <LoadButton
              caption="Load map from file"
              fileExtension="wmap"
              onUpload={handleMapLoad}
            />
          </ButtonGroup>

          <span className="pt-3 d-inline-block">
            {size > 0 ? `${size} x ${size}, ${players.filter(player => player.alive).length} players` : `Please choose a map`}
          </span>

          <span className="pt-3 d-inline-block">
            {author}
          </span>
        </div>

        {showListOfMaps && (
          <ListOfMapsModal
            onClose={() => setShowListOfMaps(false)}
            handleMapLoad={handleMapLoad}
          />
        )}

        {showCreateRandomMap && (
          <CreateRandomMapModal
            afterCreate={() => dispatch(setScreen(ScreenEnum.NewGameMapScreen))}
            onCancel={() => setShowCreateRandomMap(false)}
          />
        )}
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={() => dispatch(setScreen(uidIfOnline === null ? ScreenEnum.MainMenuScreen : ScreenEnum.ListOfOnlineGamesScreen))}>
          Back
        </Button>

        <Button
          disabled={size === 0}
          onClick={handleNext}
        >
          Next
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
