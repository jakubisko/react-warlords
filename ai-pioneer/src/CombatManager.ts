import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { Place } from 'ai-interface/types/place';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { PredictionForArmy, CombatInputsAndResult, CombatEventType } from 'ai-interface/types/combat';
import { ObstacleType } from 'ai-interface/types/obstacle';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { isStructureFlaggable, Structure } from 'ai-interface/constants/structure';

export interface ExpectedUtilityOfCombat {
  loseChance: number; // Chance that we will die from 0 to 1.
  expectedMaterialUtility: number; // Expected utility of this combat calculated in gold. Positive is material gain on our side, negative is material loss on our side.
  situationalUtility: number; // Bonus utility based on a surrounding situation, such as killing an enemy scout.
}

export class CombatManager {

  private size: number;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi
  ) {
    this.size = land.getSize();
  }

  private expectedMaterialUtilityLossOfArmy(armies: PredictionForArmy[]): number {
    let result = 0;

    for (const { armyTypeId, probabilityOfSurvival } of armies) {
      result += (1 - probabilityOfSurvival) * standardArmyTypes[armyTypeId].buildingCost;
    }

    return result / 3;
  }

  public predictExpectedUtilityOfCombat(groupId: string, place: Place, owner: number): ExpectedUtilityOfCombat | null {
    try {
      const { attackingArmies, defendingArmies } = this.game.predictCombatIfGroupAttacks(groupId, place);
      const loseChance = defendingArmies[0].probabilityOfSurvival;

      const text = Math.round(100 - 100 * loseChance) + '%';
      this.draw.drawTextWhenGroupIsSelected(text, groupId, { row: place.row - 0.5, col: place.col });

      return {
        loseChance,
        expectedMaterialUtility: this.expectedMaterialUtilityLossOfArmy(defendingArmies)
          - this.expectedMaterialUtilityLossOfArmy(attackingArmies),
        situationalUtility: this.calculateSituationalUtility(groupId, place, owner, attackingArmies, defendingArmies, loseChance)
      };
    } catch (e: any) {
      if (e.type === ObstacleType.NO_ARMIES_PRESENT) {
        // An unguarded enemy city, razed enemy city or flaggable structure.
        return null;
      }
      throw e;
    }
  }

  private calculateSituationalUtility(groupId: string, enemyPlace: Place, owner: number, attackingArmies: PredictionForArmy[],
    defendingArmies: PredictionForArmy[], loseChance: number): number {
    let result = 0;

    // With a hero, we want to be more conservative when attacking strong armies and more aggressive when attacking weak armies (farming).
    if (attackingArmies.some(army => standardArmyTypes[army.armyTypeId].hero)) {
      if (loseChance < 0.1) {
        const exp = defendingArmies.reduce((sum, { armyTypeId }) => sum + standardArmyTypes[armyTypeId].buildingCost, 0);
        result += exp / 10 * (1 - 10 * loseChance);
      } else if (loseChance > 0.25) {
        const level = parseInt(attackingArmies.find(army => standardArmyTypes[army.armyTypeId].hero)!.armyTypeId.charAt(3));
        result -= loseChance * level * 100;
      }
    }

    // We used to have a problem that the AI ignored lone enemy scouts because there was always something more important to do.
    // They however snatched all unguarded flaggables. The AI would flag them back, but because the scouts were unharmed, they would steal it again.
    if (owner !== neutralPlayerId // Neutral player doesn't have scouts
      && loseChance <= 0.04 // The victory is guaranteed.
      && this.land.getFlaggableIdAt(enemyPlace) === undefined // Enemy isn't standing directly at flaggable. If it were, then utility would be high enough for standard algorithm anyway.
    ) {
      const near = 5;
      const { row: row1, col: col1 } = this.game.getPlaceOfGroup(groupId);
      const { row: row2, col: col2 } = enemyPlace;

      // We're next to a scout. It's not worth to chase scouts; they have high mobility anyway.
      if ((row1 - row2) ** 2 + (col1 - col2) ** 2 <= near ** 2) {
        const structures = this.land.getStructures();

        for (let r = Math.max(0, row2 - near); r <= Math.min(row2 + near, this.size - 1); r++) {
          for (let c = Math.max(0, col2 - near); c <= Math.min(col2 + near, this.size - 1); c++) {
            const there: Place = { row: r, col: c };
            const structure = structures[r][c];
            if (structure === Structure.Temple || structure === Structure.Encampment
              || (isStructureFlaggable[structure] && this.game.getArmyTypeIdsAt(there).length === 0)) {
              // An unguarded flaggable that could be snatched by the scout.
              // We will deny opponent an easy access to a Temple or an Encampment.
              result += 200;
              this.draw.drawTextWhenGroupIsSelected('Thief', groupId, { row: row2 + 0.5, col: col2 });
            }
          }
        }
      }
    }

    return result;
  }

  public utilityUpdatedWithCombat(utilityOfPlace: number, expectedUtilityOfCombat: ExpectedUtilityOfCombat): number {
    const { loseChance, expectedMaterialUtility, situationalUtility } = expectedUtilityOfCombat;

    let utility = (1 - loseChance) * Math.max(utilityOfPlace, 1); // We access the place only if we win.

    if (expectedMaterialUtility >= 0) {
      utility += expectedMaterialUtility;
    } else {
      // When we lose the combat and the value of our armies is higher than enemy's, the utility goes down.
      // However, it should never go bellow zero - otherwise we would never attack opponent's strongest group.
      // Sigmoid function // TODO - sigmoid function is garbage, it needs to be replaced
      utility = 2 * utility / (1 + Math.exp(-expectedMaterialUtility / utility));
    }

    utility += situationalUtility;

    return utility;
  }

  public extractDestroyedArmyTypeIds(combat: CombatInputsAndResult): { attackingArmyTypeIds: string[], defendingArmyTypeIds: string[] } {
    const { combatInputs: { attackingArmies, defendingArmies }, combatResult: { combatEvents } } = combat;
    const dyingEvents = combatEvents.filter(event => event.type !== CombatEventType.Damage);

    return {
      attackingArmyTypeIds: attackingArmies
        .filter(army => dyingEvents.some(event => event.losingArmyId === army.id))
        .map(army => army.armyTypeId),
      defendingArmyTypeIds: defendingArmies
        .filter(army => dyingEvents.some(event => event.losingArmyId === army.id))
        .map(army => army.armyTypeId)
    };
  }

}
