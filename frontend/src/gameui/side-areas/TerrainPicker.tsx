import './TwoItemsPerColumnContainer.scss';
import type { JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button } from 'react-bootstrap';
import { IconImage, OtherIcon } from '../common/IconImage';
import { Terrain, terrainToName } from 'ai-interface/constants/terrain';
import { setTerrainBrush, setTerrainBrushSize } from '../../model/editor/actions';

const brushes = [
  { size: 1, icon: OtherIcon.Brush1 },
  { size: 3, icon: OtherIcon.Brush3 },
  { size: 5, icon: OtherIcon.Brush5 },
  { size: 11, icon: OtherIcon.Brush9 }
];

function componentsToTwoPerRow(components: JSX.Element[]): JSX.Element[] {
  const result: JSX.Element[] = [];
  for (let i = 0; i < components.length; i += 2) {
    result.push(
      <div className="button-row" key={i}>
        {components[i]}
        {components[i + 1]}
      </div>
    );
  }
  return result;
}

export default function TerrainPicker() {
  const terrainBrush = useSelector((state: RootState) => state.editor.terrainBrush);
  const terrainBrushSize = useSelector((state: RootState) => state.editor.terrainBrushSize);
  const dispatch: AppDispatch = useDispatch();

  return (
    <div className="two-items-per-column-container">

      {componentsToTwoPerRow(Object.values(Terrain).map(terrain => (
        <Button
          key={terrain}
          active={terrain === terrainBrush}
          onClick={() => dispatch(setTerrainBrush(terrain))}
        >
          <IconImage
            icon={terrain}
            tooltip={terrainToName[terrain]}
            tooltipPlacement="right"
          />
        </Button>
      )))}

      <div className="mt-auto" />

      {componentsToTwoPerRow(brushes.map(({ size, icon }) => (
        <Button
          key={size}
          active={terrainBrushSize === size}
          onClick={() => dispatch(setTerrainBrushSize(size))}
        >
          <IconImage
            icon={icon}
            tooltip={size + 'x' + size}
            tooltipPlacement="right"
          />
        </Button>
      )))}

    </div >
  );
}
