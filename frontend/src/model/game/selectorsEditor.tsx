import { createSelector } from '@reduxjs/toolkit';
import { experienceForRuin } from 'ai-interface/constants/experience';
import { RootState } from '../root/configureStore';

const getGameState = (state: RootState) => state.game;

export const getTerrainUnderRuin = createSelector(
  getGameState, (rootState: RootState, ruinId: number) => ruinId,
  ({ terrain, ruins }, ruinId) => {
    const { row, col } = ruins[ruinId];
    return terrain[row][col];
  }
);

export const getAllCityNames = createSelector(
  getGameState,
  ({ cities }) => Object.keys(cities).map(cityId => cities[cityId].name)
);

export const getNumberOfRuinsByLevel = createSelector(
  (state: RootState) => state.game.ruins,
  (ruins) => {
    const result = experienceForRuin.map(() => 0);
    for (const ruinId in ruins) {
      const { level } = ruins[ruinId];
      result[level]++;
    }
    return result;
  }
);
