import { FreeformSprite, Sheet } from './spriteMetaDataTypes';

export const spriteSheetCities: Sheet<FreeformSprite> = {
  0: {
    N: 'cities',
    L: 298,
    T: 227,
    W: 176,
    H: 153,
    X: 26,
    Y: 32,
    F: { x: 87, y: -31 }
  },
  1: {
    N: 'cities',
    L: 803,
    T: 20,
    W: 179,
    H: 171,
    X: 29,
    Y: 43,
    F: { x: 67, y: -41 }
  },
  2: {
    N: 'cities',
    L: 1069,
    T: 237,
    W: 172,
    H: 143,
    X: 19,
    Y: 19,
    F: { x: 38, y: 5 }
  },
  3: {
    N: 'cities',
    L: 550,
    T: 217,
    W: 181,
    H: 167,
    X: 27,
    Y: 39,
    F: { x: 68, y: -36 }
  },
  4: {
    N: 'cities',
    L: 544,
    T: 57,
    W: 180,
    H: 126,
    X: 32,
    Y: 5,
    F: { x: 64, y: 2 }
  },
  5: {
    N: 'cities',
    L: 818,
    T: 278,
    W: 168,
    H: 97,
    X: 14,
    Y: -19,
    F: { x: 65, y: 29 }
  },
  6: {
    N: 'cities',
    L: 1066,
    T: 13,
    W: 180,
    H: 179,
    X: 22,
    Y: 51,
    F: { x: 63, y: -49 }
  },
  7: {
    N: 'cities',
    L: 305,
    T: 24,
    W: 176,
    H: 167,
    X: 21,
    Y: 39,
    F: { x: 56, y: -18 }
  },
  8: {
    N: 'cities',
    L: 45,
    T: 64,
    W: 180,
    H: 128,
    X: 19,
    Y: 2,
    F: { x: 65, y: 5 }
  },
};

export const spriteSheetRazed: Sheet<FreeformSprite> = {
  0: {
    N: 'razed',
    L: 294,
    T: 218,
    W: 180,
    H: 166,
    X: 25,
    Y: 38
  },
  1: {
    N: 'razed',
    L: 805,
    T: 13,
    W: 180,
    H: 179,
    X: 29,
    Y: 50
  },
  2: {
    N: 'razed',
    L: 1060,
    T: 207,
    W: 181,
    H: 177,
    X: 26,
    Y: 50
  },
  3: {
    N: 'razed',
    L: 548,
    T: 207,
    W: 180,
    H: 177,
    X: 24,
    Y: 49
  },
  4: {
    N: 'razed',
    L: 545,
    T: 23,
    W: 180,
    H: 164,
    X: 32,
    Y: 43
  },
  5: {
    N: 'razed',
    L: 810,
    T: 227,
    W: 179,
    H: 157,
    X: 24,
    Y: 29
  },
  6: {
    N: 'razed',
    L: 1063,
    T: 13,
    W: 180,
    H: 175,
    X: 22,
    Y: 47
  },
  7: {
    N: 'razed',
    L: 299,
    T: 28,
    W: 176,
    H: 164,
    X: 21,
    Y: 37
  },
  8: {
    N: 'razed',
    L: 39,
    T: 17,
    W: 181,
    H: 175,
    X: 20,
    Y: 47
  },
};
