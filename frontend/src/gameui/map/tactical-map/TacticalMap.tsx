import './TacticalMap.scss';

import armiesSprites from './sprites/armies.webp';
import citiesSprites from './sprites/cities.webp';
import desertSprites from './sprites/desert.webp';
import edgeSprites from './sprites/edge.webp';
import flagsSprites from './sprites/flags.png';
import fogOfWarSprites from './sprites/fogOfWar.webp';
import forest1Sprites from './sprites/forest1.webp';
import forest2Sprites from './sprites/forest2.webp';
import hillSprites from './sprites/hill.webp';
import ice1Sprites from './sprites/ice1.webp';
import ice2Sprites from './sprites/ice2.webp';
import mountainSprites from './sprites/mountain.webp';
import openSprites from './sprites/open.webp';
import pathSprites from './sprites/path.webp';
import razedSprites from './sprites/razed.webp';
import ridgeDesertSprites from './sprites/ridgeDesert.webp';
import ridgeIceSprites from './sprites/ridgeIce.webp';
import ridgeOpenSprites from './sprites/ridgeOpen.webp';
import structuresSprites from './sprites/structures.webp';
import swampSprites from './sprites/swamp.webp';
import volcanicSprites from './sprites/volcanic.webp';
import waterSprites from './sprites/water.webp';
import waterfallSprites from './sprites/waterfall.webp';

import { Component, createRef, RefObject } from 'react';
import { connect } from 'react-redux';
import Hammer from 'hammerjs';
import { RootState } from '../../../model/root/configureStore';
import { spriteSheetFogOfWar, spriteSheetArmies, flagLeft, flagWidth, flagHeight, spriteSheetEdge, spriteSheetFlags } from './spriteMetaData';
import { setTacticalMapSize, setMapFocus, tacticalMapRightClick, tacticalMapLeftClickPlaying } from '../../../model/ui/actions';
import { Tile, City, Group, Flaggable, Ruin, AiOutput, AiOutputForGroup } from '../../../model/game/types';
import { getGroupsDataCalculator, GroupsData, getUnexploredForReadingOnly } from '../../../model/game/selectorsPlaying';
import { getPathSprites } from '../../../model/path/selectors';
import { ProgramMode } from '../../../model/ui/types';
import { tacticalMapLeftClickMapEditor } from '../../../model/editor/actions';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { Place } from 'ai-interface/types/place';
import { calculateEdgeSpriteId, calculateFogOfWarSpriteId } from './calculatedSprites';
import { clamp, hashCoords, shallowEqualObjects } from '../../../functions';
import { FreeformSprite, sheetResolution, TileSprite } from './spriteMetaDataTypes';
import { spriteSheetCities, spriteSheetRazed } from './spriteSheetCitiesMetaData';
import { spriteSheetStructures } from './spriteSheetStructuresMetaData';

interface TacticalMapProps {
  programMode: ProgramMode;
  tacticalMapWidth: number;
  tacticalMapHeight: number;
  pixelsPerTile: number;
  focusedRow: number;
  focusedCol: number;
  showingCityId: string | null;
  activeGroupId: string | null;
  pathSprites: { [rowAndCol: string]: TileSprite };

  size: number;
  terrain: Terrain[][];
  structures: Structure[][];
  backgroundSprites: TileSprite[][][];
  tiles: Tile[][];
  fogOfWar: boolean[][];
  unexplored: boolean[][] | null;
  cities: { [cityId: string]: City };
  flaggables: { [flaggableId: string]: Flaggable };
  ruins: { [ruinId: string]: Ruin };
  groups: { [groupId: string]: Group };
  currentTeamId: number;
  showGrid: boolean;
  aiOutput: AiOutput;

  groupsDataCalculator: (place: Place) => GroupsData | null;

  setTacticalMapSize: (widthInTiles: number, heightInTiles: number, pixelsPerTile: number) => void;
  setMapFocus: (focusedRow: number, focusedCol: number) => void;
  tacticalMapLeftClickMapEditor: (clickRow: number, clickCol: number) => void;
  tacticalMapLeftClickPlaying: (clickRow: number, clickCol: number) => void;
  tacticalMapRightClick: (clickRow: number, clickCol: number) => void;
}

interface TacticalMapState {
  lastClientX: number;
  lastClientY: number;
  totalDragDistance: number; // Used to differentiate between clicking and dragging. We consider releasing drag after tiny distance, such as 3 pixels, as click.
  initialPixelsPerTile: number; // This is the value of pixelsPerTile when pinch gesture (zoom) was started.
}

class TacticalMap extends Component<TacticalMapProps, TacticalMapState> {

  private parentRef = createRef<HTMLDivElement>();
  private canvasRef = createRef<HTMLCanvasElement>();
  private imageRefs: { [key: string]: RefObject<HTMLImageElement | null> } = {
    armies: createRef<HTMLImageElement>(),
    cities: createRef<HTMLImageElement>(),
    desert: createRef<HTMLImageElement>(),
    edge: createRef<HTMLImageElement>(),
    flags: createRef<HTMLImageElement>(),
    fogOfWar: createRef<HTMLImageElement>(),
    forest1: createRef<HTMLImageElement>(),
    forest2: createRef<HTMLImageElement>(),
    hill: createRef<HTMLImageElement>(),
    ice1: createRef<HTMLImageElement>(),
    ice2: createRef<HTMLImageElement>(),
    mountain: createRef<HTMLImageElement>(),
    open: createRef<HTMLImageElement>(),
    path: createRef<HTMLImageElement>(),
    razed: createRef<HTMLImageElement>(),
    ridgeDesert: createRef<HTMLImageElement>(),
    ridgeIce: createRef<HTMLImageElement>(),
    ridgeOpen: createRef<HTMLImageElement>(),
    structures: createRef<HTMLImageElement>(),
    swamp: createRef<HTMLImageElement>(),
    volcanic: createRef<HTMLImageElement>(),
    water: createRef<HTMLImageElement>(),
    waterfall: createRef<HTMLImageElement>()
  };
  private imagesLoading = Object.values(this.imageRefs).length;

  constructor(props: TacticalMapProps) {
    super(props);

    this.handleResize = this.handleResize.bind(this);
    this.handleImageLoaded = this.handleImageLoaded.bind(this);

    this.state = {
      lastClientX: 0,
      lastClientY: 0,
      totalDragDistance: 0,
      initialPixelsPerTile: sheetResolution / 2
    };
  }

  componentDidMount() {
    this.updateZoom();

    this.initializeHammerJs();

    window.addEventListener('resize', this.handleResize);
  }

  handleImageLoaded() {
    if (--this.imagesLoading === 0) {
      this.updateCanvas();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  shouldComponentUpdate(nextProps: TacticalMapProps): boolean {
    // Only update when the props change; not when the state changes. Our state has only fields that don't make the canvas look different.
    return !shallowEqualObjects(this.props, nextProps);
  }

  componentDidUpdate() {
    this.updateCanvas();
  }

  handleResize() {
    this.updateZoom(this.props.pixelsPerTile);
  }

  updateZoom(newPixelsPerTile?: number) {
    const parentDiv = this.parentRef.current!;
    const { clientWidth, clientHeight } = parentDiv;

    // By default, 11 tiles should be seen vertically and horizontally.
    if (newPixelsPerTile === undefined) {
      newPixelsPerTile = Math.min(clientWidth, clientHeight) / 11;
    }
    newPixelsPerTile = clamp(16, sheetResolution, Math.round(newPixelsPerTile));
    this.props.setTacticalMapSize(clientWidth / newPixelsPerTile, clientHeight / newPixelsPerTile, newPixelsPerTile);
  }

  /** Add hammerjs events for gesture to canvas. */
  initializeHammerJs() {
    const canvas = this.canvasRef.current!;
    const hammer = new Hammer(canvas);

    hammer.get('pinch').set({ enable: true });
    hammer.on('pinchstart', () => this.setState({ initialPixelsPerTile: this.props.pixelsPerTile }));
    hammer.on('pinch', e => this.updateZoom(this.state.initialPixelsPerTile * e.scale));
  }

  /** Returns a fraction part of given number. */
  fraction(x: number): number {
    return (x < 0 ? 1 : 0) + x % 1;
  }

  /**
   * Calls callback function for each tile visible on TacticalMap.
   * Callback will be called even for tiles only partially visible.
   * Callback will be called even for tiles outside of the map if it is visible. Callback function has to check boundaries.
   * 
   * Some Sprites, such as City, are bigger than one tile. Thus it is possible that part of them should be visible on screen even when
   * their core tile is outside. The callback function would not be called for a tile outside of the screen and such Sprites would vanish
   * when scrolling. To fix this, it is possible to increase range of iterated tiles beyond visible screen. Set parameter 
   * "increaseRadiusByTiles" to achieve this. Note that the value should be kept to minimum required - test empirically for all directions.
   */
  forEachTileOnScreen(increaseRadiusByTiles: number, callback: (row: number, col: number, canvasX: number, canvasY: number) => void) {
    const { tacticalMapHeight, tacticalMapWidth, focusedRow, focusedCol, pixelsPerTile } = this.props;

    const rowRadius = Math.ceil(tacticalMapHeight / 2 + increaseRadiusByTiles);
    const colRadius = Math.ceil(tacticalMapWidth / 2 + increaseRadiusByTiles);

    let canvasY = Math.floor((tacticalMapHeight / 2 - rowRadius - this.fraction(focusedRow)) * pixelsPerTile);
    for (let row = Math.floor(focusedRow) - rowRadius; row < Math.ceil(focusedRow) + rowRadius; row++, canvasY += pixelsPerTile) {

      let canvasX = Math.floor((tacticalMapWidth / 2 - colRadius - this.fraction(focusedCol)) * pixelsPerTile);
      for (let col = Math.floor(focusedCol) - colRadius; col < Math.ceil(focusedCol) + colRadius; col++, canvasX += pixelsPerTile) {

        callback(row, col, canvasX, canvasY);
      }
    }
  }

  forEachTileOnScreenDrawSprite(
    ctx: CanvasRenderingContext2D,
    increaseRadiusByTiles: number,
    callback: (row: number, col: number, drawSprite: (sprite: FreeformSprite, owner: number | null) => void) => void
  ) {
    const { pixelsPerTile } = this.props;
    const zoom = pixelsPerTile / sheetResolution;

    this.forEachTileOnScreen(increaseRadiusByTiles, (row, col, canvasX, canvasY) =>
      callback(row, col, ({ N, L, T, W, H, X, Y, F }, owner) => {
        ctx.drawImage(this.imageRefs[N].current!,
          L, T,
          W, H,
          canvasX - X * zoom, canvasY - Y * zoom,
          W * zoom, H * zoom
        );

        if (owner !== null && F !== undefined) {
          const flag = spriteSheetFlags[owner];
          ctx.drawImage(this.imageRefs[flag.N].current!,
            flag.L, flag.T,
            flag.W, flag.H,
            canvasX + (F.x - flag.X) * zoom, canvasY + (F.y - flag.Y) * zoom,
            flag.W * zoom, flag.H * zoom
          );
        }
      })
    );
  }

  drawBackgroundSprites(ctx: CanvasRenderingContext2D) {
    const { size, backgroundSprites, unexplored, pixelsPerTile } = this.props;

    this.forEachTileOnScreen(0, (row, col, canvasX, canvasY) => {
      if (row >= 0 && col >= 0 && row < size && col < size && (unexplored === null || !unexplored[row][col])) {
        for (const { L, T, W, H, N } of backgroundSprites[row][col]) {

          ctx.drawImage(this.imageRefs[N].current!,
            L, T,
            W * sheetResolution, H * sheetResolution,
            canvasX, canvasY,
            W * pixelsPerTile, H * pixelsPerTile);
        }
      }
    });
  }

  getStructureSprite(row: number, col: number): FreeformSprite | undefined {
    const { structures, tiles, ruins, terrain, currentTeamId } = this.props;
    let spriteId = structures[row][col] as string;

    switch (spriteId) {
      case Structure.Ruin: {
        const { ruinId } = tiles[row][col];
        const { level, lastSeenExplored } = ruins[ruinId!];
        const visibleExplored = lastSeenExplored[currentTeamId];
        spriteId = Structure.Ruin + level + (visibleExplored ? 'e' : 'u');
        break;
      }
    }

    return spriteSheetStructures[spriteId + terrain[row][col]]
      ?? spriteSheetStructures[spriteId];
  }

  drawNonCityStructures(ctx: CanvasRenderingContext2D) {
    const { size, tiles, flaggables, currentTeamId } = this.props;

    this.forEachTileOnScreenDrawSprite(ctx, 1.5, (row, col, drawSprite) => {
      if (row >= 0 && col >= 0 && row < size && col < size) {
        const sprite = this.getStructureSprite(row, col);
        if (sprite !== undefined) {
          if (sprite.F !== undefined) {
            const { flaggableId } = tiles[row][col];
            const visibleOwner = flaggables[flaggableId!].lastSeenOwner[currentTeamId];
            drawSprite(sprite, visibleOwner);
          } else {
            drawSprite(sprite, null);
          }
        }
      }
    });
  }

  drawCities(ctx: CanvasRenderingContext2D) {
    const { size, tiles, cities, currentTeamId } = this.props;

    this.forEachTileOnScreenDrawSprite(ctx, 1.5, (row, col, drawSprite) => {
      if (row >= 0 && col >= 0 && row < size - 1 && col < size - 1) { // We ignore last row and column since top left corner of city can't be there
        const { cityId } = tiles[row][col];
        if (cityId !== undefined && cityId === tiles[row + 1][col + 1].cityId) { // Draw sprite only for the top left corner of the city
          const city = cities[cityId];
          const visibleRazed = city.lastSeenRazed[currentTeamId];
          const visibleOwner = city.lastSeenOwner[currentTeamId];
          const capitol = city.lastSeenCapitol[currentTeamId];
          const sprite = (visibleRazed ? spriteSheetRazed : spriteSheetCities)[visibleOwner];
          drawSprite(sprite, capitol ? visibleOwner : null);
        }
      }
    });
  }

  drawGroupsAndBags(ctx: CanvasRenderingContext2D) {
    const { size, groupsDataCalculator, tiles, fogOfWar, pixelsPerTile } = this.props;
    const zoom = pixelsPerTile / sheetResolution;

    this.forEachTileOnScreen(0, (row, col, canvasX, canvasY) => {
      if (row >= 0 && col >= 0 && row < size && col < size && !fogOfWar[row][col]) {
        const groupsData = groupsDataCalculator({ row, col });
        if (groupsData !== null) {
          const { highestArmyTypeId, isShip, owner } = groupsData;
          let { groupSize } = groupsData;
          const { L, T, W, H } = spriteSheetArmies[isShip ? 'ship' : highestArmyTypeId];

          // Draw army type image
          ctx.drawImage(this.imageRefs.armies.current!,
            L, T,
            W * sheetResolution, H * sheetResolution,
            canvasX, canvasY,
            pixelsPerTile, pixelsPerTile);

          // Draw flag pole
          ctx.drawImage(this.imageRefs.flags.current!,
            188, 0,
            6, 80,
            canvasX - 2 * zoom, canvasY - 8 * zoom,
            6 * zoom, 80 * zoom);

          // Draw bottom flag if applicable
          if (groupSize > 4) {
            groupSize -= 4;
            ctx.drawImage(this.imageRefs.flags.current!,
              flagLeft[0], (flagHeight + 1) * owner,
              flagWidth[0], flagHeight,
              canvasX, canvasY + 8 * zoom,
              flagWidth[0] * zoom, flagHeight * zoom);
          }

          // Draw top flag
          ctx.drawImage(this.imageRefs.flags.current!,
            flagLeft[groupSize - 1], (flagHeight + 1) * owner,
            flagWidth[groupSize - 1], flagHeight,
            canvasX, canvasY - 8 * zoom,
            flagWidth[groupSize - 1] * zoom, flagHeight * zoom);
        }

        // Draw bag of artifacts
        if (tiles[row][col].artifactIds) {
          const { L, T, W, H } = spriteSheetArmies['bag'];
          ctx.drawImage(this.imageRefs.armies.current!,
            L, T,
            W * sheetResolution, H * sheetResolution,
            canvasX, canvasY,
            pixelsPerTile, pixelsPerTile);
        }
      }
    });
  }

  drawFogOfWarAndUnexploredAndMapEdge(ctx: CanvasRenderingContext2D) {
    const { size, fogOfWar, unexplored, pixelsPerTile } = this.props;

    this.forEachTileOnScreen(0, (row, col, canvasX, canvasY) => {
      if (row >= 0 && col >= 0 && row < size && col < size) {

        let idUnexplored = unexplored === null ? 0 : (50 + calculateFogOfWarSpriteId(unexplored, row, col, size));
        if (idUnexplored in spriteSheetFogOfWar) {
          if (idUnexplored === 70) {
            idUnexplored += hashCoords(row, col, 6);
          }

          const { L, T } = spriteSheetFogOfWar[idUnexplored];
          ctx.drawImage(this.imageRefs.fogOfWar.current!,
            L, T,
            sheetResolution, sheetResolution,
            canvasX, canvasY,
            pixelsPerTile, pixelsPerTile);
        }

        if (idUnexplored < 70) {
          const idFogOfWar = calculateFogOfWarSpriteId(fogOfWar, row, col, size);
          if (idFogOfWar in spriteSheetFogOfWar) {
            const { L, T } = spriteSheetFogOfWar[idFogOfWar];
            ctx.drawImage(this.imageRefs.fogOfWar.current!,
              L, T,
              sheetResolution, sheetResolution,
              canvasX, canvasY,
              pixelsPerTile, pixelsPerTile);
          }
        }

      } else {
        const id = calculateEdgeSpriteId(col, row, size);
        const { L, T } = spriteSheetEdge[id];
        ctx.drawImage(this.imageRefs.edge.current!,
          L, T,
          sheetResolution, sheetResolution,
          canvasX, canvasY,
          pixelsPerTile, pixelsPerTile);
      }
    });
  }

  drawPath(ctx: CanvasRenderingContext2D) {
    const { activeGroupId, pathSprites, pixelsPerTile } = this.props;

    if (activeGroupId !== null) {
      this.forEachTileOnScreen(0, (row, col, canvasX, canvasY) => {
        const place = row + ',' + col;
        const sprite = pathSprites[place];
        if (sprite !== undefined) {

          ctx.drawImage(this.imageRefs.path.current!,
            sprite.L, sprite.T,
            sheetResolution, sheetResolution,
            canvasX, canvasY,
            pixelsPerTile, pixelsPerTile);
        }
      });
    }
  }

  drawGrid(ctx: CanvasRenderingContext2D) {
    const { showGrid, pixelsPerTile } = this.props;

    if (!showGrid) {
      return;
    }

    ctx.strokeStyle = '#000';
    ctx.lineWidth = Math.min(pixelsPerTile / 32, 1); // When zooming in, we don't want the lines to get thick; but when zooming out, we want them to get thin.
    ctx.beginPath();

    this.forEachTileOnScreen(0, (row, col, canvasX, canvasY) => {
      ctx.moveTo(canvasX + 0.5, canvasY + 0.5);
      ctx.lineTo(canvasX + 0.5, canvasY + pixelsPerTile + 0.5);
      ctx.lineTo(canvasX + pixelsPerTile + 0.5, canvasY + pixelsPerTile + 0.5);
    });

    ctx.stroke();
  }

  drawAiOutput(ctx: CanvasRenderingContext2D) {
    const { pixelsPerTile, tacticalMapHeight, tacticalMapWidth, focusedRow, focusedCol, activeGroupId, showingCityId, aiOutput } = this.props;
    const offsetCol = 0.5 + tacticalMapWidth / 2 - focusedCol;
    const offsetRow = 0.5 + tacticalMapHeight / 2 - focusedRow;

    ctx.font = `${pixelsPerTile * 0.4}px Arial`;
    ctx.fillStyle = 'white';
    ctx.textBaseline = 'middle';

    let line = 0;

    function drawAiOutputForGroup(output: AiOutputForGroup) {
      if (output !== undefined) {

        ctx.textAlign = 'center';
        for (const { row, col, text } of output.atPlace) {
          ctx.fillText(text, (col + offsetCol) * pixelsPerTile, (row + offsetRow) * pixelsPerTile);
        }

        ctx.textAlign = 'left';
        for (const text of output.sticky) {
          ctx.fillText(text, 20, ++line * 0.7 * pixelsPerTile);
        }
      }
    }

    drawAiOutputForGroup(aiOutput['ALWAYS']);
    drawAiOutputForGroup(aiOutput[activeGroupId ?? showingCityId ?? 'NONE']);
  }

  updateCanvas() {
    const { tacticalMapWidth, tacticalMapHeight, pixelsPerTile } = this.props;

    const canvas = this.canvasRef.current!;
    const ctx = canvas.getContext('2d')!;
    canvas.width = tacticalMapWidth * pixelsPerTile;
    canvas.height = tacticalMapHeight * pixelsPerTile;

    this.drawBackgroundSprites(ctx);
    this.drawCities(ctx);
    this.drawNonCityStructures(ctx);
    this.drawGroupsAndBags(ctx);
    this.drawGrid(ctx);
    this.drawFogOfWarAndUnexploredAndMapEdge(ctx);
    this.drawPath(ctx);
    this.drawAiOutput(ctx);
  }

  handleClick(clientX: number, clientY: number, leftButton: boolean) {
    if (this.state.totalDragDistance > 3) {
      return;
    }

    const { size, focusedRow, focusedCol, pixelsPerTile, programMode } = this.props;

    // Convert from canvas coordinates to in-game coordinates. Center of the canvas displays focused row & col.
    const canvas = this.canvasRef.current!;
    const { left, top, width, height } = canvas.getBoundingClientRect();
    const clickCol = Math.floor(focusedCol + (clientX - left - width / 2) / pixelsPerTile);
    const clickRow = Math.floor(focusedRow + (clientY - top - height / 2) / pixelsPerTile);

    // Check land boundary
    if (clickRow < 0 || clickCol < 0 || clickRow >= size || clickCol >= size) {
      return;
    }

    if (leftButton) {
      if (programMode === ProgramMode.Playing) {
        this.props.tacticalMapLeftClickPlaying(clickRow, clickCol);
      } else {
        this.props.tacticalMapLeftClickMapEditor(clickRow, clickCol);
      }
    } else {
      this.props.tacticalMapRightClick(clickRow, clickCol);
    }
  }

  handleDrag(clientX: number, clientY: number) {
    const { size, focusedRow, focusedCol, pixelsPerTile } = this.props;

    this.setState(({ lastClientX, lastClientY, totalDragDistance }) => {
      this.props.setMapFocus(
        clamp(0, size, focusedRow + (lastClientY - clientY) / pixelsPerTile),
        clamp(0, size, focusedCol + (lastClientX - clientX) / pixelsPerTile)
      );

      return {
        lastClientX: clientX,
        lastClientY: clientY,
        totalDragDistance: totalDragDistance + Math.abs(lastClientX - clientX) + Math.abs(lastClientY - clientY)
      };
    });
  }

  render() {
    return (
      <div ref={this.parentRef} className="tactical-map">
        <canvas ref={this.canvasRef}
          onMouseDown={e => this.setState({ totalDragDistance: 0, lastClientX: e.clientX, lastClientY: e.clientY })}
          onMouseMove={e => e.buttons !== 0 && this.handleDrag(e.clientX, e.clientY)}
          onTouchStart={e => this.setState({ totalDragDistance: 0, lastClientX: e.touches[0].clientX, lastClientY: e.touches[0].clientY })}
          onTouchMove={e => this.handleDrag(e.touches[0].clientX, e.touches[0].clientY)}
          onClick={e => this.handleClick(e.clientX, e.clientY, true)}
          onContextMenu={e => this.handleClick(e.clientX, e.clientY, false)}
          onWheel={e => this.updateZoom(this.props.pixelsPerTile - 4 * Math.sign(e.deltaY))}
        />
        <img ref={this.imageRefs.armies} src={armiesSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.cities} src={citiesSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.desert} src={desertSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.edge} src={edgeSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.flags} src={flagsSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.fogOfWar} src={fogOfWarSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.forest1} src={forest1Sprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.forest2} src={forest2Sprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.hill} src={hillSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.ice1} src={ice1Sprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.ice2} src={ice2Sprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.mountain} src={mountainSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.open} src={openSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.path} src={pathSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.razed} src={razedSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.ridgeDesert} src={ridgeDesertSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.ridgeIce} src={ridgeIceSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.ridgeOpen} src={ridgeOpenSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.structures} src={structuresSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.swamp} src={swampSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.volcanic} src={volcanicSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.water} src={waterSprites} alt="" onLoad={this.handleImageLoaded} />
        <img ref={this.imageRefs.waterfall} src={waterfallSprites} alt="" onLoad={this.handleImageLoaded} />
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  programMode: state.ui.programMode,
  tacticalMapWidth: state.ui.tacticalMapWidth,
  tacticalMapHeight: state.ui.tacticalMapHeight,
  pixelsPerTile: state.ui.pixelsPerTile,
  focusedRow: state.ui.focusedRow,
  focusedCol: state.ui.focusedCol,
  showingCityId: state.ui.showingCityId,
  activeGroupId: state.ui.activeGroupId,
  pathSprites: getPathSprites(state),

  size: state.game.size,
  terrain: state.game.terrain,
  structures: state.game.structures,
  backgroundSprites: state.game.backgroundSprites,
  tiles: state.game.tiles,
  fogOfWar: state.game.fogOfWar,
  unexplored: getUnexploredForReadingOnly(state),
  cities: state.game.cities,
  flaggables: state.game.flaggables,
  ruins: state.game.ruins,
  groups: state.game.groups,
  currentTeamId: state.game.players[state.game.currentPlayerId].team,
  showGrid: !!state.game.players[state.game.currentPlayerId].mapDisplaySettings.showGrid,
  aiOutput: state.game.aiOutput,

  groupsDataCalculator: getGroupsDataCalculator(state)
});

const mapDispatchToProps = {
  setTacticalMapSize,
  setMapFocus,
  tacticalMapLeftClickMapEditor,
  tacticalMapLeftClickPlaying,
  tacticalMapRightClick
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TacticalMap);
