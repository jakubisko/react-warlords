import { RootState } from '../../model/root/configureStore';
import { Modal } from 'react-bootstrap';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import { useSelector } from 'react-redux';
import { Spinner3 } from '../common/Spinner3';

export default function AiTakingTurnScreen() {
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const currentPlayerName = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].name);

  return (
    <Modal className="screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <ModalTitleWithFlags owner={currentPlayerId} caption={currentPlayerName} />
      </Modal.Header>

      <Modal.Body className="p-3">
        Computer player is taking the turn, please wait.
      </Modal.Body>

      <Modal.Footer>
        <Spinner3 />
      </Modal.Footer>
    </Modal>
  );
}
