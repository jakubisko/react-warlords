import { DrawApi } from 'ai-interface/apis/DrawApi';
import { Place } from 'ai-interface/types/place';
import { IllegalPlace } from 'ai-interface/types/obstacle';
import { AppDispatch } from '../root/configureStore';
import { addToAiOutput } from '../game/actionsTurnPassing';

export class DrawApiImpl implements DrawApi {

  constructor(private dispatch: AppDispatch) {
  }

  private handleDrawing(text: string, groupId: string, place?: Place) {
    // Drawing outside of the land boundaries and on non-integer coordinates is allowed
    if (place !== undefined) {
      const { row, col } = place;
      if (isNaN(row) || isNaN(col) || row === Infinity || col === Infinity) {
        throw new IllegalPlace(`Place [${row}, ${col}] has coordinates that are not numbers`);
      }
    }

    this.dispatch(addToAiOutput(text, groupId, place));
  }

  drawText(text: string, place?: Place): void {
    this.handleDrawing(text, 'ALWAYS', place);
  }

  drawTextWhenGroupIsSelected(text: string, groupId: string, place?: Place): void {
    this.handleDrawing(text, groupId, place);
  }

  drawTextWhenCityIsSelected(text: string, cityId: string, place?: Place): void {
    this.handleDrawing(text, cityId, place);
  }

  drawTextWhenNothingIsSelected(text: string, place?: Place): void {
    this.handleDrawing(text, 'NONE', place);
  }

}

/** Implementation of DrawApi that does nothing. Use when you don't want to see the output. */
export class DrawApiImplEmpty implements DrawApi {
  drawText(): void { /** Does nothing. */ }
  drawTextWhenGroupIsSelected(): void { /** Does nothing. */ }
  drawTextWhenCityIsSelected(): void { /** Does nothing. */ }
  drawTextWhenNothingIsSelected(): void { /** Does nothing. */ }
}
