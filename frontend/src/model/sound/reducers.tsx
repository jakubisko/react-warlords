import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actions';
import { SoundState } from './types';

export const initialState: SoundState = {
  soundRow: 0,
  soundCol: 0
};

export function soundReducer(state = initialState, action: AppAction): SoundState {
  switch (action.type) {

    case ActionTypes.SET_SOUND_SOURCE: {
      const { soundRow, soundCol } = action.payload;
      return {
        ...state,
        soundRow,
        soundCol
      };
    }

    default:
      return state;
  }
}
