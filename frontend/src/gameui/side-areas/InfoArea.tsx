import { useSelector } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import GroupInfo from './GroupInfo';
import KingdomInfo from './KingdomInfo';
import { ProgramMode } from '../../model/ui/types';
import { EditorTool } from '../../model/editor/types';
import StructurePicker from './StructurePicker';
import TerrainPicker from './TerrainPicker';

export default function InfoArea() {
  const isEditor = useSelector((state: RootState) => state.ui.programMode === ProgramMode.MapEditor);
  const tool = useSelector((state: RootState) => state.editor.tool);
  const isActiveGroup = useSelector((state: RootState) => state.ui.activeGroupId !== null);

  function editorInfoArea() {
    switch (tool) {
      case EditorTool.StructurePlacing: return <StructurePicker />;
      case EditorTool.TerrainBrush: return <TerrainPicker />;
      case EditorTool.ArmyPlacing: return <>Click on map to place or edit a group.</>;
      case EditorTool.ArtifactPlacing: return <>Click on map to place or edit a bag with artifacts.</>;
      case EditorTool.Erase: return <>Click on map to remove objects.</>;
      case EditorTool.PlaceInfo: return <>Click on map to see object description and edit its properties.</>;
    }
  }

  function gameInfoArea() {
    return isActiveGroup ?
      <GroupInfo />
      :
      <KingdomInfo />;
  }

  return isEditor ? editorInfoArea() : gameInfoArea();
}
