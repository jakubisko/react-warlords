import { TileSprite, sheetResolution } from '../../gameui/map/tactical-map/spriteMetaDataTypes';
import { createGrid, hashCoords } from '../../functions';
import { Place } from 'ai-interface/types/place';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { hasCornerTiles, TempTerrain, IsTerrainPredicate, terrainToSpriteSheet } from './constants';
import { placeSpritesForWater, placeSpritesForPorts } from './tilingWater';
import { placeSpritesForRidges } from './tilingRidge';

/** This file uses pseudo-random generator instead of true random. Loading the same map twice must draw the same tile sprites. */
let randomSeed: number;

/** Generates a next pseudo random integer in a sequence. rnd(N) returns a number from 0 to N-1. */
function rnd(upperBound: number): number {
  randomSeed = randomSeed * 16807 % 2147483647;
  return randomSeed % upperBound;
}

/** Pseudo-randomly shuffles given array in place. */
function shuffle<T>(a: T[]): void {
  for (let i = a.length - 1; i > 0; i--) {
    const j = rnd(i + 1);
    [a[i], a[j]] = [a[j], a[i]];
  }
}

/** Places background sprites like water, forest and hills on two-dimensional grid based on terrain. */
export function calculateBackgroundSprites(terrain: Terrain[][], structures: Structure[][]): TileSprite[][][] {
  const size = terrain.length;
  const backgroundSprites = createGrid(size, () => [] as TileSprite[]);

  /**
   * Returns true when the tile is considered to be of that terrain.
   * There is a situations when one terrain behaves as if it was another:
   * Forest around structures behaves as special "Forest Clearing" terrain. It draws smaller tree which doesn't cover the structure.
   */
  const isTerrain: IsTerrainPredicate = function (row: number, col: number, consideredTerrain: Terrain | TempTerrain): boolean {
    let substituteTerrain: Terrain | TempTerrain = terrain[row][col];

    if (substituteTerrain === Terrain.Forest) {
      if ((structures[row][col] > Structure.Ridge) ||
        (row > 0 && structures[row - 1][col] > Structure.Ridge) ||
        (col > 0 && structures[row][col - 1] > Structure.Ridge) ||
        (row < size - 1 && structures[row + 1][col] > Structure.Ridge) ||
        (col < size - 1 && structures[row][col + 1] > Structure.Ridge)) {
          substituteTerrain = structures[row][col] === Structure.Road ? Terrain.Open : TempTerrain.ForestClearing;
      }
    }

    return substituteTerrain === consideredTerrain;
  };

  placeSpritesForWater(backgroundSprites, isTerrain);

  // Place 1x1 background sprites including roads
  const placeBackgroundSpritesForOneTerrain = function (placing: Terrain | TempTerrain) {
    randomSeed = 1;

    const spriteSheet = terrainToSpriteSheet[placing];
    let noOfBasicTiles = 0;
    while (spriteSheet[20 + ++noOfBasicTiles]);

    for (let row = 0; row < size; row++) {
      for (let col = 0; col < size; col++) {
        const road = structures[row][col] === Structure.Road;

        if (isTerrain(row, col, placing)) {
          let id = 0;
          // If there is a road, calculate the sprite based on adjacent roads.
          if (road) {
            id = (row < size - 1 && structures[row + 1][col] === Structure.Road ? 8 : 0)
              + (col > 0 && structures[row][col - 1] === Structure.Road ? 4 : 0)
              + (row > 0 && structures[row - 1][col] === Structure.Road ? 2 : 0)
              + (col < size - 1 && structures[row][col + 1] === Structure.Road ? 1 : 0);

            // If the road goes straight towards the map edge (not parallel), extend it so it looks as if it was leaving the map. 
            if ((row === size - 1 && id === 2) || (row === 0 && id === 8)) {
              id = 10;
            } else if ((col === size - 1 && id === 4) || (col === 0 && id === 1)) {
              id = 5;
            }

            // There is no sprite for road with no adjacent roads, but we need to display something when user draws 1 tile of road.
            id = id || 1;
          }

          // Full sprite covers anything that was on the tile before; replace the list with single sprite. 
          backgroundSprites[row][col] = [spriteSheet[id > 0 ? id : (20 + hashCoords(row, col, noOfBasicTiles))]];

        } else if (40 in spriteSheet) {
          // Border sprite draws on top of existing terrain; add it to list. It is possible to have multiple border sprites at one tile.
          const tileSprites = backgroundSprites[row][col];

          const terrainHere = terrain[row][col];
          const corner = (row: number, col: number) => {
            const terrainThere = terrain[row][col];
            return terrainThere !== terrainHere && hasCornerTiles[terrainThere];
          };

          const cornerTop = row > 0 && corner(row - 1, col);
          const cornerLeft = col > 0 && corner(row, col - 1);
          const cornerRight = col < size - 1 && corner(row, col + 1);
          const cornerBottom = row < size - 1 && corner(row + 1, col);

          if (col < size - 1 && isTerrain(row, col + 1, placing)) {
            const id = road && structures[row][col + 1] === Structure.Road ? 17 :
              (cornerTop ? (cornerBottom ? 50 : 51) : (cornerBottom ? 52 : 50));
            tileSprites.push(spriteSheet[id]);
          }

          if (row > 0 && isTerrain(row - 1, col, placing)) {
            const id = road && structures[row - 1][col] === Structure.Road ? 16 :
              (cornerLeft ? (cornerRight ? 40 : 41) : (cornerRight ? 42 : 40));
            tileSprites.push(spriteSheet[id]);
          }

          if (col > 0 && isTerrain(row, col - 1, placing)) {
            const id = road && structures[row][col - 1] === Structure.Road ? 19 :
              (cornerTop ? (cornerBottom ? 60 : 61) : (cornerBottom ? 62 : 60));
            tileSprites.push(spriteSheet[id]);
          }

          if (row < size - 1 && isTerrain(row + 1, col, placing)) {
            const id = road && structures[row + 1][col] === Structure.Road ? 18 :
              (cornerLeft ? (cornerRight ? 70 : 71) : (cornerRight ? 72 : 70));
            tileSprites.push(spriteSheet[id]);
          }
        }
      }
    }
  };

  // Place background sprites larger than 1x1. These replace anything that was there before. They won't be placed to overlap roads.
  const placeBigBackgroundSpritesForOneTerrain = function (placing: Terrain, percent: number) {
    const places: Place[] = [];
    const free = createGrid(size, (row, col) => {
      if (!isTerrain(row, col, placing)) {
        return false;
      }
      const structure = structures[row][col];
      if (structure === Structure.Road || structure === Structure.Ridge) {
        return false;
      }

      places.push({ row, col });
      return true;
    });

    shuffle(places);

    const spriteSheet = terrainToSpriteSheet[placing];
    let noOfBigTiles = 0;
    while (spriteSheet[30 + ++noOfBigTiles]);

    for (let s = 0; s < places.length * percent / 100; s++) {
      const { L, T, W, H, N } = spriteSheet[30 + rnd(noOfBigTiles)];
      const row = places[s].row - rnd(H);
      const col = places[s].col - rnd(W);

      // Is there a place for sprite of such size?
      let ok = true;
      for (let rowOff = 0; rowOff < H; rowOff++) {
        for (let colOff = 0; colOff < W; colOff++) {
          if (row + rowOff >= 0 && col + colOff >= 0 && row + rowOff < size && col + colOff < size && !free[row + rowOff][col + colOff]) {
            ok = false;
          }
        }
      }
      if (!ok) {
        continue;
      }

      // Full sprite covers anything that was on the tile before; replace all old sprites with single sprite. 
      for (let rowOff = 0; rowOff < H; rowOff++) {
        for (let colOff = 0; colOff < W; colOff++) {
          if (row + rowOff >= 0 && col + colOff >= 0 && row + rowOff < size && col + colOff < size) {
            backgroundSprites[row + rowOff][col + colOff] = [{
              L: L + sheetResolution * colOff,
              T: T + sheetResolution * rowOff,
              W: 1,
              H: 1,
              N
            }];

            free[row + rowOff][col + colOff] = false;
          }
        }
      }
    }
  };

  placeBackgroundSpritesForOneTerrain(TempTerrain.ForestClearing);

  placeBackgroundSpritesForOneTerrain(Terrain.Open);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Open, 12);

  placeBackgroundSpritesForOneTerrain(Terrain.Desert);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Desert, 10);

  placeBackgroundSpritesForOneTerrain(Terrain.Ice);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Ice, 12);

  placeBackgroundSpritesForOneTerrain(Terrain.Swamp);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Swamp, 12);

  placeBackgroundSpritesForOneTerrain(Terrain.Hill);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Hill, 66);

  placeBackgroundSpritesForOneTerrain(Terrain.Volcanic);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Volcanic, 12);

  placeBackgroundSpritesForOneTerrain(Terrain.Mountain);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Mountain, 80);

  placeSpritesForRidges(backgroundSprites, terrain, structures);

  placeBackgroundSpritesForOneTerrain(Terrain.Forest);
  placeBigBackgroundSpritesForOneTerrain(Terrain.Forest, 50);

  placeSpritesForPorts(backgroundSprites, isTerrain, structures);

  return backgroundSprites;
}
