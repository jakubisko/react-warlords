import { memo, useEffect, useState } from 'react';
import BackgroundMusic from './BackgroundMusic';
import { StructureMusic } from './StructureMusic';
import { Howler } from 'howler';
import { useSelector } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import { getVolumeOfStructures } from '../../model/sound/selectors';

function Sounds() {
  const volumeOfStructures = useSelector((state: RootState) => getVolumeOfStructures(state));
  const backgroundVolume = 1 - 0.5 * Math.max(...Object.values(volumeOfStructures), 0);

  const isVisible = useVisibilityState() === 'visible';
  useEffect(() => {
    Howler.mute(!isVisible);
  });

  return (
    <>
      <BackgroundMusic volumeRequested={backgroundVolume} />
      <StructureMusic volumeOfStructures={volumeOfStructures} />
    </>
  );
}

export default memo(Sounds);

/**
 * Returns true while the application is visible. Returns false while the user is on another tab or the browser is minimized.
 * NOTE: It seems that this code fails when this hook is used more than once.
 */
function useVisibilityState() {
  const [state, setState] = useState<DocumentVisibilityState>('visible');

  useEffect(() => {
    function onVisibilityChange() {
      setState(document.visibilityState);
    }

    document.addEventListener('visibilitychange', onVisibilityChange, false);
    return () => document.removeEventListener('visibilitychange', onVisibilityChange, false);
  });

  return state;
}
