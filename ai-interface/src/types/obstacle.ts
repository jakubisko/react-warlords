export enum ObstacleType {
  STRING_TOO_LONG = 'STRING_TOO_LONG',
  ILLEGAL_ORDERING = 'ILLEGAL_ORDERING',
  ILLEGAL_PLACE = 'ILLEGAL_PLACE',
  YOU_DONT_CONTROL_SUCH_GROUP = 'YOU_DONT_CONTROL_SUCH_GROUP',
  NOT_ENOUGH_GOLD = 'NOT_ENOUGH_GOLD',
  SUCH_ARMY_TYPE_DOESNT_EXIST = 'SUCH_ARMY_TYPE_DOESNT_EXIST',
  SUCH_ARMY_TYPE_CANT_BE_TRAINED = 'SUCH_ARMY_TYPE_CANT_BE_TRAINED',
  SUCH_CITY_DOESNT_EXIST = 'SUCH_CITY_DOESNT_EXIST',
  YOU_DONT_CONTROL_SUCH_CITY = 'YOU_DONT_CONTROL_SUCH_CITY',
  CITY_IS_RAZED = 'CITY_IS_RAZED',
  CITY_DOESNT_HAVE_SUCH_TRAINING_FACILITY = 'CITY_DOESNT_HAVE_SUCH_TRAINING_FACILITY',
  CITY_ALREADY_HAS_SUCH_TRAINING_FACILITY = 'CITY_ALREADY_HAS_SUCH_TRAINING_FACILITY',
  CITY_CANT_HAVE_MORE_TRAINING_FACILITIES = 'CITY_CANT_HAVE_MORE_TRAINING_FACILITIES',
  CITY_CANT_HAVE_MORE_INCOMING_CARAVANS = 'CITY_CANT_HAVE_MORE_INCOMING_CARAVANS',
  NO_HERO_OFFER = 'NO_HERO_OFFER',
  NO_ARMIES_PRESENT = 'NO_ARMIES_PRESENT',
  NOT_ENOUGH_FREE_SPACE_AT_PLACE = 'NOT_ENOUGH_FREE_SPACE_AT_PLACE',
  SUCH_FLAGGABLE_DOESNT_EXIST = 'SUCH_FLAGGABLE_DOESNT_EXIST',
  SUCH_RUIN_DOESNT_EXIST = 'SUCH_RUIN_DOESNT_EXIST',
  GROUP_DOESNT_CONTAIN_SUCH_ARMY = 'GROUP_DOESNT_CONTAIN_SUCH_ARMY',
  SOURCE_AND_DESTINATION_ARE_IDENTICAL = 'SOURCE_AND_DESTINATION_ARE_IDENTICAL',
  YOU_DONT_CONTROL_SUCH_HERO = 'YOU_DONT_CONTROL_SUCH_HERO',
  GROUPS_DONT_STAND_AT_SAME_PLACE = 'GROUPS_DONT_STAND_AT_SAME_PLACE',
  SUCH_ARTIFACT_DOESNT_EXIST = 'SUCH_ARTIFACT_DOESNT_EXIST',
  HERO_CANT_CARRY_MORE_ARTIFACTS = 'HERO_CANT_CARRY_MORE_ARTIFACTS',
  NO_SUCH_ARTIFACT_AT_PLACE = 'NO_SUCH_ARTIFACT_ON_PLACE',
  HERO_DOESNT_CARRY_SUCH_ARTIFACT = 'HERO_DOESNT_CARRY_SUCH_ARTIFACT',
  PLACE_NOT_EXPLORABLE = 'PLACE_NOT_EXPLORABLE',
  ILLEGAL_MOVE_TYPE = 'ILLEGAL_MOVE_TYPE',
  ZERO_LENGTH_PATH = 'ZERO_LENGTH_PATH',
  PATH_PLACES_ARE_NOT_ADJACENT = 'PATH_PLACES_ARE_NOT_ADJACENT',
  IMPASSABLE_TERRAIN = 'IMPASSABLE_TERRAIN',
  CROSSING_COAST_OFF_PORT = 'CROSSING_COAST_OFF_PORT',
  ILLEGAL_GROUP_SIZE = 'ILLEGAL_GROUP_SIZE',
  ILLEGAL_INITIAL_MOVE_LEFT = 'ILLEGAL_INITIAL_MOVE_LEFT',
  DESTINATION_IS_OUTSIDE_OF_CALCULATED_VALUES = 'DESTINATION_IS_OUTSIDE_OF_CALCULATED_VALUES',
  NO_MOVE_LEFT = 'NO_MOVE_LEFT',
  SAME_PLAYER = 'SAME_PLAYER',
  VALUE_OUT_OF_RANGE = 'VALUE_OUT_OF_RANGE'
}

export interface Obstacle {
  readonly type: string;
}

export class StringTooLong implements Obstacle {
  readonly type = ObstacleType.STRING_TOO_LONG;
  constructor(public readonly errorText: string) { }
}

export class IllegalOrdering implements Obstacle {
  readonly type = ObstacleType.ILLEGAL_ORDERING;
  constructor(public readonly errorText: string) { }
}

export class IllegalPlace implements Obstacle {
  readonly type = ObstacleType.ILLEGAL_PLACE;
  constructor(public readonly errorText: string) { }
}

export class YouDontControlSuchGroup implements Obstacle {
  readonly type = ObstacleType.YOU_DONT_CONTROL_SUCH_GROUP;
  constructor(public readonly errorText: string) { }
}

export class NotEnoughGold implements Obstacle {
  readonly type = ObstacleType.NOT_ENOUGH_GOLD;
  constructor(public readonly errorText: string) { }
}

export class SuchArmyTypeDoesntExist implements Obstacle {
  readonly type = ObstacleType.SUCH_ARMY_TYPE_DOESNT_EXIST;
  constructor(public readonly errorText: string) { }
}

export class SuchArmyTypeCantBeTrained implements Obstacle {
  readonly type = ObstacleType.SUCH_ARMY_TYPE_CANT_BE_TRAINED;
  constructor(public readonly errorText: string) { }
}

export class SuchCityDoesntExist implements Obstacle {
  readonly type = ObstacleType.SUCH_CITY_DOESNT_EXIST;
  constructor(public readonly errorText: string) { }
}

export class YouDontControlSuchCity implements Obstacle {
  readonly type = ObstacleType.YOU_DONT_CONTROL_SUCH_CITY;
  constructor(public readonly errorText: string) { }
}

export class CityIsRazed implements Obstacle {
  readonly type = ObstacleType.CITY_IS_RAZED;
  constructor(public readonly errorText: string) { }
}

export class CityDoesntHaveSuchTrainingFacility implements Obstacle {
  readonly type = ObstacleType.CITY_DOESNT_HAVE_SUCH_TRAINING_FACILITY;
  constructor(public readonly errorText: string) { }
}

export class CityAlreadyHasSuchTrainingFacility implements Obstacle {
  readonly type = ObstacleType.CITY_ALREADY_HAS_SUCH_TRAINING_FACILITY;
  constructor(public readonly errorText: string) { }
}

export class CityCantHaveMoreTrainingFacilities implements Obstacle {
  readonly type = ObstacleType.CITY_CANT_HAVE_MORE_TRAINING_FACILITIES;
  constructor(public readonly errorText: string) { }
}

export class CityCantHaveMoreIncomingCaravans implements Obstacle {
  readonly type = ObstacleType.CITY_CANT_HAVE_MORE_INCOMING_CARAVANS;
  constructor(public readonly errorText: string) { }
}

export class NoHeroOffer implements Obstacle {
  readonly type = ObstacleType.NO_HERO_OFFER;
  constructor(public readonly errorText: string) { }
}

export class NoArmiesPresent implements Obstacle {
  readonly type = ObstacleType.NO_ARMIES_PRESENT;
  constructor(public readonly errorText: string) { }
}

export class NotEnoughFreeSpaceAtPlace implements Obstacle {
  readonly type = ObstacleType.NOT_ENOUGH_FREE_SPACE_AT_PLACE;
  constructor(public readonly errorText: string) { }
}

export class SuchFlaggableDoesntExist implements Obstacle {
  readonly type = ObstacleType.SUCH_FLAGGABLE_DOESNT_EXIST;
  constructor(public readonly errorText: string) { }
}

export class SuchRuinDoesntExist implements Obstacle {
  readonly type = ObstacleType.SUCH_RUIN_DOESNT_EXIST;
  constructor(public readonly errorText: string) { }
}

export class GroupDoesntContainSuchArmy implements Obstacle {
  readonly type = ObstacleType.GROUP_DOESNT_CONTAIN_SUCH_ARMY;
  constructor(public readonly errorText: string) { }
}

export class GroupsDontStandAtSamePlace implements Obstacle {
  readonly type = ObstacleType.GROUPS_DONT_STAND_AT_SAME_PLACE;
  constructor(public readonly errorText: string) { }
}

export class SourceAndDestinationAreIdentical implements Obstacle {
  readonly type = ObstacleType.SOURCE_AND_DESTINATION_ARE_IDENTICAL;
  constructor(public readonly errorText: string) { }
}

export class YouDontControlSuchHero implements Obstacle {
  readonly type = ObstacleType.YOU_DONT_CONTROL_SUCH_HERO;
  constructor(public readonly errorText: string) { }
}

export class SuchArtifactDoesntExist implements Obstacle {
  readonly type = ObstacleType.SUCH_ARTIFACT_DOESNT_EXIST;
  constructor(public readonly errorText: string) { }
}

export class HeroCantCarryMoreArtifacts implements Obstacle {
  readonly type = ObstacleType.HERO_CANT_CARRY_MORE_ARTIFACTS;
  constructor(public readonly errorText: string) { }
}

export class NoSuchArtifactAtPlace implements Obstacle {
  readonly type = ObstacleType.NO_SUCH_ARTIFACT_AT_PLACE;
  constructor(public readonly errorText: string) { }
}

export class HeroDoesntCarrySuchArtifact implements Obstacle {
  readonly type = ObstacleType.HERO_DOESNT_CARRY_SUCH_ARTIFACT;
  constructor(public readonly errorText: string) { }
}

export class PlaceNotExplorable implements Obstacle {
  readonly type = ObstacleType.PLACE_NOT_EXPLORABLE;
  constructor(public readonly errorText: string) { }
}

export class IllegalMoveType implements Obstacle {
  readonly type = ObstacleType.ILLEGAL_MOVE_TYPE;
  constructor(public readonly errorText: string) { }
}

export class ZeroLengthPath implements Obstacle {
  readonly type = ObstacleType.ZERO_LENGTH_PATH;
  constructor(public readonly errorText: string) { }
}

export class PathPlacesAreNotAdjacent implements Obstacle {
  readonly type = ObstacleType.PATH_PLACES_ARE_NOT_ADJACENT;
  constructor(public readonly errorText: string) { }
}

export class ImpassableTerrain implements Obstacle {
  readonly type = ObstacleType.IMPASSABLE_TERRAIN;
  constructor(public readonly errorText: string) { }
}

export class CrossingCoastOffPort implements Obstacle {
  readonly type = ObstacleType.CROSSING_COAST_OFF_PORT;
  constructor(public readonly errorText: string) { }
}

export class IllegalGroupSize implements Obstacle {
  readonly type = ObstacleType.ILLEGAL_GROUP_SIZE;
  constructor(public readonly errorText: string) { }
}

export class IllegalInitialMoveLeft implements Obstacle {
  readonly type = ObstacleType.ILLEGAL_INITIAL_MOVE_LEFT;
  constructor(public readonly errorText: string) { }
}

export class DestinationIsOutsideOfCalculatedValues implements Obstacle {
  readonly type = ObstacleType.DESTINATION_IS_OUTSIDE_OF_CALCULATED_VALUES;
  constructor(public readonly errorText: string) { }
}

export class NoMoveLeft implements Obstacle {
  readonly type = ObstacleType.NO_MOVE_LEFT;
  constructor(public readonly errorText: string) { }
}

export class SamePlayer implements Obstacle {
  readonly type = ObstacleType.SAME_PLAYER;
  constructor(public readonly errorText: string) { }
}

export class ValueOutOfRange implements Obstacle {
  readonly type = ObstacleType.VALUE_OUT_OF_RANGE;
  constructor(public readonly errorText: string) { }
}
