import { useSelector } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import { ProgramMode } from '../../model/ui/types';
import EditorActions from '../side-areas/EditorActions';
import GameActions from '../side-areas/GameActions';

export default function ActionArea() {
  const isEditor = useSelector((state: RootState) => state.ui.programMode === ProgramMode.MapEditor);

  return isEditor ? <EditorActions /> : <GameActions />;
}
