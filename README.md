# React Warlords

Browser strategy game. This repository contains multiple workspaces, which together create the whole game of Warlords.

### frontend
The playable game itself.

### ai-interface
Extension point when you want to write your custom AI plugin for the Warlords game.
Your AI project should use this as a dependency.

### ai-pioneer
Complex AI capable of playing the game. This is the default AI available.

## To install the game with all dependencies, do the following once:
1. Download and install volta. It's a JavaScript Tool Manager which will ensure you'll install a correct version of node.
2. Download and install node.js. It's a JavaScript runtime environment. that will handle dependencies of the project.
3. Run `npm run install` in this directory to install all projects at once.

## To play the game:
1. Switch to subdirectory `frontend`.
2. Run `npm start` in that subdirectory.
3. Your browser should automatically display the game. If not, head to page http://localhost:3000/
