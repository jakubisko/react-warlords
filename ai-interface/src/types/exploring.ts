export interface RuinData {

  /** Unique id. */
  readonly id: string;

  /** Level of the ruin. */
  readonly level: number;

  /** Explored ruin no longer contains enemies and treasure. */
  readonly explored: boolean;

  /** Position of the ruin. */
  readonly row: number;

  /** Position of the ruin. */
  readonly col: number;
}

/** When visiting a Sage Tower, you can ask him for Armies, Gold, Maps or Experience. */
export enum SageTowerChoice {
  Armies = "Armies",
  Gold = "Gold",
  Maps = "Maps",
  Experience = "Experience"
}

/** Describes entire course and the result of exploring a ruin or a Sage Tower. */
export interface ExploringResult {

  /** Which hero was exploring. */
  readonly heroId: string;

  /** Text that is displayed to the user. To add some drama, the message is displayed gradually. It has exactly four lines. */
  readonly message: string[];

  /** Whether the ruin becomes explored after the exploration. */
  readonly becomesExplored: boolean;

  /** Whether the hero died. */
  readonly heroDies: boolean;

  /** Amount of experience awarded by the hero. */
  readonly experiences: number;

  /** Amount of gold found. */
  readonly gold: number;

  /** Zero or more artifacts that were found. */
  readonly artifactIdsFound: string[];

  /** Zero or more army types that joined the hero. */
  readonly armyTypeIdsFound: string[];

  /** If some part of map is explored, these are the coordinates of the rectangle explored (inclusive). */
  readonly mapExplored: { minRow: number, maxRow: number, minCol: number, maxCol: number };
}
