
export interface MoveCostCalculator {

  /** Returns the cost to walk at given place. Returns Infinity if the group is not able to walk on such terrain. */
  getMoveCostAt(row: number, col: number): number;

  /**
   * Returns true when a group would illegally cross coast while moving between given two places.
   * This means:
   * 1. If the group flies, allows the move.
   * 2. If neither of the places is water, allows the move.
   * 3. If both places are water, but they are adjacent and disconnected lakes, forbids the move.
   * 4. If both places are water, allows the move.
   * 4. If any of the places is a port and the group doesn't move diagonally, allows the move.
   * 5. Forbids the move.
   * Call this only for two adjacent places.
   */
  isCoastCrossingForbiddenAt(row1: number, col1: number, row2: number, col2: number): boolean;

  /**
   * Returns true when a non-flying army moving between given two places would board/deboard a ship, thus losing rest of
   * the movement for the day.
   * This method does not check whether there is a port for that - call isCoastCrossingForbiddenAt() before this method to check that.
   * Call this only for two adjacent places.
   */
  isBoardingDeboardingShipAt(row1: number, col1: number, row2: number, col2: number): boolean;

  /** Returns true when the given place is water and the group doesn't fly. */
  isShipRequiredAt(row: number, col: number): boolean;

}
