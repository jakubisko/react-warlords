import './ArmyImage.scss';
import { IconImage, OtherIcon } from './IconImage';
import { Ability } from 'ai-interface/constants/ability';

interface IncomeAndFortificationDescriptionProps {
  income?: number;
  fortification?: number;
}

export const IncomeAndFortificationDescription = ({ income, fortification }: IncomeAndFortificationDescriptionProps) => (
  <div className="m-1 d-flex align-items-center justify-content-center">
    {income !== undefined && (
      <>
        <IconImage
          icon={OtherIcon.Price}
          tooltip="Daily income"
        />
        <span className="mx-2">
          {income} gold per day
        </span>
      </>
    )}

    {fortification !== undefined && (
      <>
        <IconImage
          icon={Ability.FortificationBonus}
          tooltip={`Fortification - Each defending army gets +${fortification} Strength.`}
        />
        <span className="mx-2">
          {fortification}
        </span>
      </>
    )}
  </div>
);
