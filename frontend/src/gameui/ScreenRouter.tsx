import { useSelector } from 'react-redux';
import { RootState } from '../model/root/configureStore';
import { ScreenEnum } from '../model/ui/types';
import GameOverScreen from './screens/GameOverScreen';
import GameScreen from './screens/GameScreen';
import MainMenuScreen from './screens/MainMenuScreen';
import NewGameMapScreen from './screens/NewGameMapScreen';
import NewGamePlayersScreen from './screens/NewGamePlayersScreen';
import NewGameOptionsScreen from './screens/NewGameOptionsScreen';
import MapZoomedScreen from './screens/MapZoomedScreen';
import TimelineScreen from './screens/TimelineScreen';
import { WaitScreen } from './screens/WaitScreen';
import ListOfOnlineGamesScreen from './screens/ListOfOnlineGamesScreen';
import OnlineLoginScreen from './screens/OnlineLoginScreen';
import LoginGuard from './common/LoginGuard';
import AiTakingTurnScreen from './screens/AiTakingTurnScreen';
import AiDisqualifiedScreen from './screens/AiDisqualifiedScreen';
import StartOfTurnScreen from './screens/StartOfTurnScreen';
import DuplicateStartOfTurnScreen from './screens/DuplicateStartOfTurnScreen';
import FatalErrorScreen from './screens/FatalErrorScreen';
import OnlineSavingFailedScreen from './screens/OnlineSavingFailedScreen';
import InitialLoadingScreen from './screens/InitialLoadingScreen';

export default function ScreenRouter() {
  const screen = useSelector((state: RootState) => state.ui.screen);

  switch (screen) {
    case ScreenEnum.MainMenuScreen: return <MainMenuScreen />;
    case ScreenEnum.LandScreen: return <GameScreen />;
    case ScreenEnum.OnlineLoginScreen: return <OnlineLoginScreen />;
    case ScreenEnum.ListOfOnlineGamesScreen: return <LoginGuard><ListOfOnlineGamesScreen /></LoginGuard>;
    case ScreenEnum.NewGameMapScreen: return <NewGameMapScreen />;
    case ScreenEnum.NewGamePlayersScreen: return <NewGamePlayersScreen />;
    case ScreenEnum.NewGameOptionsScreen: return <NewGameOptionsScreen />;
    case ScreenEnum.ZoomedMapScreen: return <MapZoomedScreen />;
    case ScreenEnum.WaitScreen: return <WaitScreen />;
    case ScreenEnum.AiTakingTurnScreen: return <AiTakingTurnScreen />;
    case ScreenEnum.AiDisqualifiedScreen: return <AiDisqualifiedScreen />;
    case ScreenEnum.StartOfTurnScreen: return <StartOfTurnScreen />;
    case ScreenEnum.DuplicateStartOfTurnScreen: return <DuplicateStartOfTurnScreen />;
    case ScreenEnum.OnlineSavingFailedScreen: return <OnlineSavingFailedScreen />;
    case ScreenEnum.GameOverScreen: return <GameOverScreen />;
    case ScreenEnum.TimelineScreen: return <TimelineScreen />;
    case ScreenEnum.FatalErrorScreen: return <FatalErrorScreen />;
    default: return <InitialLoadingScreen />;
  }
}
