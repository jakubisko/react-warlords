export function drawStar(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number, spikes: number, innerRadius: number, outerRadius: number) {
  ctx.beginPath();
  let rot = Math.PI / 2 * 3; // Orient first spike up, not right.

  for (let i = 0; i < spikes; i++) {
    const x1 = cx + Math.cos(rot) * outerRadius * pixelsPerTile;
    const y1 = cy + Math.sin(rot) * outerRadius * pixelsPerTile;
    ctx.lineTo(x1, y1);

    rot += Math.PI / spikes;

    const x2 = cx + Math.cos(rot) * innerRadius * pixelsPerTile;
    const y2 = cy + Math.sin(rot) * innerRadius * pixelsPerTile;
    ctx.lineTo(x2, y2);

    rot += Math.PI / spikes;
  }
  ctx.closePath();
}

export function drawPort(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) {
  ctx.moveTo(cx, cy - 0.5 * pixelsPerTile);
  ctx.lineTo(cx, cy + 0.5 * pixelsPerTile);
  ctx.moveTo(cx - 0.5 * pixelsPerTile, cy);
  ctx.lineTo(cx + 0.5 * pixelsPerTile, cy);
}

export function drawTemple(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) {
  // 3 vertical pillars
  ctx.moveTo(cx - 0.75 * pixelsPerTile, cy - 0.25 * pixelsPerTile);
  ctx.lineTo(cx - 0.75 * pixelsPerTile, cy + 1 * pixelsPerTile);

  ctx.moveTo(cx, cy - 0.25 * pixelsPerTile);
  ctx.lineTo(cx, cy + pixelsPerTile);

  ctx.moveTo(cx + 0.75 * pixelsPerTile, cy - 0.25 * pixelsPerTile);
  ctx.lineTo(cx + 0.75 * pixelsPerTile, cy + pixelsPerTile);

  // Triangle roof. Has to be drawn in four lines, because line ends and line bends look differently.
  ctx.moveTo(cx, cy - 0.5 * pixelsPerTile);
  ctx.lineTo(cx - 0.5 * pixelsPerTile, cy - 0.5 * pixelsPerTile);
  ctx.lineTo(cx, cy - 0.75 * pixelsPerTile);
  ctx.lineTo(cx + 0.5 * pixelsPerTile, cy - 0.5 * pixelsPerTile);
  ctx.lineTo(cx, cy - 0.5 * pixelsPerTile);
}

export function drawEncampment(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) {
  // Triangle roof. Has to be drawn in four lines, because line ends and line bends look differently.
  ctx.moveTo(cx, cy + 0.75 * pixelsPerTile);
  ctx.lineTo(cx - 0.6 * pixelsPerTile, cy + 0.75 * pixelsPerTile);
  ctx.lineTo(cx, cy - 0.5 * pixelsPerTile);
  ctx.lineTo(cx + 0.6 * pixelsPerTile, cy + 0.75 * pixelsPerTile);
  ctx.lineTo(cx, cy + 0.75 * pixelsPerTile);
}

export function drawGrove(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) {
  ctx.moveTo(cx, cy + 0.25 * pixelsPerTile);
  ctx.lineTo(cx, cy + 1 * pixelsPerTile);
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(cx, cy - 0.25 * pixelsPerTile, 0.5 * pixelsPerTile, 0, 2 * Math.PI);
}

export function drawSageTower(ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) {
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(cx, cy - 0.5 * pixelsPerTile, 0.25 * pixelsPerTile, 0, 2 * Math.PI);

  ctx.stroke();
  ctx.beginPath();
  ctx.arc(cx + 0.6 * pixelsPerTile, cy + 0.5 * pixelsPerTile, 0.25 * pixelsPerTile, 0, 2 * Math.PI);

  ctx.stroke();
  ctx.beginPath();
  ctx.arc(cx - 0.6 * pixelsPerTile, cy + 0.5 * pixelsPerTile, 0.25 * pixelsPerTile, 0, 2 * Math.PI);
}
