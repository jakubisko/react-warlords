import './RuinEditor.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import { setRuinLevel } from '../../../model/game/actionsEditor';
import { pushToUndoQueue } from '../../../model/editor/actions';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { experienceForRuin } from 'ai-interface/constants/experience';

interface RuinEditorProps {
  ruinId: string;
}

export default function RuinEditor({ ruinId }: RuinEditorProps) {
  const level = useSelector((state: RootState) => state.game.ruins[ruinId].level);
  const dispatch: AppDispatch = useDispatch();

  function handleSetRuinLevel(level: number) {
    dispatch(setRuinLevel(ruinId, level));
    dispatch(pushToUndoQueue());
  }

  return (
    <div>
      Ruin level:
      <DropdownButton
        id="ruinEditor"
        title={'Level ' + level}
        className="d-inline-block"
      >
        {experienceForRuin.map((_, index) => index).filter(level => level > 0).map(level => (
          <Dropdown.Item key={level} onClick={() => handleSetRuinLevel(level)}>
            Level {level}
          </Dropdown.Item>
        ))}
      </DropdownButton>
    </div>
  );
}
