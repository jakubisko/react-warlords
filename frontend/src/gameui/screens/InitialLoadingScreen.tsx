import './InitialLoadingScreen.scss';
import { useCallback, useMemo, useState } from 'react';
import backgroundMainMenu from '../common/images/backgroundMainMenu.jpg';
import parchmentBottom from '../common/images/parchment-bottom.webp';
import parchmentMiddle from '../common/images/parchment-middle.webp';
import parchmentTop from '../common/images/parchment-top.webp';
import wood from '../common/images/wood.webp';
import { IconBattle } from '../common/svg/IconBattle';
import { EMPTY_ARRAY } from '../../functions';
import MainMenuScreen from './MainMenuScreen';

export default function InitialLoadingScreen() {
  const [imagesLoading, setImagesLoading] = useState(5); // Number of images to load
  const handleImageLoaded = useCallback(() => setImagesLoading((value) => value - 1), EMPTY_ARRAY);

  // This loader is not the same element as the one on index.html, which would cause the animation to start from beginning.
  // However, if we offset both animations based on the time, we can synchronize them, preventing the glitching.
  const animationDelay = useMemo(() => performance.now() % 1000, EMPTY_ARRAY);

  return (
    <div className='initial-loading-screen'>
      <img className="opacity-0" src={backgroundMainMenu} alt="" onLoad={handleImageLoaded} />
      <img className="opacity-0" src={parchmentBottom} alt="" onLoad={handleImageLoaded} />
      <img className="opacity-0" src={parchmentMiddle} alt="" onLoad={handleImageLoaded} />
      <img className="opacity-0" src={parchmentTop} alt="" onLoad={handleImageLoaded} />
      <img className="opacity-0" src={wood} alt="" onLoad={handleImageLoaded} />

      <div id="splash-screen" className="fade-out-2">
        <div className={imagesLoading > 0 ? undefined : 'fade-out-1'}>
          <IconBattle />
        </div>
        <div className={imagesLoading > 0 ? undefined : 'opacity-0'}>
          <div id="loader" style={{ animationDelay: `-${animationDelay}ms` }} />
        </div>
      </div>

      <MainMenuScreen />
    </div>
  );
}
