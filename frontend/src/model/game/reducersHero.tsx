import { updateGrid, withoutUndefinedProperties } from '../../functions';
import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actionsHero';
import { reducer as reducerEditor } from './reducersEditor';
import { GameState } from './types';

export function reducer(state: GameState, action: AppAction): GameState {
  switch (action.type) {

    case ActionTypes.ADD_EXPERIENCES: {
      const { heroId, amount } = action.payload;
      const hero = state.heroes[heroId];
      return {
        ...state,
        heroes: {
          ...state.heroes,
          [heroId]: {
            ...hero,
            experience: Math.max(0, hero.experience + amount)
          }
        }
      };
    }

    case ActionTypes.SET_HERO_LEVEL: {
      const { heroId, level } = action.payload;
      const { heroes, groups, tiles } = state;
      const hero = heroes[heroId];
      const { row, col } = groups[hero.groupId];

      return {
        ...state,
        tiles: updateGrid(tiles, row, col, oldTile => ({
          ...oldTile,
          armies: oldTile.armies!.map(army => army.id === hero.armyId ? {
            ...army,
            armyTypeId: `hro${level}`
          } : army)
        })),
        heroes: {
          ...heroes,
          [heroId]: {
            ...hero,
            level
          }
        }
      };
    }

    case ActionTypes.CREATE_ARTIFACT_ON_GROUND: {
      const { row, col, artifactId } = action.payload;
      return {
        ...state,
        tiles: updateGrid(state.tiles, row, col, oldTile => ({
          ...oldTile,
          artifactIds: (oldTile.artifactIds === undefined) ? [artifactId] : [...oldTile.artifactIds, artifactId]
        }))
      };
    }

    case ActionTypes.CREATE_ARTIFACT_FOR_HERO: {
      const { heroId, artifactId } = action.payload;
      const hero = state.heroes[heroId];
      return {
        ...state,
        heroes: {
          ...state.heroes,
          [heroId]: {
            ...hero,
            artifactIds: (hero.artifactIds === undefined) ? [artifactId] : [...hero.artifactIds, artifactId]
          }
        }
      };
    }

    case ActionTypes.DESTROY_ARTIFACT_ON_GROUND: {
      const { row, col, artifactId } = action.payload;
      return {
        ...state,
        tiles: updateGrid(state.tiles, row, col, oldTile => {
          const { ...newTile } = oldTile;
          const newArtifactIds = [...newTile.artifactIds!];

          newArtifactIds.splice(newArtifactIds.indexOf(artifactId), 1);
          if (newArtifactIds.length > 0) {
            newTile.artifactIds = newArtifactIds;
          } else {
            delete newTile.artifactIds;
          }

          return newTile;
        })
      };
    }

    case ActionTypes.DESTROY_ARTIFACT_FOR_HERO: {
      const { heroId, artifactId } = action.payload;
      const { ...newHero } = state.heroes[heroId];
      const newArtifactIds = [...newHero.artifactIds!];

      newArtifactIds.splice(newArtifactIds.indexOf(artifactId), 1);
      if (newArtifactIds.length > 0) {
        newHero.artifactIds = newArtifactIds;
      } else {
        delete newHero.artifactIds;
      }

      return {
        ...state,
        heroes: {
          ...state.heroes,
          [heroId]: newHero
        }
      };
    }

    case ActionTypes.SET_RUIN_EXPLORED: {
      const { ruinId, explored } = action.payload;
      const { ruins, players, currentPlayerId } = state;
      const ruin = ruins[ruinId];

      return {
        ...state,
        ruins: {
          ...ruins,
          [ruinId]: withoutUndefinedProperties({
            ...ruin,
            explored: explored ? true : undefined,
            lastSeenExplored: ruin.lastSeenExplored.map((oldVal, teamId) => teamId === players[currentPlayerId].team ? explored : oldVal)
          })
        }
      };
    }

    default:
      return reducerEditor(state, action);
  }

}
