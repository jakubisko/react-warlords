import { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';

export default function FatalErrorScreen() {
  const [confirmed, setConfirmed] = useState(false);

  function handleConfirm() {
    setConfirmed(true);
    window.location.reload();
  }

  return (
    <Modal className="screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Error</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        An unrecoverable error occurred and the game needs to be restarted.
      </Modal.Body>

      <Modal.Footer className="justify-content-center">
        <Button
          onClick={handleConfirm}
          disabled={confirmed}
        >
          OK
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
