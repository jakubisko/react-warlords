/** How many armies can stand on one tile. How many armies can be in one group. */
export const maxTileCapacity = 8;

/** When moving straight horizontally or straight vertically, army's moveLeft is decreased by this multiplied by the terrain's move cost. */
export const straightMoveCost = 10;

/** When moving diagonally, army's moveLeft is decreased by this multiplied by the terrain's move cost. */
export const diagonalMoveCost = 14;

/** Daily movement on an army on a ship. */
export const shipMove = 20 * straightMoveCost;
