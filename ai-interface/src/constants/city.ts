/** Owner of the city receives this amount of gold each turn. */
export const cityIncome = 20;

/** Maximum number of different army types that can be trained in one city. */
export const maxCityTrainingCapacity = 4;

/** Maximum number of other cities that may redirect their training to one city. */
export const maxCaravanCapacity = 4;

/** How many extra days it takes for a trained army to appear in another city when using caravan to redirect training to other city. */
export const caravanDelayDays = 2;

/** How much gold it costs to demolish a training facility in a city. */
export const demolishCost = 25;

/** How much gold it costs to raze a city. */
export const razeCost = 50;
