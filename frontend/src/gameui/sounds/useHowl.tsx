import { Howl, HowlOptions } from 'howler';
import useSound from 'use-sound';

/**
 * Workaround for a wrong type declaration in "use-sound".
 *
 * Returned object will be null for a while until the browser loads the file. Always check for non-null value!
 */
export default function useHowl(howlOptions?: HowlOptions) {
  // eslint-disable-next-line
  return useSound('', howlOptions as any)[1].sound as (Howl | null);
}
