import './ListOfOnlineGamesScreen.scss';
import { useEffect, useMemo, useRef } from 'react';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import { ScreenEnum } from '../../model/ui/types';
import { useDispatch, useSelector } from 'react-redux';
import { setScreen } from '../../model/ui/actions';
import { createEmptyMap } from '../../model/game/actionsEditor';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import OnlineGameElement from '../modals/subcomponents/OnlineGameElement';
import PageableList from '../common/PageableList';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { Howler } from 'howler';
import FullscreenButton from '../common/FullscreenButton';
import { useOnlineUsers } from '../../model/online/useOnlineUsers';
import { useOnlineGames } from '../../model/online/useOnlineGames';
import { OnlineUser } from '../../model/online/types';

export default function ListOfOnlineGamesScreen() {
  // This screen is behind LoginGuard, so we're guaranteed to have a value.
  const uid = useSelector((state: RootState) => state.online.loggedInUser?.uid);

  const { games, loadingGames } = useOnlineGames();
  const { users, loadingUsers } = useOnlineUsers();

  const userUidToUser = useMemo(
    () => users.reduce(
      (acc, user) => { acc[user.uid] = user; return acc; },
      {} as { [uid: string]: OnlineUser }
    ),
    [users]
  );

  const dispatch: AppDispatch = useDispatch();

  const possibleActions = (!loadingGames && !loadingUsers)
    ? games.filter(game => game.userUids[game.currentPlayerId] === uid || game.gameOver).length
    : Infinity;
  const lastPossibleActions = useRef(Infinity);

  useEffect(() => {
    if (possibleActions > lastPossibleActions.current) {
      // We want this sound to be heard even when the app is not focused and thus it's muted.
      Howler.mute(false);
      const volumeBefore = Howler.volume();
      Howler.volume(0.5);

      soundEffects.play(SoundEffect.OnlineActionAvailable);
      soundEffects.once('end', () => {
        Howler.volume(volumeBefore);
      });
    }
    lastPossibleActions.current = possibleActions;
  }, [possibleActions]);

  function handleNewGame() {
    dispatch(createEmptyMap(0));
    dispatch(setScreen(ScreenEnum.NewGameMapScreen));
  }

  return (
    <Modal className="screen list-of-online-games-screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Online play</Modal.Title>
        <FullscreenButton />
      </Modal.Header>

      <Modal.Body className="pb-0">
        <PageableList
          list={games.map(game => (
            <OnlineGameElement key={game.id} onlineGame={game} userUidToUser={userUidToUser} />
          ))}
          working={loadingGames || loadingUsers}
          maxItemsPerPage={5}
          itemHeightPx={140}
          restOfHeightPx={150}
          footer={(
            <ButtonGroup>
              <Button onClick={handleNewGame}>New game</Button>
              <Button onClick={() => dispatch(setScreen(ScreenEnum.MainMenuScreen))}>Exit</Button>
            </ButtonGroup>
          )}
        />
      </Modal.Body>
    </Modal>
  );
}
