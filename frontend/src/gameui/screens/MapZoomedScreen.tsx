import './MapZoomedScreen.scss';
import '../side-areas/TwoItemsPerColumnContainer.scss';
import { useEffect } from 'react';
import StrategicMap from '../map/strategic-map/StrategicMap';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { useDispatch, useSelector } from 'react-redux';
import { ScreenEnum, ProgramMode } from '../../model/ui/types';
import { setMapFocus, setScreen } from '../../model/ui/actions';
import { Button } from 'react-bootstrap';
import { MapDisplaySettings } from '../../model/game/types';
import { setMapDisplaySettings } from '../../model/game/actionsPlaying';
import KingdomInfo from '../side-areas/KingdomInfo';
import { withoutUndefinedProperties } from '../../functions';
import { Place } from 'ai-interface/types/place';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBorderAll, faDragon, faLandmarkFlag, faRing, faSmog, faTree } from '@fortawesome/free-solid-svg-icons';
import { IconRoad } from '../common/svg/IconRoad';
import { IconCity } from '../common/svg/IconCity';
import { IconRuin } from '../common/svg/IconRuin';
import { IconBattle } from '../common/svg/IconBattle';

export default function MapZoomedScreen() {
  const programMode = useSelector((state: RootState) => state.ui.programMode);
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const mapDisplaySettings = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].mapDisplaySettings);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown, true);
    return () => window.removeEventListener('keydown', handleKeyDown, true);
  });

  function handleKeyDown(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      dispatch(setScreen(ScreenEnum.LandScreen));
    }
  }

  function handleStrategicMapOnClick({ row, col }: Place) {
    dispatch(setMapFocus(row, col));
    dispatch(setScreen(ScreenEnum.LandScreen));
  }

  function handleToggle(key: keyof MapDisplaySettings) {
    const newSettings: MapDisplaySettings = {
      ...mapDisplaySettings,
      [key]: mapDisplaySettings[key] ? undefined : true
    };
    dispatch(setMapDisplaySettings(currentPlayerId, withoutUndefinedProperties(newSettings)));
  }

  return (
    <div className="map-zoomed-screen screen h-100">

      <div className="side-menu">
        <KingdomInfo />
      </div>

      <div className="center-area">
        <div className="map-wrapper">
          <StrategicMap
            showFocusRectangle={true}
            onClick={handleStrategicMapOnClick}
          />
        </div>
      </div>

      <div className="side-menu">
        <div className="two-items-per-column-container align-items-center">
          <div className="button-row mt-auto">
            <Button active={mapDisplaySettings.showTerrain} onClick={() => handleToggle('showTerrain')}>
              <FontAwesomeIcon icon={faTree} />
              <div className="caption">Terrain</div>
            </Button>
            <Button active={mapDisplaySettings.showRoads} onClick={() => handleToggle('showRoads')}>
              <IconRoad />
              <div className="caption">Roads</div>
            </Button>
          </div>
          <div className="button-row">
            <Button active={mapDisplaySettings.showCities} onClick={() => handleToggle('showCities')}>
              <IconCity />
              <div className="caption">Cities</div>
            </Button>
            <Button active={mapDisplaySettings.showFlaggables} onClick={() => handleToggle('showFlaggables')}>
              <FontAwesomeIcon icon={faLandmarkFlag} />
              <div className="caption">Flaggables</div>
            </Button>
          </div>
          <div className="button-row">
            <Button active={mapDisplaySettings.showRuins} onClick={() => handleToggle('showRuins')}>
              <IconRuin />
              <div className="caption">Ruins</div>
            </Button>
            <Button active={mapDisplaySettings.showArtifacts} onClick={() => handleToggle('showArtifacts')}>
              <FontAwesomeIcon icon={faRing} />
              <div className="caption">Artifacts</div>
            </Button>
          </div>
          {programMode === ProgramMode.Playing && (
            <div className="button-row">
              <Button active={mapDisplaySettings.showFogOfWar} onClick={() => handleToggle('showFogOfWar')}>
                <FontAwesomeIcon icon={faSmog} />
                <div className="caption">Fog of war</div>
              </Button>
              <Button active={mapDisplaySettings.showWarReport} onClick={() => handleToggle('showWarReport')}>
                <IconBattle />
                <div className="caption">Battles</div>
              </Button>
            </div>
          )}
          <div className="button-row">
            <Button active={mapDisplaySettings.showArmies} onClick={() => handleToggle('showArmies')}>
              <FontAwesomeIcon icon={faDragon} />
              <div className="caption">Armies</div>
            </Button>
            <Button active={mapDisplaySettings.showGrid} onClick={() => handleToggle('showGrid')}>
              <FontAwesomeIcon icon={faBorderAll} />
              <div className="caption">Grid</div>
            </Button>
          </div>
          <div className="mb-auto" />
        </div>
      </div>
    </div>
  );
}
