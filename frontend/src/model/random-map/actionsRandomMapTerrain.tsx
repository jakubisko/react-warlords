import { AppDispatch, RootState } from '../root/configureStore';
import { drawWithTerrainBrush, setTerrain } from '../game/actionsEditor';
import { Terrain } from 'ai-interface/constants/terrain';
import { createNoise2D } from 'simplex-noise';
import { createGrid, createArray, shuffle, clamp } from '../../functions';
import { Place } from 'ai-interface/types/place';
import { getAllWaterAreas } from './selectors';
import { RandomMapSettings } from '../editor/types';

// Higher coarseness leads to lower noise in the heights.
const heightCoefs = [
  { coarseness: 5, weight: 0.04 },
  { coarseness: 20, weight: 0.22 },
  { coarseness: 40, weight: 0.32 },
  { coarseness: 80, weight: 0.42 }
];

/** Linear extrapolation. Curried for tiny speed-up.  */
function lerp(xMin: number, xMax: number, yMin: number, yMax: number): (x: number) => number {
  const slope = (yMax - yMin) / (xMax - xMin);
  return x => yMin + slope * (x - xMin);
}

/** Returns grid of random heights that are guaranteed to be in the range from 0 inclusive to 1 exclusive. */
function generateHeights(settings: RandomMapSettings): number[][] {
  const { size, water, mountains } = settings;

  const noise2D = createArray(heightCoefs.length, () => createNoise2D());

  const heights = createGrid(size, (row, col) => {
    let height = 0;
    for (let i = 0; i < heightCoefs.length; i++) {
      const { coarseness, weight } = heightCoefs[i];
      height += (noise2D[i])(row / coarseness, col / coarseness) * weight;
    }
    return height;
  });

  // Now normalize the height map so that the requested percent of map is covered by water and the requested percent is covered by mountains.
  const allHeights = heights.flat().sort((a, b) => a - b);
  const waterCoverage = (water ** 1.3) * 0.5;
  const mountainCoverage = (1 - waterCoverage) * (mountains ** 1.3) * 0.2; // The percent of the solid ground covered by mountains.
  const xMin = allHeights[Math.floor(waterCoverage * (size * size - 1))];
  const xMax = allHeights[Math.floor((1 - mountainCoverage) * (size * size - 1))];
  const yMin = 1 / 9; // Because our Whittaker table of biomes is 9x9. 1 row is water and rounding is up.
  const yMax = 8 / 9; // 1 row is mountains and rounding is up.

  const normalizeHeight = lerp(xMin, xMax, yMin, yMax);
  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      heights[row][col] = clamp(0, 0.999, normalizeHeight(heights[row][col]));
    }
  }

  return heights;
}

function generateTemperatures(settings: RandomMapSettings, heights: number[][]): number[][] {
  const { size, heat, cold } = settings;

  const mid = (heat + 1 - cold) / 2;
  const tempMin = lerp(0, 1, mid, -0.1)(cold);
  const tempMax = lerp(0, 1, mid, 1.1)(heat);
  const rotation = Math.random() * 2 * Math.PI;

  const toUnitCircle = lerp(0, size - 1, -1, 1);
  const toTemperature = lerp(-1, 1, tempMin, tempMax);

  return createGrid(size, (row, col) => {
    const dist = toUnitCircle(col) * Math.sin(rotation) + toUnitCircle(row) * Math.cos(rotation);
    const temperature = 0.25 * (1 - heights[row][col]) + 0.75 * toTemperature(dist);
    return clamp(0, 0.999, temperature);
  });
}

/** Higher precipitation means more swamps. */
function generatePrecipitation(settings: RandomMapSettings): number[][] {
  const { size, mountains, water } = settings;
  // Mountainous terrain decreases precipitation and the number of swamps. Watery terrain increases precipitation and the number of swamps.
  const swampy = 0.5 - 0.4 * mountains + 0.3 * water;

  const noise2D = createNoise2D();

  return createGrid(size, (row, col) => {
    const precipitation = swampy + 0.5 * noise2D(row / 70, col / 70);
    return clamp(0, 0.999, precipitation);
  });
}

// TOP - sea level
// BOTTOM - highest mountain
// LEFT - cold
// RIGHT - hot
// S - if precipitation is higher than 0.5, Swamp is created. Otherwise it will be Open.
// T - if precipitation is higher than 0.5, Forest is created. Otherwise it will be Ice. (T = Tundra)
const whittakerBiomes: string[][] = [
  ['~', '~', '~', '~', '~', '~', '~', '~', '~'],
  ['I', 'T', 'O', 'O', 'S', 'S', 'S', 'O', 'D'],
  ['I', 'T', 'O', 'O', 'O', 'S', 'S', 'O', 'D'],
  ['I', 'T', 'F', 'O', 'O', 'S', 'O', 'D', 'D'],
  ['I', 'I', 'F', 'F', 'O', 'O', 'D', 'D', 'D'],
  ['I', 'I', 'O', 'F', 'F', 'F', 'O', 'D', 'D'],
  ['I', 'I', 'I', 'O', 'F', 'F', 'F', 'O', 'D'],
  ['H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H'],
  ['M', 'M', 'M', 'M', 'M', 'M', 'V', 'V', 'V']
];

/** Returns a biome as defined by local conditions. Uses a Whittaker Diagram modified for our terrain types. */
function determineBiome(height: number, temperature: number, precipitation: number): Terrain {
  let terrain = whittakerBiomes[Math.floor(9 * height)][Math.floor(9 * temperature)];
  if (terrain === Terrain.Swamp && precipitation < 0.5) {
    terrain = Terrain.Open;
  } else if (terrain === 'T') {
    terrain = precipitation < 0.5 ? Terrain.Ice : Terrain.Forest;
  }

  return terrain as Terrain;
}

const drawRivers = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size } = getState().game;

  function constructRandomRiver(row1: number, col1: number, length: number) {
    const river: Place[] = [{ row: row1, col: col1 }];
    let rowFloat = row1;
    let colFloat = col1;
    let direction = Math.random() * 2 * Math.PI;

    for (let i = 0; i < length; i++) {
      direction += (Math.random() - 0.5) * (0.8 + i / 20); // The river will get more curvy farther from the sea it is.
      rowFloat += Math.sin(direction);
      colFloat += Math.cos(direction);
      const row2 = Math.round(rowFloat);
      const col2 = Math.round(colFloat);

      if (row2 < 0 || col2 < 0 || row2 >= size || col2 >= size) {
        break;
      }

      if (row1 !== row2 || col1 !== col2) {
        river.push({ row: row2, col: col2 });
        if (row1 !== row2 && col1 !== col2) {
          // We moved diagonally. We need to add extra tile so that the entire river is connected by sides.
          river.push({ row: row1, col: col2 });
        }

        row1 = row2;
        col1 = col2;
      }
    }

    if (river.length > 5) {
      river.forEach(({ row, col }) => dispatch(drawWithTerrainBrush(row, col, Terrain.Water, 1)));
    }
  }

  // Randomly pick some shores and start rivers there.
  const allShores = shuffle(getAllWaterAreas(getState()).flatMap(x => x.adjacentContinents.flatMap(x => x.shore)));
  for (let i = Math.floor(Math.min(allShores.length / 10, size / 10)) - 1; i >= 0; i--) {
    const { row, col } = allShores[i];
    constructRandomRiver(row, col, 10 + size / 10);
  }
};

export const createRandomMapTerrain = (settings: RandomMapSettings) => (dispatch: AppDispatch) => {
  const { size } = settings;

  const heights = generateHeights(settings);
  const temperatures = generateTemperatures(settings, heights);
  const precipitations = generatePrecipitation(settings);

  const terrain = createGrid(size, (row, col) => determineBiome(heights[row][col], temperatures[row][col], precipitations[row][col]));
  dispatch(setTerrain(terrain));

  drawRivers(); // requires the terrain to be already placed, because it calls getAllWaterAreas(getState()).
  dispatch(drawRivers());
};
