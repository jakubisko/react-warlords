import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actions';
import { OnlineState } from './types';

export const initialState: OnlineState = {
  onlineGameId: null,
  onlineTimesLoaded: 0,
  onlineToken: '',
  loggedInUser: null
};

export function onlineReducer(state = initialState, action: AppAction): OnlineState {
  switch (action.type) {

    case ActionTypes.SET_ONLINE_GAME_ID: {
      const { onlineGameId } = action.payload;
      return {
        ...state,
        onlineGameId
      };
    }

    case ActionTypes.SET_ONLINE_TIMES_LOADED: {
      const { onlineTimesLoaded } = action.payload;
      return {
        ...state,
        onlineTimesLoaded
      };
    }

    case ActionTypes.SET_ONLINE_TOKEN: {
      const { onlineToken } = action.payload;
      return {
        ...state,
        onlineToken
      };
    }

    case ActionTypes.SET_LOGGED_IN_USER: {
      const { loggedInUser } = action.payload;
      return {
        ...state,
        loggedInUser
      };
    }

    default:
      return state;
  }
}
