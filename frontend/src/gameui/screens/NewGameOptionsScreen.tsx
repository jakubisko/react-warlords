import './NewGameOptionsScreen.scss';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, ButtonGroup } from 'react-bootstrap';
import { ScreenEnum } from '../../model/ui/types';
import { setScreen } from '../../model/ui/actions';
import { setAiAdvantage, setAiOutputVisible, setWinCondition } from '../../model/game/actionsTurnPassing';
import { startNewGame } from '../../model/game/actionsTurnPassingThunks';
import { WinCondition } from 'ai-interface/constants/winCondition';

function generateRandomTitle(): string {
  const dateTimeFormat = new Intl.DateTimeFormat('en', { day: 'numeric', month: 'long', hour: '2-digit', hour12: false, minute: '2-digit' });
  return dateTimeFormat.format(new Date());
}

export default function NewGameOptionsScreen() {
  const players = useSelector((state: RootState) => state.game.players);
  const onlineGameId = useSelector((state: RootState) => state.online.onlineGameId);
  const dispatch: AppDispatch = useDispatch();

  const [_winCondition, _setWinCondition] = useState(WinCondition.CaptureAllEnemyCities);
  const [exploredMap, setExploredMap] = useState(false);
  const [shuffleStartingPositions, setShuffleStartingPositions] = useState(false);
  const [_aiOutputVisible, _setAiOutputVisible] = useState(players.every(({ alive, aiName }) => !alive || aiName !== undefined));
  const [_aiAdvantage, _setAiAdvantage] = useState(0);
  const [title, setTitle] = useState(onlineGameId === null ? undefined : generateRandomTitle());

  function handleStartNewGame() {
    dispatch(setWinCondition(_winCondition));
    dispatch(setAiOutputVisible(_aiOutputVisible));
    dispatch(setAiAdvantage(_aiAdvantage));
    dispatch(startNewGame(shuffleStartingPositions, exploredMap, title));
  }

  let problem = '';
  if (!_aiOutputVisible && !players.some(({ alive, aiName }) => alive && aiName === undefined)) {
    problem = 'Show data required when no humans play';
  } else if (title !== undefined && title.trim().length === 0) {
    problem = 'Game title required';
  }

  return (
    <Modal className="new-game-options-screen screen overflow-hidden" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Additional options</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {onlineGameId !== null && (
          <div className="row">
            <div className="col-5 d-inline-flex align-items-center">
              Game title
            </div>
            <div className="col-7">
              <input
                className="form-control"
                type="text"
                placeholder="Enter title"
                maxLength={30}
                value={title}
                onChange={e => setTitle(e.target.value)}
              />
            </div>
          </div>
        )}

        <div className="row">
          <div className="col-5 d-inline-flex align-items-center">
            Win/loss condition
          </div>
          <div className="col-7 d-inline-flex align-items-center justify-content-between">
            <select
              className="form-control"
              value={_winCondition}
              onChange={e => _setWinCondition(e.target.value as WinCondition)}
            >
              {Object.values(WinCondition).map(val => (
                <option key={val} value={val}>
                  {val}
                </option>
              ))}
            </select>
          </div>
        </div>

        {players.some(({ alive, aiName }) => alive && aiName !== undefined) && (
          <div className="row">
            <div className="col-5 d-inline-flex align-items-center">
              Computer advantage
            </div>
            <div className="col-7 d-inline-flex align-items-center justify-content-between">
              <select
                className="form-control"
                value={_aiAdvantage}
                onChange={e => _setAiAdvantage(parseInt(e.target.value))}
              >
                {[-50, -25, 0, 25, 50, 75, 100].map(val => (
                  <option key={val} value={val}>
                    {val === 0 ? `Same as human` : ((val > 0 ? '+' : '') + val + '% income & experience')}
                  </option>
                ))}
              </select>
            </div>
          </div>
        )}

        <div className="row">
          <div className="col-8 d-inline-flex align-items-center">
            Start with explored map
          </div>
          <div className="col-4">
            <ButtonGroup>
              <Button active={exploredMap} onClick={() => setExploredMap(true)}>Yes</Button>
              <Button active={!exploredMap} onClick={() => setExploredMap(false)}>No</Button>
            </ButtonGroup>
          </div>
        </div>

        <div className="row">
          <div className="col-8 d-inline-flex align-items-center">
            Shuffle starting positions
          </div>
          <div className="col-4">
            <ButtonGroup>
              <Button active={shuffleStartingPositions} onClick={() => setShuffleStartingPositions(true)}>Yes</Button>
              <Button active={!shuffleStartingPositions} onClick={() => setShuffleStartingPositions(false)}>No</Button>
            </ButtonGroup>
          </div>
        </div>

        {players.some(({ alive, aiName }) => alive && aiName !== undefined) && onlineGameId === null && (
          <div className="row">
            <div className="col-8 d-inline-flex align-items-center">
              Show computer players&apos; debug data
            </div>
            <div className="col-4">
              <ButtonGroup>
                <Button active={_aiOutputVisible} onClick={() => _setAiOutputVisible(true)}>Yes</Button>
                <Button active={!_aiOutputVisible} onClick={() => _setAiOutputVisible(false)}>No</Button>
              </ButtonGroup>
            </div>
          </div>
        )}
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={() => dispatch(setScreen(ScreenEnum.NewGamePlayersScreen))}>
          Back
        </Button>

        <Button
          disabled={problem !== ''}
          onClick={() => handleStartNewGame()}
        >
          {problem || 'Start'}
        </Button>
      </Modal.Footer>
    </Modal >
  );
}
