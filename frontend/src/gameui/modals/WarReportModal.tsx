import './WarReportModal.scss';
import type { JSX } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal } from 'react-bootstrap';
import { setShowingWarReport } from '../../model/ui/actions';
import { WarReportType } from 'ai-interface/types/warReport';
import PageableList from '../common/PageableList';

export default function WarReportModal() {
  const players = useSelector((state: RootState) => state.game.players);
  const warReports = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].warReports);
  const dispatch: AppDispatch = useDispatch();

  function processWarReportItem(caption: string, reportType: WarReportType) {
    const value = warReports.map(item => item.reportType === reportType ? item.value : 0).reduce((sum, val) => sum + val, 0);
    if (value > 0) {
      reportRows.push(
        <div className="row">
          <div className="col-10 py-2">{caption}</div>
          <div className="col-2 py-2 text-right">{value}</div>
        </div>
      );
    }
  }

  const reportRows: JSX.Element[] = [];

  for (const { reportType, playerId } of warReports) {
    if (reportType === WarReportType.PlayerEliminated) {
      reportRows.push(
        <div className="row text-truncate">
          <div className="col py-2">
            {players[playerId].name} was eliminated.
          </div>
        </div>
      );
    } else if (reportType === WarReportType.PlayerEliminatedAfterError) {
      reportRows.push(
        <div className="row text-truncate">
          <div className="col py-2">
            Computer {players[playerId].name} threw an error.
          </div>
        </div>
      );
    }
  }

  processWarReportItem('Cities lost:', WarReportType.CityLost);
  processWarReportItem('Flaggable structures lost:', WarReportType.FlaggableLost);
  processWarReportItem('Armies deserted:', WarReportType.ArmiesDeserted);
  processWarReportItem('Armies lost in combat:', WarReportType.ArmiesLostInCombat);
  processWarReportItem('Enemy armies killed:', WarReportType.EnemyArmiesKilled);
  processWarReportItem('Heroes promoted:', WarReportType.HeroPromoted);

  return (
    <Modal className="war-report-modal overflow-hidden" show centered onHide={() => dispatch(setShowingWarReport(false))}>
      <Modal.Header>
        <Modal.Title>War report</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="container">
          <PageableList
            list={reportRows}
            maxItemsPerPage={5} // This can't be larger than 6 because we need this modal to be smaller than the HeroOfferModal.
            itemHeightPx={36}
            restOfHeightPx={140}
            footer={<Button onClick={() => dispatch(setShowingWarReport(false))}>Close</Button>}
          />
        </div>
      </Modal.Body>
    </Modal>
  );
}
