import './DuplicateStartOfTurnScreen.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { setScreen } from '../../model/ui/actions';
import { Button, Modal } from 'react-bootstrap';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import { endUiTurn } from '../../model/game/actionsTurnPassingThunks';
import { ScreenEnum } from '../../model/ui/types';

export default function DuplicateStartOfTurnScreen() {
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const currentPlayerName = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].name);
  const onlineTimesLoaded = useSelector((state: RootState) => state.online.onlineTimesLoaded);
  const dispatch: AppDispatch = useDispatch();

  return (
    <Modal className="screen duplicate-start-of-turn-screen overflow-hidden" animation={false} show centered backdrop="static">
      <Modal.Header>
        <ModalTitleWithFlags owner={currentPlayerId} caption={currentPlayerName} />
      </Modal.Header>

      <Modal.Body className="p-3">
        You have already loaded this game state {onlineTimesLoaded > 1 ? `${onlineTimesLoaded} times` : `once`}  in the past;
        possibly in another window or on another device.
        To prevent cheating, you can&apos;t load the same game state multiple times. Please finish you turn in the first opened window.
        If you lost the first window with this game, you will have to skip your entire turn.
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={() => dispatch(endUiTurn())}>
          Skip turn
        </Button>

        <Button onClick={() => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen))}>
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
