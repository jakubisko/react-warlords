import { RootState } from '../root/configureStore';
import { OnlineGame, OnlineGameState } from './types';
import { maxPlayers } from 'ai-interface/constants/player';
import { compressObject, buildVersion } from '../game/serialization';
import { isGameOverForVisiblePlayers } from '../game/selectorsTurnPassing';

export async function getDataForOnlineSave(rootState: RootState): Promise<{
  onlineGame: Omit<OnlineGame, 'id' | 'createdAt' | 'title'>,
  onlineGameState: OnlineGameState
}> {
  const { game, game: { players, turnNumber, currentPlayerId } } = rootState;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { backgroundSprites, ...saveGameData } = game;
  const stateSerialized = await compressObject(saveGameData, 'game', 'string');
  const onlineGameState: OnlineGameState = { stateSerialized };

  const onlineGame: Omit<OnlineGame, 'id' | 'createdAt' | 'title'> = {
    buildVersion,
    gameOver: isGameOverForVisiblePlayers(rootState),
    currentPlayerId,
    timesLoaded: 0, // Reset the counter each turn.
    token: `${turnNumber}:${currentPlayerId}:${Math.random()}`,
    userUids: players.map(player => player.onlineUid ?? '').slice(0, maxPlayers),
    aiNames: players.map(player => player.aiName ?? '').slice(0, maxPlayers),
    alive: players.map(player => player.alive).slice(0, maxPlayers)
  };

  return {
    onlineGame,
    onlineGameState
  };
}