import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { Place } from 'ai-interface/types/place';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { SearchPathArgs, MoveType } from 'ai-interface/types/path';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { Ability } from 'ai-interface/constants/ability';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { CityData } from 'ai-interface/types/city';
import { PRETEND_ITS_TURN_NUMBER } from './main';
import { clamp } from './helpers/functions';

/** Maximum travel time of enemy armies when calculating the threat map. (0 - one day, 1 - two days etc.) */
const MAX_DAY = 2;

/** threatPowers[s] = number of armies of power "s" that can reach given city. Power = Strength * HitPoints. */
type ThreatPowers = number[];

/**
 * Threat = combined power of the most powerful group that your enemy could muster in given number of days against a given city.
 * Basically merges all enemy armies that are within given number of days of walk and then selects the most powerful group from them.
 * Power = Strength * HitPoints.
 * 
 * There are two sources of enemies:
 * 1. Enemies already on the map.
 * 2. Cities that create new armies.
 * 
 * When you destroy an enemy army or when you capture an enemy city, we will then decrease the threat.
 */
export class ThreatManager {

  /** Power = Strength * HitPoints. Guard Power also includes some defensive abilities. */
  public readonly armyTypeIdToGuardPower: { [armyTypeId: string]: number } = {};

  /** Power = Strength * HitPoints. Attack Power also includes some offensive abilities. */
  public readonly armyTypeIdToAttackPower: { [armyTypeId: string]: number } = {};

  private readonly maxAttackPower!: number;

  private readonly isEnemy: boolean[] = [];

  /** Armies that are farther away have lesser weight for getThreatToCityInNearFuture(). */
  private readonly chanceOfAttackOnDay: number[] = [];

  /** [0] is armies that can arrive in 1 day; [1] is armies that can arrive in 1 or 2 days etc. */
  private readonly threatsToCity: { [cityId: string]: ThreatPowers[] } = {};

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi
  ) {
    for (const { id, strength, hitPoints, abilities, hero } of Object.values(standardArmyTypes)) {
      this.armyTypeIdToGuardPower[id] = hitPoints * (
        (hero ? 0 : strength) // We don't want the hero to get stuck guarding.
        + (abilities[Ability.StrengthBonusInCity] ?? 0)
        + (abilities[Ability.MoraleBonus] ?? 0) * 2
        + (abilities[Ability.FrighteningBonus] ?? 0) * 2
        + Math.floor((abilities[Ability.Ambush] ?? 0) / 16)
        + (abilities[Ability.StrengthBonusWhenDefending] ?? 0)
      );

      this.armyTypeIdToAttackPower[id] = hitPoints * (
        strength
        + (abilities[Ability.StrengthBonusInCity] ?? 0)
        + (abilities[Ability.MoraleBonus] ?? 0) * 2
        + (abilities[Ability.Leadership] ?? 0) * 2
        + (abilities[Ability.FrighteningBonus] ?? 0) * 2
        + Math.floor((abilities[Ability.Ambush] ?? 0) / 16)
        + (abilities[Ability.StrengthBonusWhenAttacking] ?? 0)
        + (abilities[Ability.CancelEnemyFortificationBonus] ?? 0) * 2
        + (hero ? parseInt(id.charAt(3)) : 0) * 2 // Represents that the hero carries some artifacts; more on higher levels
      );
    }

    this.maxAttackPower = Math.max(...Object.values(this.armyTypeIdToAttackPower));

    const playersTeam = this.game.getPlayersTeam();
    for (let playerId = 0; playerId <= maxPlayers; playerId++) {
      this.isEnemy.push(playerId !== neutralPlayerId && playersTeam[playerId] !== playersTeam[game.getMyPlayerId()]);
    }

    this.determineAndDrawChanceOfAttackOnDay();
  }

  /** Power of the most powerful group that could attack the given city in given day in near future. (0 - next day, 1 - in next two days etc.) */
  public getThreatToCityUpToDay(cityId: string, daysInFuture: number): number {
    const threatPowers = this.threatsToCity[cityId][daysInFuture];
    let result = 0;
    let slotsLeft = maxTileCapacity;

    for (let power = this.maxAttackPower; power > 0 && slotsLeft > 0; power--) {
      // It is possible that ThreatPowers contains a negative value, indicating -2 armies of some power. Ignore such values.
      // This is because when destroying enemies mid-turn, it's hard to determine movement of those destroyed armies.
      // For example, if enemy group is destroyed in two combats, its movement type may have changed in between.
      let delta = clamp(0, slotsLeft, threatPowers[power]);
      result += delta * power;
      slotsLeft -= delta;
    }

    return result;
  }

  /**
   * Weighted power of the most powerful armies that could attack the given city in near future.
   * Immediate threats are usually smaller but have higher weight.
   */
  public getThreatToCityInNearFuture(cityId: string): number {
    const threatForEachDay: number[] = [];
    for (let day = 0; day <= MAX_DAY; day++) {
      threatForEachDay.push(this.getThreatToCityUpToDay(cityId, day) * this.chanceOfAttackOnDay[day]);
    }
    return Math.ceil(Math.max(...threatForEachDay));
  }

  public calculateThreatToEachCity(): void {
    const size = this.land.getSize();

    const zeroThreat: ThreatPowers = [];
    for (let i = 0; i <= this.maxAttackPower; i++) {
      zeroThreat.push(0);
    }

    for (const cityId of this.land.getAllCityIds()) {
      const threatsToCity: ThreatPowers[] = [];
      for (let i = 0; i <= MAX_DAY; i++) {
        threatsToCity.push([...zeroThreat]);
      }
      this.threatsToCity[cityId] = threatsToCity;
    }

    // Search entire land for all enemy armies and spread their threat.
    for (let row = 0; row < size; row++) {
      for (let col = 0; col < size; col++) {
        const place = { row, col };
        const armyTypeIds = this.game.getArmyTypeIdsAt(place);
        if (armyTypeIds.length > 0) {
          const owner = this.game.getOwnerOfPlace(place)!;
          if (this.isEnemy[owner]) {
            this.spreadThreatOfArmyTypes(place, armyTypeIds, 1);
          }
        }
      }
    }

    // Search all enemy cities and spread threat of armies that will be produced and/or arrive through the caravan next turn.
    for (const city of this.land.getAllCityIds()
      .map(cityId => this.game.getCity(cityId))
    ) {
      const armyTypeIds = this.threatProducedByCity(city);
      if (armyTypeIds.length > 0) {
        this.spreadThreatOfArmyTypes(city, armyTypeIds, 1);
      }
    }
  }

  /** Given a city, if it belongs to an enemy, returns the armyTypeIds that will be produced and/or arrive through the caravan next turn. */
  public threatProducedByCity(city: CityData): string[] {
    if (city.razed || !this.isEnemy[city.owner]) {
      return [];
    }

    const armyTypeIds = city.incomingCaravans
      .filter(x => x.days === 1)
      .map(x => x.armyTypeId);
    if (city.turnsTrainingLeft === 1) {
      armyTypeIds.push(city.currentlyTrainingArmyTypeId!);
    }
    return armyTypeIds;
  }

  /** This CAN be used to spread negative enemy threat - for example when a part of enemy armies was destroyed. */
  public spreadThreatOfArmyTypes(start: Place, armyTypeIds: string[], positive: 1 | -1): void {
    // Enemy can split the group and move independently. This enables him to do tricks like move Light Cavalry together with Orcs to
    // decrease penalty on Swamp, then after Orcs have no move left, separate Light Cavalry and move farther. There is no simple way how
    // to analyze this.
    // But still we want to at least offer some simple & fast method to prevent trick like Giant Crow splitting from ground army and flying
    // above the mountain to grab an empty city. As such:
    // 1. We'll spread threat for flying armies separately.
    // 2. We'll use the average speed of the armies instead of the speed of the slowest army.

    for (let flies of [0, 1]) {
      const armyTypeIdsFiltered = armyTypeIds.filter(armyTypeId => (standardArmyTypes[armyTypeId].abilities[Ability.Flies] ?? 0) === flies);

      if (armyTypeIdsFiltered.length > 0) {
        const armyPowers = armyTypeIdsFiltered.map(armyTypeId => this.armyTypeIdToAttackPower[armyTypeId]);
        const avgMovePerTurn = armyTypeIdsFiltered.reduce((sum, armyTypeId) => sum + standardArmyTypes[armyTypeId].move, 0) / armyTypeIdsFiltered.length;
        const moveType: MoveType = {
          // .slice(...) is an ugly fix - getMoveTypeOfArmyTypes() doesn't allow group size to be above the maximum.
          // However, when spreading negative threat after enemy city is defeated, there may be more than maximum group size defeated enemies.
          ...this.game.getMoveTypeOfArmyTypes(armyTypeIdsFiltered.filter((v, i, a) => a.indexOf(v) === i).slice(0, maxTileCapacity)),
          groupSize: Math.min(armyTypeIdsFiltered.length, maxTileCapacity),
          groupMoveOnLand: Math.ceil(avgMovePerTurn)
        };

        this.spreadThreatOfMovementType(start, moveType, armyPowers, positive);
      }
    }
  }

  private spreadThreatOfMovementType(start: Place, moveType: MoveType, armyPowers: number[], positive: 1 | -1): void {
    const reachableCityIds: { [cityId: string]: boolean } = {};

    const searchArgs: SearchPathArgs = {
      start,
      isDestination: (row, col) => {
        const cityId = this.land.getCityIdAt({ row, col });
        if (cityId !== undefined) {
          reachableCityIds[cityId] = true;
        }
        return false;
      },
      isToBeAvoided: () => false,
      moveType,
      initialMoveLeft: moveType.groupMoveOnLand,
      maxDay: MAX_DAY
    };
    const { days } = this.game.calculateTimeMatrix(searchArgs);

    for (const cityId in reachableCityIds) {
      const { row, col } = this.game.getCity(cityId);
      for (let day = days[row][col]; day <= MAX_DAY; day++) {
        const threatsToCity = this.threatsToCity[cityId][day];

        for (const power of armyPowers) {
          threatsToCity[power] += positive;
        }
      }
    }
  }

  private determineAndDrawChanceOfAttackOnDay() {
    // Experience shows that it's best to take threats from higher distances into account only in longer game.
    // Otherwise we would get stuck in the starting city.
    // Following are NOT relevant:
    // * Size of the map - even on small map we like to check higher distance threats in a longer game.
    // * Number of enemies - that is directly in the value of the threat.
    // * My gold or income - that is taken into account in the SpendingManager which determines how much to spend on guarding.
    // In long game, our target is [1, 0.5, 0.33]. In short game, we first fill one item, then second etc.
    const turn = ((PRETEND_ITS_TURN_NUMBER ?? this.game.getTurnNumber()) + 4) / 5;
    for (let day = 0; day <= MAX_DAY; day++) {
      const max = 1 / (day + 1);
      const progress = clamp(0, 1, turn - day);
      this.chanceOfAttackOnDay[day] = max * progress;
    }

    this.draw.drawText('Weight of threat attacking on day:', { row: -4, col: 2 });
    for (let day = 0; day <= MAX_DAY; day++) {
      const text = Math.round(100 * this.chanceOfAttackOnDay[day]) + '%';
      this.draw.drawText('' + day, { row: -3, col: day * 2 });
      this.draw.drawText(text, { row: -2, col: day * 2 });
    }
  }

  public drawThreatToEachCity() {
    const myPlayerId = this.game.getMyPlayerId();
    const cities = this.land.getAllCityIds()
      .map(cityId => this.game.getCity(cityId))
      .filter(city => !city.razed && city.owner === myPlayerId);

    for (const city of cities) {
      let text = 'Threats: ' + this.chanceOfAttackOnDay
        .map((_, day) => this.getThreatToCityUpToDay(city.id, day))
        .join(', ')
        + ' ~> ' + this.getThreatToCityInNearFuture(city.id);

      const place: Place = {
        row: city.row + 2,
        col: city.col + 0.5
      };
      this.draw.drawTextWhenNothingIsSelected(text, place);
    }
  }

}
