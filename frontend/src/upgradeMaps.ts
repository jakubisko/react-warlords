import fs from 'fs';
import { globSync } from 'glob';
import { compressObject, decompressObject } from './model/game/serialization';

function convertMapData(m: object): object {
  // console.log(m);

  // implement conversion here

  return m;
}

async function convertMapFile(path: string) {
  const buffer = fs.readFileSync(path);
  const mapData = await decompressObject(buffer, 'map', false);

  const mapDataConverted = convertMapData(mapData);

  const content = await compressObject(mapDataConverted, 'map', 'nodebuffer');
  fs.writeFileSync(path, content);
}


const dirPath = `public/**/${process.argv[2] ?? '*'}.wmap`;

const files = globSync(dirPath);
console.log(files);
files.forEach(convertMapFile);
