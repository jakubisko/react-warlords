import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { Place } from 'ai-interface/types/place';
import { Task, NOP } from './helpers/types';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { getChanceToDieWhenExploringRuin } from 'ai-interface/constants/exploring';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { ArtifactManager } from './ArtifactManager';
import { WinCondition } from 'ai-interface/constants/winCondition';
import { clamp } from './helpers/functions';

export class RuinManager {

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly artifactManager: ArtifactManager
  ) {
  }

  public visitRuinTask(groupId: string, place: Place): Task {
    let utility = 0;
    let action = NOP;

    const ruinId = this.land.getRuinIdAt(place)!;
    const { explored, level: ruinLevel } = this.game.getRuin(ruinId);
    if (explored) {
      return { utility, action };
    }

    const heroIds = this.game.getArmiesInGroup(groupId)
      .filter(army => standardArmyTypes[army.armyTypeId].hero)
      .map(army => army.id);

    for (const heroId of heroIds) {
      const { level: heroLevel } = this.game.getHero(heroId);
      const otherArmyTypeIds = this.game.getArmiesInGroup(groupId)
        .filter(army => army.id !== heroId)
        .map(army => army.armyTypeId);
      let chanceToDie = getChanceToDieWhenExploringRuin(ruinLevel, heroLevel, otherArmyTypeIds);
      chanceToDie = clamp(0, 1, chanceToDie / 100);

      // Deliberately skew the chances to die a little higher so that the PC is more conservative with the hero.
      chanceToDie **= 2 / 3;

      const utilityNew = (ruinLevel !== 4 && this.game.getWinCondition() === WinCondition.ObtainALevel4Artifact ? 2 : 1)
        * (1 - chanceToDie) * 300 * ruinLevel
        - chanceToDie * 275 * heroLevel;

      if (utilityNew > utility) {
        utility = utilityNew;
        action = () => {
          // It could happen that we reached the place without the hero. We need to check whether we have a hero with us.
          const hero = this.game.getArmiesInGroup(groupId).find(army => standardArmyTypes[army.armyTypeId].hero);
          if (hero !== undefined) {
            this.game.exploreRuin(hero.id);
            // If this is our 9th artifact, the new one will be on the ground. Maybe we want to swap it for an artifact we are carrying.
            this.artifactManager.pickArtifactsIfThereAreAnyTask(groupId, place).action();
          }
        }
      }
    }

    return { utility, action };
  }

}
