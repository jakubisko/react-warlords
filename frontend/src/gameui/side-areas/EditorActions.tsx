import './TwoItemsPerColumnContainer.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button } from 'react-bootstrap';
import { EditorTool } from '../../model/editor/types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faUndo, faSearch, faDragon, faEraser, faTree, faRing, faRedo, faLandmark } from '@fortawesome/free-solid-svg-icons';
import { redo, setEditorTool, undo } from '../../model/editor/actions';
import { setShowingMenu } from '../../model/ui/actions';

export default function EditorActions() {
  const tool = useSelector((state: RootState) => state.editor.tool);
  const disableUndo = useSelector((state: RootState) => state.editor.undoQueuePosition === 0);
  const disableRedo = useSelector((state: RootState) => state.editor.undoQueuePosition === state.editor.undoQueue.length - 1);
  const dispatch: AppDispatch = useDispatch();

  return (
    <div className="two-items-per-column-container">
      <div className="button-row">
        <Button
          onClick={() => dispatch(setShowingMenu(true))}
        >
          <FontAwesomeIcon icon={faCog} />
          <div className="caption">System</div>
        </Button>
      </div>

      <div className="button-row">
        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.TerrainBrush))}
          active={tool === EditorTool.TerrainBrush}
        >
          <FontAwesomeIcon icon={faTree} />
          <div className="caption">Terrain</div>
        </Button>

        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.StructurePlacing))}
          active={tool === EditorTool.StructurePlacing}
        >
          <FontAwesomeIcon icon={faLandmark} />
          <div className="caption">Building</div>
        </Button>
      </div>

      <div className="button-row">
        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.ArmyPlacing))}
          active={tool === EditorTool.ArmyPlacing}
        >
          <FontAwesomeIcon icon={faDragon} />
          <div className="caption">Army</div>
        </Button>

        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.ArtifactPlacing))}
          active={tool === EditorTool.ArtifactPlacing}
        >
          <FontAwesomeIcon icon={faRing} />
          <div className="caption">Artifact</div>
        </Button>
      </div>

      <div className="button-row">
        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.PlaceInfo))}
          active={tool === EditorTool.PlaceInfo}
        >
          <FontAwesomeIcon icon={faSearch} />
          <div className="caption">Place</div>
        </Button>

        <Button
          onClick={() => dispatch(setEditorTool(EditorTool.Erase))}
          active={tool === EditorTool.Erase}
        >
          <FontAwesomeIcon icon={faEraser} />
          <div className="caption">Erase</div>
        </Button>
      </div>

      <div className="button-row">
        <Button
          disabled={disableUndo}
          onClick={() => dispatch(undo())}
        >
          <FontAwesomeIcon icon={faUndo} />
          <div className="caption">Undo</div>
        </Button>

        <Button
          disabled={disableRedo}
          onClick={() => dispatch(redo())}
        >
          <FontAwesomeIcon icon={faRedo} />
          <div className="caption">Redo</div>
        </Button>
      </div>
    </div >
  );
}
