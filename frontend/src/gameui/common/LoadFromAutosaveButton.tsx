import { useDispatch } from 'react-redux';
import { setErrorMessage } from '../../model/ui/actions';
import { Button } from 'react-bootstrap';
import { localStorageKeyAutosave } from '../../model/root/constantsStorage';
import { AppDispatch } from '../../model/root/configureStore';
import { UserError } from '../../model/ui/types';

interface LoadFromAutosaveButtonProps {
  disabled?: boolean;

  /** No need for the caller to wrap this in try...catch block. LoadFromAutosaveButton handles all errors. */
  onUpload: (stringData: string) => Promise<void>;
}

export default function LoadFromAutosaveButton({ disabled, onUpload }: LoadFromAutosaveButtonProps) {
  const dispatch: AppDispatch = useDispatch();

  async function handleLoadGameFromAutosave() {
    const stringData = window.localStorage.getItem(localStorageKeyAutosave);
    try {
      if (stringData === null) {
        throw new UserError('There is no autosave available.');
      }
      await onUpload(stringData);
    } catch (err) {
      dispatch(setErrorMessage('Loading failed', err));
    }
  }

  return (
    <Button
      disabled={disabled}
      onClick={handleLoadGameFromAutosave}
    >
      Load game from autosave
    </Button>
  );
}
