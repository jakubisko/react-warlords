import './ErrorMessageModal.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Modal } from 'react-bootstrap';
import { setErrorMessage } from '../../model/ui/actions';
import ButtonWithShortcut from '../common/ButtonWithShortcut';

export default function ErrorMessageModal() {
  const errorTitle = useSelector((state: RootState) => state.ui.errorTitle);
  const errorText = useSelector((state: RootState) => state.ui.errorText);
  const dispatch: AppDispatch = useDispatch();

  return (
    <Modal
      className="error-message-modal"
      show={errorTitle !== null || errorText !== null}
      centered
      onHide={() => dispatch(setErrorMessage(null, null))}
    >
      <Modal.Header className="text-center">
        <Modal.Title>
          {errorTitle}
        </Modal.Title>
      </Modal.Header>

      {errorText !== null && (
        <Modal.Body>
          {errorText}
        </Modal.Body>
      )}

      <Modal.Footer className="justify-content-center">
        <ButtonWithShortcut
          shortcutCode="Enter"
          allowShortcutWhileModalIsOpen={true}
          onClick={() => dispatch(setErrorMessage(null, null))}
        >
          OK
        </ButtonWithShortcut>
      </Modal.Footer>
    </Modal>
  );
}
