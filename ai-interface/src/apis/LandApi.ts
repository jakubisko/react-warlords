import { Place } from '../types/place';
import { MoveCostCalculator } from './MoveCostCalculator';
import { Abilities } from '../types/army';
import { Terrain } from '../constants/terrain';
import { Structure } from '../constants/structure';

/**
 * API to be used by your custom AI.
 *
 * Use LandApi methods to retrieve values about land that don't change during the entire game, such as the size of the land and the terrain.
 * All lists returned by methods of this class are frozen (immutable). For example, if you try to insert an item into the list of all city
 * ids, your program will throw an error.
 */
export interface LandApi {

  getSize(): number;

  /** Returns a two-dimensional array. First index is the row, second is the column of the place. */
  getTerrain(): Terrain[][];

  /** Returns a two-dimensional array. First index is the row, second is the column of the place. */
  getStructures(): Structure[][];

  getAllCityIds(): string[];
  getAllFlaggableIds(): string[];
  getAllRuinIds(): string[];

  getCityIdAt(place: Place): string | undefined;
  getFlaggableIdAt(place: Place): string | undefined;
  getRuinIdAt(place: Place): string | undefined;

  /** Use MoveCostCalculator to get move cost for individual places on map. Abilities such as flying can modify these costs. */
  createMoveCostCalculator(abilities: Abilities): MoveCostCalculator;
}
