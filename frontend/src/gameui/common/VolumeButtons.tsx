import './VolumeButtons.scss';
import { useEffect, useState } from 'react';
import { faVolumeDown, faVolumeUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Howler } from 'howler';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { localStorageKeyVolume } from '../../model/root/constantsStorage';
import { clamp } from '../../functions';

const maxVolume = 100;

export default function VolumeButtons() {
  const savedVolumeString = window.localStorage.getItem(localStorageKeyVolume)
    ?? ('' + Math.round(maxVolume * Howler.volume()));
  const savedVolumeNumber = clamp(0, maxVolume, parseInt(savedVolumeString));
  const [volume, setVolume] = useState(savedVolumeNumber);

  useEffect(() => {
    window.localStorage.setItem(localStorageKeyVolume, `${volume}`);
    Howler.volume(volume / maxVolume);
  }, [volume]);

  return (
    <div className="volume-buttons">
      <FontAwesomeIcon
        size="lg"
        icon={faVolumeDown}
        onClick={() => {
          setVolume(Math.max(0, volume - 5));
          soundEffects.play(SoundEffect.PickArtifactFromGround);
        }}
      />
      <span className="d-inline-block text-center volume-value">
        {volume}%
      </span>
      <FontAwesomeIcon
        size="lg"
        icon={faVolumeUp}
        onClick={() => {
          setVolume(Math.min(maxVolume, volume + 5));
          soundEffects.play(SoundEffect.PickArtifactFromGround);
        }}
      />
    </div>
  );
}
