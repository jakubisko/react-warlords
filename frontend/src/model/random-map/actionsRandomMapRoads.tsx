import { AppDispatch, RootState } from '../root/configureStore';
import { createAndPlaceStructure, setSignpostMessage, drawWithTerrainBrushEraseIllegalTiles } from '../game/actionsEditor';
import { shuffle, getRandom } from '../../functions';
import { Structure } from 'ai-interface/constants/structure';
import { canPlaceStructure } from '../editor/selectors';
import { getNearestCity, getSomeNearFreeTileForStructure } from './selectors';
import { Place } from 'ai-interface/types/place';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { SearchPathArgs } from 'ai-interface/types/path';
import { calculateTimeMatrix, calculatePathToPlaceFromTimeMatrix } from '../path/dijkstrasAlgorithm';
import { Ability } from 'ai-interface/constants/ability';
import { Terrain } from 'ai-interface/constants/terrain';
import { City } from '../game/types';
import { RandomMapSettings } from '../editor/types';

const isStructureWorthRoad: { [structure: string]: true } = {
  [Structure.Ruin]: true,
  [Structure.SageTower]: true,
  [Structure.Temple]: true,
  [Structure.Port]: true,
  [Structure.City]: true,
  [Structure.Village]: true,
  [Structure.Watchtower]: true,
  [Structure.Encampment]: true,
  [Structure.SpyDen]: true
};

const createRoad = (start: Place, dest: Place) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game, game: { size, terrain, structures } } = getState();
  const { row: destRow, col: destCol } = dest;

  const searchPathArgs: SearchPathArgs = {
    start,
    isDestination: (row, col) => row === destRow && col === destCol,
    isToBeAvoided: (row, col) => (Math.random() < 0.25 || // Avoid random tiles to make the road zig zagged.
      row === 0 || col === 0 || row === size - 1 || col === size - 1) && // Avoid edge of the map as it looks bad.
      (structures[row][col] !== Structure.Road) && // Don't avoid roads.
      (row !== destRow || col !== destCol), // The destination must not be avoided.
    moveType: {
      abilities: {
        [Ability.NoPenaltyInSwamp]: 1,
        [Ability.NoPenaltyInForest]: 1,
        [Ability.NoPenaltyInHills]: 1,
        [Ability.NoPenaltyInDesert]: 1,
        [Ability.NoPenaltyOnIce]: 1,
        [Ability.WalksOnVolcanic]: 1,
        [Ability.WalksOverMountains]: 1
      },
      groupMoveOnLand: 1E6,
      groupSize: 1
    },
    initialMoveLeft: 1E6
  };

  const timeMatrix = calculateTimeMatrix(game, searchPathArgs);
  const path = calculatePathToPlaceFromTimeMatrix(timeMatrix, destRow, destCol);

  if (path.length > 0) {
    const drawOneRoadTile = (row: number, col: number) => {
      if (canPlaceStructure(getState(), { row, col, structure: Structure.Road })) {
        dispatch(createAndPlaceStructure(row, col, Structure.Road));
      }
    };

    let { row: lastRow, col: lastCol } = path[0];
    for (const { row, col } of path) {
      drawOneRoadTile(row, col);
      if (terrain[row][col] !== Terrain.Water && row !== lastRow && col !== lastCol) {
        // We moved diagonally. We need to add extra tile so that the entire road is connected by sides.
        // The added tile should not be on the edge as it looks bad. If one of the tiles is invalid, try the other one.
        if (row !== 0 && row !== size - 1 && lastRow !== 0 && lastRow !== size - 1
          && canPlaceStructure(getState(), { row, col: lastCol, structure: Structure.Road })) {
          drawOneRoadTile(row, lastCol);
        } else {
          drawOneRoadTile(lastRow, col);
        }
      }
      lastRow = row;
      lastCol = col;
    }
  }
};

const directions = ['west', 'southwest', 'south', 'southeast', 'east', 'northeast', 'north', 'northwest'];
function getDirections(from: Place, to: Place): string {
  const row = from.row - to.row;
  const col = to.col - from.col;
  const angle = Math.floor(0.5 + 4 * (1 + Math.atan2(row, col) / Math.PI)) % 8;
  return `${Math.round((row ** 2 + col ** 2) ** 0.5)} miles to the ${directions[angle]}`;
}

/**
 * Searches the surroundings for some near free tile and places a signpost there.
 * The text of the signpost will describe directions to given city.
 */
const createSignpostWithDirections = (aroundPlace: Place, to: City) => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();

  const destination = getSomeNearFreeTileForStructure(state, {
    row: aroundPlace.row + 0.5,
    col: aroundPlace.col + 0.5,
    structure: Structure.Signpost
  });
  if (destination === undefined) {
    return;
  }

  const { row: signRow, col: signCol } = destination;

  const message = `City of ${to.name} - ${getDirections(destination, to)}`;
  dispatch(createAndPlaceStructure(signRow, signCol, Structure.Signpost));
  dispatch(setSignpostMessage(signRow, signCol, message));
  if (state.game.terrain[signRow][signCol] === Terrain.Forest) {
    // I don't like how the sign looks on the Forest terrain.
    dispatch(drawWithTerrainBrushEraseIllegalTiles(signRow, signCol, Terrain.Open, 1));
  }
};

/** Each starting city gets roads to a few of the nearest cities. */
const createRoadsForStartingCities = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;

  const startingCities = Object.values(cities).filter(city => city.owner !== neutralPlayerId);
  const noOfRoads = startingCities.length * 3 <= Object.values(cities).length ? 2 : 1; // Prevents road overcrowding on tiny maps.

  for (const city of startingCities) {
    const nearestCities: City[] = [];

    for (let i = 0; i < noOfRoads; i++) {
      const city2 = getNearestCity(getState(), city, [city, ...nearestCities]);
      if (city2 === undefined) {
        break;
      }

      dispatch(createRoad(city, city2));
      nearestCities.push(city2);
    }

    if (nearestCities.length > 0) {
      dispatch(createSignpostWithDirections({
        row: city.row + getRandom(2),
        col: city.col + getRandom(2)
      }, nearestCities[0]));
    }
  }
};

/** Pick some structures at random and make roads to them. */
const createRoadsToRandomStructures = (noOfRoads: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size, tiles, structures, cities } = getState().game;

  const allStructures: Place[] = [];
  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      if (isStructureWorthRoad[structures[row][col]]) {
        // Only include cities once
        const { cityId } = tiles[row][col];
        if (cityId !== undefined && (row !== cities[cityId].row || col !== cities[cityId].col)) {
          continue;
        }

        allStructures.push({ row, col });
      }
    }
  }

  // Add some roads to the edges of the map just for fun.
  for (let i = 0; i < size; i += 20) {
    allStructures.push({ row: 0, col: getRandom(size) });
    allStructures.push({ row: size - 1, col: getRandom(size) });
    allStructures.push({ row: getRandom(size), col: 0 });
    allStructures.push({ row: getRandom(size), col: size - 1 });
  }

  shuffle(allStructures);

  for (let i = Math.floor(Math.min(allStructures.length / 5, noOfRoads)) - 1; i >= 0; i--) {
    const place = allStructures[i];
    const { row, col } = place;
    const { cityId } = tiles[row][col];

    // It would be pointless to always create the shortest road to a nearest city - that is the default way an army would take anyway.
    const ignoreCities: City[] = [];
    for (let nth = (cityId === undefined ? 1 : 2) + getRandom(3); nth > 0; nth--) {
      const city2 = getNearestCity(getState(), place, ignoreCities);
      if (city2 !== undefined) {
        ignoreCities.push(city2);
      }
    }

    const city1 = getNearestCity(getState(), place, ignoreCities);
    if (city1 !== undefined) {
      dispatch(createRoad(city1, place));
    }
  }
};

const offsets = [
  { index: 0, dRow: -1, dCol: -1 },
  { index: 1, dRow: -1, dCol: 0 },
  { index: 2, dRow: -1, dCol: 1 },
  { index: 4, dRow: 0, dCol: -1 },
  { index: 6, dRow: 0, dCol: 1 },
  { index: 8, dRow: 1, dCol: -1 },
  { index: 9, dRow: 1, dCol: 0 },
  { index: 10, dRow: 1, dCol: 1 }
];

/** Random generation causes some ugly roads such as 2x2 squares. This removes such things. */
export const fixRoads = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size } = getState().game;

  function checkTile(row: number, col: number) {
    const { structures } = getState().game;
    if (structures[row][col] !== Structure.Road) {
      return;
    }

    function matches(pattern: string): boolean {
      for (const { index, dRow, dCol } of offsets) {
        if ((pattern.charAt(index) === 'R' && structures[row + dRow][col + dCol] !== Structure.Road) ||
          (pattern.charAt(index) === '0' && structures[row + dRow][col + dCol] === Structure.Road)) {
          return false;
        }
      }
      return true;
    }

    if (matches('RRR|.RR|..R') || matches('RRR|RR.|R..') || matches('..R|.RR|RRR') || matches('R..|RR.|RRR') ||
      matches('.0.|0RR|.RR') || matches('.0.|RR0|RR.') || matches('.RR|0RR|.0.') || matches('RR.|RR0|.0.') ||
      matches('.0R|0RR|.0R') || matches('RRR|0R0|.0.') || matches('R0.|RR0|R0.') || matches('.0.|0R0|RRR')
    ) {
      // Remove the road from this tile and search around.
      dispatch(createAndPlaceStructure(row, col, Structure.Nothing));
      if (row > 1) { checkTile(row - 1, col); }
      if (col > 1) { checkTile(row, col - 1); }
      if (col < size - 2) { checkTile(row, col + 1); }
      if (row < size - 2) { checkTile(row + 1, col); }
    }
  }

  for (let row = 1; row < size - 1; row++) {
    for (let col = 1; col < size - 1; col++) {
      checkTile(row, col);
    }
  }
};

export const createRandomMapRoads = (settings: RandomMapSettings) => (dispatch: AppDispatch) => {
  const { size, cities, players } = settings;

  // When there are a lot of cities, the average length of road decreases. So we should increase the number of roads.
  // Each starting city gets a road by default. As such, decrease the number of other roads when the number of players increases.
  const noOfRandomRoads = size / (6 - 2 * cities) + 2 - players;

  dispatch(createRoadsForStartingCities());
  dispatch(createRoadsToRandomStructures(noOfRandomRoads));
  dispatch(fixRoads());
};
