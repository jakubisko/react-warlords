import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { IsTerrainPredicate } from './constants';
import { TileSprite } from '../../gameui/map/tactical-map/spriteMetaDataTypes';
import { spriteSheetWater } from '../../gameui/map/tactical-map/spriteMetaData';

/**
 * Call for water tile to retrieve its sprite id in "spriteSheetWater".
 * The index to this array should be calculated as follows:
 * 
 * +1 if there is water at the tile left of the above tile
 * +2 if there is water at the above tile
 * +4 if there is water at the tile right of the above tile
 * +8 if there is water at the tile on left
 * +16 if there is water at the tile on right
 * +32 if there is water at the tile left of the tile under
 * +64 if there is water at the tile under
 * +128 if there is water at the tile right of the tile under
 */
const mappingToWaterSpriteId: number[] = [
  72, 72, 43, 43, 72, 72, 43, 43, // 0+
  65, 65, 55, 31, 65, 65, 55, 31, // 8+
  44, 44, 63, 63, 44, 44, 36, 36, // 16+
  46, 46, 34, 17, 46, 46, 12, 22, // 24+
  72, 72, 43, 43, 72, 72, 43, 43, // 32+
  65, 65, 55, 31, 65, 65, 55, 31, // 40+
  44, 44, 63, 63, 44, 44, 36, 36, // 48+
  46, 46, 34, 17, 46, 46, 12, 22, // 56+
  32, 32, 52, 52, 32, 32, 52, 52, // 64+
  35, 35, 42, 18, 35, 35, 42, 18, // 72+
  53, 53, 33, 33, 53, 53, 11, 11, // 80+
  64, 64, 45, 68, 64, 64, 67, 24, // 88+
  32, 32, 52, 52, 32, 32, 52, 52, // 96+
  51, 51, 14, 61, 51, 51, 14, 61, // 104+
  53, 53, 33, 33, 53, 53, 11, 11, // 112+
  13, 13, 58, 41, 13, 13, 23, 21, // 120+
  72, 72, 43, 43, 72, 72, 43, 43, // 128+
  65, 65, 55, 31, 65, 65, 55, 31, // 136+
  44, 44, 63, 63, 44, 44, 36, 36, // 144+
  46, 46, 34, 17, 46, 46, 12, 22, // 152+
  72, 72, 43, 43, 72, 72, 43, 43, // 160+
  65, 65, 55, 31, 65, 65, 55, 31, // 168+
  44, 44, 63, 63, 44, 44, 36, 36, // 176+
  46, 46, 34, 17, 46, 46, 12, 22, // 184+
  32, 32, 52, 52, 32, 32, 52, 52, // 192+
  35, 35, 42, 18, 35, 35, 42, 18, // 200+
  56, 56, 15, 15, 56, 56, 66, 66, // 208+
  16, 16, 57, 75, 16, 16, 25, 26, // 216+
  32, 32, 52, 52, 32, 32, 52, 52, // 224+
  51, 51, 14, 61, 51, 51, 14, 61, // 232+
  56, 56, 15, 15, 56, 56, 66, 66, // 240+
  73, 73, 74, 71, 73, 73, 76, 77 // 248+
];

export function placeSpritesForWater(backgroundSprites: TileSprite[][][], isTerrain: IsTerrainPredicate) {
  const size = backgroundSprites.length;

  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      if (isTerrain(row, col, Terrain.Water)) {
        const around = ((row === 0 || col === 0 || isTerrain(row - 1, col - 1, Terrain.Water)) ? 1 : 0)
          + ((row === 0 || isTerrain(row - 1, col, Terrain.Water)) ? 2 : 0)
          + ((row === 0 || col === size - 1 || isTerrain(row - 1, col + 1, Terrain.Water)) ? 4 : 0)
          + ((col === 0 || isTerrain(row, col - 1, Terrain.Water)) ? 8 : 0)
          + ((col === size - 1 || isTerrain(row, col + 1, Terrain.Water)) ? 16 : 0)
          + ((row === size - 1 || col === 0 || isTerrain(row + 1, col - 1, Terrain.Water)) ? 32 : 0)
          + ((row === size - 1 || isTerrain(row + 1, col, Terrain.Water)) ? 64 : 0)
          + ((row === size - 1 || col === size - 1 || isTerrain(row + 1, col + 1, Terrain.Water)) ? 128 : 0);

        const id = mappingToWaterSpriteId[around];
        backgroundSprites[row][col] = [spriteSheetWater[id]];
      }
    }
  }
}

/** Sprites for ports. */
export function placeSpritesForPorts(backgroundSprites: TileSprite[][][], isTerrain: IsTerrainPredicate, structures: Structure[][]) {
  const size = backgroundSprites.length;

  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      if (structures[row][col] === Structure.Port) {
        const landOnTop = row > 0 && !isTerrain(row - 1, col, Terrain.Water);
        const landOnBottom = row < size - 1 && !isTerrain(row + 1, col, Terrain.Water);
        const landOnLeft = col > 0 && !isTerrain(row, col - 1, Terrain.Water);
        const landOnRight = col < size - 1 && !isTerrain(row, col + 1, Terrain.Water);

        let id = 28; // It is possible that port may be floating mid-ocean if it was placed correctly but then water was placed around it.
        if (landOnBottom && !landOnTop) {
          id = 28;
        } else if (landOnLeft && !landOnRight) {
          id = 47;
        } else if (landOnTop && !landOnBottom) {
          id = 48;
        } else if (landOnRight && !landOnLeft) {
          id = 38;
        } else if (landOnRight && landOnLeft) {
          id = 27;
        } else if (landOnTop && landOnBottom) {
          id = 37;
        }
        backgroundSprites[row][col].push(spriteSheetWater[id]);
      }
    }
  }
}
