import { Ability } from '../constants/ability';

/** Mapping from an ability of an army, hero or artifact to the value of that ability. */
export type Abilities = Partial<{ [key in Ability]: number }>;

/** Army is one guy, for example "1 Bat" or "1 Hero". */
export interface Army {

  /** Unique id. */
  readonly id: string;

  /** Id of the army's current group. */
  readonly groupId: string;

  /** Army's type defines strength, abilities etc. */
  readonly armyTypeId: string;

  /** Army's movement left for this day. Values less than zero are allowed! Move that starts with positive moveLeft and ends with negative moveLeft is allowed. */
  readonly moveLeft: number;

  /** Army that visited Temple has permanent Strength bonus. */
  readonly blessed?: boolean;
}

/**
 * Defines combat strength, move and cost of one army. These values do not change during game.
 * Class ArmyType acts as an enum. There is only one instance of each ArmyType. You should refer to them only by their ids.
 * However, you can have multiple Army of the same ArmyType.
 */
export interface ArmyType {

  /** Unique id. */
  readonly id: string;

  /** Name of the army type. Not relevant for gameplay. */
  readonly name: string;

  /** Strength in combat. */
  readonly strength: number;

  /** How many times an army has to be damaged before it dies in a combat. */
  readonly hitPoints: number;

  /** Values of abilities such as NoPenaltyInSwamp, Ambush etc. */
  readonly abilities: Abilities;

  /** Army's daily movement. */
  readonly move: number;

  /** Amount of gold the army takes each day. If there is not enough gold at the start of the turn, the army will desert. */
  readonly upkeep: number;

  /** How many days to produce one army of this type. */
  readonly trainingTime: number;

  /** Cost to build a facility for training this army type in a city. Also the amount of experience a hero gets for killing this army. */
  readonly buildingCost: number;

  /** Whether this is a hero or not. */
  readonly hero: boolean;

  /** A group of neutral armies of this army type placed on a random map will start the game with this number of armies. */
  readonly neutralStartSize: number;

  /** A group of neutral armies of this army type will grow each week until it reaches this limit. */
  readonly neutralMaxSize: number;
}
