import { getRandom } from '../functions';
import { default as cityNames } from './city-names.json';
import { default as heroNames } from './hero-names.json';
import { default as ruinMonsters } from './ruin-monsters.json';

function getRandomItem<T>(list: T[]): T {
  return list[Math.floor(list.length * Math.random())];
}

export function getRandomUnusedCityName(usedCityNames: string[]): string {
  let name = '';
  for (let tries = 0; tries < 10; tries++) {
    name = getRandomItem(cityNames.prefixes) + ' ' + getRandomItem(cityNames.suffixes);
    if (!usedCityNames.includes(name)) {
      break;
    }
  }
  return name;
}

export function getRandomUnusedHeroPortraitId(usedHeroPortraitIds: number[]): number {
  const usedIds: { [portraitId: number]: true } = {};
  for (const portraitId of usedHeroPortraitIds) {
    usedIds[portraitId] = true;
  }

  const unusedIds: number[] = [];
  for (let portraitId = 0; portraitId < heroNames.length; portraitId++) {
    if (!(portraitId in usedIds)) {
      unusedIds.push(portraitId);
    }
  }

  return unusedIds.length === 0
    ? getRandom(heroNames.length)
    : getRandomItem(unusedIds);
}

export function getPossibleRuinMonsterNames(ruinLevel: number, isShip: boolean): string[] {
  // NOTE: We're using monster name in a sentence with an indefinite article ("a"). Monster name shouldn't start with a vowel.
  const list = isShip ? ruinMonsters.ship : ruinMonsters.land;
  return list[ruinLevel - 1];
}
