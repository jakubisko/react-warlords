import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actions';
import { ProgramMode, ScreenEnum, UiState } from './types';

export const initialState: UiState = {
  errorTitle: null,
  errorText: null,
  programMode: ProgramMode.Playing,
  screen: ScreenEnum.InitialLoadingScreen,
  showingMenu: false,
  showingCityId: null,
  showingPlaceInfo: null,
  showingHeroId: null,
  showingWarReport: false,
  showingSpyReport: false,
  showingHeroOffer: false,
  showingCombat: false,
  showingRuinExploring: false,
  showingManual: false,
  showingArmyTypeId: null,
  showingToastMessage: null,
  tacticalMapWidth: 1,
  tacticalMapHeight: 1,
  pixelsPerTile: 0,
  focusedRow: 0,
  focusedCol: 0,
  activeGroupId: null,
  lastActiveGroupId: ''
};

export function uiReducer(state = initialState, action: AppAction): UiState {
  switch (action.type) {

    case ActionTypes.SET_ERROR_MESSAGE: {
      const { errorTitle, errorText } = action.payload;
      return {
        ...state,
        errorTitle,
        errorText
      };
    }

    case ActionTypes.SET_PROGRAM_MODE: {
      const { programMode } = action.payload;
      return {
        ...state,
        programMode
      };
    }

    case ActionTypes.SET_SCREEN: {
      const { screen } = action.payload;
      return {
        ...state,
        screen
      };
    }

    case ActionTypes.SET_SHOWING_MENU: {
      return {
        ...state,
        showingMenu: action.payload.showingMenu
      };
    }

    case ActionTypes.SET_SHOWING_CITY: {
      return {
        ...state,
        showingCityId: action.payload.showingCityId
      };
    }

    case ActionTypes.SET_SHOWING_PLACE_INFO: {
      return {
        ...state,
        showingPlaceInfo: action.payload.showingPlaceInfo
      };
    }

    case ActionTypes.SET_SHOWING_HERO: {
      return {
        ...state,
        showingHeroId: action.payload.showingHeroId
      };
    }

    case ActionTypes.SET_SHOWING_WAR_REPORT: {
      return {
        ...state,
        showingWarReport: action.payload.showingWarReport
      };
    }

    case ActionTypes.SET_SHOWING_SPY_REPORT: {
      return {
        ...state,
        showingSpyReport: action.payload.showingSpyReport
      };
    }

    case ActionTypes.SET_SHOWING_RECRUITMENT_OFFER: {
      return {
        ...state,
        showingHeroOffer: action.payload.showingHeroOffer
      };
    }

    case ActionTypes.SET_SHOWING_COMBAT: {
      return {
        ...state,
        showingCombat: action.payload.showingCombat
      };
    }

    case ActionTypes.SET_SHOWING_EXPLORING: {
      return {
        ...state,
        showingRuinExploring: action.payload.showingExploring
      };
    }

    case ActionTypes.SET_SHOWING_MANUAL: {
      return {
        ...state,
        showingManual: action.payload.showingManual
      };
    }

    case ActionTypes.SET_SHOWING_ARMY_TYPE: {
      return {
        ...state,
        showingArmyTypeId: action.payload.showingArmyTypeId
      };
    }

    case ActionTypes.SET_SHOWING_TOAST_MESSAGE: {
      const newMessage = action.payload.showingToastMessage;
      const oldMessage = state.showingToastMessage;

      return {
        ...state,
        showingToastMessage: newMessage === null ? null : (oldMessage ? (oldMessage + ', ' + newMessage) : newMessage)
      };
    }

    case ActionTypes.SET_TACTICAL_MAP_SIZE: {
      const { widthInTiles, heightInTiles, pixelsPerTile } = action.payload;
      return {
        ...state,
        tacticalMapWidth: widthInTiles,
        tacticalMapHeight: heightInTiles,
        pixelsPerTile
      };
    }

    case ActionTypes.SET_MAP_FOCUS: {
      const { focusedRow, focusedCol } = action.payload;
      return {
        ...state,
        focusedRow,
        focusedCol
      };
    }

    case ActionTypes.SET_ACTIVE_GROUP: {
      return {
        ...state,
        activeGroupId: action.payload.groupId
      };
    }

    case ActionTypes.SET_LAST_ACTIVE_GROUP: {
      return {
        ...state,
        lastActiveGroupId: action.payload.groupId
      };
    }

    default:
      return state;
  }
}
