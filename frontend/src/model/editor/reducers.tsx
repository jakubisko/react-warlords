import { ActionTypes } from './actions';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { EditorState, EditorTool } from './types';
import { EMPTY_ARRAY } from '../../functions';
import { MapData } from '../game/types';
import { AppAction } from '../root/configureStore';

export const initialState: EditorState = {
  tool: EditorTool.TerrainBrush,
  terrainBrush: Terrain.Water,
  terrainBrushSize: 1,
  structure: Structure.City,
  undoQueue: EMPTY_ARRAY as MapData[],
  undoQueuePosition: 0,
  lastRandomMapSettings: {
    size: 40,
    players: 2,
    water: 0.5,
    mountains: 0.5,
    heat: 0.5,
    cold: 0.5,
    cities: 0.5,
    ruins: 0.5
  }
};

export function editorReducer(state = initialState, action: AppAction): EditorState {
  switch (action.type) {

    case ActionTypes.SET_EDITOR_TOOL: {
      const { tool } = action.payload;
      return {
        ...state,
        tool
      };
    }

    case ActionTypes.SET_TERRAIN_BRUSH: {
      const { terrainBrush } = action.payload;
      return {
        ...state,
        terrainBrush
      };
    }

    case ActionTypes.SET_TERRAIN_BRUSH_SIZE: {
      const { terrainBrushSize } = action.payload;
      return {
        ...state,
        terrainBrushSize
      };
    }

    case ActionTypes.SET_STRUCTURE_TYPE: {
      const { structure } = action.payload;
      return {
        ...state,
        structure
      };
    }

    case ActionTypes.SET_UNDO_QUEUE: {
      const { undoQueue, undoQueuePosition } = action.payload;
      return {
        ...state,
        undoQueue,
        undoQueuePosition
      };
    }

    case ActionTypes.SET_LAST_RANDOM_MAP_SETTINGS: {
      const { randomMapSettings } = action.payload;
      return {
        ...state,
        lastRandomMapSettings: randomMapSettings
      };
    }

    default:
      return state;
  }
}
