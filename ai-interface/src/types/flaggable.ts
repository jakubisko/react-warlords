import { Structure } from '../constants/structure';

/** Any structure that can be flagged (owned by a player) other than a city. For example a village. */
export interface FlaggableData {

  /** Unique id. */
  readonly id: string;

  /** Type of the flaggable structure; for example a village. */
  readonly structure: Structure;

  /** Index of the player owning the flaggable. Equals to "neutralPlayerId" for neutral flaggables. */
  readonly owner: number;

  /** Position of the flaggable. */
  readonly row: number;

  /** Position of the flaggable. */
  readonly col: number;
}
