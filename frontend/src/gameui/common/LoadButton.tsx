import { ChangeEvent } from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../model/root/configureStore';
import { setErrorMessage } from '../../model/ui/actions';

interface LoadButtonProps {
  caption: string;
  disabled?: boolean;
  fileExtension: string;

  /** No need for the caller to wrap this in try...catch block. LoadButton handles all errors. */
  onUpload: (fileData: ArrayBuffer) => Promise<void>; 
}

export default function LoadButton({ caption, disabled, fileExtension, onUpload }: LoadButtonProps) {
  const dispatch: AppDispatch = useDispatch();

  async function handleFileUpload(event: ChangeEvent<HTMLInputElement>) {
    const { currentTarget } = event; // Event won't be accessible later in the async callback.
    if (currentTarget?.files === null || currentTarget?.files?.length === 0) {
      return;
    }

    try {
      const file = currentTarget.files[0];
      const fileData = await file.arrayBuffer();
      await onUpload(fileData);
    } catch (err) {
      dispatch(setErrorMessage('Loading failed', err));
      currentTarget.value = ''; // Clean the input. Without this, uploading the same file again would be ignored.
    }
  }

  return (
    <label
      className={'btn mb-0' + (disabled ? ' disabled' : '')}
      style={{ marginTop: -1 }}
    >
      <input
        disabled={disabled}
        className="d-none"
        type="file"
        accept={'.' + fileExtension}
        onChange={handleFileUpload}
      />
      {caption}
    </label>
  );
}
