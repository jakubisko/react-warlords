import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { getNumberOfCities } from '../game/selectorsPlaying';
import { maxPlayers } from 'ai-interface/constants/player';
import { Place } from 'ai-interface/types/place';
import { SearchPathArgs } from 'ai-interface/types/path';
import { straightMoveCost } from 'ai-interface/constants/path';
import { calculateTimeMatrix } from '../path/dijkstrasAlgorithm';
import { Terrain } from 'ai-interface/constants/terrain';

/** This may be negative when given player has more cities than his strongest enemy. */
export const getNumberOfCitiesByWhichPlayerIsBehindBestEnemy = createSelector(
  (rootState: RootState) => rootState,
  (rootState: RootState, playerId: number) => playerId,
  (rootState, playerId) => {
    const { players } = rootState.game;
    const { team } = players[playerId];

    let maxEnemyCities = 0;
    for (let playerId2 = 0; playerId2 < maxPlayers; playerId2++) {
      if (players[playerId2].alive && players[playerId2].team !== team) {
        const enemyCities = getNumberOfCities(rootState, playerId2);
        if (enemyCities > maxEnemyCities) {
          maxEnemyCities = enemyCities;
        }
      }
    }

    return maxEnemyCities - getNumberOfCities(rootState, playerId);
  }
);

/**
 * When player chooses to receive "maps" in Sage Tower, this determines which places will be explored.
 * This function is designed to be most useful on water maps, where it's hardest to find cities.
 */
export const getPlacesToExploreBySage = createSelector(
  (state: RootState) => state.game,
  (rootState: RootState, place: Place) => place,
  (rootState: RootState, place: Place, noOfTiles: number) => noOfTiles,
  (gameState, place, noOfTiles) => {
    const { terrain, unexploredByTeam, players, currentPlayerId } = gameState;
    const { team } = players[currentPlayerId];
    const unexplored = unexploredByTeam[team];

    const result = {
      minRow: place.row,
      maxRow: place.row,
      minCol: place.col,
      maxCol: place.col
    };
    if (unexplored === null) {
      return result;
    }

    const searchPathArgs: SearchPathArgs = {
      start: place,
      isDestination: (row, col) => {
        if (unexplored[row][col]) {
          if (row < result.minRow) {
            result.minRow = row;
          } else if (row > result.maxRow) {
            result.maxRow = row;
          }
          if (col < result.minCol) {
            result.minCol = col;
          } else if (col > result.maxCol) {
            result.maxCol = col;
          }
          noOfTiles -= terrain[row][col] === Terrain.Water ? 0.67 : 1; // Deliberately more effective on islands map.
          return noOfTiles-- <= 0;
        }
        return false;
      },
      isToBeAvoided: () => false,
      moveType: {
        abilities: { WalksOnVolcanic: 1, WalksOverMountains: 1 },
        groupMoveOnLand: straightMoveCost,
        groupSize: 1
      },
      initialMoveLeft: straightMoveCost
    };

    calculateTimeMatrix(gameState, searchPathArgs);

    return result;
  }
);
