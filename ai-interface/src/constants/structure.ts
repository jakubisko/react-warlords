/** Type of a structure on top of terrain such as road, city, ruin, etc. Some structures can be placed on water. */
export enum Structure {
  Nothing = ' ',

  /** Decreases move cost of the terrain. */
  Road = '#',

  /** Holds a text readable by human players. No game effect. */
  Signpost = ':',

  /** Impassable to armies without flying. */
  Ridge = '+',

  /** Hero who chooses to explore will fight a monster for an artifact and gold. Works only once. Ruin may be placed on water. */
  Ruin = 'R',

  /** Provides help to weak players. Each player may visit one sage per week. Only a hero can visit the sage. */
  SageTower = 'S',

  /** Blesses visiting armies, giving them a permanent Strength bonus. */
  Temple = 'T',

  /** Port is placed on water next to the land. Non-flying groups board and unboard ships here. */
  Port = 'P',

  /** Player without a hero will receive a free hero if his group stays here overnight. */
  Encampment = 'E',

  /** Level 1 hero resting here overnight will receive experience. */
  Grove = 'G',

  /** City covers 2x2 places. Army may enter any of those squares. When referring to city's coordinates, top-left square will be returned. */
  City = 'C',

  /** Flaggable structure that increases daily income of its owner. Village may be placed on water. */
  Village = 'V',

  /** Flaggable structure that provides large view radius to its owner. */
  Watchtower = 'W',

  /** Flaggable structure that provides its owner full spy report on other players. */
  SpyDen = 'D'
}

/** Returns whether a structure is flaggable. City is a special case and is not considered flaggable. */
export const isStructureFlaggable: { [structure: string]: boolean } = Object.freeze({
  [Structure.Nothing]: false,
  [Structure.Road]: false,
  [Structure.Signpost]: false,
  [Structure.Ridge]: false,
  [Structure.Ruin]: false,
  [Structure.SageTower]: false,
  [Structure.Temple]: false,
  [Structure.Port]: false,
  [Structure.Encampment]: false,
  [Structure.Grove]: false,
  [Structure.City]: false,
  [Structure.Village]: true,
  [Structure.Watchtower]: true,
  [Structure.SpyDen]: true
});
