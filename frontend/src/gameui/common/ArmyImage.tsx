import './ArmyImage.scss';
import { useDispatch } from 'react-redux';
import { TileSprite, spriteSheetArmies } from '../map/tactical-map/spriteMetaData';
import { setShowingArmyType } from '../../model/ui/actions';
import { AppDispatch } from '../../model/root/configureStore';

interface ArmyImageProps {
  armyTypeId: string | null; // For null, empty slot with no army is displayed.
  selectable?: boolean; // Draw circle around as if this was a radio button.
  selected?: boolean; // The circle will be white.
  damaged?: boolean; // Flash "damage" animation in front.
  dead?: boolean; // Show skull instead of the army image.
  ambushed?: boolean; // Show skull with dagger instead of the army image.
  blessed?: boolean; // Draw blessed army image.
  grayed?: boolean; // Show only black/white image.
  alwaysSmall?: boolean; // Prevent stretching the image when on desktop resolution.

  onLeftClick?: (() => void) | 'INFO'; // When 'INFO', show ArmyTypeModal. When method, call it. When undefined, do nothing.
  onRightClick?: (() => void) | 'INFO'; // When 'INFO', show ArmyTypeModal. When method, call it. When undefined, do nothing.
}

export default function ArmyImage(props: ArmyImageProps) {
  const dispatch: AppDispatch = useDispatch();

  function handleClick(handler: (() => void) | 'INFO' | undefined) {
    const { armyTypeId } = props;

    if (handler === 'INFO' && armyTypeId !== null) {
      dispatch(setShowingArmyType(armyTypeId));
    } else if (handler !== undefined && handler !== 'INFO') {
      handler();
    }
  }

  const { armyTypeId, selectable, selected, damaged, dead, ambushed, blessed, grayed, alwaysSmall, onLeftClick, onRightClick } = props;
  let sprite: TileSprite | null = null;
  if (ambushed) {
    sprite = spriteSheetArmies['ambu'];
  } else if (dead) {
    sprite = spriteSheetArmies['kill'];
  } else if (armyTypeId !== null) {
    sprite = spriteSheetArmies[armyTypeId];
  }

  return (
    <div
      className={'army-image' + (selectable && sprite ? ' selectable' : '') + (alwaysSmall ? ' always-small' : '')}
      onClick={() => handleClick(onLeftClick)}
      onContextMenu={() => handleClick(onRightClick)}
    >
      {sprite &&
        <div className={'image' + (blessed && !dead && !ambushed ? ' blessed' : '')} style={{
          backgroundPositionX: -sprite.L,
          backgroundPositionY: -sprite.T,
          filter: grayed ? 'contrast(0.33)' : undefined
        }} />
      }

      {selectable &&
        <div className={'circle' + (selected ? ' selected' : '') + (selected && blessed ? ' selected-blessed' : '')} />
      }

      {damaged &&
        <div className="damaged" />
      }
    </div>
  );
}
