import './StrategicMap.scss';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../../model/root/configureStore';
import { MapDisplaySettings, Ruin, Group } from '../../../model/game/types';
import { City, Flaggable, Tile } from '../../../model/game/types';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { WarReportItem, WarReportType } from 'ai-interface/types/warReport';
import { Place } from 'ai-interface/types/place';
import { getUnexploredForReadingOnly } from '../../../model/game/selectorsPlaying';
import { drawEncampment, drawStar, drawTemple, drawGrove, drawPort, drawSageTower } from './strategicMapSymbols';
import { shallowEqualObjects } from '../../../functions';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { playerColors } from '../../../model/game/constants';

interface StrategicMapProps {
  /** Whether to show the focus rectangle that corresponds to the part of the land on TacticalMap. */
  showFocusRectangle?: boolean;

  /** Optionally draws a circle around given place on the map. */
  drawCircle?: Place;

  /** Optionally draws several lines between given places on the map. A line may be oriented or bidirectional. */
  drawLines?: { from: Place, to: Place, bidirectional: boolean }[];

  /** Optional event handler called when user clicks on the map. Coordinates are fractional numbers! */
  onClick?: (place: Place) => void;

  /** Optional event handler called when user double clicks or opens context menu on the map. Coordinates are fractional numbers! */
  onDoubleClick?: (place: Place) => void;

  /** Optional event handler called when user drags the map. Coordinates are fractional numbers! */
  onDrag?: (place: Place) => void;

  tacticalMapWidth: number,
  tacticalMapHeight: number,
  focusedRow: number,
  focusedCol: number,

  size: number,
  terrain: Terrain[][],
  structures: Structure[][],

  tiles: Tile[][],
  fogOfWar: boolean[][],
  unexplored: boolean[][] | null,
  groups: { [groupId: string]: Group },
  cities: { [cityId: string]: City },
  flaggables: { [flaggableId: string]: Flaggable },
  ruins: { [ruinId: string]: Ruin },
  warReports: WarReportItem[],
  currentPlayerId: number;
  currentTeamId: number;
  mapDisplaySettings: MapDisplaySettings;

  movingAnimation: boolean;
}

class StrategicMap extends Component<StrategicMapProps> {

  private parentRef = createRef<HTMLDivElement>();
  private staticCanvasRef = createRef<HTMLCanvasElement>();
  private overlayCanvasRef = createRef<HTMLCanvasElement>();
  private timeOfLastClick: number | null = null;

  constructor(props: StrategicMapProps) {
    super(props);

    this.handleResize = this.handleResize.bind(this);
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  componentDidUpdate(prevProps: StrategicMapProps) {
    const {
      focusedRow: focusedRow1,
      focusedCol: focusedCol1,
      showFocusRectangle: showFocusRectangle1,
      drawCircle: drawCircle1,
      drawLines: drawLines1,
      ...restOfProps1
    } = prevProps;
    const {
      focusedRow: focusedRow2,
      focusedCol: focusedCol2,
      showFocusRectangle: showFocusRectangle2,
      drawCircle: drawCircle2,
      drawLines: drawLines2,
      ...restOfProps2
    } = this.props as StrategicMapProps;

    if (focusedRow1 !== focusedRow2
      || focusedCol1 !== focusedCol2
      || showFocusRectangle1 !== showFocusRectangle2
      || restOfProps1.tacticalMapWidth !== restOfProps2.tacticalMapWidth
      || restOfProps1.tacticalMapHeight !== restOfProps2.tacticalMapHeight
      || drawCircle1 !== drawCircle2
      || drawLines1 !== drawLines2
    ) {
      this.updateOverlayCanvas();
    }

    // Massively decreases rendering time by not redrawing the StrategicMap during the movement animation. It was very slow on mobile on large maps.
    if (!this.props.movingAnimation && !shallowEqualObjects(restOfProps1, restOfProps2)) {
      this.updateStaticCanvas();
    }
  }

  handleResize() {
    const parentDiv = this.parentRef.current!;
    const staticCanvas = this.staticCanvasRef.current!;
    const overlayCanvas = this.overlayCanvasRef.current!;

    staticCanvas.width = staticCanvas.height = overlayCanvas.width = overlayCanvas.height = Math.min(parentDiv.clientWidth, parentDiv.clientHeight);
    this.updateStaticCanvas();
    this.updateOverlayCanvas();
  }

  drawTerrainAndFogOfWar(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { size, terrain, structures, fogOfWar, unexplored, mapDisplaySettings } = this.props;
    const { showTerrain, showFogOfWar } = mapDisplaySettings;

    let getExploredTileColor: (row: number, col: number) => string;

    if (showTerrain) {
      getExploredTileColor = (row, col) => {
        if (fogOfWar[row][col] && showFogOfWar) {
          switch (terrain[row][col]) {
            case Terrain.Water: return structures[row][col] === Structure.Ridge ? '#5A3312' : '#007E9B';
            case Terrain.Open: return structures[row][col] === Structure.Ridge ? '#5A3312' : '#2E6000';
            case Terrain.Swamp: return '#005D46';
            case Terrain.Forest: return '#082B04';
            case Terrain.Hill: return '#17440D';
            case Terrain.Mountain: return '#5A3312';
            case Terrain.Desert: return structures[row][col] === Structure.Ridge ? '#5A3312' : '#8B5004';
            case Terrain.Volcanic: return '#000';
            case Terrain.Ice: return structures[row][col] === Structure.Ridge ? '#5A3312' : '#909090';
          }
        } else {
          switch (terrain[row][col]) {
            case Terrain.Water: return structures[row][col] === Structure.Ridge ? '#8A6342' : '#15AECB';
            case Terrain.Open: return structures[row][col] === Structure.Ridge ? '#8A6342' : '#5E900C';
            case Terrain.Swamp: return '#028D76';
            case Terrain.Forest: return '#045B04';
            case Terrain.Hill: return '#47743D';
            case Terrain.Mountain: return '#8A6342';
            case Terrain.Desert: return structures[row][col] === Structure.Ridge ? '#8A6342' : '#BB8034';
            case Terrain.Volcanic: return '#50504E';
            case Terrain.Ice: return structures[row][col] === Structure.Ridge ? '#8A6342' : '#C3C3C3';
          }
        }
        return '#000';
      };
    } else if (showFogOfWar) {
      getExploredTileColor = (row, col) => fogOfWar[row][col] ? '#999' : '#ecb779';
    } else {
      getExploredTileColor = () => '#ecb779';
    }

    const getTileColor = (unexplored !== null)
      ? ((row: number, col: number) => unexplored[row][col] ? '#777' : getExploredTileColor(row, col))
      : getExploredTileColor;

    // Speed optimization - It's faster to draw one bigger rectangle than to draw small rectangles.
    // Expand the rectangle width as wide as one color goes.
    // The tiles are deliberately drawn 1 pixel bigger to fill gaps caused by imprecise math rounding.
    for (let row = 0; row < size; row++) {
      let currentColor = getTileColor(row, 0);
      let width = 1;

      for (let col = 1; col < size; col++) {
        const newColor = getTileColor(row, col);

        if (currentColor === newColor) {
          width++;
        } else {
          ctx.fillStyle = currentColor;
          ctx.fillRect((col - width) * pixelsPerTile, row * pixelsPerTile, pixelsPerTile * width + 1, pixelsPerTile + 1);

          currentColor = newColor;
          width = 1;
        }
      }

      ctx.fillStyle = currentColor;
      ctx.fillRect((size - width) * pixelsPerTile, row * pixelsPerTile, pixelsPerTile * width + 1, pixelsPerTile + 1);
    }
  }

  private structureToDrawFunction: { [structure: string]: (ctx: CanvasRenderingContext2D, pixelsPerTile: number, cx: number, cy: number) => void } = {
    [Structure.Port]: drawPort,
    [Structure.Temple]: drawTemple,
    [Structure.Encampment]: drawEncampment,
    [Structure.Grove]: drawGrove,
    [Structure.SageTower]: drawSageTower
  };

  drawRoads(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { size, unexplored, structures } = this.props;

    ctx.strokeStyle = '#433';
    ctx.lineWidth = pixelsPerTile / 2;
    ctx.beginPath();

    for (let row = 0; row < size; row++) {
      for (let col = 0; col < size; col++) {
        if (unexplored === null || !unexplored[row][col]) {
          const structure = structures[row][col];
          if (structure === Structure.Road) {

            if (row > 0 && structures[row - 1][col] === Structure.Road && (unexplored === null || !unexplored[row - 1][col])) {
              ctx.moveTo((col + 0.5) * pixelsPerTile, (row + 0.5) * pixelsPerTile);
              ctx.lineTo((col + 0.5) * pixelsPerTile, (row - 0.5) * pixelsPerTile);
            }
            if (col > 0 && structures[row][col - 1] === Structure.Road && (unexplored === null || !unexplored[row][col - 1])) {
              ctx.moveTo((col + 0.5) * pixelsPerTile, (row + 0.5) * pixelsPerTile);
              ctx.lineTo((col - 0.5) * pixelsPerTile, (row + 0.5) * pixelsPerTile);
            }
          } else {
            const drawFunction = this.structureToDrawFunction[structures[row][col]];
            if (drawFunction !== undefined) {
              const cx = (col + 0.5) * pixelsPerTile;
              const cy = (row + 0.5) * pixelsPerTile;
              drawFunction(ctx, pixelsPerTile, cx, cy);
            }
          }
        }
      }
    }

    ctx.stroke();
  }

  drawCities(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { unexplored, cities, currentTeamId, currentPlayerId } = this.props;

    for (const cityId in cities) {
      const { lastSeenRazed, lastSeenOwner, row, col, currentlyTrainableArmyTypeIds, currentlyTrainingArmyTypeId, owner } = cities[cityId];
      if (unexplored !== null && unexplored[row][col] && unexplored[row + 1][col] && unexplored[row][col + 1] && unexplored[row + 1][col + 1]) {
        continue;
      }

      if (lastSeenRazed[currentTeamId]) {
        for (let dr = -0.5; dr < 2; dr++) {
          for (let dc = -0.5; dc < 2; dc++) {
            ctx.fillStyle = (dr + dc) % 2 === 0 ? '#AAA' : '#000';
            ctx.fillRect((col + dc) * pixelsPerTile, (row + dr) * pixelsPerTile, pixelsPerTile, pixelsPerTile);
          }
        }
      } else {
        ctx.fillStyle = ((currentPlayerId === neutralPlayerId && currentlyTrainableArmyTypeIds === undefined)
          || (currentPlayerId !== neutralPlayerId && owner === currentPlayerId && currentlyTrainingArmyTypeId === undefined))
          ? '#F00' : '#000';
        ctx.fillRect((col - 0.5) * pixelsPerTile, (row - 0.5) * pixelsPerTile, 3 * pixelsPerTile, 3 * pixelsPerTile);

        const visibleOwner = lastSeenOwner[currentTeamId];
        ctx.fillStyle = playerColors[visibleOwner];
        ctx.beginPath();
        ctx.arc((col + 1) * pixelsPerTile, (row + 1) * pixelsPerTile, 1.3 * pixelsPerTile, 0, 2 * Math.PI);
        ctx.fill();
      }
    }
  }

  drawRuins(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { unexplored, ruins, currentTeamId } = this.props;

    ctx.beginPath();
    ctx.strokeStyle = 'black';
    ctx.lineWidth = pixelsPerTile / 3;

    for (const ruinId in ruins) {
      const { row, col, lastSeenExplored } = ruins[ruinId];
      if (unexplored !== null && unexplored[row][col]) {
        continue;
      }

      const seenExplored = lastSeenExplored[currentTeamId];
      if (!seenExplored) {
        ctx.rect(col * pixelsPerTile, row * pixelsPerTile, pixelsPerTile, pixelsPerTile);
      }
    }

    ctx.stroke();
  }

  drawFlaggables(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { unexplored, flaggables, currentTeamId } = this.props;

    for (const flaggableId in flaggables) {
      const { lastSeenOwner, row, col } = flaggables[flaggableId];
      if (unexplored !== null && unexplored[row][col]) {
        continue;
      }

      const visibleOwner = lastSeenOwner[currentTeamId];

      ctx.fillStyle = playerColors[visibleOwner];
      ctx.beginPath();
      ctx.arc((col + 0.5) * pixelsPerTile, (row + 0.5) * pixelsPerTile, 0.7 * pixelsPerTile, 0, 2 * Math.PI);
      ctx.fill();
    }
  }

  drawGroups(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { groups, fogOfWar } = this.props;

    ctx.lineWidth = pixelsPerTile / 3;

    for (const groupId in groups) {
      const { row, col, owner } = groups[groupId];
      if (fogOfWar[row][col]) {
        continue;
      }

      ctx.beginPath();
      ctx.strokeStyle = playerColors[owner];
      ctx.moveTo(col * pixelsPerTile, row * pixelsPerTile);
      ctx.lineTo((col + 1) * pixelsPerTile, (row + 1) * pixelsPerTile);
      ctx.moveTo((col + 1) * pixelsPerTile, row * pixelsPerTile);
      ctx.lineTo(col * pixelsPerTile, (row + 1) * pixelsPerTile);
      ctx.stroke();
    }
  }

  drawReport(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { warReports } = this.props;

    ctx.lineWidth = pixelsPerTile / 6;

    for (const warReportItem of warReports) {
      const { row, col, reportType } = warReportItem;
      const cx = (col + 0.5) * pixelsPerTile;
      const cy = (row + 0.5) * pixelsPerTile;

      const gradient = ctx.createRadialGradient(cx, cy, 0, cx, cy, pixelsPerTile);
      ctx.strokeStyle = gradient;

      switch (reportType) {
        case WarReportType.ArmiesDeserted:
          gradient.addColorStop(0.5, 'red');
          gradient.addColorStop(0.6, 'yellow');
          drawStar(ctx, pixelsPerTile, cx, cy, 8, 0.55, 0.55);
          break;

        case WarReportType.CityLost:
        case WarReportType.FlaggableLost:
        case WarReportType.ArmiesLostInCombat:
        case WarReportType.EnemyArmiesKilled:
          gradient.addColorStop(0.3, 'red');
          gradient.addColorStop(1, 'yellow');
          drawStar(ctx, pixelsPerTile, cx, cy, 8, 0.45, 0.9);
      }

      ctx.stroke();
    }
  }

  drawArtifacts(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { tiles, fogOfWar, size } = this.props;

    for (let row = 0; row < size; row++) {
      const tileRow = tiles[row];
      for (let col = 0; col < size; col++) {
        if (tileRow[col].artifactIds && !fogOfWar[row][col]) {
          const cx = (col + 0.5) * pixelsPerTile;
          const cy = (row + 0.5) * pixelsPerTile;

          const gradient = ctx.createLinearGradient(cx - pixelsPerTile / 2, cy - pixelsPerTile / 2, cx + pixelsPerTile / 2, cy + pixelsPerTile / 2);
          gradient.addColorStop(0, 'white');
          gradient.addColorStop(0.5, 'gold');
          gradient.addColorStop(1, 'brown');
          ctx.fillStyle = gradient;
          drawStar(ctx, pixelsPerTile, cx, cy, 5, 0.45, 0.9);
          ctx.fill();
        }
      }
    }
  }

  drawFocusRectangle(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { focusedRow, focusedCol, tacticalMapWidth, tacticalMapHeight } = this.props;

    ctx.strokeStyle = 'white';
    ctx.lineWidth = 2;
    ctx.strokeRect(
      (focusedCol - tacticalMapWidth / 2) * pixelsPerTile,
      (focusedRow - tacticalMapHeight / 2) * pixelsPerTile,
      tacticalMapWidth * pixelsPerTile,
      tacticalMapHeight * pixelsPerTile);
  }

  drawCircle(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { drawCircle, size } = this.props;

    ctx.strokeStyle = 'red';
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.arc(
      (drawCircle!.col + 0.5) * pixelsPerTile,
      (drawCircle!.row + 0.5) * pixelsPerTile,
      size * 0.075 * pixelsPerTile,
      0,
      2 * Math.PI);
    ctx.stroke();
  }

  drawLines(ctx: CanvasRenderingContext2D, pixelsPerTile: number) {
    const { drawLines } = this.props;

    ctx.lineWidth = 2;
    for (const { from, to, bidirectional } of drawLines!) {
      const x1 = (from.col + 0.5) * pixelsPerTile;
      const y1 = (from.row + 0.5) * pixelsPerTile;
      const x2 = (to.col + 0.5) * pixelsPerTile;
      const y2 = (to.row + 0.5) * pixelsPerTile;

      const gradient = ctx.createLinearGradient(x1, y1, x2, y2);
      if (bidirectional) {
        gradient.addColorStop(0, 'white');
        gradient.addColorStop(0.5, 'red');
      } else {
        gradient.addColorStop(0, 'red');
      }
      gradient.addColorStop(1, 'white');

      ctx.strokeStyle = gradient;
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      ctx.lineTo(x2, y2);
      ctx.stroke();
    }
  }

  updateStaticCanvas() {
    const { size, mapDisplaySettings } = this.props;

    const staticCanvas = this.staticCanvasRef.current!;
    const ctx = staticCanvas.getContext('2d')!;
    const pixelsPerTile = staticCanvas.width / size;

    this.drawTerrainAndFogOfWar(ctx, pixelsPerTile);

    if (mapDisplaySettings.showRoads) {
      this.drawRoads(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showRuins) {
      this.drawRuins(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showCities) {
      this.drawCities(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showFlaggables) {
      this.drawFlaggables(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showArtifacts) {
      this.drawArtifacts(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showWarReport) {
      this.drawReport(ctx, pixelsPerTile);
    }
    if (mapDisplaySettings.showArmies) {
      this.drawGroups(ctx, pixelsPerTile);
    }
  }

  updateOverlayCanvas() {
    const { size, drawLines, drawCircle, showFocusRectangle } = this.props;

    const overlayCanvas = this.overlayCanvasRef.current!;
    const ctx = overlayCanvas.getContext('2d')!;
    const pixelsPerTile = overlayCanvas.width / size;

    ctx.clearRect(0, 0, overlayCanvas.width, overlayCanvas.height);

    if (drawLines) {
      this.drawLines(ctx, pixelsPerTile);
    }
    if (drawCircle) {
      this.drawCircle(ctx, pixelsPerTile);
    }
    if (showFocusRectangle) {
      this.drawFocusRectangle(ctx, pixelsPerTile);
    }
  }

  /* Convert from canvas coordinates to in-game coordinates. Coordinates will be fractional numbers! */
  canvasCoordsToPlace(clientX: number, clientY: number): Place {
    const staticCanvas = this.staticCanvasRef.current!;
    const rect = staticCanvas.getBoundingClientRect();

    // Left top pixel of canvas displays col and row 0. Right bottom pixel displays col and row equal to land size - 1.
    const col = (clientX - rect.left) * this.props.size / rect.width;
    const row = (clientY - rect.top) * this.props.size / rect.height;
    return { row, col };
  }

  handleDrag(clientX: number, clientY: number) {
    if (this.props.onDrag !== undefined) {
      const place = this.canvasCoordsToPlace(clientX, clientY);
      this.props.onDrag(place);
    }
  }

  handleClick(clientX: number, clientY: number) {
    const place = this.canvasCoordsToPlace(clientX, clientY);

    // OnDoubleClick event doesn't trigger on touch devices, so we have to check double click ourselves.
    const timeCurrent = new Date().getTime();

    if (this.timeOfLastClick === null || timeCurrent - this.timeOfLastClick > 300) {
      this.timeOfLastClick = timeCurrent;

      if (this.props.onClick !== undefined) {
        this.props.onClick(place);
      }
    } else {
      this.timeOfLastClick = null;

      if (this.props.onDoubleClick !== undefined) {
        this.props.onDoubleClick(place);
      }
    }
  }

  render() {
    return (
      <div ref={this.parentRef} className="strategic-map">
        <canvas ref={this.staticCanvasRef} />
        <canvas ref={this.overlayCanvasRef}
          className={this.props.onClick || this.props.onDoubleClick ? 'clickable' : ''}
          onMouseDown={e => this.handleDrag(e.clientX, e.clientY)}
          onMouseMove={e => e.buttons !== 0 && this.handleDrag(e.clientX, e.clientY)}
          onTouchStart={e => this.handleDrag(e.touches[0].clientX, e.touches[0].clientY)}
          onTouchMove={e => this.handleDrag(e.touches[0].clientX, e.touches[0].clientY)}
          onClick={e => this.handleClick(e.clientX, e.clientY)}
          onContextMenu={e => e.preventDefault()}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  tacticalMapWidth: state.ui.tacticalMapWidth,
  tacticalMapHeight: state.ui.tacticalMapHeight,
  focusedRow: state.ui.focusedRow,
  focusedCol: state.ui.focusedCol,

  size: state.game.size,
  terrain: state.game.terrain,
  structures: state.game.structures,
  tiles: state.game.tiles,
  fogOfWar: state.game.fogOfWar,
  unexplored: getUnexploredForReadingOnly(state),
  groups: state.game.groups,
  cities: state.game.cities,
  flaggables: state.game.flaggables,
  ruins: state.game.ruins,
  warReports: state.game.players[state.game.currentPlayerId].warReports,
  currentPlayerId: state.game.currentPlayerId,
  currentTeamId: state.game.players[state.game.currentPlayerId].team,
  mapDisplaySettings: state.game.players[state.game.currentPlayerId].mapDisplaySettings,

  movingAnimation: state.path.movingAnimation
});

export default connect(
  mapStateToProps
)(StrategicMap);
