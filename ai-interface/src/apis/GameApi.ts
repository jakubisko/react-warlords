import { Place, PlaceWithTime } from '../types/place';
import { Army } from '../types/army';
import { MoveType, SearchPathArgs, TimeMatrix, BasicAvoidingStrategy } from '../types/path';
import { ExploringResult, RuinData, SageTowerChoice } from '../types/exploring';
import { CombatInputsAndResult, CombatPrediction } from '../types/combat';
import { WarReportItem } from '../types/warReport';
import { CityData } from '../types/city';
import { FlaggableData } from '../types/flaggable';
import { HeroData } from '../types/hero';
import { HeroOffer } from '../types/heroOffer';
import { SpyReport } from '../types/spyReport';
import { WinCondition } from '../constants/winCondition';

/** The maximum string size of the state that can be saved by GameApi.saveState(). */
export const maxLengthOfState = 100_000;

/**
 * API to be used by your custom AI. These are actions available in-game. Actions such as Load game or Edit map are not available.
 * 
 * The AI is not affected by the fog of war and the "unexplored shroud" - it can always see the entire map as if it had an infinite view radius.
 * This doesn't give it access to other hidden values such as what artifacts an enemy hero has equipped.
 * 
 * Use GameApi methods to retrieve part of the game state that changes, and to issue commands.
 * A command that would break game rules throws an error.
 * If you try to execute an illegal action, that method won't be applied and an error is thrown. You can catch it and continue the turn.
 * Uncaught error will cause your AI to be removed from the game.
 * 
 * Each method of the class has an equivalent method on "GameApiObstacle" object, which can be used to check validity of arguments without
 * executing it. If the equivalent method is missing, the action is always valid.
 */
export interface GameApi {

  //*********** Persistence of state between turns. ***********/
  /**
   * Saves a string that can be retrieved at later time, for example in the next turn.
   * You can use the standard JSON.stringify() function to convert an object into a string.
   * There is a maximum size limit for the string - see "maxLengthOfState".
   * TIP: Don't save data that can be retrieved by API's methods.
   */
  saveState(state: string): void;

  /** Returns the last string saved with saveState(). Returns an empty string if there is nothing saved. */
  getSavedState(): string;

  //*********** Global ***********/
  getMyPlayerId(): number;
  getTurnNumber(): number;

  /** To check whether the player 3 is alive, check playerAlive[3]. The values don't change during the turn. */
  getPlayerAlive(): boolean[];

  /** Players 1 and 3 are allies when playersTeam[1] === playersTeam[3]. The values don't change during the game. */
  getPlayersTeam(): number[];

  /** Sets the ordering by which your armies enter the combat. First army type in this list will be the first fighting. */
  setMyOrdering(armyTypeIds: string[]): void;

  /** Returns the ordering by which your armies enter the combat. First army type in this list will be the first fighting. */
  getMyOrdering(): string[];

  /** If there is an army, flaggable structure or a city at given place, returns its owner. Otherwise, returns null. */
  getOwnerOfPlace(place: Place): number | null;

  getPreviousTurnWarReportItems(): WarReportItem[];
  getSpyReport(): SpyReport;
  getWinCondition(): WinCondition;
  resign(): void;

  //*********** Gold, income and spending ***********/
  getGold(): number;
  getIncome(): number;
  getUpkeepOfExistingArmies(): number;
  getUpkeepOfArmiesCreatedTomorrow(): number;

  //*********** Cities ***********/
  getCity(cityId: string): CityData;
  setTrainingInCity(cityId: string, armyTypeId?: string): void;
  buildTrainingFacility(cityId: string, armyTypeId: string): void;
  demolishTrainingFacility(cityId: string, armyTypeId: string): void;
  setCaravanFromCityToCity(fromCityId: string, toCityId?: string): void;
  razeCity(cityId: string): void;

  //*********** Flaggable structures ***********/
  getFlaggable(flaggableId: string): FlaggableData;

  //*********** Ruins, Sage Tower ***********/
  getRuin(ruinId: string): RuinData;
  exploreRuin(heroId: string): ExploringResult;
  exploreSageTower(heroId: string, choice: SageTowerChoice): ExploringResult;
  /** Beside the obvious, this determines the size of help a player gets from the Sage Tower (10 or more - maximum help. 0 or less - no help). */
  getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(playerId: number): number;
  wasSageVisitedThisWeek(): boolean;

  //*********** Groups ***********/
  getAllMyGroupIds(): string[];
  groupExists(groupId: string): boolean;
  getPlaceOfGroup(groupId: string): Place;
  dismissGroup(groupId: string): void;

  //*********** Armies ***********/
  getAllMyArmies(): Army[];
  getArmiesInGroup(groupId: string): Army[];
  transferArmyFromGroupToOtherGroup(armyId: string, srcGroupId: string, destGroupId: string): void;
  /** Returns the id of the newly created group. */
  transferArmyFromGroupToNewGroup(armyId: string, srcGroupId: string): string;
  dismissArmy(groupId: string, armyId: string): void;
  getMyArmiesAt(place: Place): Army[];
  /** Returns army types at given place. This works on both your and enemy armies. For a city, this doesn't include armies from other tiles. */
  getArmyTypeIdsAt(place: Place): string[];

  //*********** Heroes ***********/
  /** If a hero offer to join the player, returns details about the offer. If none offer or if the offer was already accepted, returns null. */
  getHeroOffer(): HeroOffer | null;
  acceptHeroOffer(): void;
  getHero(heroId: string): HeroData;
  getArtifactIdsOnGround(place: Place): string[];
  pickArtifactFromGround(heroId: string, artifactId: string): void;
  dropArtifactOntoGround(heroId: string, artifactId: string): void;

  //*********** Group move ***********/

  /**
   * Returns whether given group flies, has no move penalty on some terrain, how much steps it can take per turn etc.
   * Artifacts that modify group's move type are taken into account by this method.
   */
  getMoveTypeOfGroup(groupId: string): MoveType;

  /**
   * Given a list of army types in a group, returns the move type of this group.
   * For example, an army consisting of Griffins and Unicorns doesn't fly, and has no move penalty in Forest.
   * Artifacts that modify group's move type are not taken into account by this method. Use getMoveTypeOfGroup() for that.
   */
  getMoveTypeOfArmyTypes(armyTypeIds: string[]): MoveType;

  /**
   * Creates a function that can be used as an argument for calculateTimeMatrix().
   * When called with GO_AROUND_ALLIES_AND_ENEMIES strategy, it's equal to the default function used by UI when playing as a human player.
   *
   * With this function, your group will not step on a tile with:
   * - your armies so large that there is not enough space for both groups to fit
   *
   * When GO_AROUND_ALLIES is used, additionally:
   * - ally's armies, cities and flaggable structures
   * 
   * When GO_AROUND_ALLIES_AND_ENEMIES is used, additionally:
   * - enemy's armies, cities and flaggable structures
   *
   * RETURNED OBJECT REFLECTS THE GAME STATE WHEN IT WAS CREATED. As such, it is useless to store it for later use, because armies move.
   */
  createBasicAvoidingFunction(yourGroupSize: number, strategy: BasicAvoidingStrategy): (row: number, col: number) => boolean;

  /**
   * Returns a two-dimensional array describing travel times from some place. It can be used in two ways:
   * a) To calculate the shortest path from the initial place to another.
   * b) To look around initial place so that you know what you can reach in some time, say two days.
   * 
   * This method uses Dijkstra's algorithm to search for the shortest path from initial place to each other place in land.
   * It may stop before covering entire land if it encounters the destination before that. If so, then the results are relevant only for some tiles.
   * Use method isCalculated() of the TimeMatrix to check for this.
   * 
   * THIS FUNCTION IS QUITE EXPENSIVE. Do not spam-call it in some weird cycle. Think about arguments - some can be used to speed it up.
   * 
   * RETURNED OBJECT IS QUITE LARGE. Do not store it - analyze relevant data and throw it away. Usually store just the result path.
   * 
   * IF YOUR TILE-AVOIDING FUNCTION REFERENCES GAME STATE, THEN THE RESULT TIME MATRIX IS ALSO RELEVANT ONLY TO THAT GAME STATE.
   * For example, if you are avoiding armies, then the result becomes obsolete once armies move. Don't store obsolete matrixes.
   */
  calculateTimeMatrix(searchPathArgs: SearchPathArgs): TimeMatrix;

  /**
   * Calculates the shortest path from initial place in the TimeMatrix to given place.
   * If the destination can't be reached, this returns a zero length array.
   * 
   * This function is quite fast. Calculating the TimeMatrix is not. If you want multiple paths starting from the same initial place,
   * reuse the matrix.
   */
  calculatePathToPlaceFromTimeMatrix(timeMatrix: TimeMatrix, destination: Place): PlaceWithTime[];

  /**
   * Given a path of Places that are missing times, returns the same path with calculated times.
   * You want to use this with paths that you created manually. Paths calculated from the TimeMatrix already include times.
   */
  calculateTimesOnPath(path: Place[], moveType: MoveType, initialMoveLeft: number): PlaceWithTime[];

  /** A group may move when it has 1 or more move left. */
  getMoveLeftOfGroup(groupId: string): number;

  /**
   * Moves a group to an adjacent place. After calling this, you should call getLastCombat() to check whether there was a combat.
   * If there was, the group maybe no longer exists!
   */
  moveGroupOneStep(groupId: string, destination: Place): void;

  //*********** Combat ***********/

  /** Call this immediately after the moveGroupOneStep() to get the result of the combat if there was one. Returns null if there wasn't. */
  getLastCombat(): CombatInputsAndResult | null;

  /**
   * Calculates probable result of combat based on multiple simulations. Note that chances of 0 or 100% don't guarantee actual results!
   * All relevant bonuses, such as artifacts and fortification, are included in the calculation.
   * 
   * You can use this version of the method for combat prediction when your group is the attacker.
   */
  predictCombatIfGroupAttacks(attackingGroupId: string, defendingPlace: Place): CombatPrediction;

  /**
   * Calculates probable result of combat based on multiple simulations. Note that chances of 0 or 100% don't guarantee actual results!
   * All relevant bonuses, such as artifacts and fortification, are included in the calculation.
   * 
   * You can use this version of the method for combat prediction between armies owned by any players.
   */
  predictCombatIfAllArmiesAtPlaceAttack(attackingPlace: Place, defendingPlace: Place): CombatPrediction;

  //*********** Unexplored ***********/

  /**
   * Use this if you want to help your ally with exploring the map. AI is unaffected by "unexplored shroud".
   * Returns a two-dimensional array. First index is the row, second is the column of the place.
   */
  getUnexploredGrid(): boolean[][];

  /** Use this if you want to help your ally with exploring the map. AI is unaffected by "unexplored shroud". */
  isPlaceUnexplored(place: Place): boolean;

}
