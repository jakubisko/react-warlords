/** Whether reporting on lost city, army killed in combat etc. */
export enum WarReportType {
  CityLost = 'CityLost',
  FlaggableLost = 'FlaggableLost',
  ArmiesDeserted = 'ArmiesDeserted',
  ArmiesLostInCombat = 'ArmiesLostInCombat',
  EnemyArmiesKilled = 'EnemyArmiesKilled',
  PlayerEliminated = 'PlayerEliminated',
  PlayerEliminatedAfterError = 'PlayerEliminatedAfterError',
  HeroPromoted = 'HeroPromoted'
}

/** Holds report on armies, cities etc. lost since last turn. */
export interface WarReportItem {
  readonly row: number;
  readonly col: number;
  readonly reportType: WarReportType;

  /** How many armies were lost etc. */
  readonly value: number;

  /**
   * In case of CityLost, FlaggableLost, ArmiesKilled, EnemyArmiesKilled this identifies the attacking player.
   * In case of ArmiesDeserted, HeroPromoted this is equal to the owner.
   * In case of PlayerEliminated, PlayerEliminatedAfterError this identifies the player eliminated.
   */
  readonly playerId: number;
}
