import { LandApi } from 'ai-interface/apis/LandApi';
import { GameState } from '../game/types';
import { Place } from 'ai-interface/types/place';
import { IllegalPlace } from 'ai-interface/types/obstacle';
import { MoveCostCalculator } from 'ai-interface/apis/MoveCostCalculator';
import { MoveCostCalculatorImplChecked } from './MoveCostCalculatorImplChecked';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { Abilities } from 'ai-interface/types/army';

export class LandApiImpl implements LandApi {

  // Exposed parts of the state are frozen. Since redux already requires state to be immutable, reusing doesn't break anything.
  private readonly size: number;
  private terrain?: Terrain[][];
  private structures?: Structure[][];
  private allCityIds?: string[];
  private allFlaggableIds?: string[];
  private allRuinIds?: string[];

  constructor(private gameState: GameState) {
    this.size = gameState.size;
  }

  private assertLegalPlace(place: Place) {
    const { row, col } = place;
    if (row % 1 !== 0 || col % 1 !== 0) {
      throw new IllegalPlace(`Place [${row}, ${col}] has coordinates that are not integers`);
    } else if (row < 0 || row >= this.size || col < 0 || col >= this.size) {
      throw new IllegalPlace(`Place [${row}, ${col}] is outside of the land boundaries`);
    }
  }

  getSize(): number {
    return this.size;
  }

  getTerrain(): Terrain[][] {
    if (this.terrain === undefined) {
      this.terrain = this.gameState.terrain;
      Object.freeze(this.terrain);
      this.terrain.forEach(Object.freeze);
    }

    return this.terrain;
  }

  getStructures(): Structure[][] {
    if (this.structures === undefined) {
      this.structures = this.gameState.structures;
      Object.freeze(this.structures);
      this.structures.forEach(Object.freeze);
    }

    return this.structures;
  }

  getAllCityIds(): string[] {
    if (this.allCityIds === undefined) {
      this.allCityIds = Object.keys(this.gameState.cities);
      Object.freeze(this.allCityIds);
    }

    return this.allCityIds;
  }

  getAllFlaggableIds(): string[] {
    if (this.allFlaggableIds === undefined) {
      this.allFlaggableIds = Object.keys(this.gameState.flaggables);
      Object.freeze(this.allFlaggableIds);
    }

    return this.allFlaggableIds;
  }

  getAllRuinIds(): string[] {
    if (this.allRuinIds === undefined) {
      this.allRuinIds = Object.keys(this.gameState.ruins);
      Object.freeze(this.allRuinIds);
    }

    return this.allRuinIds;
  }

  getCityIdAt(place: Place): string | undefined {
    this.assertLegalPlace(place);
    const { row, col } = place;
    return this.gameState.tiles[row][col].cityId;
  }

  getFlaggableIdAt(place: Place): string | undefined {
    this.assertLegalPlace(place);
    const { row, col } = place;
    return this.gameState.tiles[row][col].flaggableId;
  }

  getRuinIdAt(place: Place): string | undefined {
    this.assertLegalPlace(place);
    const { row, col } = place;
    return this.gameState.tiles[row][col].ruinId;
  }

  createMoveCostCalculator(abilities: Abilities): MoveCostCalculator {
    return new MoveCostCalculatorImplChecked(this.gameState, abilities);
  }

}
