import './GameMenuModal.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, ButtonGroup } from 'react-bootstrap';
import { setScreen, setShowingManual, setShowingMenu } from '../../model/ui/actions';
import { compressObject, decompressObject } from '../../model/game/serialization';
import { ScreenEnum } from '../../model/ui/types';
import { setPlayerAiState } from '../../model/game/actionsTurnPassing';
import { destroyEverythingOwnedByPlayer } from '../../model/game/actionsPlayingThunks';
import SaveButton from '../common/SaveButton';
import LoadFromAutosaveButton from '../common/LoadFromAutosaveButton';
import LoadButton from '../common/LoadButton';
import FullscreenButton from '../common/FullscreenButton';
import { maxPlayers } from 'ai-interface/constants/player';
import { loadGame } from '../../model/game/actionsTurnPassingThunks';
import VolumeButtons from '../common/VolumeButtons';
import { SoundEffect } from '../../model/sound/types';
import { soundEffects } from '../sounds/soundEffects';

export default function GameMenuModal() {
  const gameState = useSelector((state: RootState) => state.game);
  const onlineGameId = useSelector((state: RootState) => state.online.onlineGameId);
  const dispatch: AppDispatch = useDispatch();

  function getGameDownloadData(): Promise<Blob> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { backgroundSprites, ...saveGameData } = gameState;
    return compressObject(saveGameData, 'game', 'blob');
  }

  async function handleLoadGame(fileData: string | ArrayBuffer) {
    const saveGameData = await decompressObject(fileData, 'game');
    dispatch(loadGame(saveGameData));
    dispatch(setShowingMenu(false));
  }

  function handleShowManual() {
    dispatch(setShowingManual(true));
    dispatch(setShowingMenu(false));
  }

  function handleResign() {
    soundEffects.play(SoundEffect.Raze);
    dispatch(destroyEverythingOwnedByPlayer(gameState.currentPlayerId));
    dispatch(setShowingMenu(false));
  }

  function handleExitToMainMenu() {
    for (let playerId = 0; playerId < maxPlayers; playerId++) {
      dispatch(setPlayerAiState(playerId, ''));
    }
    dispatch(setScreen(ScreenEnum.MainMenuScreen));
  }

  return (
    <Modal className="game-menu-modal overflow-hidden" show centered onHide={() => dispatch(setShowingMenu(false))}>
      <Modal.Header>
        <Modal.Title>System menu</Modal.Title>
        <VolumeButtons />
        <FullscreenButton />
      </Modal.Header>

      <Modal.Body className="text-center">
        <ButtonGroup vertical>
          <LoadFromAutosaveButton
            disabled={onlineGameId !== null}
            onUpload={handleLoadGame}
          />
          <SaveButton
            caption="Save game to file"
            disabled={onlineGameId !== null}
            fileName="warlords-game"
            fileExtension="wsav"
            getDownloadData={getGameDownloadData}
          />
          <LoadButton
            caption="Load game from file"
            disabled={onlineGameId !== null}
            fileExtension="wsav"
            onUpload={handleLoadGame}
          />
          <Button onClick={handleShowManual}>
            Manual
          </Button>
          <Button onClick={handleResign}>
            Resign
          </Button>
          <Button onClick={handleExitToMainMenu}>
            Exit to main menu
          </Button>
          <Button onClick={() => dispatch(setShowingMenu(false))}>
            Return to game
          </Button>
        </ButtonGroup>
      </Modal.Body>

      <Modal.Footer />
    </Modal>
  );
}
