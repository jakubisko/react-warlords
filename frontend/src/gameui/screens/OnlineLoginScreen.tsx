import './OnlineLoginScreen.scss';
import { Button, ButtonGroup, Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { setErrorMessage, setScreen } from '../../model/ui/actions';
import { ScreenEnum } from '../../model/ui/types';
import { sessionStorageKeyOnlineLogin } from '../../model/root/constantsStorage';
import { useEffect, useState } from 'react';
import { auth, authProvider } from '../../model/online/firebaseConfig';
import { onAuthStateChanged } from 'firebase/auth';
import { signInWithPopup, signInWithRedirect } from 'firebase/auth';
import { signOut } from 'firebase/auth';
import { createOrUpdateUserOnServer, setLoggedInUser } from '../../model/online/actions';
import { OnlineUser } from '../../model/online/types';
import { Spinner3 } from '../common/Spinner3';

export default function OnlineLoginScreen() {
  const [isUpdating, setIsUpdating] = useState(false);

  useEffect(() => onAuthStateChanged(auth, (userInfo) => {
    if (userInfo !== null) {
      const user: OnlineUser = {
        uid: userInfo.uid,
        email: userInfo.email ?? '',
        displayName: userInfo.displayName ?? '',
        photoURL: userInfo.photoURL ?? '',
      };
      setIsUpdating(true);
      dispatch(createOrUpdateUserOnServer(user,
        () => { setIsUpdating(false); dispatch(setLoggedInUser(user)); },
        () => setIsUpdating(false)
      ));
    }
  }), [auth]);

  const loggedInUser = useSelector((state: RootState) => state.online.loggedInUser);
  const dispatch: AppDispatch = useDispatch();

  const loginWithGoogle = async () => {
    try {
      // Login with redirection doesn't work on localhost. We must use popup.
      // Login with popup doesn't work when used in PWA on mobile. We must use redirection.
      // After the login, google will redirect to the main screen.
      // If this key is set, the main screen will then redirect back to this screen.
      sessionStorage.setItem(sessionStorageKeyOnlineLogin, 'true');

      const isDevelopment = process.env.NODE_ENV == 'development';
      const signIn = isDevelopment ? signInWithPopup : signInWithRedirect;
      await signIn(auth, authProvider);
    } catch (err) {
      dispatch(setErrorMessage('Failed to sign in with Google', err));
    }
  };

  const logout = async () => signOut(auth)
    .then(() => dispatch(setLoggedInUser(null)))
    .catch((err) => dispatch(setErrorMessage('Failed to sign out', err)));

  const loggedOutContent = () => (
    <>
      <p>Please login before continuing.</p>
      <Button onClick={loginWithGoogle}>Login with Google</Button>
    </>
  );

  const loggedInContent = () => (
    <>
      <img src={loggedInUser?.photoURL ?? undefined} referrerPolicy="no-referrer" alt="User photo" draggable={false} />
      <p className="mt-3">{loggedInUser?.displayName}</p>

      <ButtonGroup>
        <Button onClick={() => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen))}>Continue</Button>
        <Button onClick={logout}>Log out</Button>
      </ButtonGroup>
    </>
  );

  return (
    <Modal className="screen background-main-menu overflow-hidden online-login-screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Online play</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {isUpdating
          ? <div><p>Loading...</p><Spinner3 /></div>
          : (loggedInUser === null)
            ? loggedOutContent()
            : loggedInContent()
        }
      </Modal.Body>

      <Modal.Footer className="justify-content-center">
        <Button onClick={() => dispatch(setScreen(ScreenEnum.MainMenuScreen))}>Exit to main menu</Button>
      </Modal.Footer>
    </Modal>
  );
}
