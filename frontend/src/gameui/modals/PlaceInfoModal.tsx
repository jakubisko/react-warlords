import './PlaceInfoModal.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import {
  getStructureName, getStructureDescription, getTerrainNameAndDescription, getLastSeenOwnerOfPlace, getStructureDescriptionExpanded,
  isUnexplored, getPlaceIncomeAndFortification
} from '../../model/game/selectorsPlaceInfo';
import ArmyImage from '../common/ArmyImage';
import { ProgramMode } from '../../model/ui/types';
import PlaceInfoEditor from './subcomponents/PlaceInfoEditor';
import { setShowingPlaceInfo } from '../../model/ui/actions';
import { Place } from 'ai-interface/types/place';
import { changePlaceOwner } from '../../model/game/actionsPlayingThunks';
import { PlayerPicker } from '../common/PlayerPicker';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import { pushToUndoQueue } from '../../model/editor/actions';
import { getVisibleArmiesDefendingPlace } from '../../model/game/selectorsPlaying';
import { IncomeAndFortificationDescription } from '../common/IncomeAndFortificationDescription';

interface PlaceInfoModalProps {
  place: Place;
}

export default function PlaceInfoModal({ place }: PlaceInfoModalProps) {
  const programMode = useSelector((state: RootState) => state.ui.programMode);
  const unexplored = useSelector((state: RootState) => isUnexplored(state, place));
  const structureName = useSelector((state: RootState) => getStructureName(state, place));
  const structureDescription = useSelector((state: RootState) => getStructureDescription(state, place));
  const structureDescriptionExpanded = useSelector((state: RootState) => getStructureDescriptionExpanded(state, place));
  const { income, fortification } = useSelector((state: RootState) => getPlaceIncomeAndFortification(state, place));
  const terrainNameAndDescription = useSelector((state: RootState) => getTerrainNameAndDescription(state, place));
  const owner = useSelector((state: RootState) => getLastSeenOwnerOfPlace(state, place));
  const armies = useSelector((state: RootState) => getVisibleArmiesDefendingPlace(state, place));
  const dispatch: AppDispatch = useDispatch();

  const placeInfoHeader = () => owner !== null && programMode === ProgramMode.Playing
    ? (
      <ModalTitleWithFlags owner={owner} caption={structureName} />
    )
    : (
      <Modal.Title className="d-flex">
        <span className="structure-name prevent-cutting">
          {structureName}
        </span>
        {owner !== null && programMode === ProgramMode.MapEditor && (
          <PlayerPicker
            value={owner}
            onChange={playerId => {
              dispatch(changePlaceOwner(place.row, place.col, playerId));
              dispatch(pushToUndoQueue());
            }}
          />
        )}
      </Modal.Title>
    );

  const placeInfoBody = () => (
    <>
      {programMode === ProgramMode.Playing && (income !== undefined || fortification !== undefined) && (
        <IncomeAndFortificationDescription income={income} fortification={fortification} />
      )}

      <p className="text-break">
        {structureDescription}

        {structureDescriptionExpanded !== null && (
          <OverlayTrigger placement="left" overlay={
            <Tooltip id="lim-desc">{structureDescriptionExpanded}</Tooltip>
          }>
            <span> (<span className="link">Details</span>)</span>
          </OverlayTrigger>
        )}
      </p>

      {programMode === ProgramMode.Playing && armies.length > 0 &&
        <div>
          {armies.map(army => (
            <ArmyImage
              key={army.id}
              armyTypeId={army.armyTypeId}
              onLeftClick="INFO"
              onRightClick="INFO"
            />
          ))}
        </div>
      }

      <p>{terrainNameAndDescription}</p>

      {programMode === ProgramMode.MapEditor && <PlaceInfoEditor place={place} />}
    </>
  );

  return (
    <Modal className="place-info-modal overflow-hidden" show centered onHide={() => dispatch(setShowingPlaceInfo(null))}>

      <Modal.Header>
        {unexplored ? <p /> : placeInfoHeader()}
      </Modal.Header>

      <Modal.Body className="py-0 text-center">
        {unexplored ? <p>Unexplored</p> : placeInfoBody()}
      </Modal.Body>

      <Modal.Footer className="justify-content-center">
        <Button onClick={() => dispatch(setShowingPlaceInfo(null))}>Close</Button>
      </Modal.Footer>

    </Modal>
  );
}
