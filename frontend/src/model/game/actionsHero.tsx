import { AppDispatch, RootState } from '../root/configureStore';
import { allArtifacts } from './constants';
import { setArmyBlessed, setTurnsTrainingLeft, addWarReportItem } from './actionsPlaying';
import { getHerosExperienceBonus } from './selectorsHero';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { ArtifactAbility } from 'ai-interface/constants/artifactAbility';
import { getRandomItem } from '../../functions';
import { getLevelOfHeroFromExperience } from 'ai-interface/constants/experience';
import { WarReportType } from 'ai-interface/types/warReport';
import { createArmyAtOrAroundPlace } from './actionsPlayingThunks';
import { setShowingToastMessage } from '../ui/actions';

export enum ActionTypes {
  ADD_EXPERIENCES = '[Game] ADD_EXPERIENCES',
  SET_HERO_LEVEL = '[Game] SET_HERO_LEVEL',
  CREATE_ARTIFACT_ON_GROUND = '[Game] CREATE_ARTIFACT_ON_GROUND',
  CREATE_ARTIFACT_FOR_HERO = '[Game] CREATE_ARTIFACT_FOR_HERO',
  DESTROY_ARTIFACT_ON_GROUND = '[Game] DESTROY_ARTIFACT_ON_GROUND',
  DESTROY_ARTIFACT_FOR_HERO = '[Game] DESTROY_ARTIFACT_FOR_HERO',
  SET_RUIN_EXPLORED = '[Game] SET_RUIN_EXPLORED',
}

export interface AddExperiences {
  type: ActionTypes.ADD_EXPERIENCES;
  payload: {
    heroId: string;
    amount: number;
  }
}

export interface SetHeroLevel {
  type: ActionTypes.SET_HERO_LEVEL;
  payload: {
    heroId: string;
    level: number;
  }
}

export interface CreateArtifactOnGround {
  type: ActionTypes.CREATE_ARTIFACT_ON_GROUND;
  payload: {
    row: number;
    col: number;
    artifactId: string;
  }
}

export interface CreateArtifactForHero {
  type: ActionTypes.CREATE_ARTIFACT_FOR_HERO;
  payload: {
    heroId: string;
    artifactId: string;
  }
}

export interface DestroyArtifactOnGround {
  type: ActionTypes.DESTROY_ARTIFACT_ON_GROUND;
  payload: {
    row: number;
    col: number;
    artifactId: string;
  }
}

export interface DestroyArtifactForHero {
  type: ActionTypes.DESTROY_ARTIFACT_FOR_HERO;
  payload: {
    heroId: string;
    artifactId: string;
  }
}

export interface SetRuinExplored {
  type: ActionTypes.SET_RUIN_EXPLORED;
  payload: {
    ruinId: string;
    explored: boolean;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

/**
 * Some artifacts execute action at the start of turn. This will loop all heroes of current player and execute that action. Artifacts
 * generating gold are not included - those are included in the income.
 */
export const triggerArtifactsAtTheStartOfTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { tiles, groups, currentPlayerId, turnNumber, heroes } = state.game;

  for (const hero of Object.values(heroes)) {
    const { armyId, groupId } = hero;
    const { row, col, owner } = groups[groupId];
    if (owner === currentPlayerId) {

      dispatch(addExperiences(armyId, getHerosExperienceBonus(hero)));

      if (hero.artifactIds !== undefined) {
        for (const artifactId of hero.artifactIds) {
          for (const [ability, value] of Object.entries(allArtifacts[artifactId].abilities)) {
            switch (ability) {
              case ArtifactAbility.SummonsArmiesOfLevel: {
                if (turnNumber % 7 === 1) {
                  const armyTypeId = getRandomItem(Object.keys(standardArmyTypes)
                    .filter(armyTypeId => standardArmyTypes[armyTypeId].trainingTime === value));
                  dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, owner));
                }
                break;
              }

              case ArtifactAbility.Blessing: {
                tiles[row][col].armies!
                  .filter(army => !army.blessed && standardArmyTypes[army.armyTypeId].strength <= (value ?? 0))
                  .forEach(army => dispatch(setArmyBlessed(army.groupId, army.id, true)));
                break;
              }

              case ArtifactAbility.DecreaseTrainingTime: {
                const { cityId } = tiles[row][col];
                if (cityId !== undefined) {
                  const { turnsTrainingLeft } = getState().game.cities[cityId]; // getState() needed when hero has two such artifacts
                  if ((turnsTrainingLeft ?? 0) > 1) {
                    dispatch(setTurnsTrainingLeft(cityId, turnsTrainingLeft! - 1));
                  }
                }
                break;
              }
            }
          }
        }
      }
    }
  }
};

export const levelUpHeroes = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { groups, currentPlayerId, heroes } = state.game;

  for (const hero of Object.values(heroes)) {
    const { armyId, groupId, level, experience } = hero;
    const { owner, row, col } = groups[groupId];
    if (owner === currentPlayerId) {
      const trueLevel = getLevelOfHeroFromExperience(experience);
      if (level !== trueLevel) {
        dispatch(setHeroLevel(armyId, trueLevel));

        dispatch(addWarReportItem(owner, {
          row,
          col,
          reportType: WarReportType.HeroPromoted,
          value: 1,
          playerId: owner
        }));
      }
    }
  }
};

export const addExperiences = (heroId: string, amount: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { groups, heroes, players, aiAdvantage } = getState().game;
  const { owner } = groups[heroes[heroId].groupId];

  if (players[owner].aiName !== undefined && amount > 0) {
    amount = Math.round(amount * (100 + aiAdvantage) / 100);
  }

  dispatch(setShowingToastMessage(`${amount} exp`));

  dispatch(_addExperiences(heroId, amount));
};

/** Low-level action. Use addExperiences(). */
const _addExperiences = (heroId: string, amount: number): AddExperiences => ({
  type: ActionTypes.ADD_EXPERIENCES,
  payload: {
    heroId,
    amount
  }
});

const setHeroLevel = (heroId: string, level: number): SetHeroLevel => ({
  type: ActionTypes.SET_HERO_LEVEL,
  payload: {
    heroId,
    level
  }
});

export const createArtifactOnGround = (row: number, col: number, artifactId: string): CreateArtifactOnGround => ({
  type: ActionTypes.CREATE_ARTIFACT_ON_GROUND,
  payload: {
    row,
    col,
    artifactId
  }
});

export const createArtifactForHero = (heroId: string, artifactId: string): CreateArtifactForHero => ({
  type: ActionTypes.CREATE_ARTIFACT_FOR_HERO,
  payload: {
    heroId,
    artifactId
  }
});

export const destroyArtifactOnGround = (row: number, col: number, artifactId: string): DestroyArtifactOnGround => ({
  type: ActionTypes.DESTROY_ARTIFACT_ON_GROUND,
  payload: {
    row,
    col,
    artifactId
  }
});

export const destroyArtifactForHero = (heroId: string, artifactId: string): DestroyArtifactForHero => ({
  type: ActionTypes.DESTROY_ARTIFACT_FOR_HERO,
  payload: {
    heroId,
    artifactId
  }
});

export const setRuinExplored = (ruinId: string, explored: boolean): SetRuinExplored => ({
  type: ActionTypes.SET_RUIN_EXPLORED,
  payload: {
    ruinId,
    explored
  }
});

export type HeroAction = AddExperiences
  | SetHeroLevel
  | CreateArtifactOnGround
  | CreateArtifactForHero
  | DestroyArtifactOnGround
  | DestroyArtifactForHero
  | SetRuinExplored;
