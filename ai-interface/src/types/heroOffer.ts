/** Holds details about an offer to buy a new hero. */
export interface HeroOffer {

  /** Cost that has to be paid. */
  readonly cost: number;

  /** City where the armies will be created. */
  readonly cityId: string;

  /** Determines hero's portrait image and name. No gameplay effect. */
  readonly portraitId: number;

  /** Armies that will be bought. The first one will be the hero, the rest will be one zero or more allies. */
  readonly armyTypeIds: string[];

}
