import './ArtifactEditor.scss';
import { useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import { Place } from 'ai-interface/types/place';
import { createArtifactOnGround, destroyArtifactOnGround } from '../../../model/game/actionsHero';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import { allArtifacts } from '../../../model/game/constants';
import ArtifactImage from '../../common/ArtifactImage';
import ArtifactPicker from './ArtifactPicker';
import { pushToUndoQueue } from '../../../model/editor/actions';
import ButtonWithShortcut from '../../common/ButtonWithShortcut';
import { EMPTY_ARRAY } from '../../../functions';

interface ArtifactEditorProps {
  place: Place;
}

export default function ArtifactEditor({ place }: ArtifactEditorProps) {
  const artifactIds = useSelector((state: RootState) => state.game.tiles[place.row][place.col].artifactIds ?? EMPTY_ARRAY as string[]);
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(1);
  const [isAdding, setAdding] = useState(false);
  const [removingId, setRemovingId] = useState<string | null>(null);

  const addArtifactSubmodal = () => (
    <Modal
      className="artifact-editor-submodal overflow-hidden"
      show centered
      onHide={() => setAdding(false)}
    >
      <Modal.Header>
        <Modal.Title>
          Choose artifact
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <ArtifactPicker
          artifactIds={Object.keys(allArtifacts).filter(artifactId => allArtifacts[artifactId].rarity === subpage)}
          actionButton={(artifactId, close) => (
            <ButtonWithShortcut
              shortcutCode="Enter"
              allowShortcutWhileModalIsOpen={true}
              onClick={() => {
                dispatch(createArtifactOnGround(place.row, place.col, artifactId));
                dispatch(pushToUndoQueue());
                setAdding(false);
                close();
              }}
            >
              Add
            </ButtonWithShortcut>
          )}
        />
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <ButtonGroup>
          <Button active={subpage === 1} onClick={() => setSubpage(1)}>Common</Button>
          <Button active={subpage === 2} onClick={() => setSubpage(2)}>Minor</Button>
          <Button active={subpage === 3} onClick={() => setSubpage(3)}>Major</Button>
          <Button active={subpage === 4} onClick={() => setSubpage(4)}>Relic</Button>
        </ButtonGroup>

        <Button onClick={() => setAdding(false)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );

  function currentArtifacts(): JSX.Element {
    const items: JSX.Element[] = [];

    for (let i = 0; i < artifactIds.length; i++) {
      items.push(
        <ArtifactImage
          key={i}
          artifactId={artifactIds[i]}
          selectable={true}
          onClick={() => setRemovingId(artifactIds[i])}
        />
      );
    }

    items.push(
      <ArtifactImage
        key={artifactIds.length}
        artifactId={null}
        selectable={true}
        onClick={() => setAdding(true)}
      />
    );

    return <div className="current-artifacts">{items}</div>;
  }

  // If user deletes the last artifact at the place, hide the editor.
  if (artifactIds.length === 0) {
    return <></>;
  }

  return (
    <div className="artifact-editor">
      {currentArtifacts()}

      {isAdding && addArtifactSubmodal()}

      {removingId !== null && (
        <Modal
          className="artifact-detail overflow-hidden" // A little hack - this is from style sheet of "ArtifactPicker".
          show centered
          onHide={() => setRemovingId(null)}
        >
          <Modal.Header>
            <Modal.Title>
              {allArtifacts[removingId].name}
            </Modal.Title>
          </Modal.Header>

          <Modal.Body className="py-0 text-center">
            <ArtifactImage
              artifactId={removingId}
              alwaysLarge={true}
              includeText={true}
            />
          </Modal.Body>

          <Modal.Footer className="justify-content-between">
            <ButtonWithShortcut
              shortcutCode="Enter"
              allowShortcutWhileModalIsOpen={true}
              onClick={() => {
                dispatch(destroyArtifactOnGround(place.row, place.col, removingId));
                dispatch(pushToUndoQueue());
                setRemovingId(null);
              }}
            >
              Remove
            </ButtonWithShortcut>

            <Button onClick={() => setRemovingId(null)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
}
