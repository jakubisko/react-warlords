import desertSfx from '../sounds/terrain/desert.mp3';
import forestSfx from '../sounds/terrain/forest.mp3';
import hillSfx from '../sounds/terrain/hill.mp3';
import iceSfx from '../sounds/terrain/ice.mp3';
import mountainSfx from '../sounds/terrain/mountain.mp3';
import openSfx from '../sounds/terrain/open.mp3';
import riverSfx from '../sounds/terrain/river.mp3';
import swampSfx from '../sounds/terrain/swamp.mp3';
import volcanicSfx from '../sounds/terrain/volcanic.mp3';
import waterSfx from '../sounds/terrain/water.mp3';

import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import { getBackgroundMusic } from '../../model/sound/selectors';
import { Terrain } from 'ai-interface/constants/terrain';
import { OtherMusic } from '../../model/sound/types';
import { Howl, HowlOptions } from 'howler';
import useHowl from './useHowl';
import { EMPTY_ARRAY } from '../../functions';

/**
 * How long fading between silence and full volume lasts.
 * Smaller volume differences take appropriately shorter time.
 * One music transition consists of fading out then fading in.
 */
const fadeDuration = 1000;

interface BackgroundMusicProps {
  volumeRequested: number;
}

export default function BackgroundMusic({ volumeRequested }: BackgroundMusicProps) {
  const musicRequested = useSelector((state: RootState) => getBackgroundMusic(state));

  const [musicPlaying, setMusicPlaying] = useState<Terrain | OtherMusic | null>(null);
  const [fading, setFading] = useState<'PAUSED' | 'FADING' | 'FIXED'>('PAUSED');

  // Workaround: Instead of handling the event in the event handler, we modify the state and handle it in the useEffect.
  // We can't access the sound in the "onfade" callback because that callback is an argument to the constructor of the sound.
  // Reading the state in the callback also doesn't work.
  const [fadingEnded, setFadingEnded] = useState(false);

  const howlOptions: HowlOptions = {
    src: '',
    loop: true,
    onfade: useCallback(() => setFadingEnded(true), EMPTY_ARRAY)
  };

  const musicToHowl: { [key in Terrain | OtherMusic]: Howl | null } = {
    [Terrain.Water]: useHowl({ ...howlOptions, src: waterSfx }),
    [Terrain.Open]: useHowl({ ...howlOptions, src: openSfx }),
    [Terrain.Swamp]: useHowl({ ...howlOptions, src: swampSfx }),
    [Terrain.Forest]: useHowl({ ...howlOptions, src: forestSfx }),
    [Terrain.Hill]: useHowl({ ...howlOptions, src: hillSfx }),
    [Terrain.Mountain]: useHowl({ ...howlOptions, src: mountainSfx }),
    [Terrain.Desert]: useHowl({ ...howlOptions, src: desertSfx }),
    [Terrain.Volcanic]: useHowl({ ...howlOptions, src: volcanicSfx }),
    [Terrain.Ice]: useHowl({ ...howlOptions, src: iceSfx }),
    [OtherMusic.River]: useHowl({ ...howlOptions, src: riverSfx })
  };

  useEffect(() => {
    if (fadingEnded) {
      const howl = musicPlaying === null ? null : musicToHowl[musicPlaying];
      if (howl !== null) {
        setFadingEnded(false);
        if (howl.volume() === 0) {
          setFading('PAUSED');
          howl.pause();
        } else {
          setFading('FIXED');
        }
      }
    } else if (fading === 'PAUSED' && volumeRequested > 0) {
      const howl = musicRequested === null ? null : musicToHowl[musicRequested];
      if (howl !== null) {
        setMusicPlaying(musicRequested);
        setFading('FADING');
        howl.play();
        howl.fade(0, volumeRequested, fadeDuration * volumeRequested);
      }
    } else if (fading === 'FIXED') {
      const howl = musicPlaying === null ? null : musicToHowl[musicPlaying];
      if (howl !== null && (musicRequested !== musicPlaying || volumeRequested !== howl.volume())) {
        setFading('FADING');
        const to = musicRequested !== musicPlaying ? 0 : volumeRequested;
        howl.fade(howl.volume(), to, fadeDuration * Math.abs(to - howl.volume()));
      }
    }
    // "musicToHowl" must be outside of the dependencies, because unfortunately it reinitializes on each render
  }, [musicRequested, musicPlaying, fadingEnded, fading, volumeRequested]);

  return <></>;
}
