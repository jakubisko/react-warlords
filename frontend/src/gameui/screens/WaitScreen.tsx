import { Modal } from 'react-bootstrap';
import { Spinner3 } from '../common/Spinner3';

/**
 * This component deliberately isn't connected to Redux and doesn't contain any child components.
 * Entire point of this screen is to hide other components so they are not rerendered many times while waiting for a lot of actions to end.
 */
export const WaitScreen = () => (
  <Modal className="screen" animation={false} show centered backdrop="static">
    <Modal.Header />

    <Modal.Body className="p-3">
      Please wait...
    </Modal.Body>

    <Modal.Footer>
      <Spinner3 />
    </Modal.Footer>
  </Modal>
);
