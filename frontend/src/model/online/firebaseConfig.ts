import { initializeApp, FirebaseOptions } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

export const firebaseConfig: FirebaseOptions = {
  apiKey: 'AIzaSyAQXM0eeX7woSDdpGlme0RV-rS5SvBrL08',
  authDomain: 'mobile-warlords.firebaseapp.com',
  databaseURL: 'https://mobile-warlords.firebaseio.com',
  projectId: 'mobile-warlords',
  storageBucket: 'mobile-warlords.firebasestorage.app',
  messagingSenderId: '312908725014',
  appId: '1:312908725014:web:f24d99cc0c5c5be78cee37'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const firestore = getFirestore(app);

export const authProvider = new GoogleAuthProvider();
authProvider.setCustomParameters({
  // Prompt the user to select an account. Relevant if multiple accounts were used on one device.
  prompt: 'select_account'
});
