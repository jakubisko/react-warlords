/** Radius around each army and group. */
export const groupViewRadius = 4;

/** Radius around the city that is always visible to its owner. */
export const cityViewRadius = 4;

/** Radius around the flaggable that is always visible to its owner. */
export const flaggableViewRadius = 2;

/** Radius around the Watchtower that is always visible to its owner. */
export const watchtowerViewRadius = 12;

/**
 * Returns whether it is possible to see from one place an another. View radius has a shape of a circle.
 * Tiles are considered visible when the distance between their center points is less than view radius plus 0.5.
 */
export function isWithinViewRadius(rowDifference: number, colDifference: number, viewRadius: number): boolean {
  return rowDifference ** 2 + colDifference ** 2 < (viewRadius + 0.5) ** 2;
}
