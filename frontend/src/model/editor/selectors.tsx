import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { Place } from 'ai-interface/types/place';
import { allArmyTypes } from '../game/constants';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { structureAllowedTerrains } from './constants';
import { StructureAtPlace } from './types';

const getGameState = (state: RootState) => state.game;

/**
 * Return whether it is allowed to place given structure on given place. Example reason why it's not ok:
 * - Trying to place structure on a tile that already has a structure
 * - Trying to place city partially outside of the map
 * - Trying to place port away from shore
 * - Trying to place village on volcanic
 */
export const canPlaceStructure = createSelector(
  getGameState, (rootState: RootState, structureAtPlace: StructureAtPlace) => structureAtPlace,
  (gameState, structureAtPlace) => {
    const { row: row0, col: col0, structure, border = 0 } = structureAtPlace;
    const { size, terrain, structures } = gameState;

    for (let row = Math.max(0, row0 - border); row <= Math.min(size - 1, row0 + border); row++) {
      for (let col = Math.max(0, col0 - border); col <= Math.min(size - 1, col0 + border); col++) {

        if (!structureAllowedTerrains[structure].includes(terrain[row][col]) || structures[row][col] !== Structure.Nothing) {
          return false;
        }

        switch (structure) {
          case Structure.Port:
            if ((row <= 0 || terrain[row - 1][col] === Terrain.Water) && (row >= size - 1 || terrain[row + 1][col] === Terrain.Water)
              && (col <= 0 || terrain[row][col - 1] === Terrain.Water) && (col >= size - 1 || terrain[row][col + 1] === Terrain.Water)) {
              return false;
            }
            break;

          case Structure.City:
            if ((row >= size - 1) || (col >= size - 1)
              || !structureAllowedTerrains[structure].includes(terrain[row + 1][col]) || (structures[row + 1][col] !== Structure.Nothing)
              || !structureAllowedTerrains[structure].includes(terrain[row][col + 1]) || (structures[row][col + 1] !== Structure.Nothing)
              || !structureAllowedTerrains[structure].includes(terrain[row + 1][col + 1]) || (structures[row + 1][col + 1] !== Structure.Nothing)) {
              return false;
            }
        }
      }
    }

    return true;
  }
);

/**
 * Given a place, this returns some army type that can be placed here according to the rules. For example, for volcanic, an Elemental
 * will be returned.
 */
export const getArmyTypePlaceableAtPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, { row, col }) => {
    const terrain = gameState.terrain[row][col];

    if (terrain === Terrain.Volcanic) {
      return allArmyTypes.elem;
    } else if (terrain === Terrain.Mountain || terrain === Terrain.Water) {
      return allArmyTypes.crow;
    }
    return allArmyTypes.rnd0;
  }
);

export const canEditorPlaceArmyAtPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  ({ tiles, cities }, { row, col }) => {
    let tile = tiles[row][col];

    const cityId = tile.cityId;
    if (cityId !== undefined) {
      const { row, col } = cities[cityId];
      tile = tiles[row][col];
    }

    return tile.armies === undefined;
  }
);

export const canEditorPlaceArtifactAtPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  ({ terrain, tiles }, { row, col }) => tiles[row][col].artifactIds === undefined && terrain[row][col] !== Terrain.Volcanic
);
