import './OnlineSavingFailedScreen.scss';
import { useDispatch } from 'react-redux';
import { setScreen } from '../../model/ui/actions';
import { Button, Modal } from 'react-bootstrap';
import { ScreenEnum } from '../../model/ui/types';
import { AppDispatch } from '../../model/root/configureStore';
import { updateGameOnServer } from '../../model/online/actions';

export default function OnlineSavingFailedScreen() {
  const dispatch: AppDispatch = useDispatch();

  function handleRetry() {
    dispatch(setScreen(ScreenEnum.WaitScreen));
    dispatch(updateGameOnServer(
      () => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen)),
      () => dispatch(setScreen(ScreenEnum.OnlineSavingFailedScreen))
    ));
  }

  return (
    <Modal className="screen online-saving-failed-screen overflow-hidden" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>
          Saving failed
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className="p-3">
        The game was not saved to the server. Would you like to retry?
        Note that to prevent cheating, if you don&apos;t save the game, you will have to skip your entire turn.
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={handleRetry}>
          Retry
        </Button>

        <Button onClick={() => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen))}>
          Abort
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
