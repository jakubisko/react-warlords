import { ActionTypes } from './actionsPlaying';
import { City, Group, Flaggable, Ruin, GameState, Tile, TimelineItem } from './types';
import { allArmyTypes } from './constants';
import { EMPTY_ARRAY, EMPTY_MAP, updateGrid, withoutUndefinedProperties } from '../../functions';
import { reducer as reducerHero } from './reducersHero';
import { generateIds } from './selectorsPlaying';
import { Army } from 'ai-interface/types/army';
import { heroExperienceLevels } from 'ai-interface/constants/experience';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { WinCondition } from 'ai-interface/constants/winCondition';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { TileSprite } from '../../gameui/map/tactical-map/spriteMetaData';
import { WarReportItem } from 'ai-interface/types/warReport';
import { AppAction } from '../root/configureStore';

export const initialState: GameState = {
  size: 0,
  terrain: EMPTY_ARRAY as Terrain[][],
  structures: EMPTY_ARRAY as Structure[][],
  tiles: EMPTY_ARRAY as Tile[][],
  cities: EMPTY_MAP,
  flaggables: EMPTY_MAP,
  ruins: EMPTY_MAP,
  groups: EMPTY_MAP,
  heroes: EMPTY_MAP,
  lastIds: EMPTY_ARRAY as number[],
  winCondition: WinCondition.CaptureAllEnemyCities,
  backgroundSprites: EMPTY_ARRAY as TileSprite[][][],
  fogOfWar: EMPTY_ARRAY as boolean[][],
  unexploredByTeam: EMPTY_ARRAY as null[],
  players: [{ // There is a TacticalMap component on MainMenuScreen to pre-load all in-game images. It would throw an error without this.
    team: 0,
    name: '',
    alive: false,
    gold: 0,
    armyTypeIdToOrder: EMPTY_MAP,
    mapDisplaySettings: EMPTY_MAP,
    savedMapFocus: { row: 0, col: 0 },
    warReports: EMPTY_ARRAY as WarReportItem[],
    totalKills: 0,
    totalLosses: 0
  }],
  currentPlayerId: 0,
  turnNumber: 0,
  timeline: EMPTY_ARRAY as TimelineItem[],
  heroOffer: null,
  aiOutputVisible: false,
  aiStates: EMPTY_ARRAY as string[],
  aiAdvantage: 0,
  aiOutput: EMPTY_MAP
};

export function gameReducer(state = initialState, action: AppAction): GameState {
  switch (action.type) {

    case ActionTypes.REVEAL_FOG_OF_WAR_AT_PLACES: {
      const { places } = action.payload;
      const { fogOfWar, tiles, cities, flaggables, ruins, players, currentPlayerId } = state;
      const currentTeamId = players[currentPlayerId].team;

      const newFogOfWar = [...fogOfWar];
      let newCities: { [cityId: string]: City } | null = null; // Lazy initialized - copy only if it changes
      let newFlaggables: { [flaggableId: string]: Flaggable } | null = null;
      let newRuins: { [ruinId: string]: Ruin } | null = null;

      let lastRow = -1;
      for (const { row, col } of places) {
        if (row !== lastRow) {
          lastRow = row;
          newFogOfWar[row] = [...fogOfWar[row]];
        }

        newFogOfWar[row][col] = false;

        // If there is city, update its visible state
        const cityId = tiles[row][col].cityId;
        if (cityId !== undefined) {
          const city = cities[cityId];
          if (city.owner !== city.lastSeenOwner[currentTeamId] ||
            !!city.capitol !== city.lastSeenCapitol[currentTeamId] ||
            !!city.razed !== city.lastSeenRazed[currentTeamId]) {
            if (newCities === null) {
              newCities = { ...cities };
            }
            newCities[cityId] = {
              ...city,
              lastSeenOwner: city.lastSeenOwner.map((oldVal, teamId) => teamId === currentTeamId ? city.owner : oldVal),
              lastSeenCapitol: city.lastSeenCapitol.map((oldVal, teamId) => teamId === currentTeamId ? !!city.capitol : oldVal),
              lastSeenRazed: city.lastSeenRazed.map((oldVal, teamId) => teamId === currentTeamId ? !!city.razed : oldVal)
            };
          }
        }

        // If there is a flaggable structure, update its visible state
        const flaggableId = tiles[row][col].flaggableId;
        if (flaggableId !== undefined) {
          const flaggable = flaggables[flaggableId];
          if (flaggable.owner !== flaggable.lastSeenOwner[currentTeamId]) {
            if (newFlaggables === null) {
              newFlaggables = { ...flaggables };
            }
            newFlaggables[flaggableId] = {
              ...flaggable,
              lastSeenOwner: flaggable.lastSeenOwner.map((oldVal, teamId) => teamId === currentTeamId ? flaggable.owner : oldVal)
            };
          }
        }

        // If there is ruin, update its visible state
        const ruinId = tiles[row][col].ruinId;
        if (ruinId !== undefined) {
          const ruin = ruins[ruinId];
          if (!!ruin.explored !== ruin.lastSeenExplored[currentTeamId]) {
            if (newRuins === null) {
              newRuins = { ...ruins };
            }
            newRuins[ruinId] = {
              ...ruin,
              lastSeenExplored: ruin.lastSeenExplored.map((oldVal, teamId) => teamId === currentTeamId ? !!ruin.explored : oldVal)
            };
          }
        }

      }

      return {
        ...state,
        fogOfWar: newFogOfWar,
        cities: newCities ?? state.cities,
        flaggables: newFlaggables ?? state.flaggables,
        ruins: newRuins ?? state.ruins
      };
    }

    case ActionTypes.REVEAL_UNEXPLORED_AT_PLACES: {
      const { places } = action.payload;
      const { unexploredByTeam, currentPlayerId, players } = state;
      const { team } = players[currentPlayerId];
      const unexplored = unexploredByTeam[team];

      if (unexplored === null) {
        return state;
      }

      const newUnexplored = [...unexplored];

      let lastRow = -1;
      for (const { row, col } of places) {
        if (row !== lastRow) {
          lastRow = row;
          newUnexplored[row] = [...unexplored[row]];
        }

        newUnexplored[row][col] = false;
      }

      return {
        ...state,
        unexploredByTeam: state.unexploredByTeam.map((oldVal, id) => id !== team ? oldVal : newUnexplored)
      };
    }

    case ActionTypes.SET_UNEXPLORED_BY_TEAM: {
      const { team, unexplored } = action.payload;
      return {
        ...state,
        unexploredByTeam: state.unexploredByTeam.map((oldVal, id) => id !== team ? oldVal : unexplored)
      };
    }

    case ActionTypes.SET_PLAYER_ALIVE: {
      const { playerId, alive } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          alive
        })
      };
    }

    case ActionTypes.SET_SAGE_VISITED_THIS_WEEK: {
      const { playerId, sageVisitedThisWeek } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : withoutUndefinedProperties({
          ...player,
          sageVisitedThisWeek: sageVisitedThisWeek ? true : undefined
        }))
      };
    }

    case ActionTypes.SET_ARMY_TYPE_ID_TO_ORDER: {
      const { playerId, armyTypeIdToOrder } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          armyTypeIdToOrder
        })
      };
    }

    case ActionTypes.SET_MAP_DISPLAY_SETTINGS: {
      const { playerId, mapDisplaySettings } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          mapDisplaySettings
        })
      };
    }

    case ActionTypes.RESET_WAR_REPORTS: {
      const { playerId } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          warReports: EMPTY_ARRAY as WarReportItem[]
        })
      };
    }

    case ActionTypes.ADD_WAR_REPORT_ITEM: {
      const { playerId, reportItem } = action.payload;
      return (playerId === neutralPlayerId) ? state : {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          warReports: [...state.players[playerId].warReports, reportItem]
        })
      };
    }

    case ActionTypes.ADD_KILLS_LOSSES: {
      const { playerId, kills, losses } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          totalKills: player.totalKills + kills,
          totalLosses: player.totalLosses + losses
        })
      };
    }

    case ActionTypes.ADD_GOLD: {
      const { playerId, amount } = action.payload;
      return {
        ...state,
        players: state.players.map((player, id) => id !== playerId ? player : {
          ...player,
          gold: state.players[playerId].gold + amount
        })
      };
    }

    case ActionTypes.SET_CITY_CAPITOL: {
      const { cityId, capitol } = action.payload;
      const { cities, players, currentPlayerId } = state;
      const city = cities[cityId];

      return {
        ...state,
        cities: {
          ...cities,
          [cityId]: withoutUndefinedProperties({
            ...city,
            capitol: capitol ? true : undefined,
            lastSeenCapitol: city.lastSeenCapitol.map((oldVal, teamId) => (teamId === players[currentPlayerId].team || teamId === players[city.owner].team) ? capitol : oldVal)
          })
        }
      };
    }

    case ActionTypes.SET_CITY_RAZED: {
      const { cityId } = action.payload;
      const { cities, players, currentPlayerId } = state;
      return {
        ...state,
        cities: {
          ...cities,
          [cityId]: {
            ...cities[cityId],
            razed: true,
            lastSeenRazed: cities[cityId].lastSeenRazed.map((oldVal, teamId) => teamId === players[currentPlayerId].team ? true : oldVal)
          }
        }
      };
    }

    case ActionTypes.SET_CURRENTLY_TRAINING: {
      const { cityId, armyTypeId } = action.payload;
      return (armyTypeId === state.cities[cityId].currentlyTrainingArmyTypeId)
        ? state : {
          ...state,
          cities: {
            ...state.cities,
            [cityId]: withoutUndefinedProperties({
              ...state.cities[cityId],
              currentlyTrainingArmyTypeId: armyTypeId,
              turnsTrainingLeft: armyTypeId === undefined ? undefined : allArmyTypes[armyTypeId].trainingTime
            })
          }
        };
    }

    case ActionTypes.SET_TURNS_TRAINING_LEFT: {
      const { cityId, turnsTrainingLeft } = action.payload;
      return {
        ...state,
        cities: {
          ...state.cities,
          [cityId]: withoutUndefinedProperties({
            ...state.cities[cityId],
            turnsTrainingLeft
          })
        }
      };
    }

    case ActionTypes.SET_CURRENTLY_TRAINABLE_ARMY_TYPE_IDS: {
      const { cityId, currentlyTrainableArmyTypeIds } = action.payload;
      return {
        ...state,
        cities: {
          ...state.cities,
          [cityId]: {
            ...state.cities[cityId],
            currentlyTrainableArmyTypeIds
          }
        }
      };
    }

    case ActionTypes.SET_CITY_OWNER: {
      const { cityId, owner } = action.payload;
      const { cities, players, currentPlayerId } = state;
      const city = cities[cityId];

      return {
        ...state,
        cities: {
          ...cities,
          [cityId]: {
            ...city,
            owner,
            lastSeenOwner: city.lastSeenOwner.map((oldVal, teamId) => (teamId === players[currentPlayerId].team || teamId === players[city.owner].team) ? owner : oldVal)
          }
        }
      };
    }

    case ActionTypes.SET_FLAGGABLE_OWNER: {
      const { flaggableId, owner } = action.payload;
      const { flaggables, players, currentPlayerId } = state;
      const flaggable = flaggables[flaggableId];

      return {
        ...state,
        flaggables: {
          ...flaggables,
          [flaggableId]: {
            ...flaggable,
            owner,
            lastSeenOwner: flaggable.lastSeenOwner.map((oldVal, teamId) => (teamId === players[currentPlayerId].team || teamId === players[flaggable.owner].team) ? owner : oldVal)
          }
        }
      };
    }

    case ActionTypes.SET_CARAVAN_DESTINATION: {
      const { fromCityId, toCityId } = action.payload;

      return {
        ...state,
        cities: {
          ...state.cities,
          [fromCityId]: withoutUndefinedProperties({
            ...state.cities[fromCityId],
            caravanToCityId: toCityId
          })
        }
      };
    }

    case ActionTypes.SET_INCOMING_CARAVANS: {
      const { cityId, incomingCaravans } = action.payload;

      return {
        ...state,
        cities: {
          ...state.cities,
          [cityId]: withoutUndefinedProperties({
            ...state.cities[cityId],
            incomingCaravans
          })
        }
      };
    }

    case ActionTypes.CREATE_ARMY: {
      const { row, col, armyTypeId, owner, heroPortraitId } = action.payload;
      const { armyId, groupId, lastIds } = generateIds(state);

      // Create new army
      const army: Army = {
        id: armyId,
        groupId,
        armyTypeId,
        moveLeft: allArmyTypes[armyTypeId].move
      };

      // If the new army is hero, add it to list of heroes.
      let newHeroes = state.heroes;
      if (heroPortraitId !== undefined) {
        const level = parseInt(armyTypeId.charAt(3));
        newHeroes = {
          ...state.heroes,
          [armyId]: {
            armyId,
            groupId,
            portraitId: heroPortraitId,
            experience: heroExperienceLevels[level],
            level
          }
        };
      }

      // Create new group
      const group: Group = {
        id: groupId,
        owner,
        row,
        col
      };

      // Add the army to tile
      const tiles = updateGrid(state.tiles, row, col, tile => ({
        ...tile,
        armies: (tile.armies === undefined) ? [army] : [...tile.armies, army]
      }));

      return {
        ...state,
        tiles,
        // Add the group to map of all groups
        groups: {
          ...state.groups,
          [groupId]: group
        },
        heroes: newHeroes,
        lastIds
      };
    }

    case ActionTypes.DESTROY_ARMY: {
      const { groupId, armyId } = action.payload;
      let { groups, heroes } = state;
      const { row, col } = groups[groupId];

      // Update tile so that its armies list will drop the army
      const tiles = updateGrid(state.tiles, row, col, tile => {
        const { armies, ...tileWithoutArmies } = tile;
        const armiesAfter = armies!.filter(army => army.id !== armyId);
        return (armiesAfter.length > 0)
          ? { ...tileWithoutArmies, armies: armiesAfter }
          : tileWithoutArmies;
      });

      // If the group now has 0 armies, remove it from the map of groups
      const { armies } = tiles[row][col];
      if (armies === undefined || !armies.some(army => army.groupId === groupId)) {
        groups = { ...groups };
        delete groups[groupId];
      }

      // If the destroyed army was a hero, remove him from the list of heroes
      if (heroes[armyId] !== undefined) {
        heroes = { ...heroes };
        delete heroes[armyId];
      }

      return {
        ...state,
        tiles,
        groups,
        heroes
      };
    }

    case ActionTypes.TRANSFER_ARMY_BETWEEN_GROUPS: {
      const { armyId, srcGroupId } = action.payload;
      let { destGroupId } = action.payload;
      const groups = { ...state.groups };
      const { row, col, owner, destRow, destCol } = groups[srcGroupId];
      let { lastIds } = state;

      // If destination group doesn't exist, add it to map of groups
      if (destGroupId === null) {
        const generated = generateIds(state);
        destGroupId = generated.groupId;
        lastIds = generated.lastIds;

        groups[destGroupId] = {
          id: destGroupId,
          owner,
          row,
          col,
          destRow,
          destCol
        };
      }

      // Update tile so that the army now holds the id of the destination group
      const tiles = updateGrid(state.tiles, row, col, tile => ({
        ...tile,
        armies: tile.armies!.map(army => army.id === armyId ? {
          ...army,
          groupId: destGroupId!
        } : army)
      }));

      // If the moved army is a hero, update the map of heroes
      let { heroes } = state;
      if (armyId in heroes) {
        heroes = {
          ...heroes,
          [armyId]: {
            ...heroes[armyId],
            groupId: destGroupId
          }
        };
      }

      // If source group now has 0 armies, remove it from the map of groups
      if (!tiles[row][col].armies!.some(army => army.groupId === srcGroupId)) {
        delete groups[srcGroupId];
      }

      return {
        ...state,
        tiles,
        groups,
        heroes,
        lastIds
      };
    }

    case ActionTypes.GROUP_JUMP_TO_PLACE: {
      const { groupId, destRow, destCol } = action.payload;
      const { row, col, ...group } = state.groups[groupId];
      delete group.waiting; // After moving, group will no longer be waiting

      const tiles = [...state.tiles];

      // Update tile we're leaving
      tiles[row] = [...tiles[row]];
      const srcTile = tiles[row][col];
      const armiesMoving = srcTile.armies!.filter(army => army.groupId === groupId);
      const { armies, ...tileWithoutArmies } = srcTile;
      const armiesAfter = armies!.filter(army => army.groupId !== groupId);
      const srcTileAfter: Tile = armiesAfter.length > 0
        ? { ...tileWithoutArmies, armies: armiesAfter }
        : tileWithoutArmies;
      tiles[row][col] = srcTileAfter;

      // Update destination tile
      if (destRow !== row) {
        tiles[destRow] = [...tiles[destRow]];
      }
      const destTile = tiles[destRow][destCol];
      tiles[destRow][destCol] = {
        ...destTile,
        armies: destTile.armies === undefined ? armiesMoving : [...destTile.armies, ...armiesMoving]
      };

      return {
        ...state,
        tiles,
        groups: {
          ...state.groups,
          [groupId]: {
            ...group,
            row: destRow,
            col: destCol
          }
        }
      };
    }

    case ActionTypes.SET_ARMY_MOVE_LEFT: {
      const { groupId, armyId, moveLeft } = action.payload;
      const { row, col } = state.groups[groupId];

      return {
        ...state,
        tiles: updateGrid(state.tiles, row, col, tile => ({
          ...tile,
          armies: tile.armies!.map(army => army.id === armyId ? {
            ...army,
            moveLeft
          } : army)
        })),
      };
    }

    case ActionTypes.DECREASE_GROUP_MOVE_LEFT: {
      const { groupId, moveCost, isBoardingDeboardingShip } = action.payload;
      const { row, col } = state.groups[groupId];

      const tiles = updateGrid(state.tiles, row, col, tile => ({
        ...tile,
        armies: tile.armies!.map(army => army.groupId === groupId ? {
          ...army,
          moveLeft: isBoardingDeboardingShip ? Math.min(army.moveLeft - moveCost, 1) : (army.moveLeft - moveCost)
        } : army)
      }));

      return {
        ...state,
        tiles,
      };
    }

    case ActionTypes.SET_GROUP_DESTINATION: {
      const { groupId, destRow, destCol } = action.payload;
      const group = state.groups[groupId];

      // Optimization to prevent recalculation of getPathSprites selector, which would cause useless redrawing of TacticalMap.
      if (group.destRow === destRow && group.destCol === destCol) {
        return state;
      }

      return {
        ...state,
        groups: {
          ...state.groups,
          [groupId]: withoutUndefinedProperties({
            ...group,
            destRow,
            destCol
          })
        }
      };
    }

    case ActionTypes.SET_GROUP_WAITING: {
      const { groupId, waiting } = action.payload;
      return {
        ...state,
        groups: {
          ...state.groups,
          [groupId]: withoutUndefinedProperties({
            ...state.groups[groupId],
            waiting: waiting === true ? true : undefined
          })
        }
      };
    }

    case ActionTypes.SET_GROUP_OWNER: {
      const { groupId, owner } = action.payload;
      return {
        ...state,
        groups: {
          ...state.groups,
          [groupId]: {
            ...state.groups[groupId],
            owner
          }
        }
      };
    }

    case ActionTypes.SET_ARMY_BLESSED: {
      const { groupId, armyId, blessed } = action.payload;
      const group = state.groups[groupId];
      const { row, col } = group;

      return {
        ...state,
        tiles: updateGrid(state.tiles, row, col, oldTile => {
          return {
            ...oldTile,
            armies: oldTile.armies!.map(army => army.id === armyId ? withoutUndefinedProperties({
              ...army,
              blessed: blessed ? true : undefined
            }) : army)
          };
        })
      };
    }

    default:
      return reducerHero(state, action);
  }

}
