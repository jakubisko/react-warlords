/** Terrain affects move speed of armies. */
export enum Terrain {
  /** Water has move cost 2 (default). */
  Water = '~',

  /** Open has move cost 2 (default). */
  Open = 'O',

  /** Swamp has move cost 5 (+150%). */
  Swamp = 'S',

  /** Forest has move cost 4 (+100%). */
  Forest = 'F',

  /** Hill has move cost 6 (+200%). */
  Hill = 'H',

  /** Mountain has move cost 4 (+100%). Impassable to non-flying armies. */
  Mountain = 'M',

  /** Desert has move cost 3 (+50%). */
  Desert = 'D',

  /** Volcanic has move cost 3 (+50%). Impassable to all armies except Elementals. */
  Volcanic = 'V',

  /** Ice has move cost 3 (+50%). */
  Ice = 'I'
}

/** Converts Terrain to the user-friendly name of the terrain. */
export const terrainToName: { [key in Terrain]: string } = Object.freeze({
  [Terrain.Water]: 'Water',
  [Terrain.Open]: 'Open',
  [Terrain.Swamp]: 'Swamp',
  [Terrain.Forest]: 'Forest',
  [Terrain.Hill]: 'Hills',
  [Terrain.Mountain]: 'Mountains',
  [Terrain.Desert]: 'Desert',
  [Terrain.Volcanic]: 'Volcanic',
  [Terrain.Ice]: 'Ice'
});
