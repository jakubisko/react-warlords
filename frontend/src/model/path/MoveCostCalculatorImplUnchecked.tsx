import { GameState } from '../game/types';
import { allArmyTypes } from '../game/constants';
import { getHeroGrantsFlying, getHerosMove, getHerosValueOfAbility } from '../game/selectorsHero';
import { MoveCostCalculator } from 'ai-interface/apis/MoveCostCalculator';
import { Terrain } from 'ai-interface/constants/terrain';
import { Ability } from 'ai-interface/constants/ability';
import { Structure } from 'ai-interface/constants/structure';
import { Abilities } from 'ai-interface/types/army';
import { MoveType } from 'ai-interface/types/path';
import { Mutable } from '../../functions';
import { straightMoveCost } from 'ai-interface/constants/path';

const terrainToTravelCost: { [terrain: string]: number } = {
  [Terrain.Water]: 2,
  [Terrain.Open]: 2,
  [Terrain.Swamp]: 5,
  [Terrain.Forest]: 4,
  [Terrain.Hill]: 6,
  [Terrain.Desert]: 3,
  [Terrain.Ice]: 3
};

// NOTE: If you add new "No penalty" ability, also check selectors for hero; heroes can have these abilities
const terrainToAbility: { [terrain: string]: Ability } = {
  [Terrain.Swamp]: Ability.NoPenaltyInSwamp,
  [Terrain.Forest]: Ability.NoPenaltyInForest,
  [Terrain.Hill]: Ability.NoPenaltyInHills,
  [Terrain.Desert]: Ability.NoPenaltyInDesert,
  [Terrain.Ice]: Ability.NoPenaltyOnIce
};

export class MoveCostCalculatorImplUnchecked implements MoveCostCalculator {
  private readonly terrain: Terrain[][];
  private readonly structures: Structure[][];

  constructor(gameState: GameState, private readonly abilities: Abilities) {
    this.terrain = gameState.terrain;
    this.structures = gameState.structures;
  }

  getMoveCostAt(row: number, col: number): number {
    const structure = this.structures[row][col];
    if (structure === Structure.Road || structure === Structure.City) {
      return 1.5;
    }

    const terrain = this.terrain[row][col];

    if (terrain === Terrain.Mountain) {
      return this.abilities[Ability.Flies] || this.abilities[Ability.WalksOverMountains] ? 4 : Infinity;
    }

    if (terrain === Terrain.Volcanic) {
      return this.abilities[Ability.WalksOnVolcanic] ? 3 : Infinity;
    }

    if (this.abilities[Ability.Flies]) {
      return 2;
    }

    if (structure === Structure.Ridge) {
      return Infinity;
    }

    if (this.abilities[terrainToAbility[terrain]]) {
      return 2;
    }

    return terrainToTravelCost[terrain];
  }

  isCoastCrossingForbiddenAt(row1: number, col1: number, row2: number, col2: number): boolean {
    if (this.abilities[Ability.Flies]) {
      return false;
    }

    if (this.terrain[row1][col1] !== Terrain.Water && this.terrain[row2][col2] !== Terrain.Water) {
      return false;
    }

    if (this.terrain[row1][col1] === Terrain.Water && this.terrain[row2][col2] === Terrain.Water) {
      // Two lakes connected just by corners are drawn as separated. Diagonal move between these lakes is forbidden for ship.
      return (row1 !== row2 && col1 !== col2 && this.terrain[row1][col2] !== Terrain.Water && this.terrain[row2][col1] !== Terrain.Water);
    }

    if (this.structures[row1][col1] === Structure.Port || this.structures[row2][col2] === Structure.Port) {
      // Diagonal move to board/deboard a ship is forbidden. This is to prevent going from the top left to the bottom right on following map:
      // O ~ ~
      // ~ P O
      // ~ O O
      return (row1 !== row2 && col1 !== col2);
    }

    return true;
  }

  isBoardingDeboardingShipAt(row1: number, col1: number, row2: number, col2: number): boolean {
    if (this.abilities[Ability.Flies]) {
      return false;
    }

    return (this.terrain[row1][col1] === Terrain.Water) !== (this.terrain[row2][col2] === Terrain.Water);
  }

  isShipRequiredAt(row: number, col: number): boolean {
    return !this.abilities[Ability.Flies] && this.terrain[row][col] === Terrain.Water;
  }

}

/** Returns whether group of given army types flies, is native to terrain etc. */
export function calculateMoveTypeOfArmyTypes(armyTypeIds: string[]): MoveType {
  const groupAbilities: Mutable<Abilities> = { [Ability.Flies]: 1, [Ability.WalksOnVolcanic]: 1, [Ability.WalksOverMountains]: 1 };

  for (const armyTypeId of armyTypeIds) {
    const { abilities } = allArmyTypes[armyTypeId];

    if (!abilities[Ability.Flies]) {
      delete groupAbilities[Ability.Flies];

      if (!abilities[Ability.WalksOverMountains]) {
        delete groupAbilities[Ability.WalksOverMountains];
      }
    }

    if (!abilities[Ability.WalksOnVolcanic]) {
      delete groupAbilities[Ability.WalksOnVolcanic];
    }

    for (const ability of Object.values(terrainToAbility)) {
      if (abilities[ability]) {
        groupAbilities[ability] = 1;
      }
    }
  }

  if (groupAbilities[Ability.Flies]) {
    delete groupAbilities[Ability.WalksOverMountains];
  } else {
    const abilities = armyTypeIds.map(armyTypeId => allArmyTypes[armyTypeId].abilities);
    if (abilities.some(a => a[Ability.CanBeCarriedByFlier]) &&
      abilities.every(a => a[Ability.CanBeCarriedByFlier] || a[Ability.Flies])
    ) {
      if (abilities.some(a => a[Ability.Flies])) {
        groupAbilities[Ability.Flies] = 1;
      } else {
        groupAbilities[Ability.CanBeCarriedByFlier] = 1; // Does nothing by itself; just an information for UI.
      }
    }
  }

  return {
    abilities: groupAbilities,
    groupSize: armyTypeIds.length,
    groupMoveOnLand: Math.min(...armyTypeIds.map(armyTypeId => allArmyTypes[armyTypeId].move))
      + Math.max(...armyTypeIds.map(armyTypeId => allArmyTypes[armyTypeId].abilities[Ability.GroupMoveBonus] ?? 0)) * straightMoveCost
  };
}

/** Returns whether given group flies, is native to terrain etc. */
export function calculateMoveTypeOfGroup(gameState: GameState, groupId: string): MoveType {
  const { tiles, groups, heroes } = gameState;
  const { row, col } = groups[groupId];
  const armies = tiles[row][col].armies!.filter(army => army.groupId === groupId);
  const heroesInGroup = armies.filter(army => allArmyTypes[army.armyTypeId].hero).map(army => heroes[army.id]);

  const abilities: Mutable<Abilities> = calculateMoveTypeOfArmyTypes(armies.map(army => army.armyTypeId)).abilities;

  // Artifact granting flying to entire group.
  if (!abilities[Ability.Flies] && heroesInGroup.some(hero => getHeroGrantsFlying(hero))) {
    abilities[Ability.Flies] = 1;
  }

  // Artifact granting no move penalty for all army types.
  if (heroesInGroup.length > 0) {
    for (const ability of Object.values(terrainToAbility)) {
      if (abilities[ability] === undefined && heroesInGroup.some(hero => getHerosValueOfAbility(hero, ability) > 0)) {
        abilities[ability] = 1;
      }
    }
  }

  return {
    abilities,
    groupSize: armies.length,
    groupMoveOnLand: Math.min(...armies.map(army =>
      allArmyTypes[army.armyTypeId].hero
        ? getHerosMove(heroes[army.id])
        : allArmyTypes[army.armyTypeId].move
    ))
      + Math.max(...armies.map(army =>
        allArmyTypes[army.armyTypeId].hero
          ? getHerosValueOfAbility(heroes[army.id], Ability.GroupMoveBonus)
          : (allArmyTypes[army.armyTypeId].abilities[Ability.GroupMoveBonus] ?? 0)
      )) * straightMoveCost
  };
}

/** Returns whether given group is ship. Non-flying group on water is ship. */
export function isGroupShip(gameState: GameState, groupId: string) {
  const { groups, terrain } = gameState;
  const { row, col } = groups[groupId];
  if (terrain[row][col] !== Terrain.Water) {
    return false;
  }

  const moveType = calculateMoveTypeOfGroup(gameState, groupId);
  return !moveType.abilities[Ability.Flies];
}
