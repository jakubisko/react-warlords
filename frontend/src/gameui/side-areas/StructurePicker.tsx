import './TwoItemsPerColumnContainer.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { setStructure } from '../../model/editor/actions';
import { Structure } from 'ai-interface/constants/structure';
import { Button } from 'react-bootstrap';
import { IconCity } from '../common/svg/IconCity';
import { IconRuin } from '../common/svg/IconRuin';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAnchor, faChessRook, faHillRockslide, faHouseFlag, faLandmark, faMask, faSignsPost, faTent, faTowerObservation } from '@fortawesome/free-solid-svg-icons';
import { IconGrove } from '../common/svg/IconGrove';
import { IconRoad } from '../common/svg/IconRoad';

export default function StructurePicker() {
  const structure = useSelector((state: RootState) => state.editor.structure);
  const dispatch: AppDispatch = useDispatch();

  return (
    <div className="two-items-per-column-container">
      <div className="button-row">
        <Button active={structure === Structure.City} onClick={() => dispatch(setStructure(Structure.City))}>
          <IconCity />
          <div className="caption">City</div>
        </Button>
        <Button active={structure === Structure.Village} onClick={() => dispatch(setStructure(Structure.Village))}>
          <FontAwesomeIcon icon={faHouseFlag} />
          <div className="caption">Village</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.Watchtower} onClick={() => dispatch(setStructure(Structure.Watchtower))}>
          <FontAwesomeIcon icon={faTowerObservation} />
          <div className="caption">Watchtower</div>
        </Button>
        <Button active={structure === Structure.SpyDen} onClick={() => dispatch(setStructure(Structure.SpyDen))}>
          <FontAwesomeIcon icon={faMask} />
          <div className="caption">Spy Den</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.Road} onClick={() => dispatch(setStructure(Structure.Road))}>
          <IconRoad />
          <div className="caption">Road</div>
        </Button>
        <Button active={structure === Structure.Signpost} onClick={() => dispatch(setStructure(Structure.Signpost))}>
          <FontAwesomeIcon icon={faSignsPost} />
          <div className="caption">Signpost</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.Ridge} onClick={() => dispatch(setStructure(Structure.Ridge))}>
          <FontAwesomeIcon icon={faHillRockslide} />
          <div className="caption">Ridge</div>
        </Button>
        <Button active={structure === Structure.Port} onClick={() => dispatch(setStructure(Structure.Port))}>
          <FontAwesomeIcon icon={faAnchor} />
          <div className="caption">Port</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.Temple} onClick={() => dispatch(setStructure(Structure.Temple))}>
          <FontAwesomeIcon icon={faLandmark} />
          <div className="caption">Temple</div>
        </Button>
        <Button active={structure === Structure.Ruin} onClick={() => dispatch(setStructure(Structure.Ruin))}>
          <IconRuin />
          <div className="caption">Ruin</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.SageTower} onClick={() => dispatch(setStructure(Structure.SageTower))}>
          <FontAwesomeIcon icon={faChessRook} />
          <div className="caption">Sage Tower</div>
        </Button>
        <Button active={structure === Structure.Encampment} onClick={() => dispatch(setStructure(Structure.Encampment))}>
          <FontAwesomeIcon icon={faTent} />
          <div className="caption">Encamp...</div>
        </Button>
      </div>
      <div className="button-row">
        <Button active={structure === Structure.Grove} onClick={() => dispatch(setStructure(Structure.Grove))}>
          <IconGrove />
          <div className="caption">Grove</div>
        </Button>
      </div>
    </div>
  );
}
