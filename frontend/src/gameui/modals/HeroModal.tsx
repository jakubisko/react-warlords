import './HeroModal.scss';

import { useEffect, useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, ButtonGroup, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { setShowingHero, setShowingExploring } from '../../model/ui/actions';
import { createArtifactOnGround, createArtifactForHero, destroyArtifactOnGround, destroyArtifactForHero } from '../../model/game/actionsHero';
import { IconImage, OtherIcon } from '../common/IconImage';
import ArtifactPicker from './subcomponents/ArtifactPicker';
import { Place } from 'ai-interface/types/place';
import { getHerosStrength, getHerosMove, getHerosUpkeep, getHerosHitPoints, getHerosAbilities, getChanceToDieWhenExploringRuinWithHero } from '../../model/game/selectorsHero';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import { getStructureDescriptionExpanded } from '../../model/game/selectorsPlaceInfo';
import { exploreRuin, exploreSageTower } from '../../model/exploring/actions';
import { heroExperienceLevels } from 'ai-interface/constants/experience';
import { straightMoveCost } from 'ai-interface/constants/path';
import { maxHeroArtifactsCapacity } from 'ai-interface/constants/artifactAbility';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { HeroPortrait } from '../common/HeroPortrait';
import { AbilitiesIcons } from '../common/AbilitiesIcons';
import heroNames from '../../predefined-data/hero-names.json';
import { SageTowerChoice } from 'ai-interface/types/exploring';
import { Structure } from 'ai-interface/constants/structure';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { EMPTY_ARRAY, clamp } from '../../functions';

interface HeroModalProps {
  heroId: string;
  place: Place;
}

export default function HeroModal({ heroId, place }: HeroModalProps) {
  const hero = useSelector(({ game }: RootState) => game.heroes[heroId]);
  const currentPlayerId = useSelector(({ game }: RootState) => game.currentPlayerId);
  const artifactIdsOnGround = useSelector(({ game }: RootState) => game.tiles[place.row][place.col].artifactIds ?? EMPTY_ARRAY as string[]);
  const structureDescriptionExpanded = useSelector((state: RootState) => getStructureDescriptionExpanded(state, place));
  const chanceToDieWhenExploringRuin = useSelector((state: RootState) => getChanceToDieWhenExploringRuinWithHero(state, heroId));
  const visitableSageTower = useSelector(({ game }: RootState) => game.structures[place.row][place.col] === Structure.SageTower
    && !game.players[currentPlayerId].sageVisitedThisWeek);
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(artifactIdsOnGround.length > 0 ? 1 : (chanceToDieWhenExploringRuin !== null ? 2 : (visitableSageTower ? 3 : 0)));

  useEffect(() => {
    if (chanceToDieWhenExploringRuin !== null) {
      soundEffects.play(SoundEffect.ExploringRuinSubmenu);
    } else if (visitableSageTower) {
      soundEffects.play(SoundEffect.SageTowerSubmenu);
    }
  }, EMPTY_ARRAY);

  function subpageOverview(): JSX.Element {
    const abilities = getHerosAbilities(hero);

    return (
      <>
        <div className="hero-portrait-right">
          <HeroPortrait imageId={hero.portraitId} />
        </div>
        <div className="container px-0">
          <div className="row d-flex align-items-center">
            <div className="col-1"><IconImage icon={OtherIcon.Strength} tooltip="Strength" /></div>
            <div className="col-3">Strength</div>
            <div className="col-1">{getHerosStrength(hero)}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-1"><IconImage icon={OtherIcon.HitPoints} tooltip="Hit points" /></div>
            <div className="col-3">Hit points</div>
            <div className="col-1">{getHerosHitPoints(hero)}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-1"><IconImage icon={OtherIcon.Move} tooltip="Move" /></div>
            <div className="col-3">Move</div>
            <div className="col-1">{getHerosMove(hero) / straightMoveCost}</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-1"><IconImage icon={OtherIcon.Price} tooltip="Daily upkeep. Paid at start of turn. Negative value indicates that the hero actually generates gold." /></div>
            <div className="col-3 text-nowrap">Daily upkeep</div>
            <div className="col-3">{getHerosUpkeep(hero)} gold</div>
          </div>
          <div className="row d-flex align-items-center">
            <div className="col-1"><IconImage icon={OtherIcon.Level} tooltip="Level - Increases hero's Strength, Leadership, Move and probability of survival in ruins" /></div>
            <div className="col-3">Level</div>
            <div className="col-1">{hero.level}</div>
            <div className="col-7">
              <OverlayTrigger placement="left" overlay={
                <Tooltip id="hm-desc">
                  To receive experience:
                  <ul>
                    <li>Defeat armies</li>
                    <li>Explore ruins</li>
                    <li>Capture cities</li>
                  </ul>
                  <p>Multiple heroes in group divide experience evenly.</p>
                </Tooltip>
              }>
                <span className="link">
                  {hero.level >= heroExperienceLevels.length - 1
                    ? 'Max level'
                    : (heroExperienceLevels[hero.level + 1] - hero.experience > 0
                      ? `Next: ${heroExperienceLevels[hero.level + 1] - hero.experience}`
                      : `Will level up`
                    )
                  }
                </span>
              </OverlayTrigger>
            </div>
          </div>
          <div className="row pt-2">
            <div className="col">
              <AbilitiesIcons abilities={abilities} />
            </div>
          </div>
        </div>
      </>
    );
  }

  function subpageArtifacts(): JSX.Element {
    const artifactIdsOnHero = hero.artifactIds ?? EMPTY_ARRAY as string[];

    return (
      <div>
        <div>Equipped artifacts:</div>
        <ArtifactPicker
          artifactIds={artifactIdsOnHero}
          numberOfAlwaysVisibleSlots={maxHeroArtifactsCapacity}
          actionButton={(artifactId: string, close: () => void) => (
            <ButtonWithShortcut
              shortcutCode="Enter"
              allowShortcutWhileModalIsOpen={true}
              onClick={() => {
                dispatch(destroyArtifactForHero(heroId, artifactId));
                dispatch(createArtifactOnGround(place.row, place.col, artifactId));
                soundEffects.play(SoundEffect.DropArtifactOntoGround);
                close();
              }}
            >
              Drop onto ground
            </ButtonWithShortcut>
          )}
        />

        {artifactIdsOnGround.length > 0 && (
          <>
            <div>Artifacts on the ground:</div>
            <ArtifactPicker
              artifactIds={artifactIdsOnGround}
              actionButton={(artifactId: string, close: () => void) => (
                <ButtonWithShortcut
                  shortcutCode="Enter"
                  allowShortcutWhileModalIsOpen={true}
                  disabled={artifactIdsOnHero.length >= maxHeroArtifactsCapacity}
                  onClick={() => {
                    dispatch(destroyArtifactOnGround(place.row, place.col, artifactId));
                    dispatch(createArtifactForHero(heroId, artifactId));
                    soundEffects.play(SoundEffect.PickArtifactFromGround);
                    close();
                  }}
                >
                  {artifactIdsOnHero.length < maxHeroArtifactsCapacity ? 'Pick from ground' : `Can't carry more artifacts`}
                </ButtonWithShortcut>
              )}
            />
          </>
        )}
      </div>
    );
  }

  const subpageRuin = () => (
    <div className="d-flex flex-column">
      <p>
        You are standing at the entrance to an ancient ruin.
        This place surely holds a treasure, guarded by a monster.
        Do you want to fight your way through?
      </p>
      <p>
        You have a {clamp(0, 100, chanceToDieWhenExploringRuin!)}% chance of dying here.
        <OverlayTrigger placement="left" overlay={
          <Tooltip id="hm-sr-desc">{structureDescriptionExpanded}</Tooltip>
        }>
          <span> (<span className="link">Details</span>)</span>
        </OverlayTrigger>
      </p>
      <Button
        className="col-6 align-self-center"
        onClick={() => {
          dispatch(exploreRuin(heroId));
          dispatch(setShowingHero(null));
          dispatch(setShowingExploring(true));
        }}
      >
        Explore ruin
      </Button>
    </div>
  );

  const sageTowerDetails: { [key in SageTowerChoice]: string } = {
    [SageTowerChoice.Armies]: 'Receive armies with build cost at least 50 gold + 160 for each city that you are behind the enemy with the most cities.'
      + ' (Rounded up to the nearest whole army). Only armies without group bonuses are given.',
    [SageTowerChoice.Gold]: 'Receive 80 gold for each city that you are behind the enemy with the most cities. (Variation ±10%)',
    [SageTowerChoice.Maps]: 'Reveal a rectangle of unexplored tiles around the tower. The rectangle will contain at least 150 nearest'
      + ' unexplored tiles + 500 for each city that you are behind the enemy with the most cities. Water counts as 2/3 of a tile.',
    [SageTowerChoice.Experience]: 'Receive 333 experience for each city that you are behind the enemy with the most cities. (Variation ±10%)',
  };

  const subpageSageTower = () => (
    <div>
      <p>
        You are standing at the entrance to an ancient tower.
        A wise sage, offering guidance for struggling heroes, lives here.
        What do you seek from the sage?
      </p>
      <div className="container">
        <div className="row">
          {Object.values(SageTowerChoice).map(choice => (
            <Button
              key={choice}
              className="col-3"
              onClick={() => {
                dispatch(exploreSageTower(heroId, choice));
                dispatch(setShowingHero(null));
                dispatch(setShowingExploring(true));
              }}
            >
              {choice}
            </Button>
          ))}
        </div>
        <div className="row">
          {Object.values(SageTowerChoice).map(choice => (
            <div className="col-3 py-2 text-center" key={choice}>
              <OverlayTrigger placement="top" overlay={
                <Tooltip id="lim-desc">
                  {sageTowerDetails[choice]}
                </Tooltip>
              }>
                <span>(<span className="link">Details</span>)</span>
              </OverlayTrigger>
            </div>
          ))}
        </div>
      </div>
    </div>
  );

  let subpageEl: JSX.Element;
  switch (subpage) {
    default: subpageEl = subpageOverview(); break;
    case 1: subpageEl = subpageArtifacts(); break;
    case 2: subpageEl = subpageRuin(); break;
    case 3: subpageEl = subpageSageTower(); break;
  }

  return (
    <Modal className="hero-modal" show centered onHide={() => dispatch(setShowingHero(null))}>
      <Modal.Header>
        <ModalTitleWithFlags owner={currentPlayerId} caption={heroNames[hero.portraitId]} />
      </Modal.Header>

      <Modal.Body>
        {subpageEl}
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <ButtonGroup>
          <Button active={subpage === 0} onClick={() => setSubpage(0)}>Overview</Button>
          <Button active={subpage === 1} onClick={() => setSubpage(1)}>Artifacts</Button>
          {chanceToDieWhenExploringRuin !== null &&
            <Button active={subpage === 2} onClick={() => setSubpage(2)}>Ruin</Button>
          }
          {visitableSageTower &&
            <Button active={subpage === 3} onClick={() => setSubpage(3)}>Sage</Button>
          }
        </ButtonGroup>

        <Button onClick={() => dispatch(setShowingHero(null))}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
