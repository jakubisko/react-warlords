import { Abilities, Army } from './army';
import { Terrain } from '../constants/terrain';

/** Determines global conditions of combat - it's place, terrain. */
export interface CombatConditions {
  readonly row: number;
  readonly col: number;
  readonly terrain: Terrain;

  /** True for both razed and unrazed cities. */
  readonly city: boolean;

  /** Fortification bonus in case of a combat in a city. */
  readonly fortification: number;
}

/** Describes everything about combat before its start. All data required is here; accessing the redux state is not required. */
export interface CombatInputs {
  readonly conditions: CombatConditions;

  /** List of attacking armies ordered by owner's ordering from the strongest. */
  readonly attackingArmies: Army[];

  /** List of defending armies ordered by owner's ordering from the strongest. */
  readonly defendingArmies: Army[];

  readonly attackingPlayerId: number;
  readonly defendingPlayerId: number;
}

export enum CombatEventType {
  /** Damage that doesn't kill the army. */
  Damage = 'Damage',
  /** Last damage that kills the army, but not ambush. */
  Kill = 'Kill',
  /** Instant kill. */
  Ambush = 'Ambush'
}

export interface CombatEvent {
  readonly type: CombatEventType;

  /** Id of the Army that received damage or got killed. */
  readonly losingArmyId: string;

  /** Group Id of the Army that received damage or got killed. */
  readonly losingGroupId: string;

  readonly winningArmyId: string;
  readonly winningGroupId: string;
}

/** Describes entire course and the result of combat. */
export interface CombatResult {
  /** Abilities affecting entire attacking group, such as Morale. */
  readonly attackingAbilities: Abilities;

  /** Abilities affecting entire defending group, such as Morale. */
  readonly defendingAbilities: Abilities;

  /** Final strength of each army after all bonuses are applied. Use Army.id as key. */
  readonly effectiveStrength: { [armyId: string]: number };

  /** List of hits and kills in the order as they happened. */
  readonly combatEvents: CombatEvent[];

  /** Total amount of experiences awarded to winning side. If there are multiple heroes, each will receive equal portion. */
  readonly experiences: number;

  readonly attackerWon: boolean;
}

/** Describes entire course and the result of a combat. */
export interface CombatInputsAndResult {
  readonly combatInputs: CombatInputs;
  readonly combatResult: CombatResult;
}


/** Describes the probability of combat survival for one army. */
export interface PredictionForArmy {
  readonly armyTypeId: string;

  /** Probability is a value from 0 to 1. */
  readonly probabilityOfSurvival: number;
}

/**
 * Describes probable result of combat based on multiple simulations. Note that chances of 0% or 100% don't guarantee actual results!
 * 
 * TIP: Player's probability of winning the combat is equal to the probability of survival of his strongest army (index 0).
 */
export interface CombatPrediction {
  /** Ordered by attacker's combat order. First army is the strongest and has the highest probability of survival. */
  readonly attackingArmies: PredictionForArmy[];

  /** Ordered by defender's combat order. First army is the strongest and has the highest probability of survival. */
  readonly defendingArmies: PredictionForArmy[];
}
