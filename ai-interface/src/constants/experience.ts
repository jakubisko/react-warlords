import { ValueOutOfRange } from '../types/obstacle';

/** Usage: experienceForRuin[ruin.level] */
export const experienceForRuin = Object.freeze([-1, 250, 500, 750, 1000]);

/** Percentual bonus to the experience when attacking a city. Attacking a capitol doubles this bonus. */
export const experiencePercentForAttackingCity = 25;

export const experienceForGrove = 1000;

// First level-up requires 500 experience. Second 1000 experience. Then each gap increases by 1000 experience. Last two levels are a bit slower.
/** Usage: heroExperienceLevels[hero.level] */
export const heroExperienceLevels = Object.freeze([-1, 0, 500, 1500, 3500, 6500, 10500, 15500, 22000, 30000]);

/** Returns the level of a hero with a given amount of experience. */
export function getLevelOfHeroFromExperience(experience: number): number {
  if (experience % 1 !== 0 || experience < 0) {
    throw new ValueOutOfRange(`'${experience}' experience is not a legal number, only whole values from 0 are allowed`);
  }

  let level = 1;
  while (level < heroExperienceLevels.length - 1 && experience >= heroExperienceLevels[level + 1]) {
    level++;
  }

  return level;
}
