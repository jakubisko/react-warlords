import './ArmyEditor.scss';
import { useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import { Place } from 'ai-interface/types/place';
import { createArmyAtOrAroundPlace, destroyArmy } from '../../../model/game/actionsPlayingThunks';
import ArmyImage from '../../common/ArmyImage';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import ArmyTypePicker from './ArmyTypePicker';
import { allArmyTypes } from '../../../model/game/constants';
import { pushToUndoQueue } from '../../../model/editor/actions';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { getOwnerOfPlace } from '../../../model/game/selectorsPlaying';
import ButtonWithShortcut from '../../common/ButtonWithShortcut';
import { EMPTY_ARRAY } from '../../../functions';
import { Army } from 'ai-interface/types/army';

interface ArmyEditorProps {
  place: Place;
}

export default function ArmyEditor({ place }: ArmyEditorProps) {
  const armies = useSelector((state: RootState) => state.game.tiles[place.row][place.col].armies ?? EMPTY_ARRAY as Army[]);
  const owner = useSelector((state: RootState) => getOwnerOfPlace(state, place));
  const dispatch: AppDispatch = useDispatch();

  // Whether we're picking the army on position 1, 2, 3 or...
  const [editingIndex, setEditingIndex] = useState<number | null>(null);

  function armyTypesSubmodal(): JSX.Element {
    if (editingIndex === null || owner === null) {
      return <></>;
    }

    return (
      <Modal
        className="army-editor-submodal"
        show centered
        onHide={() => setEditingIndex(null)}
      >
        <Modal.Header>
          <Modal.Title>
            Choose army type
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <ArmyTypePicker
            availableArmyTypeIds={Object.keys(allArmyTypes)}
            selectedArmyTypeIds={editingIndex < armies.length ? [armies[editingIndex].armyTypeId] : EMPTY_ARRAY as string[]}
            changeSelectionButton={(armyTypeId, currentlySelected, close) => (
              <ButtonWithShortcut
                shortcutCode="Enter"
                allowShortcutWhileModalIsOpen={true}
                onClick={() => {
                  if (editingIndex < armies.length) {
                    const { groupId, id } = armies[editingIndex];
                    dispatch(destroyArmy(groupId, id));
                  }
                  if (!currentlySelected) {
                    dispatch(createArmyAtOrAroundPlace(place.row, place.col, armyTypeId, owner));
                  }

                  dispatch(pushToUndoQueue());
                  setEditingIndex(null);
                  close();
                }}
              >
                {currentlySelected ? 'Remove' : (editingIndex < armies.length ? 'Replace' : 'Add')}
              </ButtonWithShortcut>
            )}
          />
        </Modal.Body>

        <Modal.Footer>
          <ButtonGroup>
            {editingIndex < armies.length && (
              <Button
                onClick={() => {
                  const { groupId, id } = armies[editingIndex];
                  dispatch(destroyArmy(groupId, id));
                  dispatch(pushToUndoQueue());
                  setEditingIndex(null);
                }}
              >
                Remove
              </Button>
            )}

            <Button onClick={() => setEditingIndex(null)}>
              Close
            </Button>
          </ButtonGroup>
        </Modal.Footer>
      </Modal>
    );
  }

  function currentArmies(): JSX.Element {
    const items: JSX.Element[] = [];

    for (let i = 0; i < armies.length; i++) {
      items.push(
        <ArmyImage
          key={armies[i].id}
          armyTypeId={armies[i].armyTypeId}
          selectable={true}
          selected={i === editingIndex}
          onLeftClick={() => setEditingIndex(i)}
          onRightClick={() => setEditingIndex(i)}
        />
      );
    }

    for (let i = items.length; i < maxTileCapacity; i++) {
      items.push(
        <ArmyImage
          key={i}
          armyTypeId={null}
          selectable={true}
          selected={i === editingIndex}
          onLeftClick={() => setEditingIndex(armies.length)}
          onRightClick={() => setEditingIndex(armies.length)}
        />
      );
    }

    return <div className="current-armies">{items}</div>;
  }

  // If user deletes the last army at the place, hide the editor.
  if (owner === null) {
    return <></>;
  }

  return (
    <div className="army-editor">
      {currentArmies()}
      {armyTypesSubmodal()}
    </div>
  );
}
