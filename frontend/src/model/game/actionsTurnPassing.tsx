import { GameState, TimelineItem } from './types';
import { Place } from 'ai-interface/types/place';
import { HeroOffer } from 'ai-interface/types/heroOffer';
import { WinCondition } from 'ai-interface/constants/winCondition';

export enum ActionTypes {
  // New game, load
  SET_GAME_STATE = '[Game] SET_GAME_STATE',
  SET_PLAYER_NAME = '[Game] SET_PLAYER_NAME',
  SET_PLAYER_ONLINE_UID = '[Game] SET_PLAYER_ONLINE_UID',
  SET_PLAYER_TEAM = '[Game] SET_PLAYER_TEAM',
  SET_PLAYER_AI_NAME = '[Game] SET_PLAYER_AI_NAME',
  SET_PLAYER_AI_STATE = '[Game] SET_PLAYER_AI_STATE',
  SET_AI_ADVANTAGE = '[Game] SET_AI_ADVANTAGE',
  SET_AI_OUTPUT_VISIBLE = '[Game] SET_AI_OUTPUT_VISIBLE',
  RESET_AI_OUTPUT = '[Game] RESET_AI_OUTPUT',
  ADD_TO_AI_OUTPUT = '[Game] ADD_TO_AI_OUTPUT',
  SET_WIN_LOSS_CONDITION = '[Game] SET_WIN_LOSS_CONDITION',

  // Turn passing
  SET_CURRENT_PLAYER = '[Game] SET_CURRENT_PLAYER',
  SET_TURN_NUMBER = '[Game] SET_TURN_NUMBER',
  RESET_FOG_OF_WAR = '[Game] RESET_FOG_OF_WAR',
  SET_PLAYER_SAVED_MAP_FOCUS = '[Game] SET_PLAYER_SAVED_MAP_FOCUS',
  ADD_TIMELINE_ITEM = '[Game] ADD_TIMELINE_ITEM',
  SET_HERO_OFFER = '[Game] SET_HERO_OFFER',
}

export interface SetGameState {
  type: ActionTypes.SET_GAME_STATE;
  payload: {
    gameState: GameState;
  }
}

export interface SetPlayerTeam {
  type: ActionTypes.SET_PLAYER_TEAM;
  payload: {
    playerId: number;
    team: number;
  }
}

export interface SetPlayerName {
  type: ActionTypes.SET_PLAYER_NAME;
  payload: {
    playerId: number;
    name: string;
  }
}

export interface SetPlayerOnlineUid {
  type: ActionTypes.SET_PLAYER_ONLINE_UID;
  payload: {
    playerId: number;
    onlineUid?: string;
  }
}

export interface SetPlayerAiName {
  type: ActionTypes.SET_PLAYER_AI_NAME;
  payload: {
    playerId: number;
    aiName?: string;
  }
}

export interface SetPlayerAiState {
  type: ActionTypes.SET_PLAYER_AI_STATE;
  payload: {
    playerId: number;
    aiState: string;
  }
}

export interface SetAiAdvantage {
  type: ActionTypes.SET_AI_ADVANTAGE;
  payload: {
    aiAdvantage: number
  }
}

export interface SetAiOutputVisible {
  type: ActionTypes.SET_AI_OUTPUT_VISIBLE;
  payload: {
    aiOutputVisible: boolean
  }
}

export interface ResetAiOutput {
  type: ActionTypes.RESET_AI_OUTPUT;
}

export interface AddToAiOutput {
  type: ActionTypes.ADD_TO_AI_OUTPUT;
  payload: {
    text: string;
    groupId: string;
    place?: Place;
  }
}

export interface SetWinCondition {
  type: ActionTypes.SET_WIN_LOSS_CONDITION;
  payload: {
    winCondition: WinCondition
  }
}

export interface SetCurrentPlayer {
  type: ActionTypes.SET_CURRENT_PLAYER;
  payload: {
    playerId: number;
  }
}

export interface SetTurnNumber {
  type: ActionTypes.SET_TURN_NUMBER;
  payload: {
    turnNumber: number;
  }
}

export interface ResetFogOfWar {
  type: ActionTypes.RESET_FOG_OF_WAR;
}

export interface SetPlayerSavedMapFocus {
  type: ActionTypes.SET_PLAYER_SAVED_MAP_FOCUS;
  payload: {
    playerId: number;
    mapFocus: Place;
  }
}

export interface AddTimelineItem {
  type: ActionTypes.ADD_TIMELINE_ITEM;
  payload: {
    item: TimelineItem;
  }
}

export interface SetHeroOffer {
  type: ActionTypes.SET_HERO_OFFER;
  payload: {
    heroOffer: HeroOffer | null;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

export const setGameState = (gameState: GameState): SetGameState => ({
  type: ActionTypes.SET_GAME_STATE,
  payload: {
    gameState
  }
});

export const setPlayerTeam = (playerId: number, team: number): SetPlayerTeam => ({
  type: ActionTypes.SET_PLAYER_TEAM,
  payload: {
    playerId,
    team
  }
});

export const setPlayerName = (playerId: number, name: string): SetPlayerName => ({
  type: ActionTypes.SET_PLAYER_NAME,
  payload: {
    playerId,
    name
  }
});

export const setPlayerOnlineUid = (playerId: number, onlineUid?: string): SetPlayerOnlineUid => ({
  type: ActionTypes.SET_PLAYER_ONLINE_UID,
  payload: {
    playerId,
    onlineUid
  }
});

export const setPlayerAiName = (playerId: number, aiName?: string): SetPlayerAiName => ({
  type: ActionTypes.SET_PLAYER_AI_NAME,
  payload: {
    playerId,
    aiName
  }
});

export const setPlayerAiState = (playerId: number, aiState: string): SetPlayerAiState => ({
  type: ActionTypes.SET_PLAYER_AI_STATE,
  payload: {
    playerId,
    aiState
  }
});

export const setAiAdvantage = (aiAdvantage: number): SetAiAdvantage => ({
  type: ActionTypes.SET_AI_ADVANTAGE,
  payload: {
    aiAdvantage
  }
});

export const setAiOutputVisible = (aiOutputVisible: boolean): SetAiOutputVisible => ({
  type: ActionTypes.SET_AI_OUTPUT_VISIBLE,
  payload: {
    aiOutputVisible
  }
});

export const resetAiOutput = (): ResetAiOutput => ({
  type: ActionTypes.RESET_AI_OUTPUT
});

export const addToAiOutput = (text: string, groupId: string, place?: Place): AddToAiOutput => ({
  type: ActionTypes.ADD_TO_AI_OUTPUT,
  payload: {
    text,
    groupId,
    place
  }
});

export const setWinCondition = (winCondition: WinCondition): SetWinCondition => ({
  type: ActionTypes.SET_WIN_LOSS_CONDITION,
  payload: {
    winCondition
  }
});

export const setCurrentPlayer = (playerId: number): SetCurrentPlayer => ({
  type: ActionTypes.SET_CURRENT_PLAYER,
  payload: {
    playerId
  }
});

export const setTurnNumber = (turnNumber: number): SetTurnNumber => ({
  type: ActionTypes.SET_TURN_NUMBER,
  payload: {
    turnNumber
  }
});

/** Low-level action. Use regenerateFogOfWar(). */
export const _resetFogOfWar = (): ResetFogOfWar => ({
  type: ActionTypes.RESET_FOG_OF_WAR
});

export const setPlayerSavedMapFocus = (playerId: number, mapFocus: Place): SetPlayerSavedMapFocus => ({
  type: ActionTypes.SET_PLAYER_SAVED_MAP_FOCUS,
  payload: {
    playerId,
    mapFocus: { row: mapFocus.row, col: mapFocus.col }
  }
});

export const addTimelineItem = (item: TimelineItem): AddTimelineItem => ({
  type: ActionTypes.ADD_TIMELINE_ITEM,
  payload: {
    item: {
      cities: [...item.cities],
      armyValue: [...item.armyValue]
    }
  }
});

export const setHeroOffer = (heroOffer: HeroOffer | null): SetHeroOffer => ({
  type: ActionTypes.SET_HERO_OFFER,
  payload: {
    heroOffer: heroOffer === null ? null : { ...heroOffer }
  }
});

export type TurnPassingAction = SetGameState
  | SetPlayerTeam
  | SetPlayerName
  | SetPlayerOnlineUid
  | SetPlayerAiName
  | SetPlayerAiState
  | SetAiAdvantage
  | SetAiOutputVisible
  | ResetAiOutput
  | AddToAiOutput
  | SetWinCondition
  | SetCurrentPlayer
  | SetTurnNumber
  | ResetFogOfWar
  | SetPlayerSavedMapFocus
  | AddTimelineItem
  | SetHeroOffer;
