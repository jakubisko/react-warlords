import { createSelector } from '@reduxjs/toolkit';
import { cityIncome } from 'ai-interface/constants/city';
import { villageIncome } from 'ai-interface/constants/flaggable';
import { Army } from 'ai-interface/types/army';
import { Place } from 'ai-interface/types/place';
import { RootState } from '../root/configureStore';
import { isGroupShip } from '../path/MoveCostCalculatorImplUnchecked';
import { ProgramMode } from '../ui/types';
import { allArmyTypes, defaultArmyTypeIdToOrder } from './constants';
import { getHerosIncomeBonus, getHerosValueOfAbility } from './selectorsHero';
import { GameState } from './types';
import { EMPTY_ARRAY, sortByMultiple } from '../../functions';
import { Structure } from 'ai-interface/constants/structure';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { SpyReport } from 'ai-interface/types/spyReport';
import { Ability } from 'ai-interface/constants/ability';
import { groupViewRadius } from 'ai-interface/constants/viewRadius';

const getUiState = (state: RootState) => state.ui;
const getGameState = (state: RootState) => state.game;

/**
 * NOTE: This will return null for AI because we want to draw entire map during AI's turn.
 * However, we do not want to actually remove the unexplored for AI player, because he may be in a team with a human player.
 * Do not use this selector when you need to modify the unexplored, for example when moving a group.
 */
export const getUnexploredForReadingOnly = createSelector(
  getGameState,
  ({ unexploredByTeam, players, currentPlayerId }) =>
    players[currentPlayerId].aiName === undefined
      ? unexploredByTeam[players[currentPlayerId].team]
      : null
);

/**
 * Returns all armies at the place that are there together with given group. Armies are grouped by their groups.
 * Armies within one group are ordered from the strongest by their owner's ordering.
 */
export const getArmiesAtPlaceOfGroupOrderedByGroups = createSelector(
  getGameState, (rootState: RootState, groupId: string) => groupId,
  (gameState, groupId) => {
    const { tiles, groups, players, heroes } = gameState;
    const { row, col } = groups[groupId];
    const armies = [...tiles[row][col].armies!];
    const { armyTypeIdToOrder } = players[groups[armies[0].groupId].owner];

    return sortByMultiple(armies,
      army => army.groupId,
      army => armyTypeIdToOrder[army.armyTypeId],
      army => allArmyTypes[army.armyTypeId].hero ? heroes[army.id].experience : 0,
      army => army.blessed ? 1 : 0,
      army => army.moveLeft
    );
  }
);

const cityOffsets = [[0, 0], [0, 1], [1, 0], [1, 1]];

/**
 * Returns all visible armies that defend given place. Optionally you may exclude armies of given player.
 * For non-razed city, armies from visible tiles of the city will be included.
 * Armies are ordered from the strongest by their owner's ordering. They are not grouped by groups.
 */
export const getVisibleArmiesDefendingPlace = createSelector(
  getGameState,
  (rootState: RootState, place: Place) => place,
  (rootState: RootState, place: Place, excludePlayerId?: number) => excludePlayerId,
  (gameState, place, excludePlayerId) => {
    const { fogOfWar, tiles, cities, groups, players, heroes } = gameState;
    const { row, col } = place;
    const tile = tiles[row][col];
    const cityId = tile.cityId;

    let armies: Army[] = [];

    if (cityId !== undefined && !cities[cityId].razed) {
      // When viewing non-razed city, show armies from city's visible tiles.
      const { row: cRow, col: cCol } = cities[cityId];
      for (const [dx, dy] of cityOffsets) {
        if (!fogOfWar[cRow + dx][cCol + dy]) {
          armies.push(...tiles[cRow + dx][cCol + dy].armies ?? (EMPTY_ARRAY as Army[]));
        }
      }
    } else {
      if (fogOfWar[row][col] || tile.armies === undefined) {
        return EMPTY_ARRAY as Army[];
      }
      armies = tile.armies;
    }

    armies = armies.filter(army => groups[army.groupId].owner !== excludePlayerId);
    if (armies.length === 0) {
      return EMPTY_ARRAY as Army[];
    }

    const { armyTypeIdToOrder } = players[groups[armies[0].groupId].owner];
    return sortByMultiple(armies,
      army => armyTypeIdToOrder[army.armyTypeId],
      army => allArmyTypes[army.armyTypeId].hero ? heroes[army.id].experience : 0,
      army => army.blessed ? 1 : 0,
      army => army.moveLeft
    );
  }
);

/** Contains data about total size and the strongest army in a set of groups. Used to draw picture of the strongest army present. */
export interface GroupsData {
  readonly owner: number;
  readonly highestArmyTypeId: string;
  readonly groupIdWithHighestArmyType: string;
  readonly groupSize: number; // Number of armies on the tile. If the active group is on the tile, then only its size.
  readonly isShip: boolean;
}

/** Returns a function that can be later called to retrieve data about armies on any place. */
export const getGroupsDataCalculator = createSelector(
  getGameState,
  (state: RootState) => state.ui.activeGroupId,
  (gameState, activeGroupId) =>
    ({ row, col }: Place) => {
      const { tiles, groups, players } = gameState;

      let armies = tiles[row][col].armies;
      if (!armies) {
        return null;
      }

      // If some group is active, ignore other groups on the tile.
      const armiesOfActiveGroupOnly = armies.filter(army => army.groupId === activeGroupId);
      if (armiesOfActiveGroupOnly.length > 0) {
        armies = armiesOfActiveGroupOnly;
      }

      // Find army type with highest ordering.
      const owner = groups[armies[0].groupId].owner;
      const armyTypeIdToOrder = players[owner].armyTypeIdToOrder;
      let highestArmyOrder = -1;
      let highestArmyTypeId = '';
      let groupIdWithHighestArmyType = '';

      for (const army of armies) {
        const armyTypeId = army.armyTypeId;
        const order = armyTypeIdToOrder[armyTypeId];
        if (order > highestArmyOrder) {
          highestArmyOrder = order;
          highestArmyTypeId = armyTypeId;
          groupIdWithHighestArmyType = army.groupId;
        }
      }

      const result: GroupsData = {
        owner,
        highestArmyTypeId,
        groupIdWithHighestArmyType,
        groupSize: armies.length,
        isShip: isGroupShip(gameState, groupIdWithHighestArmyType)
      };
      return result;
    }
);

export const getNumberOfCities = createSelector(
  getGameState, getUiState, (rootState: RootState, playerId: number) => playerId,
  ({ cities }, { programMode }, playerId) => {
    if (programMode === ProgramMode.MapEditor) {
      return Object.keys(cities).length;
    }

    let result = 0;
    for (const cityId in cities) {
      const { razed, owner } = cities[cityId];
      result += !razed && owner === playerId ? 1 : 0;
    }
    return result;
  }
);

export const getNumberOfFlaggables = createSelector(
  getGameState,
  getUiState,
  (rootState: RootState, playerId: number) => playerId,
  (rootState: RootState, playerId: number, structure?: Structure) => structure,
  ({ flaggables, structures }, { programMode }, playerId, structure) => {
    if (programMode === ProgramMode.MapEditor) {
      return Object.keys(flaggables).length;
    }

    let result = 0;
    for (const flaggableId in flaggables) {
      const { owner, row, col } = flaggables[flaggableId];
      result += (owner === playerId && (structure === undefined || structure === structures[row][col])) ? 1 : 0;
    }
    return result;
  }
);

export const getIncome = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  (gameState, playerId) => {
    const { cities, structures, flaggables, heroes, groups, players, aiAdvantage } = gameState;
    let result = 0;

    for (const cityId in cities) {
      const { razed, owner, capitol } = cities[cityId];
      if (!razed && owner === playerId) {
        result += (capitol ? 2 : 1) * cityIncome;
      }
    }

    for (const flaggableId in flaggables) {
      const { owner, row, col } = flaggables[flaggableId];
      result += (structures[row][col] === Structure.Village && owner === playerId) ? villageIncome : 0;
    }

    for (const heroId in heroes) {
      const hero = heroes[heroId];
      const { owner } = groups[hero.groupId];
      result += (owner === playerId) ? getHerosIncomeBonus(hero) : 0;
    }

    if (players[playerId].aiName !== undefined) {
      result = Math.round(result * (100 + aiAdvantage) / 100);
    }

    return Math.max(0, result); // Artifacts may decrease the income. However, don't allow negative income.
  }
);

export const getUpkeepOfExistingArmies = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  ({ tiles, groups }, playerId) => {
    let result = 0;
    for (const groupId in groups) {
      const { owner, row, col } = groups[groupId];
      if (owner === playerId) {
        const { armies } = tiles[row][col];
        for (const army of armies!) {
          if (army.groupId === groupId) {
            result += allArmyTypes[army.armyTypeId].upkeep;
          }
        }
      }
    }
    return result;
  }
);

export const getUpkeepOfArmiesCreatedTomorrow = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  ({ cities }, playerId) => {
    let result = 0;
    for (const cityId in cities) {
      const { owner, currentlyTrainingArmyTypeId, turnsTrainingLeft, caravanToCityId, incomingCaravans } = cities[cityId];
      if (owner === playerId) {
        if (turnsTrainingLeft === 1 && caravanToCityId === undefined) {
          result += allArmyTypes[currentlyTrainingArmyTypeId!].upkeep;
        }
        if (incomingCaravans !== undefined) {
          for (const { armyTypeId, days } of incomingCaravans) {
            if (days === 1) {
              result += allArmyTypes[armyTypeId].upkeep;
            }
          }
        }
      }
    }
    return result;
  }
);

export const getNumberOfArmiesPresentInCity = createSelector(
  getGameState, (rootState: RootState, cityId: string) => cityId,
  (gameState, cityId) => {
    const { tiles, cities } = gameState;
    const { row, col } = cities[cityId];
    return (tiles[row][col].armies?.length ?? 0) + (tiles[row + 1][col + 1].armies?.length ?? 0)
      + (tiles[row + 1][col].armies?.length ?? 0) + (tiles[row][col + 1].armies?.length ?? 0);
  }
);

/** Returns the list of every army of one player ordered by that player's ordering from the most powerful to the least. */
export const getAllArmiesOfPlayer = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  (gameState, playerId) => {
    const { groups, tiles } = gameState;

    const listOfArmies: Army[] = [];
    for (const groupId in groups) {
      const { owner, row, col } = groups[groupId];

      if (owner === playerId) {
        for (const army of tiles[row][col].armies!) {
          if (army.groupId === groupId) {
            listOfArmies.push(army);
          }
        }
      }
    }

    return listOfArmies;
  }
);

export const getViewRadiusOfGroup = createSelector(
  getGameState, (rootState: RootState, groupId: string) => groupId,
  (gameState, groupId) => {
    const { groups, heroes, tiles } = gameState;
    const { row, col } = groups[groupId];
    const { armies } = tiles[row][col];

    let viewRadiusBonus = 0;
    for (const army of armies!) {
      if (army.groupId === groupId) {
        viewRadiusBonus = Math.max(viewRadiusBonus,
          allArmyTypes[army.armyTypeId].hero
            ? getHerosValueOfAbility(heroes[army.id], Ability.ViewRadiusBonus)
            : (allArmyTypes[army.armyTypeId].abilities[Ability.ViewRadiusBonus] ?? 0)
        );
      }
    }

    return groupViewRadius + viewRadiusBonus;
  }
);

export const getPlaceOfActiveGroup = createSelector(
  getGameState, getUiState,
  (gameState, uiState) => {
    const { activeGroupId } = uiState;
    if (activeGroupId === null) {
      return null;
    }

    return gameState.groups[activeGroupId] as Place;
  }
);

/** Deterministically generates new ids from sequence. You then need to update lastIds in game state according to the value returned. */
export function generateIds(gameState: GameState) {
  const { currentPlayerId, lastIds } = gameState;
  const lastId = lastIds[currentPlayerId];

  return {
    armyId: `A-${currentPlayerId}-${lastId + 1}`,
    groupId: `G-${currentPlayerId}-${lastId + 1}`,
    lastIds: lastIds.map((oldValue, playerId) => playerId === currentPlayerId ? lastId + 1 : oldValue)
  };
}

/**
 * Actions "createArmy" and "transferArmyBetweenGroups" generate deterministic new ids from sequence.
 * Call this selector after one of such actions to retrieve that generated id.
 * NOTE: The generated id is not stored anywhere. It is recalculated from immediate state. Calling this later returns an arbitrary result.
 */
export const getLastGeneratedIds = createSelector(
  getGameState,
  (gameState) => {
    const { currentPlayerId, lastIds } = gameState;
    const lastId = lastIds[currentPlayerId];

    return {
      armyId: `A-${currentPlayerId}-${lastId}`,
      groupId: `G-${currentPlayerId}-${lastId}`
    };
  }
);

const getBestArmyTypeIdOfPlayer = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  ({ groups, tiles }, playerId) => {
    let bestArmyTypeId: string | undefined = undefined;

    for (const groupId0 in groups) {
      const { owner, row, col } = groups[groupId0];
      if (owner === playerId) {
        for (const { armyTypeId, groupId } of tiles[row][col].armies!) {
          if (groupId === groupId0 && !allArmyTypes[armyTypeId].hero && (bestArmyTypeId === undefined
            || defaultArmyTypeIdToOrder[bestArmyTypeId] < defaultArmyTypeIdToOrder[armyTypeId])) {
            bestArmyTypeId = armyTypeId;
          }
        }
      }
    }

    return bestArmyTypeId;
  }
);

const getMapExplored = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  ({ size, players, unexploredByTeam }, playerId) => {
    const { team } = players[playerId];
    const unexploredGrid = unexploredByTeam[team];
    if (unexploredGrid === null) {
      return 100;
    }

    let unexploredCount = 0;
    for (let row = 2; row < size; row += 5) {
      for (let col = 2; col < size; col += 5) {
        if (unexploredGrid[row][col]) {
          unexploredCount++;
        }
      }
    }

    return Math.round(100 - 2500 * unexploredCount / size / size);
  }
);

export const getSpyReport = createSelector(
  (rootState: RootState) => rootState, (rootState: RootState, playerId: number) => playerId,
  (rootState, playerId) => {
    const { cities, groups, heroes, players } = rootState.game;

    let count = players[playerId].aiName === undefined && getNumberOfFlaggables(rootState, playerId, Structure.SpyDen) === 0 ? 0 : 20;
    if (count === 0) {
      for (const cityId in cities) {
        const { razed, owner } = cities[cityId];
        count += !razed && players[owner].team === players[playerId].team ? 1 : 0;
      }
    }

    const result: SpyReport = {
      mapExplored: [],
      heroes: [],
      bestHeroLevel: [],
      artifacts: [],
      bestArmyTypeId: [],
      cities: [],
      gold: [],
      income: [],
      armiesUpkeep: []
    };

    for (let playerId2 = 0; playerId2 < maxPlayers; playerId2++) {
      const { alive, gold } = players[playerId2];
      const myHeroes = Object.values(heroes).filter(({ groupId }) => groups[groupId].owner === playerId2);

      result.mapExplored.push(alive && count >= 2 ? getMapExplored(rootState, playerId2) : undefined);
      result.heroes.push(alive && count >= 4 ? myHeroes.length : undefined);
      result.bestHeroLevel.push(alive && count >= 6 && myHeroes.length > 0 ? Math.max(...myHeroes.map(hero => hero.level)) : undefined);
      result.artifacts.push(alive && count >= 8 ? myHeroes.map(hero => hero.artifactIds?.length ?? 0).reduce((a, b) => a + b, 0) : undefined);
      result.bestArmyTypeId.push(alive && count >= 10 ? getBestArmyTypeIdOfPlayer(rootState, playerId2) : undefined);
      result.cities.push(alive && count >= 12 ? getNumberOfCities(rootState, playerId2) : undefined);
      result.gold.push(alive && count >= 14 ? gold : undefined);
      result.income.push(alive && count >= 16 ? getIncome(rootState, playerId2) : undefined);
      result.armiesUpkeep.push(alive && count >= 18 ? (getUpkeepOfExistingArmies(rootState, playerId2) + getUpkeepOfArmiesCreatedTomorrow(rootState, playerId2)) : undefined);
    }

    return result;
  }
);

/** Returns the current owner of the city, flaggable or group standing there. Otherwise, returns null. Ignores fog of war. */
export const getOwnerOfPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  ({ tiles, cities, flaggables, groups }, { row, col }) => {
    const { cityId, flaggableId, armies } = tiles[row][col];

    if (armies) {
      return groups[armies[0].groupId].owner;
    }

    // Check city ownership only after you checked there are no armies. Razed cities don't change ownership to preserve the graphics!
    if (cityId !== undefined) {
      const { owner, razed } = cities[cityId];
      return razed ? neutralPlayerId : owner;
    }

    if (flaggableId !== undefined) {
      return flaggables[flaggableId].owner;
    }

    return null;
  }
);
