import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Modal } from 'react-bootstrap';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';
import { passTurnToNextPlayer } from '../../model/game/actionsTurnPassingThunks';

export default function AiDisqualifiedScreen() {
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const currentPlayerName = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].name);
  const dispatch: AppDispatch = useDispatch();

  return (
    <Modal className="screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <ModalTitleWithFlags owner={currentPlayerId} caption={currentPlayerName} />
      </Modal.Header>

      <Modal.Body className="p-3">
        Computer player {currentPlayerName} has thrown an error and was disqualified!
      </Modal.Body>

      <Modal.Footer>
        <ButtonWithShortcut
          shortcutCode="Enter"
          allowShortcutWhileModalIsOpen={true}
          onClick={() => dispatch(passTurnToNextPlayer())}
        >
          OK
        </ButtonWithShortcut>
      </Modal.Footer>
    </Modal>
  );
}
