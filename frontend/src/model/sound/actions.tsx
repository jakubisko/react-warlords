export enum ActionTypes {
  SET_SOUND_SOURCE = '[Sound] SET_SOUND_SOURCE'
}

export interface SetSoundSource {
  type: ActionTypes.SET_SOUND_SOURCE;
  payload: {
    soundRow: number;
    soundCol: number;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

export const setSoundSource = (soundRow: number, soundCol: number): SetSoundSource => ({
  type: ActionTypes.SET_SOUND_SOURCE,
  payload: {
    soundRow,
    soundCol
  }
});

export type SoundAction = SetSoundSource;
