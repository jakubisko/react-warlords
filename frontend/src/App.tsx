import { Provider } from 'react-redux';
import { storeWithUi } from './model/root/configureStore';
import ScreenRouter from './gameui/ScreenRouter';
import ErrorMessageModal from './gameui/modals/ErrorMessageModal';
import { ScreenOrientationReminder } from './gameui/ScreenOrientationReminder';
import ErrorBoundary from './gameui/ErrorBoundary';
import Sounds from './gameui/sounds/Sounds';

export const App = () => (
  <Provider store={storeWithUi}>
    <div id="menu-blocker" onContextMenu={e => e.preventDefault()}>
      <ErrorBoundary>
        <ScreenRouter />
        <Sounds />
      </ErrorBoundary>
      <ErrorMessageModal />
      <ScreenOrientationReminder />
    </div>
  </Provider>
);
