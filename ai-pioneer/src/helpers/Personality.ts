import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';

export interface Personality {

  /**
   * Value from 0 to 1.
   * We want to spend less gold each turn than is our income.
   * This is because 1. we want to save some money, 2. we want to be able to avoid desertion if an enemy decreases our income.
   * This determines the amount for spending as part fraction from income.
   */
  partOfIncomeSpentOnArmies: number;

  /**
   * Value from 0 to 1.
   * We don't want to just stand in cities and guard.
   * This determines what part of income will be spent on armies that guard our threatened cities.
   * At the start of the game, and when we're not threatened, the actual number of armies guarding may be lower.
   * The power guarding may get slightly higher due to the capturing of new cities mid-turn and the fact that guard power can't exactly hit some numbers.
   */
  partOfIncomeSpentOnArmiesGuarding: number;

  /** Default value is the build cost. This will cause AI to prefer building some army types before others. */
  utilityOfTrainingArmyTypeId: { [armyTypeId: string]: number };

  /** Value from 0 to 1. If the AI decides that the city is undefendable and there are only enemies around, this is the probability that he'll raze it. */
  razeProbability: number;

  /** Value from 0 to 1. Higher value increases utility of joining groups together. */
  likesBigGroups: number;

  /** Aggression is a value from 0 to 1. High aggression leads to reckless attacks. See AggressionManager. */
  baseAggression: number;

  /** We need to know when we have too much armies. Then make some reckless attacks. */
  upkeepOfArmiesDismissedLastTurn: number;
}

/**
 * If the "random" is set to true, the AI will have a "personality" - some of the magical constants used through the game will be selected at random.
 * For testing purposes, don't use random.
 */
export function generatePersonality(random: boolean): Personality {
  return {
    partOfIncomeSpentOnArmies: random ? (0.6 + 0.3 * Math.random()) : 0.8,
    partOfIncomeSpentOnArmiesGuarding: random ? (0.1 + 0.35 * Math.random()) : 0.35,
    utilityOfTrainingArmyTypeId: Object.keys(standardArmyTypes)
      .filter(armyTypeId => !standardArmyTypes[armyTypeId].hero)
      .reduce((res, armyTypeId) => ({ ...res, [armyTypeId]: standardArmyTypes[armyTypeId].buildingCost * (random ? (0.75 + 0.5 * Math.random()) : 1) }), {}),
    razeProbability: random ? Math.random() : 0.5,
    likesBigGroups: random ? Math.random() : 0.5,
    baseAggression: random ? (0.2 * Math.random()) : 0.1,
    upkeepOfArmiesDismissedLastTurn: 0

    // TODO - whether he builds high-tech (and spends less) or low-tech (and goes wide)
  };
}
