import { TileSprite } from '../../gameui/map/tactical-map/spriteMetaDataTypes';
import { Place } from 'ai-interface/types/place';
import { IncomingCaravan } from 'ai-interface/types/city';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure } from 'ai-interface/constants/structure';
import { Army } from 'ai-interface/types/army';
import { WarReportItem } from 'ai-interface/types/warReport';
import { HeroOffer } from 'ai-interface/types/heroOffer';
import { WinCondition } from 'ai-interface/constants/winCondition';

/** Entire game state. */
export interface GameState extends MapData {
  readonly winCondition: WinCondition; // When does the game end for a player?
  readonly backgroundSprites: TileSprite[][][]; // [row][col][index]; Images for water, hills, forest etc. There may be multiple sprites on one tile.
  readonly fogOfWar: boolean[][]; // [row][col]; Whether the tile is covered by the fog of war for current player. Regenerates at the start of each turn.
  readonly unexploredByTeam: (boolean[][] | null)[]; // [team][row][col]; Whether the tile was seen this game by given team. Null reveals entire map. Doesn't regenerate.
  readonly players: Player[]; // Player's id is equal to index in this array. Do not remove dead players from this array, as it would change the ids.
  readonly currentPlayerId: number; // Index of player who's turn it is. Use neutral player's id for map editor.
  readonly turnNumber: number; // Starting from 1.
  readonly timeline: TimelineItem[]; // Item 0 is state at the start of the game.
  readonly heroOffer: HeroOffer | null; // If a hero or an army offers to join current player, this will contain the data about it.
  readonly aiOutputVisible: boolean; // Whether to collect and draw the debug texts produced by the computer players.
  readonly aiStates: string[]; // Holds serialized state for each computer player. Empty string for human or dead players.
  readonly aiAdvantage: number; // Percentual advantage in income and experiences for computer players. Default is 0. 100 doubles the income. -100 makes zero income.
  readonly aiOutput: AiOutput; // Debug texts drawn on the screen by the computer players if the debugging is allowed.
}

/** Subset of GameState that holds only map data. Background sprites can be calculated anew, fog of war is not part of map etc. */
export interface MapData {
  readonly size: number;
  readonly terrain: Terrain[][]; // [row][col]; Whether this is water, open, forest, desert...
  readonly structures: Structure[][]; // [row][col]; Whether there is a road, city, ruin...
  readonly tiles: Tile[][]; // [row][col]
  readonly cities: { [cityId: string]: City }; // Map from unique City id to each City on map.
  readonly flaggables: { [flaggableId: string]: Flaggable }; // Map from unique Flaggable id to each Flaggable on map.
  readonly ruins: { [ruinId: string]: Ruin }; // Map from unique Ruin id to each Ruin on map.
  readonly groups: { [groupId: string]: Group }; // Map from unique Group id to each Group on map.
  readonly heroes: { [armyId: string]: Hero }; // Map from unique Army id to each Hero on map. Sometimes hero's Army id is called heroId.
  readonly lastIds: number[]; // Sequences used for generating ids of armies and groups. Each player has his own so he can't guess number of armies enemy has.
}

// Land ///////////////////////////////////////////////////////////////////////////////////////////

/** Data of all modifiable objects on one game tile. Terrain, hills etc. not included. */
export interface Tile {
  readonly armies?: Army[]; // List of armies standing on the tile. If there are no armies, the field is left out.
  readonly artifactIds?: string[]; // List of artifacts laying on the ground. If there are no artifacts, the field is left out.
  readonly cityId?: string; // Id of City standing on this tile. Undefined if there is no city. Note that cities are bigger than one tile.
  readonly flaggableId?: string; // Id of Flaggable standing on this tile. Undefined if there is nothing flaggable.
  readonly ruinId?: string; // Id of Ruin standing on this tile. Undefined if there is no ruin.
  readonly signpost?: string; // Text of the signpost. Undefined if there is no signpost.
}

// Armies and Heroes //////////////////////////////////////////////////////////////////////////////

/** Group is one or more Armies moving together; for example "2 Bats with 1 Giant" or "1 Hero". */
export interface Group {
  readonly id: string; // Id unique for each Group
  readonly owner: number; // Index of player owning this army. Equals to "neutralPlayerId" for neutral army.
  readonly row: number;
  readonly col: number;
  readonly waiting?: true; // Whether to ignore this group for "get next group" and "end turn" purposes.
  readonly destRow?: number; // Optional destination of move
  readonly destCol?: number; // Optional destination of move
}

/** Hero object is an extension of the Army object. Since artifacts modify hero's properties, use selectors instead of reading army type. */
export interface Hero {
  readonly armyId: string;
  readonly groupId: string;
  readonly portraitId: number; // Determines hero's portrait image and name.
  readonly experience: number;
  readonly level: number;
  readonly artifactIds?: string[];
}

// Structures /////////////////////////////////////////////////////////////////////////////////////

/** Flaggable is any structure on map that can be flagged other than a City; for example a Village. */
export interface Flaggable {
  readonly id: string; // Id unique for each flaggable structure.
  readonly owner: number; // Index of player owning this structure. Equals to "neutralPlayerId" for neutral structure.
  readonly lastSeenOwner: number[]; // lastSeenOwner[4] = 7 means that TEAM 4 has last seen this structure owned by PLAYER 7.
  readonly row: number;
  readonly col: number;
}

export interface City extends Flaggable {
  readonly name: string;
  readonly capitol?: true; // Capitol has double city's income and double fortification bonus.
  readonly lastSeenCapitol: boolean[]; // lastSeenCapitol[4] = true means that TEAM 4 has last seen that this city is Capitol.
  readonly razed?: true;
  readonly lastSeenRazed: boolean[]; // lastSeenRazed[4] = true means that TEAM 4 has last seen this city razed.
  readonly currentlyTrainableArmyTypeIds?: string[]; // List of ArmyTypes that can be currently trained without additional building.
  readonly currentlyTrainingArmyTypeId?: string; // Id of ArmyType that is currently being trained. Undefined if nothing is being trained.
  readonly turnsTrainingLeft?: number; // How many turns have to pass until next army is trained. Undefined if nothing is being trained.
  readonly caravanToCityId?: string; // Owner may redirect army training to another city. If undefined, armies trained will be created in this city.
  readonly incomingCaravans?: IncomingCaravan[]; // List of armies that will soon arrive in this city with caravan. If the owner of the city changes, these armies will be lost.
}

export interface Ruin {
  readonly id: string; // Id unique for each Ruin.
  readonly level: number;
  readonly explored?: true;
  readonly lastSeenExplored: boolean[]; // lastSeenExplored[4] = false means that TEAM 4 has last seen this ruin unexplored.
  readonly row: number;
  readonly col: number;
}

// Players ////////////////////////////////////////////////////////////////////////////////////////

export interface Player {
  readonly team: number; // Equals to "neutralPlayerId" for neutral city.
  readonly name: string;
  readonly onlineUid?: string; // Determines the human account when playing online. Undefined when playing offline.
  readonly aiName?: string; // Determines the AI function used for computer players. This value is user-readable. Undefined for human players.
  readonly alive: boolean;
  readonly gold: number;
  readonly sageVisitedThisWeek?: true;
  readonly armyTypeIdToOrder: { [armyTypeId: string]: number } // Defines ordering of the player. armyTypeIdToOrder['wolf'] = 0 means that ArmyType.id 'wolf' fights first.
  readonly mapDisplaySettings: MapDisplaySettings; // Human players may customize what is displayed on the Strategic map.
  readonly savedMapFocus: Place; // Holds the place focused at the end of previous turn, so that the turn starts focused on same place.
  readonly warReports: WarReportItem[]; // This will be always empty for the neutral player.
  readonly totalKills: number; // Value of all enemy armies killed by this player through the entire game.
  readonly totalLosses: number; // Value of all armies killed by enemy for this player through the entire game.
}

export interface MapDisplaySettings {
  readonly showTerrain?: true;
  readonly showRoads?: true;
  readonly showRuins?: true;
  readonly showCities?: true;
  readonly showFlaggables?: true;
  readonly showArmies?: true;
  readonly showArtifacts?: true;
  readonly showFogOfWar?: true;
  readonly showWarReport?: true;
  readonly showGrid?: true; // Whether grid is shown on TacticalMap
}

export interface TimelineItem {
  readonly cities: number[]; // Number of cities controlled by each player at the end of the neutral player's turn.
  readonly armyValue: number[]; // Total value of armies controlled by each player at the end of the neutral player's turn.
}

export type AiOutputForGroup = {
  readonly sticky: string[]; // The text is drawn on the screen from top to bottom in lines and doesn't move when you move map focus.
  readonly atPlace: { // The text is drawn at some place of the land and moves with it when you move map focus.
    readonly row: number;
    readonly col: number;
    readonly text: string;
  }[];
};

/**
 * Debug texts drawn on the screen by the computer players. Key is either:
 * a) the groupId - drawn when that group is active (this doesn't draw the text above that group)
 * b) cityId - drawn when that city is active (this doesn't draw the text above that city)
 * c) 'NONE' - drawn when no group is active
 * d) 'ALWAYS' - drawn always
 */
export type AiOutput = { [key: string]: AiOutputForGroup };
