import { useSelector } from 'react-redux';
import { RootState } from '../../../model/root/configureStore';
import CityEditor from './CityEditor';
import RuinEditor from './RuinEditor';
import ArmyEditor from './ArmyEditor';
import ArtifactEditor from './ArtifactEditor';
import { Place } from 'ai-interface/types/place';
import { Structure } from 'ai-interface/constants/structure';
import SignpostEditor from './SignpostEditor';

interface PlaceInfoEditorProps {
  place: Place;
}

export default function PlaceInfoEditor({ place }: PlaceInfoEditorProps) {
  const structure = useSelector((state: RootState) => state.game.structures[place.row][place.col]);
  const { armies, ruinId, cityId, artifactIds } = useSelector((state: RootState) => state.game.tiles[place.row][place.col]);

  return (
    <>
      {ruinId !== undefined && <RuinEditor ruinId={ruinId} />}
      {cityId !== undefined && <CityEditor cityId={cityId} />}
      {structure === Structure.Signpost && <SignpostEditor place={place} />}
      {armies && <ArmyEditor place={place} />}
      {artifactIds && <ArtifactEditor place={place} />}
    </>
  );
}
