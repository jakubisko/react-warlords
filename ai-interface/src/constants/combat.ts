/** Number of sides on dice used in combat. Must be higher than maxStrengthOverall. */
export const combatDiceSides = 20;

/** An army will never have higher strength than this number. This is applied last, after all bonuses. */
export const maxStrengthOverall = 15;

/** Non-flying armies fighting on water (on ship) have capped maximum value for strength. This is applied last, after all bonuses. */
export const maxStrengthOnShip = 5;

/**
 * Returns the percentual chance from 0 to 100 that the army A will hit the army B.
 * In the rest of the cases, B will hit A.
 * 
 * Rule of a thumb:
 * 1. Stronger army has a slightly higher chance of hitting than the ratio of their powers.
 * 2. As the difference of strengths rises, so rises the advantage the stronger army has.
 */
export function getChanceForAtoHitB(strengthA: number, strengthB: number): number {
  strengthA = Math.max(1, Math.min(strengthA, maxStrengthOverall));
  strengthB = Math.max(1, Math.min(strengthB, maxStrengthOverall));

  const chanceA = strengthA * (combatDiceSides - strengthB);
  const chanceB = strengthB * (combatDiceSides - strengthA);
  return 100 * chanceA / (chanceA + chanceB);
}

/**
 * Returns the percentual chance from 0 to 100 that the army A will kill the army B in one-on-one combat.
 * In the rest of the cases, B will kill A.
 */
export function getChanceForAToKillB(strengthA: number, hitPointsA: number, strengthB: number, hitPointsB: number): number {
  const aWinsOnce = getChanceForAtoHitB(strengthA, strengthB) / 100;

  let sum = 0;
  for (let j = 0; j < hitPointsB; j++) {
    sum += aWinsOnce ** j * combination(hitPointsA + j - 1, j);
  }

  return 100 - 100 * (1 - aWinsOnce) ** hitPointsA * sum;
}

function combination(n: number, k: number): number {
  k = Math.max(k, n - k);
  let result = 1;
  for (let i = n; i > k; i--) {
    result *= i;
  }
  for (let i = n - k; i > 1; i--) {
    result /= i;
  }
  return result;
}
