import './ArtifactPicker.scss';
import { useState, type JSX } from 'react';
import { allArtifacts } from '../../../model/game/constants';
import { Button, Modal } from 'react-bootstrap';
import ArtifactImage from '../../common/ArtifactImage';

interface ArtifactPickerProps {
  artifactIds: string[]; // Items may repeat
  numberOfAlwaysVisibleSlots?: number; // If set, this will draw at least this many slots, even if they would be empty.

  actionButton: (artifactId: string, close: () => void) => JSX.Element;
}

export default function ArtifactPicker({ artifactIds, actionButton, numberOfAlwaysVisibleSlots }: ArtifactPickerProps) {
  const [detailArtifactId, setDetailArtifactId] = useState<string | undefined>(undefined);

  const artifact = allArtifacts[detailArtifactId ?? 0];

  const items = artifactIds
    .map(artifactId => allArtifacts[artifactId])
    .map((artifact, index) => (
      <ArtifactImage
        key={index} // artifactId is not unique - it is possible to have same artifact twice
        artifactId={artifact.id}
        selectable={true}
        onClick={() => setDetailArtifactId(artifact.id)}
      />
    ));

  for (let i = items.length; i < (numberOfAlwaysVisibleSlots ?? 0); i++) {
    items.push(
      <ArtifactImage
        key={i}
        artifactId={null}
      />
    );
  }

  return (
    <div className="artifact-picker">
      {items}

      {detailArtifactId !== undefined && (
        <Modal
          className="artifact-detail overflow-hidden"
          show centered
          onHide={() => setDetailArtifactId(undefined)}
        >
          <Modal.Header>
            <Modal.Title>
              {artifact.name}
            </Modal.Title>
          </Modal.Header>

          <Modal.Body className="py-0 text-center">
            <ArtifactImage artifactId={detailArtifactId} includeText={true} alwaysLarge={true} />
          </Modal.Body>

          <Modal.Footer className="justify-content-between">
            {actionButton(detailArtifactId, () => setDetailArtifactId(undefined))}

            <Button onClick={() => setDetailArtifactId(undefined)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
}
