import { useEffect, useMemo, useState } from 'react';
import { OnlineUser } from './types';
import { firestore } from './firebaseConfig';
import { EMPTY_ARRAY } from '../../functions';
import { collection, limit, onSnapshot, orderBy, query } from 'firebase/firestore';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../root/configureStore';
import { setErrorMessage } from '../ui/actions';

/**
 * Returns the list of users from Firebase, updated in the real time.
 * Returns an empty list without throwing an error if not logged in.
 */
export function useOnlineUsers() {
  const dispatch: AppDispatch = useDispatch();

  const isLoggedOut = useSelector((state: RootState) => state.online.onlineGameId === null || state.online.loggedInUser === null);

  const [loadingUsers, setLoadingUsers] = useState(!isLoggedOut);

  const [users, setUsers] = useState(EMPTY_ARRAY as OnlineUser[]);
  const usersQueryRef = useMemo(() => query(
    collection(firestore, 'users'),
    orderBy('displayName'),
    limit(1000)
  ), [firestore]);

  useEffect(() => {
    // Don't subscribe to updates when not logged in
    if (isLoggedOut) {
      setUsers(EMPTY_ARRAY as OnlineUser[]);
      setLoadingUsers(false);
      return;
    }

    // Subscribe to updates on the users collection in real time
    return onSnapshot(
      usersQueryRef,
      (snapshot) => {
        setUsers(snapshot.docs.map(doc => doc.data() as OnlineUser));
        setLoadingUsers(false);
      },
      (err) => {
        setUsers(EMPTY_ARRAY as OnlineUser[]);
        setLoadingUsers(false);
        dispatch(setErrorMessage('Failed to retrieve the list of online users', err));
      }
    );
  }, [isLoggedOut]);

  return { users, loadingUsers };
}
