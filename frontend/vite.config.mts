import { defineConfig } from 'vite';
import path from 'path';
import react from '@vitejs/plugin-react';
import viteTsconfigPaths from 'vite-tsconfig-paths';
import eslintPlugin from 'vite-plugin-eslint';

export default defineConfig({
  base: '',
  plugins: [
    react(),
    viteTsconfigPaths(),
    eslintPlugin({
      cache: false,
      include: ['./src/**/*.ts', './src/**/*.tsx'],
      exclude: [],
    })
  ],
  server: {
    open: true, // open browser upon server start
    port: 3000,
  },
  resolve: {
    alias: {
      'ai-interface': path.resolve(__dirname, '../ai-interface/lib'),
      'ai-pioneer': path.resolve(__dirname, '../ai-pioneer/lib'),
    },
  },
});
