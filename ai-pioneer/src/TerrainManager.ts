import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';

/**
 * For each terrain returns approximately how relevant it is in areas near some place. Near areas have higher weight.
 * Terrains may be missing. Sum equals to 1.
 */
export type TerrainWeights = { [terrain: string]: number };

export class TerrainManager {

  private readonly terrainWeightsForCity: { [cityId: string]: TerrainWeights } = {};

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi
  ) {
    for (const cityId of land.getAllCityIds()) {
      const terrainWeights = this.calculateTerrainWeightsForCity(cityId);
      this.terrainWeightsForCity[cityId] = terrainWeights;
      const text = this.terrainWeightsToString(terrainWeights);
      this.draw.drawTextWhenCityIsSelected(cityId, text);
    }
  }

  public getTerrainWeightsForCity(cityId: string) {
    return this.terrainWeightsForCity[cityId];
  }

  private terrainWeightsToString(terrainWeight: TerrainWeights): string {
    return Object.keys(terrainWeight)
      .sort((t1, t2) => terrainWeight[t2] - terrainWeight[t1])
      .map(terrain => terrain + ':' + Math.round(100 * terrainWeight[terrain]))
      .join('; ');
  }

  /** This method is cached. */
  private calculateTerrainWeightsForCity(cityId: string): TerrainWeights {
    const { row, col } = this.game.getCity(cityId);
    const terrain = this.land.getTerrain();
    const size = terrain.length;

    const result: TerrainWeights = {};

    // For bigger map sizes, larger distances are relevant
    for (let dist = 1; dist < size / 10; dist++) {
      const samples = Math.floor(8 * dist ** 0.7);
      const sizeCoef = 4 * dist;
      for (let a = 0; a < samples; a++) {
        const b = 2 * Math.PI * a / samples;
        const row2 = Math.round(row + sizeCoef * Math.sin(b));
        const col2 = Math.round(col + sizeCoef * Math.cos(b));

        if (row2 >= 0 && col2 >= 0 && row2 < size && col2 < size) {
          const t = terrain[row2][col2];
          if (t in result) {
            result[t] += 1 / samples;
          } else {
            result[t] = 1 / samples;
          }
        }
      }
    }

    // Normalize so that the sum is 1.
    const sum = Object.values(result).reduce((sum, val) => sum + val, 0);
    for (const terrain in result) {
      result[terrain] /= sum;
    }

    return result;
  }

}
