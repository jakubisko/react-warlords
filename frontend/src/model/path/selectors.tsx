import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { spriteSheetPath, TileSprite } from '../../gameui/map/tactical-map/spriteMetaData';
import { calculateMoveTypeOfGroup } from './MoveCostCalculatorImplUnchecked';
import { Place } from 'ai-interface/types/place';

const getGameState = (state: RootState) => state.game;

/** Returns the lowest move left of given group. */
export const getMoveLeftOfGroup = createSelector(
  getGameState, (rootState: RootState, groupId: string) => groupId,
  (gameState, groupId) => {
    const { groups, tiles } = gameState;
    const { row, col } = groups[groupId];
    const { armies } = tiles[row][col];

    return Math.min(...armies!.filter(army => army.groupId === groupId).map(army => army.moveLeft));
  }
);

/** Returns whether given group flies, is native to terrain etc. */
export const getMoveTypeOfGroup = createSelector(
  getGameState, (rootState: RootState, groupId: string) => groupId,
  (gameState, groupId) => calculateMoveTypeOfGroup(gameState, groupId)
);

/**
 * Constructs a map which goes from a place on path to the sprite that should be drawn there.
 * The key to this map is of format 'row,col', for example '7,15'.
 */
export const getPathSprites = createSelector(
  (state: RootState) => state.path.path,
  path => {
    const result: { [rowAndCol: string]: TileSprite } = {};

    const setSprite = (pathIndex: number, spriteId: number) =>
      result[path[pathIndex].row + ',' + path[pathIndex].col] = spriteSheetPath[spriteId];

    if (path.length > 0) {
      for (let i = 1; i < path.length - 1; i++) {
        setSprite(i, 3 + 2 * Math.min(path[i].day, 4));
      }

      // "X" sprite for the destination place.
      if (path.length >= 2 && path[path.length - 1].day === 0 && 2 * path[path.length - 1].moveLeft > path[1].moveLeft) {
        // The destination can be reached this turn and it's still possible to return back to starting place.
        setSprite(path.length - 1, 20);
      } else {
        setSprite(path.length - 1, 2 + 2 * Math.min(path[path.length - 1].day, 4));
      }

      // "Box" sprite for the current place of the group.
      setSprite(0, path[0].moveLeft > 0 ? 0 : 1);
    }

    return result;
  }
);

/**
 * Returns true when "move all groups" operation should be disabled. For this operation to be enabled, there has to exists at least
 * one group of current player that has move left, has a destination set, and is not waiting.
 */
export const isMoveAllGroupsDisabled = createSelector(
  getGameState,
  (gameState) => {
    const { tiles, groups, currentPlayerId } = gameState;

    return !Object.keys(groups).some(groupId => {
      const { row, col, destRow, destCol, waiting, owner } = groups[groupId];
      if (owner !== currentPlayerId || waiting || destRow === undefined || destCol === undefined || (destRow === row && destCol === col)) {
        return false;
      }

      const { armies } = tiles[row][col];
      return !armies!.some(army => army.groupId === groupId && army.moveLeft <= 0);
    });
  }
);

/**
 * Returns list of all groups that are affected by "move all groups" operation. Group of current player is included if it has
 * move left, has a destination set, and is not waiting.
 */
export const getGroupIdsAffectedByMoveAll = createSelector(
  getGameState,
  (gameState) => {
    const { tiles, groups, currentPlayerId } = gameState;

    return Object.keys(groups).filter(groupId => {
      const { row, col, destRow, destCol, waiting, owner } = groups[groupId];
      if (owner !== currentPlayerId || waiting || destRow === undefined || destCol === undefined || (destRow === row && destCol === col)) {
        return false;
      }

      const { armies } = tiles[row][col];
      return !armies!.some(army => army.groupId === groupId && army.moveLeft <= 0);
    });
  }
);

/**
 * Returns the id of a group that still has some move left, and its id is after given group id in the map of all groups.
 * Groups waiting will be skipped.
 * This performs cyclical search - if there is no id higher than given id, this wraps to beginning and returns the lowest id.
 * Use empty string as given group id to obtain first group with some move left. 
 * Returns null if current player has no army with move left.
 */
export const getNextGroupIdWithMove = createSelector(
  getGameState, (rootState: RootState, afterGroupId: string) => afterGroupId,
  (gameState, afterGroupId) => {
    const { tiles, groups, currentPlayerId } = gameState;

    // Get list of all group ids of current player that have move left and weren't waiting.
    const possibleGroupIds = Object.keys(groups).filter(groupId => {
      const { row, col, owner, waiting } = groups[groupId];
      if (owner !== currentPlayerId || waiting) {
        return false;
      }

      const { armies } = tiles[row][col];
      return !armies!.some(army => army.groupId === groupId && army.moveLeft <= 0);
    });

    // Find the lowest group id higher than given group id.
    let result: string | null = null;
    for (const groupId of possibleGroupIds) {
      if (groupId > afterGroupId && (result === null || groupId < result)) {
        result = groupId;
      }
    }

    // Otherwise, wrap to beginning and return the lowest group id. Wrapping has no point if we already started from start.
    if (result === null && afterGroupId !== '') {
      for (const groupId of possibleGroupIds) {
        if (groupId <= afterGroupId && (result === null || groupId < result)) {
          result = groupId;
        }
      }
    }

    return result;
  }
);


/** Returns string if something interesting (an army, an artifact) was on one of the places revealed during last move. */
export const wasSomethingSpotted = createSelector(
  (state: RootState) => state.game.tiles,
  (state: RootState) => state.path.lastRevealedPlaces,
  (tiles, lastRevealedPlaces) => {
    if (lastRevealedPlaces.some(({ row, col }) => tiles[row][col].armies !== undefined)) {
      return 'Enemy army spotted';
    } else if (lastRevealedPlaces.some(({ row, col }) => tiles[row][col].artifactIds !== undefined)) {
      return 'Bag of artifacts spotted';
    }
    return null;
  }
);

export const numberOfArmiesOfCurrentPlayerAtPlace = createSelector(
  getGameState, (rootState: RootState, place: Place) => place,
  (gameState, place) => {
    const { row, col } = place;
    const { tiles, groups, currentPlayerId } = gameState;
    const { armies } = tiles[row][col];

    if (!armies) {
      return 0;
    }

    const { owner } = groups[armies[0].groupId];
    return owner === currentPlayerId ? armies.length : 0;
  }
);