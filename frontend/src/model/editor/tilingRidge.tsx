import {
  spriteSheetWaterfall, spriteSheetRidgeDesert, spriteSheetRidgeIce, spriteSheetRidgeOpen
} from '../../gameui/map/tactical-map/spriteMetaData';
import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { createGrid } from '../../functions';
import { Sheet, TileSprite } from '../../gameui/map/tactical-map/spriteMetaDataTypes';

const terrainToRidgeSpriteSheet: { [terrain: string]: Sheet<TileSprite> } = {
  [Terrain.Water]: spriteSheetWaterfall,
  [Terrain.Desert]: spriteSheetRidgeDesert,
  [Terrain.Ice]: spriteSheetRidgeIce,
  [Terrain.Open]: spriteSheetRidgeOpen
};

/** Sprites for ridges. */
export function placeSpritesForRidges(backgroundSprites: TileSprite[][][], terrain: Terrain[][], structures: Structure[][]) {
  const size = backgroundSprites.length;

  const done = createGrid(size, (row, col) => structures[row][col] !== Structure.Ridge);

  // If the waterfall is part of a larger ridge, it needs an additional ridge placed under the water sprite.
  // It will use the tileset of the terrain around the waterfall.
  const placeSpritesUnderWaterfall = (row: number, col: number, direction: number, isTopLeftLow: boolean) => {
    let t: Terrain;

    switch (direction) {
      case 8: t = terrain[row + (row < size - 1 ? 1 : 0)][col]; break;
      case 4: t = terrain[row][col - (col > 0 ? 1 : 0)]; break;
      case 2: t = terrain[row - (row > 0 ? 1 : 0)][col]; break;
      case 1: t = terrain[row][col + (col < size - 1 ? 1 : 0)]; break;
      default: t = terrain[row][col];
    }

    if (t !== Terrain.Water && t in terrainToRidgeSpriteSheet) {
      const spriteSheet = terrainToRidgeSpriteSheet[t];
      const id = direction + (isTopLeftLow ? 0 : 16);
      backgroundSprites[row][col].push(spriteSheet[id]);
    }
  };

  const placeSpritesAlongTheRidge = (row: number, col: number, previous: number, isTopLeftLow: boolean) => {
    done[row][col] = true;

    let id = previous;
    if (previous !== 8 && row < size - 1 && !done[row + 1][col]) {
      id += 8;
      placeSpritesAlongTheRidge(row + 1, col, 2, id === 12 ? !isTopLeftLow : isTopLeftLow);
    } else if (previous !== 4 && col > 0 && !done[row][col - 1]) {
      id += 4;
      placeSpritesAlongTheRidge(row, col - 1, 1, id === 12 ? !isTopLeftLow : isTopLeftLow);
    } else if (previous !== 2 && row > 0 && !done[row - 1][col]) {
      id += 2;
      placeSpritesAlongTheRidge(row - 1, col, 8, id === 3 ? !isTopLeftLow : isTopLeftLow);
    } else if (previous !== 1 && col < size - 1 && !done[row][col + 1]) {
      id += 1;
      placeSpritesAlongTheRidge(row, col + 1, 4, id === 3 ? !isTopLeftLow : isTopLeftLow);
    }

    // If the ridge goes straight towards the map edge (not parallel), extend it so it looks as if it was leaving the map.
    if ((row === size - 1 && id === 2) || (row === 0 && id === 8)) {
      id = 10;
    } else if ((col === size - 1 && id === 4) || (col === 0 && id === 1)) {
      id = 5;
    }

    if (terrain[row][col] === Terrain.Water) {
      placeSpritesUnderWaterfall(row, col, previous, isTopLeftLow);
      placeSpritesUnderWaterfall(row, col, id - previous, isTopLeftLow);

      // In case of a waterfall, it is oriented according to the adjacent water rather than adjacent ridge.
      id = (row === size - 1 || terrain[row + 1][col] === Terrain.Water ? 8 : 0)
        + (col === 0 || terrain[row][col - 1] === Terrain.Water ? 4 : 0)
        + (row === 0 || terrain[row - 1][col] === Terrain.Water ? 2 : 0)
        + (col === size - 1 || terrain[row][col + 1] === Terrain.Water ? 1 : 0);
      if (id !== 5 && id !== 10 && id !== 15) {
        id = 0;
      }
    }

    if ((id === 3 && previous === 1) || (id === 12 && previous === 8)) {
      // For an "L" tile, it makes a difference from which side we came.
      isTopLeftLow = !isTopLeftLow;
    }
    id += isTopLeftLow ? 0 : 16;

    const spriteSheet = terrainToRidgeSpriteSheet[terrain[row][col]];
    backgroundSprites[row][col].push(spriteSheet[id]);
  };

  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      if (!done[row][col]) {
        // Since we're going from top to bottom and from left to right, there can be work only to the right and to the bottom.
        const id = (row < size - 1 && !done[row + 1][col] ? 8 : 0)
          + (col < size - 1 && !done[row][col + 1] ? 1 : 0);

        if (id === 1) {
          placeSpritesAlongTheRidge(row, col + 1, 4, true);
          placeSpritesAlongTheRidge(row, col, 1, true);
        } else if (id === 8) {
          placeSpritesAlongTheRidge(row + 1, col, 2, true);
          placeSpritesAlongTheRidge(row, col, 8, true);
        } else if (id === 9) {
          const spritesBefore = backgroundSprites[row][col].length;
          placeSpritesAlongTheRidge(row, col + 1, 4, true);

          if (backgroundSprites[row][col].length > spritesBefore) {
            // This is a closed circle. Fix the sprite on the original tile to make it look closed.
            backgroundSprites[row][col].pop();
            done[row + 1][col] = false;
            placeSpritesAlongTheRidge(row, col, 1, true);
            backgroundSprites[row + 1][col].pop();
          } else {
            // Not a closed circle. Bottom has to be done separately.
            placeSpritesAlongTheRidge(row, col, 1, true);
          }
        } else {
          placeSpritesAlongTheRidge(row, col, 1, true);
        }
      }
    }
  }
}
