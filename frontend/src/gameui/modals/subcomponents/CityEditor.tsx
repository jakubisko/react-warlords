import './CityEditor.scss';
import { useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import { setCityName } from '../../../model/game/actionsEditor';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import ArmyTypePicker from './ArmyTypePicker';
import { allArmyTypes } from '../../../model/game/constants';
import ArmyImage from '../../common/ArmyImage';
import { buildTrainingFacility, demolishTrainingFacility } from '../../../model/game/actionsPlayingThunks';
import { maxCityTrainingCapacity } from 'ai-interface/constants/city';
import { pushToUndoQueue } from '../../../model/editor/actions';
import ButtonWithShortcut from '../../common/ButtonWithShortcut';
import UnmountableComponent from '../../common/UnmountableComponent';
import { nameOfUtopia } from 'ai-interface/constants/winCondition';
import { EMPTY_ARRAY } from '../../../functions';

interface CityEditorProps {
  cityId: string;
}

export default function CityEditor({ cityId }: CityEditorProps) {
  const cities = useSelector((state: RootState) => state.game.cities);
  const city = cities[cityId];
  const dispatch: AppDispatch = useDispatch();

  const [editingIndex, setEditingIndex] = useState<number | null>(null);
  const [newName, setNewName] = useState(city.name);

  function saveCityName() {
    if (newName !== city.name && newName !== '') {
      dispatch(setCityName(cityId, newName));
      dispatch(pushToUndoQueue());
    }
  }

  function cityEditorTrainableSubmodal(): JSX.Element {
    const currentlyTrainableArmyTypeIds = city.currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[];

    if (editingIndex === null) {
      return <></>;
    }

    return (
      <Modal
        className="city-editor-trainable-submodal"
        show centered
        onHide={() => setEditingIndex(null)}
      >
        <Modal.Header>
          <Modal.Title>
            Choose army type
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <ArmyTypePicker
            availableArmyTypeIds={Object.keys(allArmyTypes).filter(armyTypeId => !allArmyTypes[armyTypeId].hero)}
            selectedArmyTypeIds={currentlyTrainableArmyTypeIds}
            changeSelectionButton={(armyTypeId, currentlySelected, close) => (
              <ButtonWithShortcut
                shortcutCode="Enter"
                allowShortcutWhileModalIsOpen={true}
                disabled={currentlyTrainableArmyTypeIds.includes(armyTypeId)} // Prevent choosing the same army type multiple times
                onClick={() => {
                  if (editingIndex < currentlyTrainableArmyTypeIds.length) {
                    dispatch(demolishTrainingFacility(cityId, currentlyTrainableArmyTypeIds[editingIndex]));
                  }
                  if (!currentlySelected) {
                    dispatch(buildTrainingFacility(cityId, armyTypeId));
                  }

                  close();
                  setEditingIndex(null);
                }}
              >
                {currentlySelected ? 'Remove' : (editingIndex < currentlyTrainableArmyTypeIds.length ? 'Replace' : 'Add')}
              </ButtonWithShortcut>
            )}
          />
        </Modal.Body>

        <Modal.Footer>
          <ButtonGroup>
            {editingIndex < currentlyTrainableArmyTypeIds.length && (
              <Button
                onClick={() => {
                  dispatch(demolishTrainingFacility(cityId, currentlyTrainableArmyTypeIds[editingIndex]));
                  setEditingIndex(null);
                }}
              >
                Remove
              </Button>
            )}

            <Button onClick={() => setEditingIndex(null)}>
              Close
            </Button>
          </ButtonGroup>
        </Modal.Footer>
      </Modal>
    );
  }

  function currentlyTrainableArmies(): JSX.Element {
    const currentlyTrainableArmyTypeIds = city.currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[];
    const items: JSX.Element[] = [];

    for (let i = 0; i < currentlyTrainableArmyTypeIds.length; i++) {
      items.push(
        <ArmyImage
          key={i}
          armyTypeId={currentlyTrainableArmyTypeIds[i]}
          selectable={true}
          selected={i === editingIndex}
          onLeftClick={() => setEditingIndex(i)}
          onRightClick={() => setEditingIndex(i)}
        />
      );
    }

    for (let i = items.length; i < maxCityTrainingCapacity; i++) {
      items.push(
        <ArmyImage
          key={i}
          armyTypeId={null}
          selectable={true}
          selected={i === editingIndex}
          onLeftClick={() => setEditingIndex(currentlyTrainableArmyTypeIds.length)}
          onRightClick={() => setEditingIndex(currentlyTrainableArmyTypeIds.length)}
        />
      );
    }

    return (
      <div className="currently-trainable-armies">
        <span className="available-text">Available training:</span>
        {items}
      </div>
    );
  }

  return (
    <UnmountableComponent onComponentWillUnmount={saveCityName}>
      <div className="city-editor">
        {currentlyTrainableArmies()}

        <input
          className={'form-control'
            + (newName === nameOfUtopia ? ' utopia' : '')
            + (Object.values(cities).some(city => city.id !== cityId && city.name === newName) ? ' duplicate' : '')
          }
          type="text"
          value={newName}
          maxLength={20}
          placeholder="City name"
          onChange={e => setNewName(e.target.value)}
        />

        {cityEditorTrainableSubmodal()}
      </div>
    </UnmountableComponent>
  );
}
