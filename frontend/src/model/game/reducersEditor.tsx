import { GameState } from './types';
import { updateGrid, createArray } from '../../functions';
import { ActionTypes } from './actionsEditor';
import { reducer as reducerTurnPassing } from './reducersTurnPassing';
import { Structure } from 'ai-interface/constants/structure';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { AppAction } from '../root/configureStore';

/** Editor actions, such as placing city, that apply on game state. */
export function reducer(state: GameState, action: AppAction): GameState {
  switch (action.type) {

    case ActionTypes.DRAW_WITH_TERRAIN_BRUSH: {
      const { row, col, terrain, brushSize } = action.payload;
      const brushRadius = (brushSize - 1) / 2;
      const { size } = state;

      const newTerrain = [...state.terrain];
      for (let row2 = Math.max(row - brushRadius, 0); row2 <= Math.min(row + brushRadius, size - 1); row2++) {
        const newRow = [...newTerrain[row2]];
        for (let col2 = Math.max(col - brushRadius, 0); col2 <= Math.min(col + brushRadius, size - 1); col2++) {
          if ((row2 - row) ** 2 + (col2 - col) ** 2 < brushSize ** 2 / 4) {
            newRow[col2] = terrain;
          }
        }
        newTerrain[row2] = newRow;
      }

      return {
        ...state,
        terrain: newTerrain
      };
    }

    case ActionTypes.SET_TERRAIN: {
      const { terrain } = action.payload;
      return {
        ...state,
        terrain
      };
    }

    case ActionTypes.SET_BACKGROUND_SPRITES: {
      const { backgroundSprites } = action.payload;
      return {
        ...state,
        backgroundSprites
      };
    }

    case ActionTypes.CREATE_RUIN: {
      const { ruinId, row, col } = action.payload;
      return {
        ...state,
        ruins: {
          ...state.ruins,
          [ruinId]: {
            id: ruinId,
            level: 1,
            lastSeenExplored: createArray(maxPlayers + 1, () => false),
            row,
            col
          }
        }
      };
    }

    case ActionTypes.CREATE_FLAGGABLE: {
      const { flaggableId, row, col } = action.payload;
      return {
        ...state,
        flaggables: {
          ...state.flaggables,
          [flaggableId]: {
            id: flaggableId,
            owner: neutralPlayerId,
            lastSeenOwner: createArray(maxPlayers + 1, () => neutralPlayerId),
            row,
            col
          }
        }
      };
    }

    case ActionTypes.CREATE_CITY: {
      const { cityId, row, col } = action.payload;
      return {
        ...state,
        cities: {
          ...state.cities,
          [cityId]: {
            id: cityId,
            owner: neutralPlayerId,
            lastSeenOwner: createArray(maxPlayers + 1, () => neutralPlayerId),
            lastSeenCapitol: createArray(maxPlayers + 1, () => false),
            name: '', // call SetCityName action later to set the name
            row,
            col,
            lastSeenRazed: createArray(maxPlayers + 1, () => false)
          }
        }
      };
    }

    case ActionTypes.PLACE_STRUCTURE: {
      const { row, col, structure, ruinId, flaggableId, cityId } = action.payload;
      const { structures, tiles } = state;

      const newTiles = updateGrid(tiles, row, col, tile => ({
        armies: tile.armies,
        artifactIds: tile.artifactIds,
        ruinId,
        flaggableId,
        cityId
      }));

      const newStructures = updateGrid(structures, row, col, () => structure);

      return {
        ...state,
        tiles: newTiles,
        structures: newStructures
      };
    }

    case ActionTypes.REMOVE_STRUCTURE: {
      const { row, col } = action.payload;
      const { tiles, structures, cities, flaggables, ruins } = state;
      const { cityId, flaggableId, ruinId } = tiles[row][col];

      const mapWithout = <T extends Record<string, unknown>>(map: T, key?: string) =>
        (key !== undefined)
          ? Object.fromEntries(Object.entries(map).filter(([myKey]) => myKey !== key)) as T
          : map;

      return {
        ...state,
        tiles: updateGrid(tiles, row, col, tile => ({ armies: tile.armies, artifactIds: tile.artifactIds })),
        structures: updateGrid(structures, row, col, () => Structure.Nothing),
        cities: mapWithout(cities, cityId),
        flaggables: mapWithout(flaggables, flaggableId),
        ruins: mapWithout(ruins, ruinId)
      };
    }

    case ActionTypes.SET_RUIN_LEVEL: {
      const { ruinId, level } = action.payload;
      return {
        ...state,
        ruins: {
          ...state.ruins,
          [ruinId]: {
            ...state.ruins[ruinId],
            level
          }
        }
      };
    }

    case ActionTypes.SET_CITY_NAME: {
      const { cityId, name } = action.payload;
      return {
        ...state,
        cities: {
          ...state.cities,
          [cityId]: {
            ...state.cities[cityId],
            name
          }
        }
      };
    }

    case ActionTypes.SET_SIGNPOST_MESSAGE: {
      const { row, col, message } = action.payload;
      return {
        ...state,
        tiles: updateGrid(state.tiles, row, col, tile => ({
          ...tile,
          signpost: message
        }))
      };
    }

    default:
      return reducerTurnPassing(state, action);
  }

}
