import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { Army } from 'ai-interface/types/army';
import { maxPlayers } from 'ai-interface/constants/player';
import { Place } from 'ai-interface/types/place';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { Personality } from './helpers/Personality';

/**
 * A group with high aggression will seek quick combat without traveling even when it's not in its favour.
 * A group with low aggression will prefer seeking tasks with the highest utility, even when it takes traveling.
 * 
 * High aggression is desired when:
 * - We want to decrease spending on army upkeep and don't care about losses.
 * - We're way ahead and thus our losses are easily replaced.
 * - We have a lot of armies concentrated at one place and thus can defeat even a strong enemy by multiple attacks.
 * - It's at a place we really want to conquer (village we keep losing to a scout etc.)
 *
 * High aggression is undesirable for longer period of time, because it leads to short-term thinking.
  */
export class AggressionManager {

  private readonly size: number;

  private readonly isMyTeam: boolean[] = [];

  private currentUpkeep!: number;
  private maximalDesiredUpkeepForArmies!: number;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly personality: Personality
  ) {
    this.size = land.getSize();

    const playersTeam = this.game.getPlayersTeam();
    for (let playerId = 0; playerId <= maxPlayers; playerId++) {
      this.isMyTeam.push(playersTeam[playerId] === playersTeam[game.getMyPlayerId()]);
    }
  }

  public calculateGlobalAggressionBasedOnSpending(maximalDesiredUpkeepForArmies: number) {
    this.currentUpkeep = this.game.getUpkeepOfExistingArmies()
      + this.personality.upkeepOfArmiesDismissedLastTurn; // If it came to dismissing last turn, we want to attack now!
    this.maximalDesiredUpkeepForArmies = maximalDesiredUpkeepForArmies;
  }

  /** When we lose armies, aggression goes down because our willingness to dismiss armies goes down. */
  public weLostArmies(armyTypeIds: string[]) {
    for (const armyTypeId of armyTypeIds) {
      this.currentUpkeep -= standardArmyTypes[armyTypeId].upkeep;
    }
  }

  /**
   * Gets the aggression of the group.
   * 0 is the default value. It will lead to attacks only if there is a material advantage to be gained.
   * 1 is the max value. It will lead to ignoring of all tasks that don't have combat in it.
   */
  public getAggressionOfGroup(groupId: string, armiesInGroup: Army[], heroPresent: boolean, neededForGuarding: boolean): number {
    let aggression = this.personality.baseAggression;

    // We want to decrease spending on army upkeep and don't care about losses. If those armies don't die in combat, we'll dismiss them.
    if (this.currentUpkeep >= 2 * this.maximalDesiredUpkeepForArmies) {
      aggression = 1;
    } else {
      aggression = Math.max(0, (this.currentUpkeep / this.maximalDesiredUpkeepForArmies) - 1);
    }

    // Groups surrounded by other friendly armies are more aggressive.
    const near = 5;
    const { row, col } = this.game.getPlaceOfGroup(groupId);
    let friendlyArmies = -armiesInGroup.length;
    for (let r = Math.max(0, row - near); r <= Math.min(row + near, this.size - 1); r++) {
      for (let c = Math.max(0, col - near); c <= Math.min(col + near, this.size - 1); c++) {
        const there: Place = { row: r, col: c };
        friendlyArmies += this.game.getMyArmiesAt(there).length;
      }
    }
    aggression += friendlyArmies / 50;

    // Bigger groups are even more aggressive
    if (armiesInGroup.length > 4) {
      aggression += (armiesInGroup.length - 4) / 10;
    }

    // Armies with hero are less aggressive.
    if (heroPresent) {
      aggression *= 0.5;
    }

    // Armies that guard cities are less aggressive.
    if (neededForGuarding) {
      aggression *= 0.5;
    }

    aggression = Math.min(1, aggression)

    const text = 'Aggression: ' + ('' + aggression).substring(0, 4);
    this.draw.drawTextWhenGroupIsSelected(text, groupId);

    return aggression;
  }

}
