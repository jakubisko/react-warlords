import { RootState } from '../root/configureStore';
import { allArmyTypes, allArtifacts } from '../game/constants';
import { getNumberOfArmiesPresentInCity, getOwnerOfPlace } from '../game/selectorsPlaying';
import { GameState } from '../game/types';
import { Place } from 'ai-interface/types/place';
import { calculateMoveTypeOfGroup, MoveCostCalculatorImplUnchecked } from '../path/MoveCostCalculatorImplUnchecked';
import { numberOfArmiesOfCurrentPlayerAtPlace, getMoveLeftOfGroup } from '../path/selectors';
import { GameApiObstacle } from 'ai-interface/apis/GameApiObstacle';
import {
  StringTooLong, IllegalPlace, YouDontControlSuchGroup, NotEnoughGold, SuchArmyTypeDoesntExist, SuchCityDoesntExist, YouDontControlSuchCity,
  CityIsRazed, CityDoesntHaveSuchTrainingFacility, CityAlreadyHasSuchTrainingFacility, CityCantHaveMoreTrainingFacilities,
  CityCantHaveMoreIncomingCaravans, NoHeroOffer, NoArmiesPresent, NotEnoughFreeSpaceAtPlace, SuchFlaggableDoesntExist,
  SuchRuinDoesntExist, GroupDoesntContainSuchArmy, SourceAndDestinationAreIdentical, YouDontControlSuchHero, SuchArtifactDoesntExist,
  HeroCantCarryMoreArtifacts, NoSuchArtifactAtPlace, HeroDoesntCarrySuchArtifact, PlaceNotExplorable, IllegalMoveType,
  PathPlacesAreNotAdjacent, ZeroLengthPath, ImpassableTerrain, CrossingCoastOffPort, IllegalGroupSize, IllegalInitialMoveLeft,
  DestinationIsOutsideOfCalculatedValues, NoMoveLeft, GroupsDontStandAtSamePlace, IllegalOrdering, SuchArmyTypeCantBeTrained, SamePlayer, ValueOutOfRange
} from 'ai-interface/types/obstacle';
import { Structure } from 'ai-interface/constants/structure';
import { MoveType, SearchPathArgs, TimeMatrix } from 'ai-interface/types/path';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { demolishCost, razeCost, maxCityTrainingCapacity } from 'ai-interface/constants/city';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { maxHeroArtifactsCapacity } from 'ai-interface/constants/artifactAbility';
import { maxCaravanCapacity } from 'ai-interface/constants/city';
import { maxLengthOfState } from 'ai-interface/apis/GameApi';
import { maxPlayers } from 'ai-interface/constants/player';
import { EMPTY_ARRAY } from '../../functions';

export class GameApiObstacleImpl implements GameApiObstacle {

  constructor(private getState: () => RootState) { }

  private illegalPlace(game: GameState, place: Place): IllegalPlace | null {
    const { size } = game;
    const { row, col } = place;
    if (row % 1 !== 0 || col % 1 !== 0) {
      return new IllegalPlace(`Place [${row}, ${col}] has coordinates that are not integers`);
    } else if (row < 0 || row >= size || col < 0 || col >= size) {
      return new IllegalPlace(`Place [${row}, ${col}] is outside of the land boundaries`);
    }
    return null;
  }

  private youDontControlSuchGroup(game: GameState, groupId: string): YouDontControlSuchGroup | null {
    const { groups, currentPlayerId } = game;
    return (!(groupId in groups) || groups[groupId].owner !== currentPlayerId)
      ? new YouDontControlSuchGroup(`There is no group '${groupId}' under your control`)
      : null;
  }

  //*********** Global ***********/

  saveState(state: string): StringTooLong | null {
    return state.length > maxLengthOfState
      ? new StringTooLong(`Can't save state because it has length ${state.length} which exceeds maximum limit of ${maxLengthOfState}`)
      : null;
  }

  setMyOrdering(armyTypeIds: string[]): SuchArmyTypeDoesntExist | IllegalOrdering | null {
    for (const armyTypeId in standardArmyTypes) {
      if (!armyTypeIds.includes(armyTypeId)) {
        return new IllegalOrdering(`Proposed ordering is missing a value for army type '${armyTypeId}'`);
      }
    }

    const present: { [armyTypeId: string]: boolean } = {};
    for (const armyTypeId of armyTypeIds) {
      if (!(armyTypeId in standardArmyTypes)) {
        return new SuchArmyTypeDoesntExist(`Proposed ordering uses an unknown army type '${armyTypeId}'`);
      }

      if (present[armyTypeId]) {
        return new IllegalOrdering(`Proposed ordering contains army type '${armyTypeId}' multiple times`);
      }
      present[armyTypeId] = true;
    }

    const heroIds = Object.keys(standardArmyTypes)
      .filter(armyTypeId => standardArmyTypes[armyTypeId].hero)
      .sort();
    const index0 = armyTypeIds.indexOf(heroIds[0]);
    for (let i = 0; i < heroIds.length; i++) {
      const indexI = armyTypeIds.indexOf(heroIds[i]);
      if (indexI !== index0 + i) {
        return new IllegalOrdering(`Proposed ordering doesn't have heroes in an ascending level order in a continuous sublist without gaps. `
          + `Army type '${heroIds[i]}' is out of order.`);
      }
    }

    return null;
  }

  getOwnerOfPlace(place: Place): IllegalPlace | null {
    const { game } = this.getState();
    return this.illegalPlace(game, place);
  }

  //*********** Cities ***********/

  private notEnoughGold(game: GameState, amount: number): NotEnoughGold | null {
    const currentGold = game.players[game.currentPlayerId].gold;
    return currentGold < amount
      ? new NotEnoughGold(`You don't have enough gold; ${amount} is required but you have only ${currentGold}`)
      : null;
  }

  private suchArmyTypeDoesntExist(armyTypeId: string): SuchArmyTypeDoesntExist | null {
    return (!(armyTypeId in standardArmyTypes))
      ? new SuchArmyTypeDoesntExist(`Army type '${armyTypeId}' doesn't exist`)
      : null;
  }

  private suchArmyTypeCantBeTrained(armyTypeId: string): SuchArmyTypeCantBeTrained | null {
    return (standardArmyTypes[armyTypeId].hero)
      ? new SuchArmyTypeCantBeTrained(`Army type '${armyTypeId}' can't be trained`)
      : null;
  }

  private suchCityDoesntExist(game: GameState, cityId: string): SuchCityDoesntExist | null {
    return (!(cityId in game.cities))
      ? new SuchCityDoesntExist(`City '${cityId}' doesn't exist`)
      : null;
  }

  private youDontControlSuchCity(game: GameState, cityId: string): YouDontControlSuchCity | null {
    return (game.cities[cityId].owner !== game.currentPlayerId)
      ? new YouDontControlSuchCity(`City '${cityId}' is not under your control`)
      : null;
  }

  private cityIsRazed(game: GameState, cityId: string): CityIsRazed | null {
    return game.cities[cityId].razed
      ? new CityIsRazed(`City '${cityId}' is razed`)
      : null;
  }

  private cityDoesntHaveSuchTrainingFacility(game: GameState, cityId: string, armyTypeId: string): CityDoesntHaveSuchTrainingFacility | null {
    const { currentlyTrainableArmyTypeIds } = game.cities[cityId];
    return (currentlyTrainableArmyTypeIds === undefined || !currentlyTrainableArmyTypeIds.includes(armyTypeId))
      ? new CityDoesntHaveSuchTrainingFacility(`City '${cityId}' doesn't have facility for training of army type '${armyTypeId}'`)
      : null;
  }

  getCity(cityId: string): SuchCityDoesntExist | null {
    const { game } = this.getState();
    return this.suchCityDoesntExist(game, cityId);
  }

  setTrainingInCity(cityId: string, armyTypeId?: string): SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained | SuchCityDoesntExist
    | YouDontControlSuchCity | CityIsRazed | CityDoesntHaveSuchTrainingFacility | null {
    const { game } = this.getState();
    return (armyTypeId !== undefined ? this.suchArmyTypeDoesntExist(armyTypeId) : null)
      ?? (armyTypeId !== undefined ? this.suchArmyTypeCantBeTrained(armyTypeId) : null)
      ?? this.suchCityDoesntExist(game, cityId)
      ?? this.youDontControlSuchCity(game, cityId)
      ?? this.cityIsRazed(game, cityId)
      ?? (armyTypeId !== undefined ? this.cityDoesntHaveSuchTrainingFacility(game, cityId, armyTypeId) : null);
  }

  buildTrainingFacility(cityId: string, armyTypeId: string): NotEnoughGold | SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained
    | SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | CityAlreadyHasSuchTrainingFacility | CityCantHaveMoreTrainingFacilities | null {
    const { game } = this.getState();
    return this.notEnoughGold(game, allArmyTypes[armyTypeId].buildingCost)
      ?? this.suchArmyTypeDoesntExist(armyTypeId)
      ?? this.suchArmyTypeCantBeTrained(armyTypeId)
      ?? this.suchCityDoesntExist(game, cityId)
      ?? this.youDontControlSuchCity(game, cityId)
      ?? this.cityIsRazed(game, cityId)
      ?? ((game.cities[cityId].currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[]).includes(armyTypeId)
        ? new CityAlreadyHasSuchTrainingFacility(`City '${cityId}' already has facility for training of army type '${armyTypeId}'`)
        : null)
      ?? ((game.cities[cityId].currentlyTrainableArmyTypeIds?.length ?? 0) >= maxCityTrainingCapacity
        ? new CityCantHaveMoreTrainingFacilities(`City '${cityId}' already has maximum training facilities; you have to demolish one`)
        : null);
  }

  demolishTrainingFacility(cityId: string, armyTypeId: string): NotEnoughGold | SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained
    | SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | CityDoesntHaveSuchTrainingFacility | null {
    const { game } = this.getState();
    return this.notEnoughGold(game, demolishCost)
      ?? this.suchArmyTypeDoesntExist(armyTypeId)
      ?? this.suchArmyTypeCantBeTrained(armyTypeId)
      ?? this.suchCityDoesntExist(game, cityId)
      ?? this.youDontControlSuchCity(game, cityId)
      ?? this.cityIsRazed(game, cityId)
      ?? this.cityDoesntHaveSuchTrainingFacility(game, cityId, armyTypeId);
  }

  setCaravanFromCityToCity(fromCityId: string, toCityId?: string): SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed
    | SourceAndDestinationAreIdentical | CityCantHaveMoreIncomingCaravans | null {
    const { game, game: { cities, currentPlayerId } } = this.getState();

    const obstacle1 = this.suchCityDoesntExist(game, fromCityId)
      ?? this.youDontControlSuchCity(game, fromCityId)
      ?? this.cityIsRazed(game, fromCityId)
      ?? null;
    if (obstacle1 || toCityId === undefined) {
      return obstacle1;
    }

    return this.suchCityDoesntExist(game, toCityId)
      ?? this.youDontControlSuchCity(game, toCityId)
      ?? this.cityIsRazed(game, toCityId)
      ?? (fromCityId === toCityId
        ? new SourceAndDestinationAreIdentical(`Can't set up caravan; destination city is same as source city '${fromCityId}'`)
        : null)
      ?? (toCityId !== cities[fromCityId].caravanToCityId && Object.values(cities)
        .filter(({ owner, caravanToCityId }) => owner === currentPlayerId && caravanToCityId === toCityId)
        .length >= maxCaravanCapacity
        ? new CityCantHaveMoreIncomingCaravans(`Can't set up caravan; no more than ${maxCaravanCapacity} cities may set their destination to the same city at any one time.`)
        : null)
      ?? null;
  }

  razeCity(cityId: string): SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | NotEnoughGold | NoArmiesPresent | null {
    const state = this.getState();
    const { game } = state;

    return this.suchCityDoesntExist(game, cityId)
      ?? this.youDontControlSuchCity(game, cityId)
      ?? this.cityIsRazed(game, cityId)
      ?? this.notEnoughGold(game, razeCost)
      ?? (getNumberOfArmiesPresentInCity(state, cityId) === 0
        ? new NoArmiesPresent(`At least one army is required to be present in the city '${cityId}' to raze it`)
        : null);
  }

  //*********** Flaggable structures ***********/

  getFlaggable(flaggableId: string): SuchFlaggableDoesntExist | null {
    const { game } = this.getState();
    return (!(flaggableId in game.flaggables))
      ? new SuchFlaggableDoesntExist(`Flaggable structure '${flaggableId}' doesn't exist`)
      : null;
  }

  //*********** Ruins, Sage Tower ***********/

  getRuin(ruinId: string): SuchRuinDoesntExist | null {
    const { game } = this.getState();
    return (!(ruinId in game.ruins))
      ? new SuchRuinDoesntExist(`Ruin '${ruinId}' doesn't exist`)
      : null;
  }

  exploreRuin(heroId: string): YouDontControlSuchHero | PlaceNotExplorable | null {
    const { game } = this.getState();

    const obstacle1 = this.youDontControlSuchHero(game, heroId);
    if (obstacle1) {
      return obstacle1;
    }

    const { structures, groups, heroes } = game;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];

    return structures[row][col] !== Structure.Ruin
      ? new PlaceNotExplorable(`Hero '${heroId}' can't explore ruin because there is no ruin at place [${row}, ${col}]`)
      : null;
  }

  exploreSageTower(heroId: string): YouDontControlSuchHero | PlaceNotExplorable | null {
    const { game } = this.getState();

    const obstacle1 = this.youDontControlSuchHero(game, heroId);
    if (obstacle1) {
      return obstacle1;
    }

    const { structures, groups, heroes } = game;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];

    return structures[row][col] !== Structure.SageTower
      ? new PlaceNotExplorable(`Hero '${heroId}' can't explore Sage Tower because there is no Sage Tower at place [${row}, ${col}]`)
      : null;
  }

  getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(playerId: number): ValueOutOfRange | null {
    return (playerId % 1 !== 0 || playerId < 0 || playerId >= maxPlayers)
      ? new ValueOutOfRange(`'${playerId}' is not a legal playerId, only whole values from 0 to ${maxPlayers - 1} are allowed`)
      : null;
  }

  //*********** Groups ***********/

  getPlaceOfGroup(groupId: string): YouDontControlSuchGroup | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId);
  }

  dismissGroup(groupId: string): YouDontControlSuchGroup | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId);
  }

  //*********** Armies ***********/

  private groupDoesntContainSuchArmy(game: GameState, groupId: string, armyId: string): GroupDoesntContainSuchArmy | null {
    const { row, col } = game.groups[groupId];
    return (!game.tiles[row][col].armies!.some(army => army.id === armyId && army.groupId === groupId))
      ? new GroupDoesntContainSuchArmy(`The group '${groupId}' doesn't contain army '${armyId}'`)
      : null;
  }

  private groupsDontStandAtSamePlace(game: GameState, groupId1: string, groupId2: string): GroupsDontStandAtSamePlace | null {
    const group1 = game.groups[groupId1];
    const group2 = game.groups[groupId2];
    return (group1.row !== group2.row || group1.col !== group2.col)
      ? new GroupsDontStandAtSamePlace(`Can't transfer army; groups '${groupId1}' and '${groupId2}' are not on the same place`)
      : null;
  }

  getArmiesInGroup(groupId: string): YouDontControlSuchGroup | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId);
  }

  transferArmyFromGroupToOtherGroup(armyId: string, srcGroupId: string, destGroupId: string): YouDontControlSuchGroup
    | SourceAndDestinationAreIdentical | GroupDoesntContainSuchArmy | GroupsDontStandAtSamePlace | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, srcGroupId)
      ?? this.youDontControlSuchGroup(game, destGroupId)
      ?? (srcGroupId === destGroupId
        ? new SourceAndDestinationAreIdentical(`Can't transfer army between groups; destination group is same as source group '${srcGroupId}'`)
        : null)
      ?? this.groupDoesntContainSuchArmy(game, srcGroupId, armyId)
      ?? this.groupsDontStandAtSamePlace(game, srcGroupId, destGroupId);
  }

  transferArmyFromGroupToNewGroup(armyId: string, srcGroupId: string): YouDontControlSuchGroup | GroupDoesntContainSuchArmy | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, srcGroupId)
      ?? this.groupDoesntContainSuchArmy(game, srcGroupId, armyId);
  }

  dismissArmy(groupId: string, armyId: string): YouDontControlSuchGroup | GroupDoesntContainSuchArmy | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId)
      ?? this.groupDoesntContainSuchArmy(game, groupId, armyId);
  }

  getMyArmiesAt(place: Place): IllegalPlace | null {
    const { game } = this.getState();
    return this.illegalPlace(game, place);
  }

  getArmyTypeIdsAt(place: Place): IllegalPlace | null {
    const { game } = this.getState();
    return this.illegalPlace(game, place);
  }

  //*********** Heroes ***********/

  private youDontControlSuchHero(game: GameState, heroId: string): YouDontControlSuchHero | null {
    const { heroes, groups, currentPlayerId } = game;
    const hero = heroes[heroId];
    return (hero === undefined || groups[hero.groupId].owner !== currentPlayerId)
      ? new YouDontControlSuchHero(`There is no hero '${heroId}' under your control`)
      : null;
  }

  private suchArtifactDoesntExist(artifactId: string): SuchArtifactDoesntExist | null {
    return !(artifactId in allArtifacts)
      ? new SuchArtifactDoesntExist(`Artifact '${artifactId}' doesn't exist`)
      : null;
  }

  acceptHeroOffer(): NoHeroOffer | NotEnoughGold | CityIsRazed | NotEnoughFreeSpaceAtPlace | null {
    const state = this.getState();
    const { game } = state;
    if (game.heroOffer === null) {
      return new NoHeroOffer(`There are currently no armies offering to join your cause`);
    }

    const { cost, armyTypeIds, cityId } = game.heroOffer;
    return this.notEnoughGold(game, cost)
      ?? this.cityIsRazed(game, cityId)
      ?? (getNumberOfArmiesPresentInCity(state, cityId) + armyTypeIds.length > 4 * maxTileCapacity
        ? new NotEnoughFreeSpaceAtPlace(`There is no space available in city '${cityId}' where recruited armies could be placed`)
        : null);
  }

  getHero(heroId: string): YouDontControlSuchHero | null {
    const { game } = this.getState();
    return this.youDontControlSuchHero(game, heroId);
  }

  getArtifactIdsOnGround(place: Place): IllegalPlace | null {
    const { game } = this.getState();
    return this.illegalPlace(game, place);
  }

  pickArtifactFromGround(heroId: string, artifactId: string): SuchArtifactDoesntExist | YouDontControlSuchHero | HeroCantCarryMoreArtifacts | NoSuchArtifactAtPlace | null {
    const { game } = this.getState();

    const obstacle1 = this.suchArtifactDoesntExist(artifactId)
      ?? this.youDontControlSuchHero(game, heroId);
    if (obstacle1) {
      return obstacle1;
    }

    const { tiles, groups, heroes } = game;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];

    return (!(tiles[row][col].artifactIds ?? EMPTY_ARRAY as string[]).includes(artifactId)
      ? new NoSuchArtifactAtPlace(`The hero '${heroId}' can't pick artifact '${artifactId}' because there is no such artifact at place [${row}, ${col}] to be picked`)
      : null)
      ?? ((heroes[heroId].artifactIds?.length ?? 0) >= maxHeroArtifactsCapacity
        ? new HeroCantCarryMoreArtifacts(`Hero '${heroId}' is already carrying maximum number of artifacts: ${maxHeroArtifactsCapacity}`)
        : null);
  }

  dropArtifactOntoGround(heroId: string, artifactId: string): SuchArtifactDoesntExist | YouDontControlSuchHero | HeroDoesntCarrySuchArtifact | null {
    const { game } = this.getState();
    return this.suchArtifactDoesntExist(artifactId)
      ?? this.youDontControlSuchHero(game, heroId)
      ?? (!(game.heroes[heroId].artifactIds ?? EMPTY_ARRAY as string[]).includes(artifactId)
        ? new HeroDoesntCarrySuchArtifact(`Hero '${heroId}' can't drop artifact '${artifactId}' because he isn't carrying it`)
        : null);
  }

  //*********** Group move ***********/

  private illegalMoveType(moveType: MoveType): IllegalMoveType | null {
    const { groupSize, groupMoveOnLand } = moveType;
    if (groupSize % 1 !== 0 || groupSize < 1 || groupSize > maxTileCapacity) {
      return new IllegalMoveType(`Group size must be a whole number between 1 and ${maxTileCapacity}, but is ${groupSize}`);
    } else if (groupMoveOnLand % 1 !== 0 || groupMoveOnLand < 1) {
      return new IllegalMoveType(`Group move per turn must be a whole number higher than 0, but is ${groupMoveOnLand}`);
    }
    return null;
  }

  private illegalInitialMoveLeft(initialMoveLeft: number): IllegalInitialMoveLeft | null {
    return initialMoveLeft % 1 !== 0
      ? new IllegalInitialMoveLeft(`Initial move left must be a whole number, but is ${initialMoveLeft}`)
      : null;
  }

  private illegalPath(game: GameState, path: Place[], moveType: MoveType): ZeroLengthPath | IllegalPlace
    | PathPlacesAreNotAdjacent | NotEnoughFreeSpaceAtPlace | ImpassableTerrain | CrossingCoastOffPort | null {
    if (path.length === 0) {
      return new ZeroLengthPath(`Path with zero length is not valid - it represents that the destination can't be reached.`);
    }

    for (const place of path) {
      const obstacle1 = this.illegalPlace(game, place);
      if (obstacle1 !== null) {
        return new IllegalPlace(`Illegal place on path; ${obstacle1.errorText}`);
      }
    }

    const moveCostCalculator = new MoveCostCalculatorImplUnchecked(game, moveType.abilities);

    for (let i = 1; i < path.length; i++) {
      const { row: srcRow, col: srcCol } = path[i - 1];
      const { row: destRow, col: destCol } = path[i];

      const obstacle2 = (
        (srcRow === destRow && srcCol === destCol) || Math.abs(srcRow - destRow) > 1 || Math.abs(srcCol - destCol) > 1
          ? new PathPlacesAreNotAdjacent(`Path places need to be exactly one step apart, but the places [${srcRow}, ${srcCol}], [${destRow}, ${destCol}] are not`)
          : null)
        ?? (moveType.groupSize + numberOfArmiesOfCurrentPlayerAtPlace(this.getState(), path[i]) > maxTileCapacity
          ? new NotEnoughFreeSpaceAtPlace(`Number of your armies at place [${destRow}, ${destCol}] would exceed the maximum capacity`)
          : null)
        ?? (moveCostCalculator.getMoveCostAt(destRow, destCol) === Infinity
          ? new ImpassableTerrain(`Terrain at place [${destRow}, ${destCol}] is impassable for given move type`)
          : null)
        ?? (moveCostCalculator.isCoastCrossingForbiddenAt(srcRow, srcCol, destRow, destCol)
          ? new CrossingCoastOffPort(`A group that doesn't fly is not allowed to cross the coast off the port between places [${srcRow}, ${srcCol}], [${destRow}, ${destCol}]`)
          : null);
      if (obstacle2 !== null) {
        return obstacle2;
      }
    }

    return null;
  }

  getMoveTypeOfGroup(groupId: string): YouDontControlSuchGroup | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId);
  }


  getMoveTypeOfArmyTypes(armyTypeIds: string[]): IllegalGroupSize | SuchArmyTypeDoesntExist | null {
    const groupSize = armyTypeIds.length;
    if (groupSize % 1 !== 0 || groupSize < 1 || groupSize > maxTileCapacity) {
      return new IllegalGroupSize(`Group size must be a whole number between 1 and ${maxTileCapacity}, but is ${groupSize}`);
    }

    for (const armyTypeId of armyTypeIds) {
      const obstacle = this.suchArmyTypeDoesntExist(armyTypeId);
      if (obstacle) {
        return obstacle;
      }
    }
    return null;
  }

  createBasicAvoidingFunction(groupSize: number): IllegalGroupSize | null {
    return (groupSize % 1 !== 0 || groupSize < 1 || groupSize > maxTileCapacity)
      ? new IllegalGroupSize(`Group size must be a whole number between 1 and ${maxTileCapacity}, but is ${groupSize}`)
      : null;
  }

  calculateTimeMatrix(searchPathArgs: SearchPathArgs): IllegalPlace | IllegalMoveType | IllegalInitialMoveLeft | null {
    const { start, moveType, initialMoveLeft } = searchPathArgs;
    const { game } = this.getState();

    return this.illegalPlace(game, start)
      ?? this.illegalMoveType(moveType)
      ?? this.illegalInitialMoveLeft(initialMoveLeft);
  }

  calculatePathToPlaceFromTimeMatrix(timeMatrix: TimeMatrix, destination: Place): IllegalPlace | DestinationIsOutsideOfCalculatedValues | null {
    const { game } = this.getState();
    const { row, col } = destination;
    return this.illegalPlace(game, destination)
      ?? (!timeMatrix.isCalculated(row, col)
        ? new DestinationIsOutsideOfCalculatedValues(`The destination [${row}, ${col}] is outside of the maximum travel time calculated for this TimeMatrix`)
        : null);
  }

  calculateTimesOnPath(path: Place[], moveType: MoveType, initialMoveLeft: number): IllegalMoveType | ZeroLengthPath
    | IllegalPlace | PathPlacesAreNotAdjacent | NotEnoughFreeSpaceAtPlace | ImpassableTerrain | CrossingCoastOffPort
    | IllegalInitialMoveLeft | null {
    const { game } = this.getState();
    return this.illegalMoveType(moveType)
      ?? this.illegalPath(game, path, moveType)
      ?? this.illegalInitialMoveLeft(initialMoveLeft);
  }

  getMoveLeftOfGroup(groupId: string): YouDontControlSuchGroup | null {
    const { game } = this.getState();
    return this.youDontControlSuchGroup(game, groupId);
  }

  moveGroupOneStep(groupId: string, destination: Place): YouDontControlSuchGroup | NoMoveLeft | ZeroLengthPath | IllegalPlace
    | PathPlacesAreNotAdjacent | NotEnoughFreeSpaceAtPlace | ImpassableTerrain | CrossingCoastOffPort | null {
    const { game } = this.getState();
    const obstacle1 = this.youDontControlSuchGroup(game, groupId)
      ?? (getMoveLeftOfGroup(this.getState(), groupId) <= 0
        ? new NoMoveLeft(`The group '${groupId}' has no move left`)
        : null);
    if (obstacle1 !== null) {
      return obstacle1;
    }

    const { row, col } = game.groups[groupId];
    const path: Place[] = [
      { row, col },
      destination
    ];
    const moveType = calculateMoveTypeOfGroup(game, groupId);

    return this.illegalPath(game, path, moveType);
  }

  //*********** Combat ***********/

  private noArmiesPresent(state: RootState, place: Place, entireCityIfPossible: boolean): NoArmiesPresent | null {
    const { row, col } = place;
    const { tiles, cities } = state.game;
    const { cityId, armies } = tiles[row][col];
    return (entireCityIfPossible && cityId !== undefined && !cities[cityId].razed
      ? getNumberOfArmiesPresentInCity(state, cityId)
      : (armies?.length ?? 0)
    ) === 0
      ? new NoArmiesPresent(`Can't predict combat results; there are no armies at [${row}, ${col}]`)
      : null;
  }

  private attacksHimself(state: RootState, attackingPlace: Place, defendingPlace: Place): SamePlayer | null {
    const { tiles, groups } = state.game;
    const { row: aRow, col: aCol } = attackingPlace;
    const { row: dRow, col: dCol } = defendingPlace;
    return groups[tiles[aRow][aCol].armies![0].groupId].owner === getOwnerOfPlace(state, defendingPlace)
      ? new SamePlayer(`Can't predict combat results; attacking armies at [${aRow}, ${aCol}] are owned by same player as defending place [${dRow}, ${dCol}]`)
      : null;
  }

  predictCombatIfGroupAttacks(attackingGroupId: string, defendingPlace: Place): IllegalPlace | YouDontControlSuchGroup | NoArmiesPresent | SamePlayer | null {
    const state = this.getState();
    const { game } = state;
    const { groups } = game;
    return this.illegalPlace(game, defendingPlace)
      ?? this.youDontControlSuchGroup(game, attackingGroupId)
      ?? this.noArmiesPresent(state, defendingPlace, true)
      ?? this.attacksHimself(state, groups[attackingGroupId], defendingPlace);
  }

  predictCombatIfAllArmiesAtPlaceAttack(attackingPlace: Place, defendingPlace: Place): IllegalPlace | NoArmiesPresent | SamePlayer | null {
    const state = this.getState();
    const { game } = state;
    return this.illegalPlace(game, defendingPlace)
      ?? this.illegalPlace(game, attackingPlace)
      ?? this.noArmiesPresent(state, defendingPlace, true)
      ?? this.noArmiesPresent(state, attackingPlace, false)
      ?? this.attacksHimself(state, attackingPlace, defendingPlace);
  }

  //*********** Unexplored ***********/

  isPlaceUnexplored(place: Place): IllegalPlace | null {
    const { game } = this.getState();
    return this.illegalPlace(game, place);
  }

}
