import { useEffect, useMemo, useState } from 'react';
import { OnlineGame } from './types';
import { firestore } from './firebaseConfig';
import { EMPTY_ARRAY } from '../../functions';
import { collection, limit, onSnapshot, orderBy, query, where } from 'firebase/firestore';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../root/configureStore';
import { setErrorMessage } from '../ui/actions';

/**
 * Returns the list of online games that contain logged-in user from Firebase, updated in the real time.
 * Returns an empty list without throwing an error if not logged in.
 */
export function useOnlineGames() {
  const dispatch: AppDispatch = useDispatch();

  const loggedInUserUid = useSelector((state: RootState) => state.online.loggedInUser?.uid);

  const [loadingGames, setLoadingGames] = useState(!!loggedInUserUid);

  const [games, setGames] = useState(EMPTY_ARRAY as OnlineGame[]);
  const gamesQueryRef = useMemo(() => query(
    collection(firestore, 'games'),
    where('userUids', 'array-contains', loggedInUserUid),
    orderBy('createdAt', 'desc'),
    limit(100)
  ), [firestore, loggedInUserUid]);

  useEffect(() => {
    // Don't subscribe to updates when not logged in
    if (!loggedInUserUid) {
      setGames(EMPTY_ARRAY as OnlineGame[]);
      return;
    }

    // Subscribe to updates on the games collection in real time
    return onSnapshot(
      gamesQueryRef,
      (snapshot) => {
        setGames(snapshot.docs.map(doc => ({
          ...(doc.data() as OnlineGame),
          id: doc.id,
        })));
        setLoadingGames(false);
      },
      (err) => {
        setGames(EMPTY_ARRAY as OnlineGame[]);
        setLoadingGames(false);
        dispatch(setErrorMessage('Failed to retrieve the list of online games', err));
      }
    );
  }, [loggedInUserUid]);

  return { games, loadingGames };
}
