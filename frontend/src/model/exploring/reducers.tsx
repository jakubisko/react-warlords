import { ExploringResult } from 'ai-interface/types/exploring';
import { ActionTypes } from './actions';
import { EMPTY_ARRAY } from '../../functions';
import { AppAction } from '../root/configureStore';

/**
 * This state contains only data that are related to exploring one ruin. They are temporary, but are also used by AI.
 * None of it is saved when land or game is saved.
 */
export interface ExploringState {
  readonly exploringResult: ExploringResult; // Result of last ruin/sage exploration.
}

export const initialState: ExploringState = {
  exploringResult: {
    heroId: '',
    message: EMPTY_ARRAY as string[],
    becomesExplored: false,
    heroDies: false,
    experiences: 0,
    gold: 0,
    artifactIdsFound: EMPTY_ARRAY as string[],
    armyTypeIdsFound: EMPTY_ARRAY as string[],
    mapExplored: { minRow: 1, minCol: 1, maxRow: 0, maxCol: 0 }
  }
};

export function exploringReducer(state = initialState, action: AppAction): ExploringState {
  switch (action.type) {

    case ActionTypes.SET_EXPLORING_RESULT: {
      const { exploringResult } = action.payload;
      return {
        ...state,
        exploringResult
      };
    }

    default:
      return state;
  }
}
