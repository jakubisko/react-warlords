import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { cleanUiAndShowLand } from '../../model/ui/actions';
import { Modal } from 'react-bootstrap';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { ModalTitleWithFlags } from '../common/ModalTitleWithFlags';

export default function StartOfTurnScreen() {
  const currentPlayerId = useSelector((state: RootState) => state.game.currentPlayerId);
  const currentPlayerName = useSelector((state: RootState) => state.game.players[state.game.currentPlayerId].name);
  const turnNumber = useSelector((state: RootState) => state.game.turnNumber);
  const aiOutputVisible = useSelector((state: RootState) => state.game.aiOutputVisible && state.game.players[state.game.currentPlayerId].aiName !== undefined);
  const dispatch: AppDispatch = useDispatch();

  return (
    <Modal className="screen" animation={false} show centered backdrop="static">
      <Modal.Header>
        <ModalTitleWithFlags owner={currentPlayerId} caption={currentPlayerName} />
      </Modal.Header>

      <Modal.Body className="p-3">
        {aiOutputVisible && `End of Computer's `}
        Week&nbsp;{Math.floor((turnNumber - 1) / 7) + 1}, Day&nbsp;{(turnNumber - 1) % 7 + 1}
      </Modal.Body>

      <Modal.Footer>
        <ButtonWithShortcut
          allowShortcutWhileModalIsOpen={true}
          shortcutCode="Enter"
          onClick={() => dispatch(cleanUiAndShowLand())}
        >
          OK
        </ButtonWithShortcut>
      </Modal.Footer>
    </Modal>
  );
}
