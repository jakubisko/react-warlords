import './EditorMenuModal.scss';
import { useState, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, ButtonGroup } from 'react-bootstrap';
import { setProgramMode, setScreen, setShowingMenu } from '../../model/ui/actions';
import { createEmptyMap, loadMap } from '../../model/game/actionsEditor';
import { compressObject, decompressObject } from '../../model/game/serialization';
import { ScreenEnum, ProgramMode } from '../../model/ui/types';
import LoadButton from '../common/LoadButton';
import SaveButton from '../common/SaveButton';
import { extractMapDataFromGameState } from '../../model/game/saveLoad';
import FullscreenButton from '../common/FullscreenButton';
import CreateRandomMapModal from './CreateRandomMapModal';

export default function EditorMenuModal() {
  const gameState = useSelector((state: RootState) => state.game);
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(0);
  const [size, setSize] = useState(40);

  function getMapDownloadData(): Promise<Blob> {
    const mapData = extractMapDataFromGameState(gameState);
    return compressObject(mapData, 'map', 'blob');
  }

  async function handleLoadMap(fileData: ArrayBuffer) {
    const mapData = await decompressObject(fileData, 'map');
    dispatch(loadMap(mapData, ProgramMode.MapEditor));
    dispatch(setShowingMenu(false));
  }

  function handleExitToMainMenu() {
    dispatch(setProgramMode(ProgramMode.Playing));
    dispatch(setScreen(ScreenEnum.MainMenuScreen));
  }

  const subpageMain = () => (
    <ButtonGroup vertical>
      <Button onClick={() => setSubpage(1)}>
        New map
      </Button>
      <SaveButton
        caption="Save map to file"
        fileName="warlords-map"
        fileExtension="wmap"
        getDownloadData={getMapDownloadData}
      />
      <LoadButton
        caption="Load map from file"
        fileExtension="wmap"
        onUpload={handleLoadMap}
      />
      <Button onClick={handleExitToMainMenu}>Exit to main menu</Button>
      <Button onClick={() => dispatch(setShowingMenu(false))}>Return to editor</Button>
    </ButtonGroup>
  );

  const subpageNewMap = () => (
    <ButtonGroup vertical>
      <Button onClick={() => setSubpage(2)}>Empty map</Button>
      <Button onClick={() => setSubpage(3)}>Create random map</Button>
      <Button onClick={() => setSubpage(0)}>Back</Button>
    </ButtonGroup>
  );

  function handleCreateEmptyMap(size: number) {
    dispatch(createEmptyMap(size));
    dispatch(setShowingMenu(false));
  }

  const subpageNewEmptyMap = () => (
    <>
      <div className="row justify-content-center">
        <div className="col-2 d-inline-flex align-items-center">
          Size
        </div>
        <div className="col-5 d-inline-flex align-items-center justify-content-between">
          <Button
            disabled={size <= 40}
            onClick={() => setSize(size - 5)}
          >
            -
          </Button>
          {size}
          <Button
            disabled={size >= 150}
            onClick={() => setSize(size + 5)}
          >
            +
          </Button>
        </div>
        <div className="col-2" />
      </div>
      <ButtonGroup vertical className="mt-4">
        <Button onClick={() => handleCreateEmptyMap(size)}>Create</Button>
        <Button onClick={() => setSubpage(1)}>Back</Button>
      </ButtonGroup>
    </>
  );

  let title: string;
  let subpageBody: JSX.Element;
  switch (subpage) {
    default:
      title = 'Map editor';
      subpageBody = subpageMain();
      break;
    case 1:
      title = 'New map';
      subpageBody = subpageNewMap();
      break;
    case 2:
      title = 'Create empty map';
      subpageBody = subpageNewEmptyMap();
      break;
    case 3:
      title = '';
      subpageBody = (
        <CreateRandomMapModal
          afterCreate={() => { dispatch(setScreen(ScreenEnum.LandScreen)); dispatch(setShowingMenu(false)); }}
          onCancel={() => setSubpage(1)}
        />
      );
      break;
  }

  return (
    <Modal className="editor-menu-modal" show centered onHide={() => dispatch(setShowingMenu(false))}>
      <Modal.Header>
        <Modal.Title>
          {title}
        </Modal.Title>
        <FullscreenButton />
      </Modal.Header>

      <Modal.Body className="text-center">
        {subpageBody}
      </Modal.Body>

      <Modal.Footer />
    </Modal>
  );
}
