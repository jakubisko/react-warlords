# Frontend

### npm i

Downloads and installs all dependencies for this project.

### npm start

Runs the game in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  
The page will reload if you edit source code.  
You will also see any lint errors in the console.  

### npm run clean

Removes folder `node_modules`.

### npm run build

Cleans folder `build`, then builds this app for production to that folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.  
The app is ready to be deployed.

### npm run preview

Runs production build.  
Enables local testing of the service worker's offline support.  
For example, you can check whether maps are cached - in the Developer Tools > Application > Service Workers, check "Offline", then pick a map.  
NOTE: After you end the testing, unregister the service worker in the Developer Tools so you develop without it.  

### firebase login
### firebase deploy

Deploys the production build to Firebase server.

### npm run upgrade-maps [optional map name without extension]

Whenever you change data model, all map files need to be converted to the newer format. Do the following:
1. In `serialization.tsx`, upgrade the buildVersion constant.
2. Implement the "convertMapData" function in the `upgradeMaps` file.
3. Test the conversion on one file by running `npm run upgrade-maps <map-name>`.
4. Run `npm run upgrade-maps` to convert all maps.

# Libraries

* vite - Frontend build tool and dev server
  https://www.npmjs.com/package/vite

* vite-tsconfig-paths - Resolve imports in vite using TypeScript's path mapping.
  https://www.npmjs.com/package/vite-tsconfig-paths

* workbox-build - Subpackage of Workbox. Build custom service worker to enable precaching of images and "warlords map" files for offline playing.  
  See file sw-build.js  
  https://developers.google.com/web/tools/workbox/modules/workbox-build  

* rimraf - Deletes directory; used during build.  
  https://www.npmjs.com/package/rimraf  

* ts-node - Run typescript files directly from the command line; used when upgrading map files to newer file format.  
  https://www.npmjs.com/package/ts-node  

* glob - Match files using the patterns the shell uses; used when upgrading map files to newer file format.  
  https://www.npmjs.com/package/foreach-cli  

* Bootstrap - front-end CSS framework. Uses popper as a dependency.  
  https://www.npmjs.com/package/bootstrap  
  https://www.npmjs.com/package/@popperjs/core  

* React Bootstrap - Components such as Modal.  
  https://react-bootstrap.github.io/  

* Redux - Predictable state container for JavaScript apps.  
  https://www.npmjs.com/package/redux  

* Redux Toolkit - Official, opinionated toolset for efficient Redux development  
  https://www.npmjs.com/package/@reduxjs/toolkit  
  It incorporates and reexports two previously stand-alone dependencies:  

    * Reselect - Library for writing selectors for Redux.  
      https://github.com/reduxjs/reselect  

    * Redux-thunk - Redux middleware for writing actions with logic.  
      https://github.com/reduxjs/redux-thunk  
      https://blog.jscrambler.com/async-dispatch-chaining-with-redux-thunk/  

* firebase - Functions for end-user client access to the Firebase. We use it for Firestore (NoSQL database with transactions).  
  https://www.npmjs.com/package/firebase  

* FastPriorityQueue.js - Used in Dijkstra's Algorithm for path-finding.  
  https://www.npmjs.com/package/fastpriorityqueue  

* FortAwesome - React wrapper for FontAwesome - font with icons.  
  https://www.npmjs.com/package/@fortawesome/react-fontawesome  

* JSZip - Creating and reading zip files when saving and loading data.  
  https://www.npmjs.com/package/jszip  

* HammerJS - Gestures on mobile phone.  
  https://www.npmjs.com/package/hammerjs  

* use-sound - React hook for sound effects. It uses Howler.js, and it's possible to directly call all of Howler's methods.  
  https://www.npmjs.com/package/use-sound  
  https://github.com/goldfire/howler.js  

* simplex-noise.js - Procedural noise used for creating random maps.  
  https://www.npmjs.com/package/simplex-noise  

* Comlink - Call Web Worker using asynchronous methods rather than using standard message event handling.  
  https://www.npmjs.com/package/comlink  
  https://github.com/FLGMwt/comlink-web-worker-in-create-react-app/  
