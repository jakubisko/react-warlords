import { GameState } from '../game/types';
import { EMPTY_ARRAY, createGrid } from '../../functions';
import FastPriorityQueue from 'fastpriorityqueue';
import { PlaceWithTime } from 'ai-interface/types/place';
import { MoveCostCalculatorImplUnchecked } from './MoveCostCalculatorImplUnchecked';
import { SearchPathArgs, TimeMatrix, Direction } from 'ai-interface/types/path';
import { shipMove, straightMoveCost, diagonalMoveCost } from 'ai-interface/constants/path';

const possibleDirections: Direction[] = [
  { rowOff: 1, colOff: 0, cost: straightMoveCost },
  { rowOff: -1, colOff: 0, cost: straightMoveCost },
  { rowOff: 0, colOff: 1, cost: straightMoveCost },
  { rowOff: 0, colOff: -1, cost: straightMoveCost },
  { rowOff: 1, colOff: 1, cost: diagonalMoveCost },
  { rowOff: 1, colOff: -1, cost: diagonalMoveCost },
  { rowOff: -1, colOff: 1, cost: diagonalMoveCost },
  { rowOff: -1, colOff: -1, cost: diagonalMoveCost }
];

/**
 * This is the first part of Dijkstra's algorithm.
 * 
 * Calculates the minimal travel time from the given place to the nearest place that passes the "isDestination" predicate.
 * All other places with at most that travel time will also have their times calculated.
 * For places farther than that, an arbitrary higher values of time will be returned; they should be ignored.
 */
export function calculateTimeMatrix(gameState: GameState, searchPathArgs: SearchPathArgs): TimeMatrix {
  const { size } = gameState;
  const { start, isToBeAvoided, isDestination, moveType, initialMoveLeft, maxDay = Infinity } = searchPathArgs;
  const { abilities, groupMoveOnLand } = moveType;

  const moveCostCalculator = new MoveCostCalculatorImplUnchecked(gameState, abilities);

  const days = createGrid(size, () => Infinity);
  days[start.row][start.col] = 0;
  const movesLeft = createGrid(size, () => 0);
  movesLeft[start.row][start.col] = initialMoveLeft;
  const directions = createGrid(size, () => possibleDirections[0]);

  const queue = new FastPriorityQueue<PlaceWithTime>((a, b) => a.day < b.day || (a.day === b.day && a.moveLeft > b.moveLeft));
  queue.add({
    row: start.row,
    col: start.col,
    day: 0,
    moveLeft: initialMoveLeft
  });

  let placeWithTime: PlaceWithTime | undefined;
  while ((placeWithTime = queue.poll()) !== undefined) {
    const { row, col, day, moveLeft } = placeWithTime;

    // It is possible that the place was inserted multiple times. We ignore places with time that was later improved.
    if (day !== days[row][col] || moveLeft !== movesLeft[row][col]) {
      continue;
    }

    if (day > maxDay) {
      break;
    }

    if (isDestination(row, col)) {
      return {
        moveType,
        start,
        destination: { row, col },
        maxCalculatedDays: day,
        minCalculatedMoveLeft: moveLeft,
        isCalculated: (row, col) => days[row][col] < day || (days[row][col] === day && movesLeft[row][col] >= moveLeft),
        days,
        movesLeft,
        directions
      };
    }

    for (const direction of possibleDirections) {
      const row2 = row + direction.rowOff;
      if (row2 < 0 || row2 >= size) {
        continue;
      }
      const col2 = col + direction.colOff;
      if (col2 < 0 || col2 >= size) {
        continue;
      }

      if (moveCostCalculator.isCoastCrossingForbiddenAt(row, col, row2, col2)) {
        continue;
      }

      const moveCost = moveCostCalculator.getMoveCostAt(row2, col2);
      if (moveCost === Infinity) {
        continue;
      }

      const oldDay2 = days[row2][col2];
      if (oldDay2 === Infinity && isToBeAvoided(row2, col2)) {
        continue;
      }

      const day2 = moveLeft <= 0 ? (day + 1) : day;
      let moveLeft2 = moveLeft
        + (moveLeft <= 0 ? (moveCostCalculator.isShipRequiredAt(row2, col2) ? shipMove : groupMoveOnLand) : 0)
        - moveCost * direction.cost;

      if (moveLeft2 > 0 && moveCostCalculator.isBoardingDeboardingShipAt(row, col, row2, col2)) {
        moveLeft2 = 1;
      }

      if (day2 > oldDay2 || (day2 === oldDay2 && moveLeft2 <= movesLeft[row2][col2])) {
        continue;
      }

      days[row2][col2] = day2;
      movesLeft[row2][col2] = moveLeft2;
      directions[row2][col2] = direction;
      queue.add({
        row: row2,
        col: col2,
        day: day2,
        moveLeft: moveLeft2
      });
    }
  }

  return {
    moveType,
    start,
    maxCalculatedDays: Infinity,
    minCalculatedMoveLeft: 0,
    isCalculated: () => true,
    days,
    movesLeft,
    directions
  };
}

/**
 * This is the second part of Dijkstra's algorithm.
 *
 * After calculating the matrix of the minimal travel times from the given place to other places, this calculates the path from
 * the place to given destination. Since we remember for each tile the direction from which it was reached, this is trivial.
 *
 * This method may be used to find path to any place that is not farther than the maxCalculatedDays. For farther places, and for
 * unreachable places, this method returns array with zero elements (you can't move on such path).
 *
 * Result path will contain both source and destination places. If source and destination is same, result will contain one place.
 */
export function calculatePathToPlaceFromTimeMatrix(timeMatrix: TimeMatrix, destRow: number, destCol: number): PlaceWithTime[] {
  // We're ignoring destination from the time matrix. This increases matrix reuse - user may search for a path to a different place.
  const { start: { row: srcRow, col: srcCol }, days, movesLeft, directions } = timeMatrix;

  if (!timeMatrix.isCalculated(destRow, destCol) || days[destRow][destCol] === Infinity) {
    return EMPTY_ARRAY as PlaceWithTime[];
  }

  const resultPath: PlaceWithTime[] = [];
  let row = destRow;
  let col = destCol;

  while (true) {
    resultPath.push({
      row,
      col,
      day: days[row][col],
      moveLeft: movesLeft[row][col]
    });

    if (row === srcRow && col === srcCol) {
      break;
    }

    const { rowOff, colOff } = directions[row][col];
    row -= rowOff;
    col -= colOff;
  }

  return resultPath.reverse();
}
