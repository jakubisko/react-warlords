import './ModalTitleWithFlags.scss';
import { PlayerFlag } from './PlayerFlag';
import { Modal } from 'react-bootstrap';

interface ModalTitleWithFlagsProps {
  caption: string;
  owner: number;
}

export const ModalTitleWithFlags = ({ caption, owner }: ModalTitleWithFlagsProps) => (
  <Modal.Title className="modal-title-with-flags">
    &nbsp;
    <span className="left">
      <PlayerFlag owner={owner} />
    </span>
    <span className="caption text-center prevent-cutting">
      {caption}
    </span>
    <span className="right">
      <PlayerFlag owner={owner} />
    </span>
  </Modal.Title>
);
