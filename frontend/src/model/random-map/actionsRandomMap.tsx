import { AppDispatch, RootState } from '../root/configureStore';
import { createEmptyMap, loadMap } from '../game/actionsEditor';
import { createRandomMapTerrain } from './actionsRandomMapTerrain';
import { createRandomMapStructures } from './actionsRandomMapStructures';
import { createRandomMapRoads } from './actionsRandomMapRoads';
import { getEmptyMap } from '../game/saveLoad';
import { RandomMapSettings } from '../editor/types';
import { MapData } from '../game/types';
import { neutralPlayerId } from 'ai-interface/constants/player';

const createOneNewRandomMap = (settings: RandomMapSettings) => (dispatch: AppDispatch) => {
  dispatch(createEmptyMap(settings.size));
  dispatch(createRandomMapTerrain(settings));
  dispatch(createRandomMapStructures(settings));
  dispatch(createRandomMapRoads(settings));
};

export const createNewRandomMap = (settings: RandomMapSettings) => (dispatch: AppDispatch, getState: () => RootState) => {
  let bestScore = -1;
  let bestMap: MapData = getEmptyMap(0);

  // Generate multiple maps and pick the one with the best score. This prevents degenerate maps.
  const samples = Math.min(5, (150 / settings.size) ** 2);
  for (let i = 0; i < samples; i++) {
    dispatch(createOneNewRandomMap(settings));

    const map: MapData = getState().game;
    const score = calculateScoreOfMap(map);
    if (score > bestScore) {
      bestScore = score;
      bestMap = map;
    }
  }

  // This will: 1. calculate background sprites; 2. reset undo queue if in editor; 3. initialize players and random armies if playing.
  const { programMode } = getState().ui;
  dispatch(loadMap(bestMap, programMode));
};

/** Some random maps are not very good. This method assigns score to the map - lower score for worse maps. */
function calculateScoreOfMap(map: MapData): number {
  let score = 0;

  const startingCities = Object.values(map.cities).filter(city => city.owner !== neutralPlayerId);
  if (startingCities.length < 2) {
    return 0;
  }

  // If the generator wasn't even able to fit the requested number of players on the map, the score will be really bad.
  score += 10_000 * startingCities.length;

  // Large distances between starting cities are preferred.
  for (let i = 0; i < startingCities.length - 1; i++) {
    for (let j = i + 1; j < startingCities.length; j++) {
      const { row: row1, col: col1 } = startingCities[i];
      const { row: row2, col: col2 } = startingCities[j];
      const dist = ((row1 - row2) ** 2 + (col1 - col2) ** 2) ** 0.5;
      score += dist ** 0.85; // Large distances shouldn't have high weight
    }
  }

  return score;
}
