import { Place } from '../types/place';
import { MoveType, SearchPathArgs, TimeMatrix, BasicAvoidingStrategy } from '../types/path';
import {
  StringTooLong, IllegalPlace, YouDontControlSuchGroup, NotEnoughGold, SuchArmyTypeDoesntExist, SuchCityDoesntExist, YouDontControlSuchCity,
  CityIsRazed, CityDoesntHaveSuchTrainingFacility, CityAlreadyHasSuchTrainingFacility, CityCantHaveMoreTrainingFacilities,
  CityCantHaveMoreIncomingCaravans, NoHeroOffer, NoArmiesPresent, NotEnoughFreeSpaceAtPlace, SuchFlaggableDoesntExist,
  SuchRuinDoesntExist, GroupDoesntContainSuchArmy, SourceAndDestinationAreIdentical, YouDontControlSuchHero, SuchArtifactDoesntExist,
  HeroCantCarryMoreArtifacts, NoSuchArtifactAtPlace, HeroDoesntCarrySuchArtifact, PlaceNotExplorable, IllegalMoveType,
  PathPlacesAreNotAdjacent, ZeroLengthPath, ImpassableTerrain, CrossingCoastOffPort, IllegalGroupSize, IllegalInitialMoveLeft,
  DestinationIsOutsideOfCalculatedValues, NoMoveLeft, GroupsDontStandAtSamePlace, IllegalOrdering, SuchArmyTypeCantBeTrained, SamePlayer, ValueOutOfRange
} from '../types/obstacle';
import { SageTowerChoice } from '../types/exploring';

/**
 * API to be used by your custom AI.
 * 
 * Use GameApiObstacle to check whether a method of GameApi would throw an error; without calling the method.
 *
 * Methods return null if arguments are valid and the action is allowed by the rules. Otherwise an object holding the error type and text
 * of the problem is returned.
 */
export interface GameApiObstacle {

  //*********** Global ***********/
  saveState(state: string): StringTooLong | null;
  setMyOrdering(armyTypeIds: string[]): SuchArmyTypeDoesntExist | IllegalOrdering | null;
  getMyArmiesAt(place: Place): IllegalPlace | null;
  getOwnerOfPlace(place: Place): IllegalPlace | null;

  //*********** Cities ***********/
  getCity(cityId: string): SuchCityDoesntExist | null;
  setTrainingInCity(cityId: string, armyTypeId?: string): SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained | SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | CityDoesntHaveSuchTrainingFacility | null;
  buildTrainingFacility(cityId: string, armyTypeId: string): NotEnoughGold | SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained | SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | CityAlreadyHasSuchTrainingFacility | CityCantHaveMoreTrainingFacilities | null;
  demolishTrainingFacility(cityId: string, armyTypeId: string): NotEnoughGold | SuchArmyTypeDoesntExist | SuchArmyTypeCantBeTrained | SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | CityDoesntHaveSuchTrainingFacility | null;
  setCaravanFromCityToCity(fromCityId: string, toCityId?: string): SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | SourceAndDestinationAreIdentical | CityCantHaveMoreIncomingCaravans | null;
  razeCity(cityId: string): SuchCityDoesntExist | YouDontControlSuchCity | CityIsRazed | NotEnoughGold | NoArmiesPresent | null;

  //*********** Flaggable structures ***********/
  getFlaggable(flaggableId: string): SuchFlaggableDoesntExist | null;

  //*********** Ruins, Sage Tower ***********/
  getRuin(ruinId: string): SuchRuinDoesntExist | null;
  exploreRuin(heroId: string): YouDontControlSuchHero | PlaceNotExplorable | null;
  exploreSageTower(heroId: string, choice: SageTowerChoice): YouDontControlSuchHero | PlaceNotExplorable | null;
  getNumberOfCitiesByWhichPlayerIsBehindBestEnemy(playerId: number): ValueOutOfRange | null;

  //*********** Groups ***********/
  getPlaceOfGroup(groupId: string): YouDontControlSuchGroup | null;
  dismissGroup(groupId: string): YouDontControlSuchGroup | null;

  //*********** Armies ***********/
  getArmiesInGroup(groupId: string): YouDontControlSuchGroup | null;
  transferArmyFromGroupToOtherGroup(armyId: string, srcGroupId: string, destGroupId: string): YouDontControlSuchGroup | SourceAndDestinationAreIdentical | GroupDoesntContainSuchArmy | GroupsDontStandAtSamePlace | null;
  transferArmyFromGroupToNewGroup(armyId: string, srcGroupId: string): YouDontControlSuchGroup | GroupDoesntContainSuchArmy | null;
  dismissArmy(groupId: string, armyId: string): YouDontControlSuchGroup | GroupDoesntContainSuchArmy | null;
  getMyArmiesAt(place: Place): IllegalPlace | null;
  getArmyTypeIdsAt(place: Place): IllegalPlace | null;

  //*********** Heroes ***********/
  acceptHeroOffer(): NoHeroOffer | NotEnoughGold | CityIsRazed | NotEnoughFreeSpaceAtPlace | null;
  getHero(heroId: string): YouDontControlSuchHero | null;
  getArtifactIdsOnGround(place: Place): IllegalPlace | null;
  pickArtifactFromGround(heroId: string, artifactId: string): SuchArtifactDoesntExist | YouDontControlSuchHero | HeroCantCarryMoreArtifacts | NoSuchArtifactAtPlace | null;
  dropArtifactOntoGround(heroId: string, artifactId: string): SuchArtifactDoesntExist | YouDontControlSuchHero | HeroDoesntCarrySuchArtifact | null;

  //*********** Group move ***********/
  getMoveTypeOfGroup(groupId: string): YouDontControlSuchGroup | null;
  getMoveTypeOfArmyTypes(armyTypeIds: string[]): IllegalGroupSize | SuchArmyTypeDoesntExist | null;
  createBasicAvoidingFunction(groupSize: number, strategy: BasicAvoidingStrategy): IllegalGroupSize | null;
  calculateTimeMatrix(searchPathArgs: SearchPathArgs): IllegalPlace | IllegalMoveType | IllegalInitialMoveLeft | null;
  calculatePathToPlaceFromTimeMatrix(timeMatrix: TimeMatrix, destination: Place): IllegalPlace | DestinationIsOutsideOfCalculatedValues | null;
  calculateTimesOnPath(path: Place[], moveType: MoveType, initialMoveLeft: number): IllegalMoveType | ZeroLengthPath | IllegalPlace | PathPlacesAreNotAdjacent | NotEnoughFreeSpaceAtPlace | ImpassableTerrain | CrossingCoastOffPort | IllegalInitialMoveLeft | null;
  getMoveLeftOfGroup(groupId: string): YouDontControlSuchGroup | null;
  moveGroupOneStep(groupId: string, destination: Place): YouDontControlSuchGroup | NoMoveLeft | ZeroLengthPath | IllegalPlace | PathPlacesAreNotAdjacent | NotEnoughFreeSpaceAtPlace | ImpassableTerrain | CrossingCoastOffPort | null;

  //*********** Combat ***********/
  predictCombatIfGroupAttacks(attackingGroupId: string, defendingPlace: Place): IllegalPlace | YouDontControlSuchGroup | NoArmiesPresent | SamePlayer | null;
  predictCombatIfAllArmiesAtPlaceAttack(attackingPlace: Place, defendingPlace: Place): IllegalPlace | NoArmiesPresent | SamePlayer | null;

  //*********** Unexplored ***********/
  isPlaceUnexplored(place: Place): IllegalPlace | null;

}
