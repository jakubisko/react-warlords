import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { Place } from 'ai-interface/types/place';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { Task, NOP } from './helpers/types';
import { Army, Abilities } from 'ai-interface/types/army';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { Ability } from 'ai-interface/constants/ability';
import { Personality } from './helpers/Personality';

export class TransferManager {

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly personality: Personality
  ) {
  }

  /**
   * NOTE: Original implementation returned constant 100 and was playing somewhat reasonably. Thus that is a good reference value.
   * Returning too big utility will lead to wasting a lot of time on creating groups instead of doing something useful.
   */
  private utilityGainedByJoiningGroups(armies1: Army[], armies2: Army[]): number {
    let utility = 100;

    // It is not preferred that a big group should chase a small one.
    utility -= 10 * armies1.length;

    // Full groups are preferred.
    if (armies1.length + armies2.length === maxTileCapacity) {
      utility += 25;
    }

    // Group with a Hero always wants many armies. However, we don't want two heroes in one group. Leadership doesn't stack and experiences are split.
    if (armies1.some(army => standardArmyTypes[army.armyTypeId].hero)) {
      if (armies2.some(army => standardArmyTypes[army.armyTypeId].hero)) {
        utility -= 80;
      } else {
        utility += 8 * armies2.reduce((sum, army) => sum + standardArmyTypes[army.armyTypeId].strength, 0);
      }
    } else if (armies2.some(army => standardArmyTypes[army.armyTypeId].hero)) {
      utility += 8 * armies1.reduce((sum, army) => sum + standardArmyTypes[army.armyTypeId].strength, 0);
    }

    // Slowing down a group decreases its utility. Slowing more armies is more severe.
    const movePerTurn1 = Math.min(...armies1.map(army => army.moveLeft)) / 10;
    const movePerTurn2 = Math.min(...armies2.map(army => army.moveLeft)) / 10;
    utility -= (movePerTurn1 > movePerTurn2) ? ((movePerTurn1 - movePerTurn2) * armies2.length) : ((movePerTurn2 - movePerTurn1) * armies1.length);

    // For each group collect abilities that at least one member of the group holds.
    const abilities1: Abilities = {};
    for (const { armyTypeId } of armies1) {
      for (const ability in standardArmyTypes[armyTypeId].abilities) {
        abilities1[ability as Ability] = 1;
      }
    }

    const abilities2: Abilities = {};
    for (const { armyTypeId } of armies2) {
      for (const ability in standardArmyTypes[armyTypeId].abilities) {
        abilities2[ability as Ability] = 1;
      }
    }

    // Group bonuses such as Morale get better for each army in the group.
    // Canceling enemy bonus or penalty works similarly - we can say that "+1 to our Group" is same as cancelling enemy's
    // "+1 to our group" or "-1 to enemy Group". Difference is that such bonus applies only when an enemy actually has such an ability.
    const utilityOfAbility = (ability: Ability, utilityPerArmy: number) => {
      if (abilities1[ability]) {
        if (!abilities2[ability]) {
          return utilityPerArmy * armies2.length;
        }
      } else if (abilities2[ability]) {
        return utilityPerArmy * armies1.length;
      }
      return 0;
    };

    utility += utilityOfAbility(Ability.NoPenaltyInSwamp, 4)
      + utilityOfAbility(Ability.NoPenaltyInForest, 4)
      + utilityOfAbility(Ability.NoPenaltyInHills, 4)
      + utilityOfAbility(Ability.NoPenaltyInDesert, 2)
      + utilityOfAbility(Ability.NoPenaltyOnIce, 2)
      + utilityOfAbility(Ability.MoraleBonus, 20)
      + utilityOfAbility(Ability.FrighteningBonus, 20)
      + utilityOfAbility(Ability.AntiAirBonus, 6)
      + utilityOfAbility(Ability.CancelEnemyTerrainBonus, 10)
      + utilityOfAbility(Ability.CancelEnemyFortificationBonus, 16)
      + utilityOfAbility(Ability.CancelEnemyMoraleBonus, 4)
      + utilityOfAbility(Ability.CancelEnemyFrighteningBonus, 4)
      + utilityOfAbility(Ability.LowerEnemyAmbush, 6)
      + utilityOfAbility(Ability.LowerEnemyLeadership, 4)
      + utilityOfAbility(Ability.IncreaseGroupAmbush, 2);

    // Flying group prefers joining with other flying groups and dislikes non-flying groups.
    const flies1 = armies1.every(army => standardArmyTypes[army.armyTypeId].abilities[Ability.Flies]);
    const flies2 = armies2.every(army => standardArmyTypes[army.armyTypeId].abilities[Ability.Flies]);
    if (flies1 && flies2) {
      utility += 40;
    } else if (flies1 && !flies2) {
      utility -= 40 * armies1.length;
    } else if (!flies1 && flies2) {
      utility -= 40 * armies2.length;
    }

    // TODO - join groups when strong enemy is near

    return utility * (1 + 2 * this.personality.likesBigGroups);
  }

  /**
   * This only transfers from this group to the other.
   * REASON: This action destroys group from which armies are transferred. We don't want to remove the other group from the list of all our
   * groups. That would require recalculation.
   */
  public transferFromThisGroup(groupId: string, place: Place): Task {
    let utility = 0;
    let action = NOP;

    const armiesAtPlace = this.game.getMyArmiesAt(place);
    if (armiesAtPlace.length === 0) {
      return { utility, action };
    }

    // Joining a group in a city is forbidden to prevent joining the guards after they were split from the group.
    const cityId = this.land.getCityIdAt(place);
    if (cityId !== undefined) {
      return { utility, action };
    }

    const theseArmies = this.game.getArmiesInGroup(groupId);

    const otherGroupIds = armiesAtPlace
      .map(army => army.groupId)
      .filter(groupId2 => groupId2 !== groupId)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos); // Remove duplicates

    for (const otherGroupId of otherGroupIds) {
      const otherArmies = armiesAtPlace.filter(army => army.groupId === otherGroupId);
      const utility2 = this.utilityGainedByJoiningGroups(theseArmies, otherArmies);

      if (utility2 > utility) {
        utility = utility2;
        action = () => theseArmies.forEach(army => this.game.transferArmyFromGroupToOtherGroup(army.id, groupId, otherGroupId));
      }
    }

    return { utility, action };
  }

  /** Used when debugging. Map editor doesn't support placing groups. With this, all armies standing at one place will become a group. */
  public joinAllArmiesStandingTogether() {
    const groupIds = this.game.getAllMyGroupIds();
    for (const groupId of groupIds) {
      if (this.game.groupExists(groupId)) {
        const place = this.game.getPlaceOfGroup(groupId);
        const armies = this.game.getMyArmiesAt(place);
        for (const army of armies) {
          if (army.groupId !== groupId) {
            this.game.transferArmyFromGroupToOtherGroup(army.id, army.groupId, groupId);
          }
        }
      }
    }
  }

}
