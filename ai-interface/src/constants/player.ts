/** Number of playable players. Limited by available sprites. Indexed 0 .. maxPlayers - 1 */
export const maxPlayers = 8;

/** Index of the neutral player. Placed after all playable players. Also used for the team of the neutral player. */
export const neutralPlayerId = maxPlayers;

/** Default names for each player. User may change these names when starting a new game. */
export const defaultPlayerNames = Object.freeze(['White', 'Yellow', 'Orange', 'Red', 'Green', 'Teal', 'Blue', 'Black', 'Neutral']);
