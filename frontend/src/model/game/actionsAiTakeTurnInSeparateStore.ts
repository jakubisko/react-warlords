/**
 * This file will be executed by a web worker.
 * Web worker shouldn't import directly nor indirectly React. If it does, you'll get errors.
 */
import { expose } from 'comlink';
import { aiFunctions } from '../../listOfAIs';
import { DrawApiImpl, DrawApiImplEmpty } from '../api/DrawApiImpl';
import { GameApiImpl } from '../api/GameApiImpl';
import { GameApiObstacleImpl } from '../api/GameApiObstacleImpl';
import { LandApiImpl } from '../api/LandApiImpl';
import { storeWithoutUi } from '../root/configureStore';
import { AppDispatch, RootState } from '../root/configureStore';
import { setGameState } from './actionsTurnPassing';
import { GameState } from './types';
import { initialState } from './reducersPlaying';

expose(aiTakeTurnInSeparateStore);

/**
 * Calculates one turn for an AI.
 * Uses a second Redux store that isn't connected to the Provider,  is not used by React, and isn't connected to Firebase.
 * Calculates the AI turn on the web worker, so that the UI won't freeze or timeout during AI's turn.
 * When AI isn't currently taking the turn, the content of the store is irrelevant and we clean it to save memory.
 */
export default function aiTakeTurnInSeparateStore(gameState: GameState): GameState {
  const { dispatch, getState } = storeWithoutUi;

  dispatch(setGameState(gameState));
  dispatch(aiTakeTurn());
  const gameStateAfter = getState().game;
  dispatch(setGameState(initialState));

  return gameStateAfter;
}

const aiTakeTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game, game: { players, currentPlayerId, aiOutputVisible } } = getState();
  const { aiName } = players[currentPlayerId];

  const landApi = new LandApiImpl(game);
  const obstacleApi = new GameApiObstacleImpl(getState);
  const gameApi = new GameApiImpl(currentPlayerId, dispatch, getState, obstacleApi);
  const drawApi = aiOutputVisible ? new DrawApiImpl(dispatch) : new DrawApiImplEmpty();

  if (aiName === undefined) {
    throw new Error(`Player ${currentPlayerId} is not an AI, yet a method was called as if it was.`);
  }
  const ai = aiFunctions[aiName];
  if (ai === undefined) {
    throw new Error(`AI with name "${aiName}" not found!`);
  }
  ai(landApi, gameApi, obstacleApi, drawApi);
};
