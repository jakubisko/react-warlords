import { Place } from '../types/place';

/**
 * API to be used by your custom AI.
 *
 * Use methods of this class to draw debug text on the screen.
 */
export interface DrawApi {

  /**
   * The text will always be visible.
   * If the place is given, the text will be drawn on the land. Otherwise, it will stick to the left side of the screen.
   */
  drawText(text: string, place?: Place): void;

  /**
   * The text will be visible only when given group is yours and is selected.
   * If the place is given, the text will be drawn on the land. Otherwise, it will stick to the left side of the screen.
   */
  drawTextWhenGroupIsSelected(text: string, groupId: string, place?: Place): void;

  /**
   * The text will be visible only when given city is yours and is selected.
   * If the place is given, the text will be drawn on the land. Otherwise, it will stick to the left side of the screen.
   */
  drawTextWhenCityIsSelected(text: string, cityId: string, place?: Place): void;

  /**
   * The text will be visible only when no group or city is selected.
   * If the place is given, the text will be drawn on the land. Otherwise, it will stick to the left side of the screen.
   */
  drawTextWhenNothingIsSelected(text: string, place?: Place): void;

}
