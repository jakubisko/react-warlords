import './AbilitiesIcons.scss';
import { IconImage } from './IconImage';
import { Abilities } from 'ai-interface/types/army';
import { Ability, abilityToText } from 'ai-interface/constants/ability';

interface AbilitiesIconsProps {
  abilities: Abilities;
  tooltipPlacement?: 'left' | 'right' | 'top' | 'bottom';
}

export const AbilitiesIcons = ({ abilities, tooltipPlacement }: AbilitiesIconsProps) => (
  <div className="abilities-icons">
    {Object.keys(abilities).map(ability =>
      <IconImage
        key={ability}
        icon={ability as Ability}
        tooltip={abilityToText[ability as Ability]
          .replace('{0}', '' + abilities[ability as Ability])
          .replace('+', (abilities[ability as Ability] ?? 0) > 0 ? '+' : '')}
        tooltipPlacement={tooltipPlacement}
      />
    )}
  </div>
);
