import './SpyReportModal.scss';

import { ReactNode, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { PlayerFlag } from '../common/PlayerFlag';
import { getSpyReport } from '../../model/game/selectorsPlaying';
import ArmyImage from '../common/ArmyImage';
import { setShowingSpyReport } from '../../model/ui/actions';
import PageableList from '../common/PageableList';

export default function SpyReportModal() {
  const players = useSelector((state: RootState) => state.game.players);
  const spyReport = useSelector((state: RootState) => getSpyReport(state, state.game.currentPlayerId));
  const winCondition = useSelector((state: RootState) => state.game.winCondition);
  const dispatch: AppDispatch = useDispatch();

  function spyDenRow(caption: string, values: (number | undefined)[]): JSX.Element {
    return (
      <div className="row">
        <div className="col-4 py-2">{caption}</div>
        {values.filter((_, playerId) => players[playerId].alive).map((value, i) => (
          <div key={i} className="col-1 px-0 py-2 text-center overflow-hidden">
            {value! > 999 ? <small>{value}</small> : value}
          </div>
        ))}
      </div>
    );
  }

  const { mapExplored, heroes, bestHeroLevel, artifacts, cities, gold, income, armiesUpkeep, bestArmyTypeId } = spyReport;
  const rows: ReactNode[] = [
    spyDenRow('Team', players.map(player => player.team + 1)),
    spyDenRow('Map explored %', mapExplored),
    spyDenRow('Heroes', heroes),
    spyDenRow('Best hero level', bestHeroLevel),
    spyDenRow('Artifacts', artifacts),
    <div className="row" key={0}>
      <div className="col-4 py-2">Best army</div>
      {bestArmyTypeId.filter((_, playerId) => players[playerId].alive).map((armyTypeId, i) => (
        <div key={i} className="col-1 p-0 text-center">
          <ArmyImage
            armyTypeId={armyTypeId ?? null}
            alwaysSmall={true}
            onLeftClick="INFO"
            onRightClick="INFO"
          />
        </div>
      ))}
    </div>,
    spyDenRow('Cities', cities),
    spyDenRow('Gold', gold),
    spyDenRow('Income', income),
    spyDenRow('Armies upkeep', armiesUpkeep),
    <div className="row" key={1}>
      <div className="col-4 py-2">Victory condition</div>
      <div className="col-8 py-2">{winCondition}</div>
    </div>
  ];

  return (
    <Modal className="spy-report-modal overflow-hidden" show centered onHide={() => dispatch(setShowingSpyReport(false))}>
      <Modal.Header>
        <Modal.Title>Spy report</Modal.Title>
      </Modal.Header>

      <Modal.Body className="pb-0">
        <div className="container">
          <div className="row">
            <div className="col-4" />
            {players.map((player, playerId) => player.alive
              ? (
                <OverlayTrigger
                  key={playerId}
                  placement="top"
                  overlay={
                    <Tooltip id={`srm-${playerId}`}>{player.name}</Tooltip>
                  }>
                  <div className="col-1 px-0 py-2" key={playerId}>
                    <PlayerFlag owner={playerId} size={1} />
                  </div>
                </OverlayTrigger>
              )
              : null
            )}
          </div>

          <PageableList
            list={rows}
            maxItemsPerPage={11}
            itemHeightPx={36}
            restOfHeightPx={165}
            footer={<Button onClick={() => dispatch(setShowingSpyReport(false))}>Close</Button>}
          />
        </div>
      </Modal.Body>
    </Modal>
  );
}
