import { Howl } from 'howler';
import { soundEffectsSprite } from '../../model/sound/types';
import soundEffectsSfx from './effects/soundEffects.mp3';

/** One global instance of Howl for sound effects. */
export const soundEffects = new Howl({
  src: [soundEffectsSfx],
  sprite: soundEffectsSprite,
  volume: 0.4
});
