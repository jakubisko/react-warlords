import { Place } from 'ai-interface/types/place';

/**
 * This state contains only data that are related to UI and are temporary. None of it matters to AI.
 * None of it is saved when land or game is saved.
 */
export interface UiState {
  readonly errorTitle: string | null; // Title of error message dialog.
  readonly errorText: string | null; // Text of error message dialog.

  readonly programMode: ProgramMode; // Whether we're playing or we're in the map editor.
  readonly screen: ScreenEnum; // Which screen is displayed to the user.

  readonly showingMenu: boolean; // Whether to display a GameMenuModal / EditorMenuModal.
  readonly showingCityId: string | null; // Displays CityModal with commands for given city.
  readonly showingPlaceInfo: Place | null; // Displays PlaceInfoModal with description of given place.
  readonly showingHeroId: string | null; // Displays HeroModal for given hero (use his Army.id).
  readonly showingWarReport: boolean; // Displays ReportModal.
  readonly showingSpyReport: boolean; // Displays SpyReportModal.
  readonly showingHeroOffer: boolean; // Displays HeroOfferModal.
  readonly showingCombat: boolean; // Displays CombatModal.
  readonly showingRuinExploring: boolean; // Displays RuinExploringModal.
  readonly showingManual: boolean; // Displays ManualModal.
  readonly showingArmyTypeId: string | null; // Displays ArmyTypeModal for given army type.
  readonly showingToastMessage: string | null; // Displays Toast with text that hides itself later.

  readonly tacticalMapWidth: number; // Visible width of tactical map in tiles. Non-integers are allowed.
  readonly tacticalMapHeight: number; // Visible height of tactical map in tiles. Non-integers are allowed.
  readonly pixelsPerTile: number; // How big is one tile rendered on TacticalMap in pixels.
  readonly focusedRow: number; // Place of land currently in the center of tactical map. Non-integers are allowed.
  readonly focusedCol: number; // Place of land currently in the center of tactical map. Non-integers are allowed.
  readonly activeGroupId: string | null; // Id of the currently selected Group.
  readonly lastActiveGroupId: string; // Used by "select next group" button. Id of the last selected Group. It's ok if it no longer exists.
}

export enum ProgramMode {
  Playing,
  MapEditor
}

export enum ScreenEnum {
  InitialLoadingScreen,

  MainMenuScreen,
  OnlineLoginScreen,
  ListOfOnlineGamesScreen,
  NewGameMapScreen,
  NewGamePlayersScreen,
  NewGameOptionsScreen,

  LandScreen,
  ZoomedMapScreen,

  WaitScreen,
  AiTakingTurnScreen,
  AiDisqualifiedScreen,
  StartOfTurnScreen,
  DuplicateStartOfTurnScreen,
  OnlineSavingFailedScreen,
  GameOverScreen,
  TimelineScreen,

  FatalErrorScreen
}

/** Throwing this error won't result in a fatal error. It's message will be shown to the user. */
export class UserError extends Error {
  constructor(userMessage: string, cause?: unknown) {
    super(userMessage, cause === undefined ? undefined : { cause });
  }
}
