import { AI } from 'ai-interface/AI';
import { takeTurn as pioneerTakeTurn } from 'ai-pioneer/main';

/** All available AIs. Keys are displayed in user interface, so they should be user friendly. */
export const aiFunctions: { [aiName: string]: AI } = {
  'Computer': pioneerTakeTurn,
};
