# AI Pioneer

AI capable of playing the game with other players.

# Simple overview of the algorithm:
1. For each group, generate all possible moves within some distance.
2. Calculate the *expected utility* of each move. (Sum of all event probabilities times their value determined by a heuristic).
3. Decrease the utility for higher distances.
4. Execute the move with the highest utility.
5. Repeat until no group has movement left.

[Article about the algorithm](https://www.gamasutra.com/view/feature/1535/designing_ai_algorithms_for_.php)

### npm i

Downloads and installs all dependencies for this project into folder `node_modules`.

### npm run build

Builds this library to the `lib` folder.

### npm run clean

Removes folders `node_modules` and `lib`.
