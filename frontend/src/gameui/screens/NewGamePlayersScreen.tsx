import './NewGamePlayersScreen.scss';
import { useCallback, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal } from 'react-bootstrap';
import { PlayerFlag } from '../common/PlayerFlag';
import { ScreenEnum } from '../../model/ui/types';
import { setScreen } from '../../model/ui/actions';
import { createArray } from '../../functions';
import { setPlayerAiName, setPlayerName, setPlayerOnlineUid, setPlayerTeam } from '../../model/game/actionsTurnPassing';
import { defaultPlayerNames, neutralPlayerId } from 'ai-interface/constants/player';
import { aiFunctions } from '../../listOfAIs';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { useOnlineUsers } from '../../model/online/useOnlineUsers';

function validateName(newName: string, allNames: string[]): string | null {
  if (newName.length === 0) {
    return 'Please enter player name';
  }
  if (newName === defaultPlayerNames[neutralPlayerId]) {
    return 'This name is reserved';
  }
  if (allNames.includes(newName)) {
    return 'This name is already used';
  }
  return null;
}

export default function NewGamePlayersScreen() {
  const players = useSelector((state: RootState) => state.game.players);
  const uidIfOnline = useSelector((state: RootState) => state.online.onlineGameId === null ? null : state.online.loggedInUser?.uid ?? null);
  const dispatch: AppDispatch = useDispatch();

  const [editPlayerId, setEditPlayerId] = useState<number | null>(null);
  const [nameValidationError, setNameValidationError] = useState<string | null>(null);

  const { users, loadingUsers } = useOnlineUsers();

  const handleChangeName = useCallback((newName: string) => {
    const allNames = players.map(player => player.name)
      .filter((x, playerId) => playerId < neutralPlayerId && playerId !== editPlayerId! && players[playerId].alive);

    dispatch(setPlayerName(editPlayerId!, newName));
    setNameValidationError(validateName(newName, allNames));
  }, [players, editPlayerId, dispatch]);

  function AiOptionsOffline() {
    const { aiName } = players[editPlayerId!];

    function handleChangeAiSelect(newAiName: string) {
      dispatch(setPlayerAiName(editPlayerId!, newAiName === '' ? undefined : newAiName));
    }

    return (
      <select
        className="form-control"
        value={aiName ?? ''}
        onChange={e => handleChangeAiSelect(e.target.value)}
      >
        <option value={''}>
          Human
        </option>

        {Object.keys(aiFunctions).map(aiName => (
          <option key={aiName} value={aiName}>
            {aiName}
          </option>
        ))}
      </select>
    );
  }

  function AiOptionsOnline() {
    const { aiName, onlineUid } = players[editPlayerId!];

    function handleChangeAiSelect(newAiName: string) {
      dispatch(setPlayerAiName(editPlayerId!, newAiName in aiFunctions ? newAiName : undefined));

      dispatch(setPlayerOnlineUid(
        editPlayerId!,
        newAiName in aiFunctions ? undefined : newAiName
      ));

      let name = (newAiName in aiFunctions
        ? undefined
        : users.find(({ uid }) => uid === newAiName)?.displayName);
      name = name ?? defaultPlayerNames[editPlayerId!];
      name = name.trim().split(' ')[0].substring(0, 10);
      if (name.length > 0) {
        name = name.charAt(0).toLocaleUpperCase() + name.substring(1).toLocaleLowerCase();
      }
      handleChangeName(name);
    }

    if (loadingUsers) {
      return <div className="m-2">Loading list of users...</div>;
    }

    return (
      <select
        className="form-control"
        value={aiName ?? onlineUid}
        onChange={e => handleChangeAiSelect(e.target.value)}
      >
        {Object.keys(aiFunctions).map(aiName => (
          <option key={aiName} value={aiName}>
            {aiName}
          </option>
        ))}

        {users.map(({ uid, displayName }) => (
          <option key={uid} value={uid}>
            {displayName}
          </option>
        ))}
      </select>
    );
  }

  function editPlayer() {
    const { name, team } = players[editPlayerId!];
    const availableTeams = createArray(players.filter(player => player.alive).length, i => i);

    return (
      <Modal className="new-game-players-submodal overflow-hidden" show centered onHide={() => setEditPlayerId(null)}>
        <Modal.Header>
          <Modal.Title>
            Player configuration
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <input
            className="form-control"
            type="text"
            placeholder="Enter name"
            maxLength={10}
            value={name}
            onChange={e => handleChangeName(e.target.value)}
          />
          {nameValidationError !== null && <div className="error-text">{nameValidationError}</div>}

          <select
            className="form-control"
            value={team}
            onChange={e => dispatch(setPlayerTeam(editPlayerId!, parseInt(e.target.value)))}
          >
            {availableTeams.map(i => (
              <option key={i} value={i}>
                Team {i + 1}
              </option>
            ))}
          </select>

          {uidIfOnline === null ? <AiOptionsOffline /> : <AiOptionsOnline />}
        </Modal.Body>

        <Modal.Footer className="justify-content-center">
          <ButtonWithShortcut
            shortcutCode="Enter"
            allowShortcutWhileModalIsOpen={true}
            disabled={nameValidationError !== null}
            onClick={() => setEditPlayerId(null)}
          >
            OK
          </ButtonWithShortcut>
        </Modal.Footer>
      </Modal>
    );
  }

  function UserDescriptionOffline({ playerId }: { playerId: number }) {
    return <span>{players[playerId].aiName ?? 'Human'}</span>;
  }

  function UserDescriptionOnline({ playerId }: { playerId: number }) {
    const { aiName, onlineUid } = players[playerId];
    const description = aiName ?? (loadingUsers
      ? 'Loading...'
      : (users.find(({ uid }) => uid === onlineUid)?.displayName ?? `Unknown user`)
    );
    return <span>{description}</span>;
  }

  const List = () => (
    <div className="container">
      {players.map((x, playerId) => playerId).filter(playerId => players[playerId].alive).map(playerId => (
        <div
          className="row"
          key={playerId}
          onClick={() => setEditPlayerId(playerId)}
        >
          <div className="col-1 px-0 text-center">
            {players[playerId].team + 1}
          </div>
          <div className="col-3 prevent-cutting">
            {players[playerId].name}
          </div>
          <div className="col-3">
            <PlayerFlag owner={playerId} />
          </div>
          <div className="col-5 prevent-cutting">
            {uidIfOnline === null ? <UserDescriptionOffline playerId={playerId} /> : <UserDescriptionOnline playerId={playerId} />}
          </div>
        </div>
      ))}
    </div>
  );

  function handleStartNewGame() {
    dispatch(setScreen(ScreenEnum.NewGameOptionsScreen));
  }

  let problem = '';
  if (players
    .filter(({ alive }) => alive)
    .map(({ team }) => team)
    .filter((elem, pos, arr) => arr.indexOf(elem) === pos) // Remove duplicates from the list of used teams
    .length <= 1
  ) {
    problem = 'At least 2 teams required';
  } else if (uidIfOnline !== null && !players.some(({ alive, onlineUid }) => alive && onlineUid === uidIfOnline)) {
    problem = 'You must be one of the players';
  }

  return (
    <Modal className="new-game-players-screen screen overflow-hidden" animation={false} show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>Player configuration</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <List />
        {editPlayerId !== null && editPlayer()}
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={() => dispatch(setScreen(ScreenEnum.NewGameMapScreen))}>
          Back
        </Button>

        <Button
          disabled={problem !== ''}
          onClick={handleStartNewGame}
        >
          {problem || 'Next'}
        </Button>
      </Modal.Footer>
    </Modal >
  );
}
