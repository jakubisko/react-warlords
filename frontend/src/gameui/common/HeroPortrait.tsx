import './HeroPortrait.scss';

interface HeroPortraitProps {
  imageId: number;
}

export const HeroPortrait = ({ imageId }: HeroPortraitProps) => (
  <div className="hero-portrait"
    style={{
      backgroundPositionX: -101 * (imageId % 6),
      backgroundPositionY: -93 * Math.floor(imageId / 6)
    }}
  />
);
