import { useState, type JSX } from 'react';
import ArmyImage from '../../common/ArmyImage';
import { allArmyTypes, defaultArmyTypeIdToOrder } from '../../../model/game/constants';
import ArmyTypeModal from '../ArmyTypeModal';
import { EMPTY_ARRAY } from '../../../functions';

interface ArmyTypePickerProps {
  availableArmyTypeIds: string[];
  selectedArmyTypeIds: string[];
  grayedArmyTypeIds?: string[];
  enforceNumberOfSlots?: number; // If given, always draws at least this many army images, even drawing extra empty slots.

  changeSelectionButton: (armyTypeId: string, currentlySelected: boolean, close: () => void) => JSX.Element;
}

export default function ArmyTypePicker(props: ArmyTypePickerProps) {
  const { availableArmyTypeIds, selectedArmyTypeIds, grayedArmyTypeIds, enforceNumberOfSlots, changeSelectionButton } = props;

  const [detailArmyTypeId, setDetailArmyTypeId] = useState<string | undefined>(undefined);

  const items = availableArmyTypeIds
    .map(armyTypeId => allArmyTypes[armyTypeId])
    .sort((a, b) => defaultArmyTypeIdToOrder[a.id] - defaultArmyTypeIdToOrder[b.id])
    .map(armyType => (
      <span className="m-1" key={armyType.id}>
        <ArmyImage
          armyTypeId={armyType.id ?? null}
          selectable={true}
          selected={selectedArmyTypeIds.includes(armyType.id)}
          grayed={(grayedArmyTypeIds ?? EMPTY_ARRAY as string[]).includes(armyType.id)}
          onLeftClick={() => setDetailArmyTypeId(armyType.id)}
          onRightClick={() => setDetailArmyTypeId(armyType.id)}
        />
      </span>
    ));

  for (let i = availableArmyTypeIds.length; i < (enforceNumberOfSlots ?? 0); i++) {
    items.push(
      <span className="m-1" key={i}>
        <ArmyImage
          armyTypeId={null}
          selectable={true}
        />
      </span>
    );
  }

  return (
    <div>
      {items}

      {detailArmyTypeId !== undefined && (
        <ArmyTypeModal
          armyTypeId={detailArmyTypeId}
          changeSelectionButton={() => changeSelectionButton(detailArmyTypeId, selectedArmyTypeIds.includes(detailArmyTypeId), () => setDetailArmyTypeId(undefined))}
          onClose={() => setDetailArmyTypeId(undefined)}
        />
      )}
    </div>
  );
}
