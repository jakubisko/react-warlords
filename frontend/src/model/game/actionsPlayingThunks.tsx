import { AppDispatch, RootState } from '../root/configureStore';
import { getAllArmiesOfPlayer } from './selectorsPlaying';
import { allArmyTypes } from './constants';
import { getRandomUnusedHeroPortraitId } from '../../predefined-data/predefinedDataGetters';
import { getAllUsedHeroPortraitIds } from './selectorsHero';
import { Place } from 'ai-interface/types/place';
import { setLastRevealedPlaces } from '../path/actions';
import { MoveCostCalculatorImplUnchecked } from '../path/MoveCostCalculatorImplUnchecked';
import { WarReportType } from 'ai-interface/types/warReport';
import { Structure } from 'ai-interface/constants/structure';
import { groupViewRadius, isWithinViewRadius, watchtowerViewRadius } from 'ai-interface/constants/viewRadius';
import { maxTileCapacity } from 'ai-interface/constants/path';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { setPlayerAiState, setHeroOffer } from './actionsTurnPassing';
import { activateGroupAndCalculatePath, setShowingToastMessage } from '../ui/actions';
import { demolishCost, razeCost } from 'ai-interface/constants/city';
import {
  addGold, addWarReportItem, _createArmy, resetWarReports, revealFogOfWarAtPlaces, revealUnexploredAtPlaces, setCaravanDestination,
  _setCityOwner, _setCityRazed, _setCurrentlyTrainableArmyTypeIds, setCurrentlyTraining, setFlaggableOwner, setGroupOwner, setGroupWaiting,
  setIncomingCaravans, _setPlayerAlive, setTurnsTrainingLeft, setUnexploredByTeam, _destroyArmy, setCityCapitol, setSageVisitedThisWeek
} from './actionsPlaying';
import { createArtifactOnGround } from './actionsHero';
import { EMPTY_ARRAY } from '../../functions';
import { Army } from 'ai-interface/types/army';

/** Removes fog of war from all tiles around given place. Optionally marks those tiles as explored for the team of the current player. */
export const revealFogOfWar = (row: number, col: number, viewRadius: number, markExplored = true) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { size, fogOfWar, unexploredByTeam, structures, players, currentPlayerId } = getState().game;
    const { team } = players[currentPlayerId];
    const unexplored = unexploredByTeam[team];

    // Watchtower has larger view radius
    if (structures[row][col] === Structure.Watchtower && watchtowerViewRadius > viewRadius) {
      viewRadius = watchtowerViewRadius;
    }

    // Collect all tiles in view radius around the given place that are currently covered and should be uncovered.
    const unfogPlaces: Place[] = [];
    const explorePlaces: Place[] = [];

    const rowOffMin = -Math.min(viewRadius, row);
    const rowOffMax = Math.min(viewRadius, size - 1 - row);
    const colOffMin = -Math.min(viewRadius, col);
    const colOffMax = Math.min(viewRadius, size - 1 - col);

    for (let rowOff = rowOffMin; rowOff <= rowOffMax; rowOff++) {
      for (let colOff = colOffMin; colOff <= colOffMax; colOff++) {
        const row1 = row + rowOff;
        const col1 = col + colOff;

        if (isWithinViewRadius(rowOff, colOff, viewRadius)) {
          if (fogOfWar[row1][col1]) {
            unfogPlaces.push({
              row: row1,
              col: col1
            });
          }

          if (unexplored !== null && unexplored[row1][col1]) {
            explorePlaces.push({
              row: row1,
              col: col1
            });
          }
        }
      }
    }

    dispatch(setLastRevealedPlaces(unfogPlaces));
    if (unfogPlaces.length > 0) {
      dispatch(revealFogOfWarAtPlaces(unfogPlaces));
    }
    if (explorePlaces.length > 0 && markExplored) {
      dispatch(revealUnexploredAtPlaces(explorePlaces));
    }
  };

/** Destroys everything owned by given player, marks him dead, and removes any unused parts of the state such as unexplored. */
export const killAndReleasePlayer = (playerId: number, reportType: WarReportType.PlayerEliminated | WarReportType.PlayerEliminatedAfterError) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { players } = getState().game;
    const { team } = players[playerId];

    dispatch(destroyEverythingOwnedByPlayer(playerId));

    if (!players.some((player, id) => id !== playerId && player.team === team && player.alive)) {
      dispatch(setUnexploredByTeam(team, null));
    }

    dispatch(_setPlayerAlive(playerId, false));
    dispatch(setPlayerAiState(playerId, ''));
    dispatch(resetWarReports(playerId));
    dispatch(setSageVisitedThisWeek(playerId, false));

    for (let reportForPlayerId = 0; reportForPlayerId < maxPlayers; reportForPlayerId++) {
      if (reportForPlayerId !== playerId && players[reportForPlayerId].alive) {
        dispatch(addWarReportItem(reportForPlayerId, {
          row: 0,
          col: 0,
          reportType,
          value: 1,
          playerId
        }));
      }
    }
  };

/**
 * Razes all cities, dismisses all armies and sets all flaggables of a player to neutral. This can be called in the middle of the turn.
 * Technically this doesn't kill the player; that will happen at the start of the next turn.
 */
export const destroyEverythingOwnedByPlayer = (playerId: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities, flaggables } = getState().game;

  dispatch(activateGroupAndCalculatePath(null));

  for (const cityId in cities) {
    const city = cities[cityId];
    if (city.owner === playerId && !city.razed) {
      dispatch(razeCity(cityId));
    }
  }

  for (const flaggableId in flaggables) {
    const flaggable = flaggables[flaggableId];
    if (flaggable.owner === playerId) {
      dispatch(setFlaggableOwner(flaggableId, neutralPlayerId));
    }
  }

  for (const army of getAllArmiesOfPlayer(getState(), playerId)) {
    dispatch(destroyArmy(army.groupId, army.id));
  }

  dispatch(addGold(playerId, -getState().game.players[playerId].gold)); // because of TimelineItem
};

export const acceptHeroOffer = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities, currentPlayerId, heroOffer } = getState().game;
  const { cost, cityId, portraitId, armyTypeIds } = heroOffer!;
  const { row, col } = cities[cityId];

  dispatch(addGold(currentPlayerId, -cost));
  for (const armyTypeId of armyTypeIds) {
    dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, currentPlayerId, portraitId));
  }
  dispatch(setHeroOffer(null));
};

export const razeCity = (cityId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities, currentPlayerId } = getState().game;

  dispatch(addGold(currentPlayerId, -razeCost));

  for (const city of Object.values(cities)) {
    if (city.caravanToCityId === cityId) {
      dispatch(setCaravanDestination(city.id, undefined));
    }
  }

  dispatch(setCityCapitol(cityId, false));
  dispatch(_setCityRazed(cityId, true));
  dispatch(_setCurrentlyTrainableArmyTypeIds(cityId, undefined));
  dispatch(setCurrentlyTraining(cityId, undefined));
  dispatch(setTurnsTrainingLeft(cityId, undefined));
  dispatch(setCaravanDestination(cityId, undefined));
  dispatch(setIncomingCaravans(cityId, undefined));
};

export const buildTrainingFacility = (cityId: string, armyTypeId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;
  const { owner, currentlyTrainableArmyTypeIds } = cities[cityId];

  dispatch(addGold(owner, -allArmyTypes[armyTypeId].buildingCost));
  dispatch(_setCurrentlyTrainableArmyTypeIds(cityId, (currentlyTrainableArmyTypeIds === undefined) ? [armyTypeId] : [...currentlyTrainableArmyTypeIds, armyTypeId]));
};

export const demolishTrainingFacility = (cityId: string, armyTypeId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;
  const { owner, currentlyTrainableArmyTypeIds, currentlyTrainingArmyTypeId } = cities[cityId];

  dispatch(addGold(owner, -demolishCost));
  dispatch(_setCurrentlyTrainableArmyTypeIds(cityId, currentlyTrainableArmyTypeIds!.filter(x => x !== armyTypeId)));
  if (currentlyTrainingArmyTypeId === armyTypeId) {
    dispatch(setCurrentlyTraining(cityId, undefined));
    dispatch(setTurnsTrainingLeft(cityId, undefined));
  }
};

/** Set owner of everything on the given place (city, flaggable, armies) to given player. Razed cities do not change ownership. */
export const changePlaceOwner = (row: number, col: number, owner: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { tiles, cities, flaggables, groups } = getState().game;
  const tile = tiles[row][col];
  const { cityId, flaggableId } = tile;
  let { armies } = tile;

  if (cityId !== undefined && cities[cityId].owner !== owner && !cities[cityId].razed) {
    dispatch(changeCityOwner(cityId, owner));
  } else if (flaggableId !== undefined && flaggables[flaggableId].owner !== owner) {
    dispatch(setFlaggableOwner(flaggableId, owner));
  }

  // Groups. When changing ownership of non-razed city, we need to change ownership of armies on all of city's tiles.
  if (cityId !== undefined) {
    const { razed, row: cRow, col: cCol } = cities[cityId];
    if (!razed) {
      armies = [
        ...tiles[cRow][cCol].armies ?? EMPTY_ARRAY as Army[],
        ...tiles[cRow + 1][cCol].armies ?? EMPTY_ARRAY as Army[],
        ...tiles[cRow][cCol + 1].armies ?? EMPTY_ARRAY as Army[],
        ...tiles[cRow + 1][cCol + 1].armies ?? EMPTY_ARRAY as Army[]
      ];
    }
  }

  if (armies !== undefined) {
    armies
      .map(army => army.groupId)
      .filter(groupId => groups[groupId].owner !== owner)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
      .forEach(groupId => dispatch(setGroupOwner(groupId, owner)));
  }
};

export const changeCityOwner = (cityId: string, owner: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;

  for (const city of Object.values(cities)) {
    if (city.caravanToCityId === cityId) {
      dispatch(setCaravanDestination(city.id, undefined));
    }
  }

  if (cities[cityId].capitol) {
    dispatch(setShowingToastMessage('Capitol destroyed'));
    dispatch(setCityCapitol(cityId, false));
  }

  dispatch(_setCityOwner(cityId, owner));
  dispatch(setCurrentlyTraining(cityId, undefined));
  dispatch(setTurnsTrainingLeft(cityId, undefined));
  dispatch(setCaravanDestination(cityId, undefined));
  dispatch(setIncomingCaravans(cityId, undefined));
};

/** Creates one army at given place. If that place is full, searches places around the place. If even those places are full, nothing is created. */
export const createArmyAtOrAroundPlace = (row: number, col: number, armyTypeId: string, owner: number, heroPortraitId?: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { game } = getState();
    const { size, tiles, groups, cities } = game;

    const { abilities, hero } = allArmyTypes[armyTypeId];
    const moveCostCalculator = new MoveCostCalculatorImplUnchecked(game, abilities);

    const offsets: { dRow: number, dCol: number }[] = [{ dRow: 0, dCol: 0 }, { dRow: 0, dCol: 1 }, { dRow: 1, dCol: 0 }];
    const { cityId } = tiles[row][col];
    if (cityId !== undefined) {
      row = cities[cityId].row;
      col = cities[cityId].col;
      offsets.push({ dRow: 1, dCol: 1 });
    } else {
      offsets.push({ dRow: 0, dCol: -1 });
      offsets.push({ dRow: -1, dCol: 0 });
    }

    for (const { dRow, dCol } of offsets) {
      const row1 = row + dRow;
      const col1 = col + dCol;
      // If it's outside of the map or if the army can't move over that terrain
      if (row1 < 0 || col1 < 0 || row1 >= size || col1 >= size || moveCostCalculator.getMoveCostAt(row1, col1) === Infinity) {
        continue;
      }

      // If there are enemy armies or if the tile is already full
      const { armies } = tiles[row1][col1];
      if (armies && (groups[armies[0].groupId].owner !== owner || armies.length >= maxTileCapacity)) {
        continue;
      }

      if (hero && heroPortraitId === undefined) {
        heroPortraitId = getRandomUnusedHeroPortraitId(getAllUsedHeroPortraitIds(getState()));
      } else if (!hero && heroPortraitId !== undefined) {
        heroPortraitId = undefined;
      }
      dispatch(_createArmy(row1, col1, armyTypeId, owner, heroPortraitId));
      dispatch(revealFogOfWar(row1, col1, groupViewRadius));
      break;
    }
  };

/** Destroys one army. If last army of a group is destroyed, the group will be removed. Destroyed heroes drop artifacts. */
export const destroyArmy = (groupId: string, armyId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { groups, heroes } = getState().game;

  const hero = heroes[armyId];
  if ((hero?.artifactIds?.length ?? 0) > 0) {
    const { row, col } = groups[groupId];
    for (const artifactId of hero.artifactIds!) {
      dispatch(createArtifactOnGround(row, col, artifactId));
    }
  }

  dispatch(_destroyArmy(groupId, armyId));
};

export const destroyGroup = (groupId: string) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { tiles, groups } = getState().game;

  const { row, col } = groups[groupId];
  const armyIds = tiles[row][col].armies!.filter(army => army.groupId === groupId).map(army => army.id);
  armyIds.forEach(armyId => dispatch(destroyArmy(groupId, armyId)));
};

export const wakeAllGroupsOfCurrentPlayer = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { groups, currentPlayerId } = getState().game;
  for (const groupId in groups) {
    const { id, owner, waiting } = groups[groupId];
    if (owner === currentPlayerId && waiting) {
      dispatch(setGroupWaiting(id, false));
    }
  }
};
