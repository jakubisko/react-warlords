import './OnlineGameElement.scss';
import { useState } from 'react';
import { Button, ButtonGroup, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../model/root/configureStore';
import QuestionModal from '../QuestionModal';
import { createArray } from '../../../functions';
import { maxPlayers } from 'ai-interface/constants/player';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDesktop, faTimes } from '@fortawesome/free-solid-svg-icons';
import { playerColors } from '../../../model/game/constants';
import { OnlineGame, OnlineUser } from '../../../model/online/types';
import { deleteGameFromServer, loadGameFromServer } from '../../../model/online/actions';

interface OnlineGameElementProps {
  onlineGame: OnlineGame;
  userUidToUser: { [uid: string]: OnlineUser };
}

export default function OnlineGameElement({ onlineGame, userUidToUser }: OnlineGameElementProps) {
  const { id, title, gameOver, currentPlayerId, userUids, aiNames, alive } = onlineGame;

  const uid = useSelector((state: RootState) => state.online.loggedInUser?.uid); // This screen is behind LoginGuard, so we're guaranteed to have a value.

  const dispatch: AppDispatch = useDispatch();

  const [isDeleteQuestionVisible, setDeleteQuestionVisible] = useState(false);
  const [working, setWorking] = useState(false);

  function handleTakeTurn() {
    setWorking(true);
    dispatch(loadGameFromServer(id,
      undefined, // No further action required as we'll leave this page.
      () => setWorking(false)
    ));
  }

  function handleDelete() {
    setWorking(true);
    setDeleteQuestionVisible(false);
    dispatch(deleteGameFromServer(id,
      undefined, // No further action required as we'll leave this page.
      () => setWorking(false)
    ));
  }

  function PlayerImage(playerId: number) {
    const userUid = userUids[playerId];
    const aiName = aiNames[playerId];
    return (
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id={`oge-${id}-${playerId}`}>
            {(userUid !== ''
              ? (userUidToUser[userUid]?.displayName ?? 'Unknown user')
              : aiName
            ) + (alive[playerId] ? '' : ' (Eliminated)')}
          </Tooltip>
        }
      >
        <div>
          {userUid !== ''
            ? <img src={userUidToUser[userUid]?.photoURL} referrerPolicy="no-referrer" alt="User photo" draggable={false} />
            : (
              <div className="ai d-flex align-items-center justify-content-center">
                <FontAwesomeIcon icon={faDesktop} color={playerColors[playerId]} />
              </div>
            )
          }
          {!alive[playerId] && <div className="eliminated d-flex align-items-center justify-content-center"><FontAwesomeIcon size="2x" icon={faTimes} /></div>}
        </div>
      </OverlayTrigger>
    );
  }

  return (
    <div className="online-game-element">
      <div className="text-nowrap text-truncate p-2">
        {title}
        {gameOver && ` (Game over)`}
      </div>

      <div className="d-flex justify-content-center">
        {createArray(maxPlayers, x => x)
          .filter(playerId => userUids[playerId] !== '' || aiNames[playerId] !== '')
          .map(playerId => (
            <div key={playerId} className={'position-relative' + (playerId === currentPlayerId && !gameOver ? ' current-player-image' : '')}>
              {PlayerImage(playerId)}
            </div>
          ))}
      </div>

      <ButtonGroup>
        <Button
          className="text-nowrap"
          disabled={working || (userUids[currentPlayerId] !== uid && !gameOver)}
          onClick={handleTakeTurn}
        >
          {gameOver ? `Game results` : `Take turn`}
        </Button>
        <Button
          disabled={working}
          onClick={() => setDeleteQuestionVisible(true)}
        >
          Delete
        </Button>
      </ButtonGroup>

      {isDeleteQuestionVisible && (
        <QuestionModal
          confirmString="Delete"
          rejectString="Cancel"
          onConfirm={handleDelete}
          onReject={() => setDeleteQuestionVisible(false)}
        >
          Are you sure that you want to permanently delete this game from server?
        </QuestionModal>
      )}
    </div>
  );
}
