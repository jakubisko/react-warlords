import './CreateRandomMapModal.scss';

import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modal, Button } from 'react-bootstrap';
import { createNewRandomMap } from '../../model/random-map/actionsRandomMap';
import { setErrorMessage, setScreen } from '../../model/ui/actions';
import { ScreenEnum } from '../../model/ui/types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquare, faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix } from '@fortawesome/free-solid-svg-icons';
import { getRandom } from '../../functions';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { setLastRandomMapSettings } from '../../model/editor/actions';
import { RandomMapSettings } from '../../model/editor/types';

interface CreateRandomMapModalProps {
  afterCreate: () => void;
  onCancel: () => void;
}

const valueIcons = [faSquare, faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix];

export default function CreateRandomMapModal({ afterCreate, onCancel }: CreateRandomMapModalProps) {
  const lastRandomMapSettings = useSelector((state: RootState) => state.editor.lastRandomMapSettings);
  const dispatch: AppDispatch = useDispatch();

  const [randomMapSettings, setRandomMapSettings] = useState<RandomMapSettings>(lastRandomMapSettings);

  function handleCreate() {
    dispatch(setScreen(ScreenEnum.WaitScreen));

    setTimeout(() => {
      try {
        dispatch(createNewRandomMap(randomMapSettings));
      } catch (err) {
        dispatch(setErrorMessage('Failed to create the map', err));
      }

      dispatch(setLastRandomMapSettings(randomMapSettings));
      afterCreate();
    }, 50);
  }

  function handleRandomize() {
    const x = valueIcons.length;
    setRandomMapSettings({
      ...randomMapSettings,
      water: getRandom(x) / (x - 1),
      mountains: getRandom(x) / (x - 1),
      heat: getRandom(x) / (x - 1),
      cold: getRandom(x) / (x - 1),
      cities: getRandom(x) / (x - 1),
      ruins: getRandom(x) / (x - 1)
    });
  }

  function zeroToOneButtons(propertyName: keyof RandomMapSettings) {
    const maxValue = valueIcons.length - 1;
    const valueX = Math.round(maxValue * randomMapSettings[propertyName]);

    const setValue = (valueX: number) => setRandomMapSettings({
      ...randomMapSettings,
      [propertyName]: valueX / maxValue
    });

    return (
      <div className="w-100 d-inline-flex align-items-center justify-content-between" >
        <Button
          disabled={valueX <= 0}
          onClick={() => setValue(valueX - 1)}
        >
          -
        </Button>

        <FontAwesomeIcon icon={valueIcons[valueX]} size="2x" />

        <Button
          disabled={valueX >= maxValue}
          onClick={() => setValue(valueX + 1)}
        >
          +
        </Button>
      </div>
    );
  }

  const { size, players } = randomMapSettings;

  return (
    <Modal className="create-random-map-modal overflow-hidden" animation={false} backdrop={false} show centered onHide={onCancel}>
      <Modal.Header>
        <Modal.Title>Create random map</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="container">

          <div className="row">
            <div className="col-2 d-inline-flex align-items-center">
              Size
            </div>
            <div className="col-4 d-inline-flex align-items-center justify-content-between">
              <Button
                disabled={size <= 40}
                onClick={() => setRandomMapSettings({
                  ...randomMapSettings,
                  size: size - 5
                })}
              >
                -
              </Button>
              {size}
              <Button
                disabled={size >= 150}
                onClick={() => setRandomMapSettings({
                  ...randomMapSettings,
                  size: size + 5
                })}
              >
                +
              </Button>
            </div>

            <div className="col-2 d-inline-flex align-items-center">
              Players
            </div>
            <div className="col-4 d-inline-flex align-items-center justify-content-between">
              <Button
                disabled={players <= 2}
                onClick={() => setRandomMapSettings({
                  ...randomMapSettings,
                  players: players - 1
                })}
              >
                -
              </Button>
              {players}
              <Button
                disabled={players >= 8}
                onClick={() => setRandomMapSettings({
                  ...randomMapSettings,
                  players: players + 1
                })}
              >
                +
              </Button>
            </div>
          </div>

          <div className="row">
            <div className="col-2 d-inline-flex align-items-center">
              Water
            </div>
            <div className="col-4">
              {zeroToOneButtons('water')}
            </div>

            <div className="col-2 d-inline-flex align-items-center">
              Mountain
            </div>
            <div className="col-4">
              {zeroToOneButtons('mountains')}
            </div>
          </div>

          <div className="row">
            <div className="col-2 d-inline-flex align-items-center">
              Heat
            </div>
            <div className="col-4">
              {zeroToOneButtons('heat')}
            </div>

            <div className="col-2 d-inline-flex align-items-center">
              Cold
            </div>
            <div className="col-4">
              {zeroToOneButtons('cold')}
            </div>
          </div>

          <div className="row">
            <div className="col-2 d-inline-flex align-items-center">
              Cities
            </div>
            <div className="col-4">
              {zeroToOneButtons('cities')}
            </div>

            <div className="col-2 d-inline-flex align-items-center">
              Ruins
            </div>
            <div className="col-4">
              {zeroToOneButtons('ruins')}
            </div>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <Button onClick={handleCreate}>
          Create
        </Button>

        <Button onClick={handleRandomize}>
          Randomize
        </Button>

        <Button onClick={onCancel}>
          Back
        </Button>
      </Modal.Footer>
    </Modal >
  );
}
