import './GameActions.scss';
import { Component } from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../model/root/configureStore';
import OrderingModal from '../modals/OrderingModal';
import { saveMapFocusOfCurrentPlayer, endUiTurn } from '../../model/game/actionsTurnPassingThunks';
import { activateGroupAndCalculatePath, setLastActiveGroup, showCityModalOrPlaceInfoModal, setShowingMenu, showReports } from '../../model/ui/actions';
import { moveActiveGroupAlongPath, moveAllGroups } from '../../model/path/actions';
import { getPlaceOfActiveGroup } from '../../model/game/selectorsPlaying';
import { getNextGroupIdWithMove, isMoveAllGroupsDisabled, getMoveLeftOfGroup } from '../../model/path/selectors';
import QuestionModal from '../modals/QuestionModal';
import { setGroupWaiting } from '../../model/game/actionsPlaying';
import { destroyGroup, wakeAllGroupsOfCurrentPlayer } from '../../model/game/actionsPlayingThunks';
import { Place } from 'ai-interface/types/place';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faScroll, faHandPointRight, faForward, faTimes, faPlay, faSearch, faSkull, faDragon, faHourglassHalf, faCog, faBell, faMoon,
  faSun
} from '@fortawesome/free-solid-svg-icons';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { HeroOffer } from 'ai-interface/types/heroOffer';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { IconHero } from '../common/svg/IconHero';

interface GameActionsProps {
  heroOffer: HeroOffer | null;
  activeGroupId: string | null;
  nextGroupIdWithMove: string | null;
  disabledMoveAllGroups: boolean;
  disabledMoveActiveGroup: boolean;
  placeOfActiveGroup: Place | null;
  waiting?: boolean;
  currentPlayerName: string;
  turnNumber: number;

  setShowingMenu: (show: boolean) => void;
  endUiTurn: () => void;
  activateGroupAndCalculatePath: (groupId: string | null, alsoSetMapFocus?: boolean) => void;
  setLastActiveGroup: (groupId: string) => void;
  moveActiveGroupAlongPath: () => void;
  moveAllGroups: () => void;
  destroyGroup: (groupId: string) => void;
  showCityModalOrPlaceInfoModal: (place: Place | null) => void;
  showReports: (...reports: ('WAR' | 'SPY' | 'OFFER')[]) => void;
  saveMapFocusOfCurrentPlayer: () => void;
  setGroupWaiting: (groupId: string, waiting: boolean) => void;
  wakeAllGroupsOfCurrentPlayer: () => void;
}

interface GameActionsState {
  showingOrdering: boolean;
  showingEndTurnQuestion: boolean;
  showingDismissQuestion: boolean;
}

class GameActions extends Component<GameActionsProps, GameActionsState> {

  constructor(props: GameActionsProps) {
    super(props);

    this.state = {
      showingOrdering: false,
      showingEndTurnQuestion: false,
      showingDismissQuestion: false
    };
  }

  handleActivateNextGroup() {
    const { nextGroupIdWithMove } = this.props;
    if (nextGroupIdWithMove !== null) {
      this.props.activateGroupAndCalculatePath(nextGroupIdWithMove, true);
      this.props.setLastActiveGroup(nextGroupIdWithMove);
    }
  }

  handleEndTurn(checkMoveLeft: boolean) {
    const { nextGroupIdWithMove, disabledMoveActiveGroup } = this.props;

    if (checkMoveLeft && (
      !disabledMoveActiveGroup || // "Move active group" is enabled
      nextGroupIdWithMove !== null // "Activate next group" is enabled
    )) {
      this.setState({ showingEndTurnQuestion: true });
      return;
    }

    this.props.endUiTurn();
  }

  handleDismissGroup() {
    const { activeGroupId } = this.props;
    if (activeGroupId !== null) {
      soundEffects.play(SoundEffect.Dismiss);
      this.props.activateGroupAndCalculatePath(null);
      this.props.destroyGroup(activeGroupId);
      this.setState({ showingDismissQuestion: false });
    }
  }

  handleWaitOrWake() {
    const { activeGroupId, waiting, nextGroupIdWithMove } = this.props;

    if (activeGroupId === null) {
      // Wakes all groups
      this.props.wakeAllGroupsOfCurrentPlayer();
      soundEffects.play(SoundEffect.WakeUp);
    } else if (waiting) {
      // Order group not to wait
      this.props.setGroupWaiting(activeGroupId, false);
      soundEffects.play(SoundEffect.WakeUp);
    } else {
      // Order group to wait
      this.props.setGroupWaiting(activeGroupId, true);

      // Activates next group unless this was the last group of the player.
      if (nextGroupIdWithMove !== null && activeGroupId !== nextGroupIdWithMove) {
        this.handleActivateNextGroup();
      }
      soundEffects.play(SoundEffect.WaitHere);
    }
  }

  handleShowMenu() {
    // We'll save the map focus; so that if the player uses the menu to save game, the GameState is already containing everything.
    // Saving isn't implemented as handler/action, so we can't do it there.
    this.props.saveMapFocusOfCurrentPlayer();
    this.props.setShowingMenu(true);
  }

  render() {
    const { heroOffer, activeGroupId, nextGroupIdWithMove, disabledMoveAllGroups, disabledMoveActiveGroup, placeOfActiveGroup,
      waiting, currentPlayerName, turnNumber } = this.props;

    return (
      <div className="game-actions two-items-per-column-container">
        <div className="button-row">
          {/* System menu */}
          <ButtonWithShortcut
            shortcutCode="KeyS"
            onClick={() => this.handleShowMenu()}
          >
            <FontAwesomeIcon icon={faCog} />
            <div className="caption"><b>S</b>ystem</div>
          </ButtonWithShortcut>

          {/* Ordering */}
          <ButtonWithShortcut
            shortcutCode="KeyO"
            onClick={() => this.setState({ showingOrdering: true })}
          >
            <FontAwesomeIcon icon={faDragon} />
            <div className="caption"><b>O</b>rdering</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row">
          {/* Reports on losses from previous turn, hero offers etc. */}
          <ButtonWithShortcut
            shortcutCode="KeyR"
            onClick={() => this.props.showReports('WAR', 'SPY')}
          >
            <FontAwesomeIcon icon={faScroll} />
            <div className="caption"><b>R</b>eports</div>
          </ButtonWithShortcut>

          {/* Hero offer */}
          <ButtonWithShortcut
            disabled={heroOffer === null}
            shortcutCode="KeyI"
            onClick={() => this.props.showReports('OFFER')}
          >
            <IconHero />
            <div className="caption"><b>H</b>ero offer</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row">
          {/* Activate next group */}
          <ButtonWithShortcut
            shortcutCode="KeyN"
            disabled={nextGroupIdWithMove === null}
            onClick={() => this.handleActivateNextGroup()}
          >
            <FontAwesomeIcon icon={faHandPointRight} />
            <div className="caption"><b>N</b>ext</div>
          </ButtonWithShortcut>

          {/* Move all groups */}
          <ButtonWithShortcut
            shortcutCode="KeyA"
            disabled={disabledMoveAllGroups}
            onClick={() => this.props.moveAllGroups()}
          >
            <FontAwesomeIcon icon={faForward} />
            <div className="caption">Move <b>A</b>ll</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row">
          {/* Deselect active group */}
          <ButtonWithShortcut
            shortcutCode="Escape"
            disabled={activeGroupId === null}
            onClick={() => this.props.activateGroupAndCalculatePath(null)}
          >
            <FontAwesomeIcon icon={faTimes} />
            <div className="caption">Unselect</div>
          </ButtonWithShortcut>

          {/* Move active group */}
          <ButtonWithShortcut
            disabled={disabledMoveActiveGroup}
            shortcutCode="KeyM"
            onClick={() => activeGroupId !== null && this.props.moveActiveGroupAlongPath()}
          >
            <FontAwesomeIcon icon={faPlay} />
            <div className="caption"><b>M</b>ove</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row">
          {/* Place info or city info. */}
          <ButtonWithShortcut
            shortcutCode="KeyP"
            disabled={placeOfActiveGroup === null}
            onClick={() => this.props.showCityModalOrPlaceInfoModal(placeOfActiveGroup)}
          >
            <FontAwesomeIcon icon={faSearch} />
            <div className="caption"><b>P</b>lace</div>
          </ButtonWithShortcut>

          {/* Dismiss active group */}
          <ButtonWithShortcut
            shortcutCode="Delete"
            disabled={activeGroupId === null}
            onClick={() => this.setState({ showingDismissQuestion: true })}
          >
            <FontAwesomeIcon icon={faSkull} />
            <div className="caption">Dismiss</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row">
          {/* Command active group to wait. It will be ignored when using command "Next". */}
          <ButtonWithShortcut
            shortcutCode="KeyW"
            onClick={() => this.handleWaitOrWake()}
          >
            <FontAwesomeIcon icon={activeGroupId === null ? faBell : (waiting ? faSun : faMoon)} />
            <div className="caption"><b>W</b>{activeGroupId === null ? 'ake all' : (waiting ? 'ake up' : 'ait here')}</div>
          </ButtonWithShortcut>

          {/* End turn */}
          <ButtonWithShortcut
            shortcutCode="KeyE"
            onClick={() => this.handleEndTurn(true)}
          >
            <FontAwesomeIcon icon={faHourglassHalf} />
            <div className="caption"><b>E</b>nd turn</div>
          </ButtonWithShortcut>
        </div>

        <div className="button-row show-only-on-desktop mt-auto flex-column fs-6 justify-content-around text-center">
          <span className="w-100 text-nowrap overflow-hidden text-truncate">
            {currentPlayerName}
          </span>
          <span className="w-100 text-nowrap overflow-hidden text-truncate">
            Week&nbsp;{Math.floor((turnNumber - 1) / 7) + 1}, Day&nbsp;{(turnNumber - 1) % 7 + 1}
          </span>
        </div>

        {this.state.showingOrdering &&
          <OrderingModal onClose={() => this.setState({ showingOrdering: false })} />
        }

        {this.state.showingEndTurnQuestion &&
          <QuestionModal
            confirmString="End turn"
            rejectString="Cancel"
            onConfirm={() => this.handleEndTurn(false)}
            onReject={() => this.setState({ showingEndTurnQuestion: false })}
          >
            One or more groups you control still have move left. Are you sure you want to end your turn?
          </QuestionModal>
        }

        {this.state.showingDismissQuestion &&
          <QuestionModal
            confirmString="Dismiss"
            rejectString="Cancel"
            onConfirm={() => this.handleDismissGroup()}
            onReject={() => this.setState({ showingDismissQuestion: false })}
          >
            Are you sure you want to dismiss selected armies?
          </QuestionModal>
        }
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  heroOffer: state.game.heroOffer,
  activeGroupId: state.ui.activeGroupId,
  nextGroupIdWithMove: getNextGroupIdWithMove(state, state.ui.lastActiveGroupId),
  disabledMoveAllGroups: isMoveAllGroupsDisabled(state),
  disabledMoveActiveGroup: state.ui.activeGroupId === null || state.path.path.length < 2 || getMoveLeftOfGroup(state, state.ui.activeGroupId) <= 0,
  placeOfActiveGroup: getPlaceOfActiveGroup(state),
  waiting: state.ui.activeGroupId !== null && state.game.groups[state.ui.activeGroupId].waiting,
  currentPlayerName: state.game.players[state.game.currentPlayerId].name,
  turnNumber: state.game.turnNumber
});

const mapDispatchToProps = {
  setShowingMenu,
  endUiTurn,
  activateGroupAndCalculatePath,
  setLastActiveGroup,
  moveActiveGroupAlongPath,
  moveAllGroups,
  destroyGroup,
  showCityModalOrPlaceInfoModal,
  showReports,
  saveMapFocusOfCurrentPlayer,
  setGroupWaiting,
  wakeAllGroupsOfCurrentPlayer
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameActions);
