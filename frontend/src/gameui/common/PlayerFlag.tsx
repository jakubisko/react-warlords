import './PlayerFlag.scss';
import { flagLeft, flagWidth, flagHeight } from '../map/tactical-map/spriteMetaData';

interface PlayerFlagProps {
  owner: number;
  size?: number; // How many armies this flag represents from 1 to 4. Default is 4.
}

export const PlayerFlag = ({ owner, size = 4 }: PlayerFlagProps) => (
  <div
    className="player-flag"
    style={{
      backgroundPositionX: -flagLeft[size - 1],
      backgroundPositionY: -(flagHeight + 1) * owner,
      width: flagWidth[size - 1],
      height: flagHeight
    }}
  />
);
