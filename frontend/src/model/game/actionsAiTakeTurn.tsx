import { AppDispatch, RootState } from '../root/configureStore';
import { setScreen } from '../ui/actions';
import { ScreenEnum } from '../ui/types';
import { WarReportType } from 'ai-interface/types/warReport';
import { killAndReleasePlayer } from './actionsPlayingThunks';
import { setGameState } from './actionsTurnPassing';
import aiTakeTurnInSeparateStore from './actionsAiTakeTurnInSeparateStore';
import { passTurnToNextPlayer } from './actionsTurnPassingThunks';

// TODO - The AI should run in a worker! It's currently running inside main thread. Old implementation is commented out.
/**
 * Assumes that the current player is an AI.
 * Creates a new web worker and executes AI turn in it.
 */
export const aiTakeTurn = () => async (dispatch: AppDispatch, getState: () => RootState) => {
  const { game, game: { players, currentPlayerId, aiOutputVisible } } = getState();
  const { aiName } = players[currentPlayerId];

  try {
    const gameStateAfter = aiTakeTurnInSeparateStore(game);
    dispatch(setGameState(gameStateAfter));

    if (aiOutputVisible) {
      dispatch(setScreen(ScreenEnum.StartOfTurnScreen));
    } else {
      dispatch(passTurnToNextPlayer());
    }

  } catch (e) {
    console.warn(`AI player ${currentPlayerId} "${aiName}" has thrown an error and was disqualified.`, e);

    dispatch(killAndReleasePlayer(currentPlayerId, WarReportType.PlayerEliminatedAfterError));
    dispatch(setScreen(ScreenEnum.AiDisqualifiedScreen));
  }
};

// // Use webpack loader syntax to load the worker with `worker-loader`.
// // With this strategy, no ejection or CRA webpack config modification is necessary.
// // eslint-disable-next-line
// // @ts-ignore
// import AiTakeTurnInSeparateStore from 'worker-loader!./actionsAiTakeTurnInSeparateStore';
// // Import the worker type directly.
// import { wrap } from 'comlink';

// /**
//  * Assumes that the current player is an AI.
//  * Creates a new web worker and executes AI turn in it.
//  */
// export const aiTakeTurn = () => async (dispatch: AppDispatch, getState: () => RootState) => {
//   const { game, game: { players, currentPlayerId, aiOutputVisible } } = getState();
//   const { aiName } = players[currentPlayerId];

//   const worker: Worker = new AiTakeTurnInSeparateStore();

//   try {
//     const wrappedMethod = wrap<typeof aiTakeTurnInSeparateStore>(worker);
//     const gameStateAfter = await wrappedMethod(game);
//     dispatch(setGameState(gameStateAfter));

//     if (aiOutputVisible) {
//       dispatch(setScreen(ScreenEnum.StartOfTurnScreen));
//     } else {
//       dispatch(passTurnToNextPlayer());
//     }

//   } catch (e) {
//     console.warn(`AI player ${currentPlayerId} "${aiName}" has thrown an error and was disqualified.`, e);

//     dispatch(killAndReleasePlayer(currentPlayerId, WarReportType.PlayerEliminatedAfterError));
//     dispatch(setScreen(ScreenEnum.AiDisqualifiedScreen));
//   } finally {
//     worker.terminate();
//   }
// };
