import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { Hero } from './types';
import { Ability } from 'ai-interface/constants/ability';
import { ArtifactAbility } from 'ai-interface/constants/artifactAbility';
import { straightMoveCost } from 'ai-interface/constants/path';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';
import { Abilities } from 'ai-interface/types/army';
import { Mutable } from '../../functions';
import { getChanceToDieWhenExploringRuin } from 'ai-interface/constants/exploring';
import { Structure } from 'ai-interface/constants/structure';

const getGameState = (state: RootState) => state.game;

export const getAllUsedHeroPortraitIds = createSelector(
  getGameState,
  ({ heroes }) => Object.keys(heroes).map(heroId => heroes[heroId].portraitId)
);

/** Returns true if some additional actions are available for a hero - picking an artifact, exploring an unexplored ruin etc. */
export const isAdditionalHeroActionAvailableAtPlaceOfActiveGroup = createSelector(
  getGameState, (rootState: RootState) => rootState.ui.activeGroupId,
  (gameState, activeGroupId) => {
    if (activeGroupId === null) {
      return false;
    }

    const { tiles, structures, groups, ruins, players } = gameState;
    const { row, col, owner } = groups[activeGroupId];
    const { ruinId, artifactIds } = tiles[row][col];

    return artifactIds !== undefined
      || (ruinId !== undefined && !ruins[ruinId].explored)
      || (structures[row][col] === Structure.SageTower && !players[owner].sageVisitedThisWeek);
  }
);

/** If the given hero stands at an unexplored ruin, returns a chance that hero will die when exploring it. Otherwise, returns null. */
export const getChanceToDieWhenExploringRuinWithHero = createSelector(
  getGameState, (rootState: RootState, heroId: string) => heroId,
  (gameState, heroId) => {
    const { tiles, ruins, groups, heroes } = gameState;
    const { groupId } = heroes[heroId];
    const { row, col } = groups[groupId];
    const { ruinId, armies } = tiles[row][col];

    if (ruinId === undefined || ruins[ruinId].explored) {
      return null;
    }

    const ruinLevel = ruins[ruinId].level;
    const heroLevel = heroes[heroId].level;
    const otherArmyTypeIds = armies!.filter(army => army.id !== heroId).map(army => army.armyTypeId);
    return getChanceToDieWhenExploringRuin(ruinLevel, heroLevel, otherArmyTypeIds);
  }
);

// Actually not selectors, just pure functions ////////////////////////////////////////////////////

/** Given an ArtifactAbility, this will iterate over all artifacts of the hero and return the values found for this ability. */
function sumOfArtifactAbility(hero: Hero, ability: ArtifactAbility): number {
  const { artifactIds } = hero;
  if (artifactIds === undefined) {
    return 0;
  }

  const cancelPenalties = artifactIds
    .some(artifactId => Object.keys(standardArtifacts[artifactId].abilities)
      .includes(ArtifactAbility.CancelsArtifactPenalties));

  return artifactIds
    .map(artifactId => standardArtifacts[artifactId].abilities[ability] ?? 0)
    .map(value => cancelPenalties ? Math.max(0, value) : value)
    .reduce((sum, value) => sum + value, 0);
}

export function getHerosStrength(hero: Hero): number {
  return Math.max(1,
    sumOfArtifactAbility(hero, ArtifactAbility.StrengthBonus)
    + standardArmyTypes[`hro${hero.level}`].strength
  );
}

export function getHerosMove(hero: Hero): number {
  return Math.max(1,
    sumOfArtifactAbility(hero, ArtifactAbility.MoveBonus) * straightMoveCost
    + standardArmyTypes[`hro${hero.level}`].move
  );
}

export function getHeroGrantsFlying(hero: Hero): boolean {
  return sumOfArtifactAbility(hero, ArtifactAbility.GroupFlying) > 0;
}

export function getHerosHitPoints(hero: Hero): number {
  return Math.max(1,
    sumOfArtifactAbility(hero, ArtifactAbility.HitPointsBonus)
    + standardArmyTypes[`hro${hero.level}`].hitPoints
  );
}

export function getHerosUpkeep(hero: Hero): number {
  return standardArmyTypes[`hro${hero.level}`].upkeep - getHerosIncomeBonus(hero);
}

export function getHerosIncomeBonus(hero: Hero): number {
  return sumOfArtifactAbility(hero, ArtifactAbility.IncomeBonus);
}

export function getHerosExperienceBonus(hero: Hero): number {
  return sumOfArtifactAbility(hero, ArtifactAbility.ExperienceBonus);
}

/** Universal getter to retrieve value of any ability that could be on a normal army, but is on a hero. */
export function getHerosValueOfAbility(hero: Hero, ability: Ability): number {
  switch (ability) {

    case Ability.StrengthBonusWhenAttacking:
      return sumOfArtifactAbility(hero, ArtifactAbility.StrengthBonusWhenAttacking);

    case Ability.StrengthBonusWhenDefending:
      return sumOfArtifactAbility(hero, ArtifactAbility.StrengthBonusWhenDefending);

    case Ability.StrengthBonusOnIce:
      return sumOfArtifactAbility(hero, ArtifactAbility.StrengthBonusOnIce);

    case Ability.CanBeCarriedByFlier:
      return standardArmyTypes[`hro${hero.level}`].abilities[Ability.CanBeCarriedByFlier] ?? 0;

    case Ability.AntiAirBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.AntiAirBonus);

    case Ability.GroupMoveBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.GroupMoveBonus);

    case Ability.CancelEnemyTerrainBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.CancelEnemyTerrainBonus);

    case Ability.CancelEnemyFortificationBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.CancelEnemyFortificationBonus);

    case Ability.FrighteningBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.FrighteningBonus);

    case Ability.Ambush:
      return sumOfArtifactAbility(hero, ArtifactAbility.AmbushBonus);

    case Ability.LowerEnemyAmbush:
      return sumOfArtifactAbility(hero, ArtifactAbility.LowerEnemyAmbush);

    case Ability.IncreaseGroupAmbush:
      return sumOfArtifactAbility(hero, ArtifactAbility.IncreaseGroupAmbush);

    case Ability.ViewRadiusBonus:
      return sumOfArtifactAbility(hero, ArtifactAbility.ViewRadiusBonus);

    case Ability.NoPenaltyInSwamp:
    case Ability.NoPenaltyInForest:
    case Ability.NoPenaltyInHills:
    case Ability.NoPenaltyInDesert:
    case Ability.NoPenaltyOnIce:
      return sumOfArtifactAbility(hero, ArtifactAbility.NoTerrainPenalty);

    case Ability.Leadership:
      return sumOfArtifactAbility(hero, ArtifactAbility.LeadershipBonus)
        + (standardArmyTypes[`hro${hero.level}`].abilities[Ability.Leadership] ?? 0);

    default:
      return 0;
  }
}

/** Universal getter to retrieve value of any ability that could be on a normal army, but is on a hero. */
export function getHerosAbilities(hero: Hero): Abilities {
  const abilities: Mutable<Abilities> = {};

  for (const abilityAsString in Ability) {
    const ability = abilityAsString as Ability;
    const value = getHerosValueOfAbility(hero, ability);
    if (value !== 0) {
      abilities[ability] = value;
    }
  }

  return abilities;
}
