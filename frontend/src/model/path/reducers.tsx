import { EMPTY_ARRAY } from '../../functions';
import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actions';
import { Place, PlaceWithTime } from 'ai-interface/types/place';

/**
 * This state contains only data that are related to move pathing. They are temporary, but are also used by AI.
 * None of it is saved when land or game is saved.
 */
export interface PathState {
  /**
   * Pathing for active group. Source and destination places are included.
   * If group doesn't have destination, length is one - the current place; or zero - no path at all.
   */
  readonly path: PlaceWithTime[];

  /** Whether the army is traveling at this moment. */
  readonly movingAnimation: boolean;

  /** Whether user manually interrupted move. This should stop both current groups move and move all command. */
  readonly interruptMoving: boolean;

  /**
   * Use this to move multiple groups in sequence. When one group ends the move, next is picked from this list and moved,
   * until this list is empty.
   */
  readonly moveAllGroupIds: string[];

  /** List of places from which fog of war was removed during last group move. */
  readonly lastRevealedPlaces: Place[];
}

export const initialState: PathState = {
  path: EMPTY_ARRAY as PlaceWithTime[],
  movingAnimation: false,
  interruptMoving: false,
  moveAllGroupIds: EMPTY_ARRAY as string[],
  lastRevealedPlaces: EMPTY_ARRAY as Place[]
};

export function pathReducer(state = initialState, action: AppAction): PathState {
  switch (action.type) {

    case ActionTypes.SET_PATH: {
      const { path } = action.payload;

      // Optimization to prevent recalculation of getPathSprites selector, which would cause useless redrawing of TacticalMap.
      if ((path.length === 0 && state.path.length === 0) ||
        (path.length === 1 && state.path.length === 1 && path[0].row === state.path[0].row && path[0].col === state.path[0].col)) {
        return state;
      }

      return {
        ...state,
        path
      };
    }

    case ActionTypes.SET_MOVING_ANIMATION: {
      const { movingAnimation } = action.payload;
      return {
        ...state,
        movingAnimation,
        interruptMoving: false
      };
    }

    case ActionTypes.SET_MOVE_ALL_GROUP_IDS: {
      const { moveAllGroupIds } = action.payload;
      return {
        ...state,
        moveAllGroupIds
      };
    }

    case ActionTypes.INTERRUPT_MOVING: {
      return {
        ...state,
        interruptMoving: true
      };
    }

    case ActionTypes.SET_LAST_REVEALED_PLACES: {
      const { lastRevealedPlaces } = action.payload;
      return {
        ...state,
        lastRevealedPlaces
      };
    }

    default:
      return state;
  }
}
