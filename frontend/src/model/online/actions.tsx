import { firestore } from './firebaseConfig';
import { getDataForOnlineSave } from './selectors';
import { AppDispatch, RootState } from '../root/configureStore';
import { setErrorMessage } from '../ui/actions';
import { OnlineUser } from './types';
import { collection, doc, runTransaction, serverTimestamp, setDoc } from 'firebase/firestore';
import { buildVersion, decompressObject } from '../game/serialization';
import { loadGame } from '../game/actionsTurnPassingThunks';
import { UserError } from '../ui/types';

export enum ActionTypes {
  SET_ONLINE_GAME_ID = '[ONLINE] SET_ONLINE_GAME_ID',
  SET_ONLINE_TIMES_LOADED = '[ONLINE] SET_ONLINE_TIMES_LOADED',
  SET_ONLINE_TOKEN = '[ONLINE] SET_ONLINE_TOKEN',
  SET_LOGGED_IN_USER = '[ONLINE] SET_LOGGED_IN_USER',
}

export interface SetOnlineGameId {
  type: ActionTypes.SET_ONLINE_GAME_ID;
  payload: {
    onlineGameId: string | null;
  }
}

export interface SetOnlineTimesLoaded {
  type: ActionTypes.SET_ONLINE_TIMES_LOADED;
  payload: {
    onlineTimesLoaded: number;
  }
}

export interface SetOnlineToken {
  type: ActionTypes.SET_ONLINE_TOKEN;
  payload: {
    onlineToken: string;
  }
}

export interface SetLoggedInUser {
  type: ActionTypes.SET_LOGGED_IN_USER;
  payload: {
    loggedInUser: OnlineUser | null;
  }
}

// Thunk action creators //////////////////////////////////////////////////////////////////////////

export const createOrUpdateUserOnServer = (user: OnlineUser, success?: () => void, error?: () => void) =>
  async (dispatch: AppDispatch) => {
    try {
      const userRef = doc(firestore, 'users', user.uid);
      await setDoc(userRef, user);

      success?.();
    } catch (e) {
      dispatch(setErrorMessage(`User couldn't be created`, e));
      error?.();
    }
  };


export const createGameOnServer = (title: string, success?: () => void, error?: () => void) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const { uid } = getState().online.loggedInUser!;
      const { onlineGame, onlineGameState } = await getDataForOnlineSave(getState());

      // If a player would create a game in which he is not participating, firestore rules would block updating it when the first turn
      // would start. It wouldn't be playable by any user. This user wouldn't see it, so he wouldn't be able to delete it.
      if (!onlineGame.userUids.includes(uid)) {
        throw new UserError(`You can't create a game in which you are not participating.`);
      }

      const gameRef = doc(collection(firestore, 'games'));
      const gameStateRef = doc(firestore, 'gameStates', gameRef.id);

      await runTransaction(firestore, async transaction => {
        transaction.set(gameRef, {
          ...onlineGame,
          title,
          createdAt: serverTimestamp()
        });
        transaction.set(gameStateRef, onlineGameState);
      });

      dispatch(setOnlineGameId(gameRef.id));
      dispatch(setOnlineTimesLoaded(0));
      dispatch(setOnlineToken(onlineGame.token));

      success?.();
    } catch (e) {
      dispatch(setErrorMessage(`Game not created`, e));
      error?.();
    }
  };


export const updateGameOnServer = (success?: () => void, error?: () => void) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const { onlineGameId, onlineToken } = getState().online;
      const { onlineGame, onlineGameState } = await getDataForOnlineSave(getState());

      const gameRef = doc(firestore, 'games', onlineGameId!);
      const gameStateRef = doc(firestore, 'gameStates', onlineGameId!);

      await runTransaction(firestore, async transaction => {
        const [gameDoc, gameStateDoc] = await Promise.all([
          transaction.get(gameRef),
          transaction.get(gameStateRef)
        ]);
        if (!gameDoc.exists || !gameStateDoc.exists) {
          throw new UserError(`Couldn't find the game on the server.`);
        }

        const buildVersionOnServer = gameDoc.get('buildVersion') as string;
        if (buildVersionOnServer !== buildVersion) {
          throw new UserError(`Your version of the game '${buildVersion}' differs from the game creator's version '${buildVersionOnServer}'.`);
        }

        // Prevent double update - if the token on the server doesn't match the token originally retrieved from the server, that means
        // somebody has already changed the game.
        const tokenOnServer = gameDoc.get('token') as string;
        if (onlineToken !== tokenOnServer) {
          throw new UserError(`There are newer data on the server.`);
        }

        transaction.update(gameRef, onlineGame);
        transaction.set(gameStateRef, onlineGameState);
      });

      success?.();
    } catch (e) {
      dispatch(setErrorMessage(`Game not saved`, e));
      error?.();
    }
  };


export const loadGameFromServer = (onlineGameId: string, success?: () => void, error?: () => void) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const { uid } = getState().online.loggedInUser!;

      const gameRef = doc(firestore, 'games', onlineGameId);
      const gameStateRef = doc(firestore, 'gameStates', onlineGameId);

      const { stateSerialized, timesLoaded, token } = await runTransaction(firestore, async transaction => {
        const [gameDoc, gameStateDoc] = await Promise.all([
          transaction.get(gameRef),
          transaction.get(gameStateRef)
        ]);
        if (!gameDoc.exists || !gameStateDoc.exists) {
          throw new UserError(`Couldn't find the game on the server.`);
        }

        const buildVersionOnServer = gameDoc.get('buildVersion') as string;
        if (buildVersionOnServer !== buildVersion) {
          throw new UserError(`Your version of the game '${buildVersion}' differs from the game creator's version '${buildVersionOnServer}'.`);
        }

        const currentPlayerId = gameDoc.get('currentPlayerId') as number;
        const userUids = gameDoc.get('userUids') as string[];
        const gameOver = gameDoc.get('gameOver') as boolean;
        if (userUids[currentPlayerId] !== uid && !gameOver) {
          throw new UserError(`It's not your turn yet.`);
        }

        const timesLoaded = gameDoc.get('timesLoaded') as number;
        const token = gameDoc.get('token') as string;
        const stateSerialized = gameStateDoc.get('stateSerialized') as string;

        // All writes in the transaction must be placed after all reads.
        transaction.update(gameRef, { timesLoaded: timesLoaded + 1 });

        return { stateSerialized, timesLoaded, token };
      });

      dispatch(setOnlineGameId(onlineGameId));
      dispatch(setOnlineTimesLoaded(timesLoaded));
      dispatch(setOnlineToken(token));

      const saveGameData = await decompressObject(stateSerialized, 'game');
      dispatch(loadGame(saveGameData));

      success?.();
    } catch (e) {
      dispatch(setErrorMessage('Failed to load the game', e));
      error?.();
    }
  };


export const deleteGameFromServer = (onlineGameId: string, success?: () => void, error?: () => void) =>
  async (dispatch: AppDispatch) => {
    try {
      const gameRef = doc(firestore, 'games', onlineGameId);
      const gameStateRef = doc(firestore, 'gameStates', onlineGameId);

      await runTransaction(firestore, async transaction => {
        transaction.delete(gameRef);
        transaction.delete(gameStateRef);
      });

      success?.();
    } catch (e) {
      dispatch(setErrorMessage('Game not deleted', e));
      error?.();
    }
  };


// Basic action creators //////////////////////////////////////////////////////////////////////////

export const setOnlineGameId = (onlineGameId: string | null): SetOnlineGameId => ({
  type: ActionTypes.SET_ONLINE_GAME_ID,
  payload: {
    onlineGameId
  }
});

export const setOnlineTimesLoaded = (onlineTimesLoaded: number): SetOnlineTimesLoaded => ({
  type: ActionTypes.SET_ONLINE_TIMES_LOADED,
  payload: {
    onlineTimesLoaded
  }
});

export const setOnlineToken = (onlineToken: string): SetOnlineToken => ({
  type: ActionTypes.SET_ONLINE_TOKEN,
  payload: {
    onlineToken
  }
});

export const setLoggedInUser = (loggedInUser: OnlineUser | null): SetLoggedInUser => ({
  type: ActionTypes.SET_LOGGED_IN_USER,
  payload: {
    loggedInUser
  }
});

export type OnlineAction =
  | SetOnlineGameId
  | SetOnlineTimesLoaded
  | SetOnlineToken
  | SetLoggedInUser;
