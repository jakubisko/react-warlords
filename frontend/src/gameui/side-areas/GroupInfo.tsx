import './GroupInfo.scss';
import { memo, type JSX } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import ArmyImage from '../common/ArmyImage';
import { getArmiesAtPlaceOfGroupOrderedByGroups } from '../../model/game/selectorsPlaying';
import { transferArmyBetweenGroups } from '../../model/game/actionsPlaying';
import { activateGroupAndCalculatePath, setShowingHero } from '../../model/ui/actions';
import { isAdditionalHeroActionAvailableAtPlaceOfActiveGroup } from '../../model/game/selectorsHero';
import { straightMoveCost, maxTileCapacity } from 'ai-interface/constants/path';
import { getMoveTypeOfGroup } from '../../model/path/selectors';
import { AbilitiesIcons } from '../common/AbilitiesIcons';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { HeroPortrait } from '../common/HeroPortrait';

/** How many move points it takes to have the move indicator display 100%. */
const maxMove = 20 * straightMoveCost;

const VISIBLE_STYLE = {};
const HIDDEN_STYLE = { visibility: 'hidden' };

function GroupInfo() {
  const activeGroupId = useSelector((state: RootState) => state.ui.activeGroupId!);
  const armies = useSelector((state: RootState) => getArmiesAtPlaceOfGroupOrderedByGroups(state, activeGroupId));
  const heroes = useSelector((state: RootState) => state.game.heroes);
  const isAdditionalHeroActionAvailable = useSelector((state: RootState) => isAdditionalHeroActionAvailableAtPlaceOfActiveGroup(state));
  const abilities = useSelector((state: RootState) => getMoveTypeOfGroup(state, activeGroupId).abilities);
  const dispatch: AppDispatch = useDispatch();

  const items: JSX.Element[] = [];

  for (let i = 0; i < armies.length; i++) {
    const { id, groupId, armyTypeId, moveLeft, blessed } = armies[i];
    const isActiveGroup = groupId === activeGroupId;

    items.push(
      <div key={id} className="army-image-with-move">

        <ArmyImage
          armyTypeId={armyTypeId}
          selectable={true}
          selected={isActiveGroup}
          grayed={!isActiveGroup}
          blessed={blessed}
          onLeftClick={() => {
            if (isActiveGroup) {
              if (armies.filter(army => army.groupId === activeGroupId).length === 1) {
                // If user tries to remove last army from group, instead deselect everything.
                dispatch(activateGroupAndCalculatePath(null));
                return;
              }
              dispatch(transferArmyBetweenGroups(id, activeGroupId, null));
            } else {
              dispatch(transferArmyBetweenGroups(id, groupId, activeGroupId));
            }
            // Path needs to be recalculated because move type of group changes.
            dispatch(activateGroupAndCalculatePath(activeGroupId));
          }}
          onRightClick="INFO"
        />

        <div className="move">
          {moveLeft > 0 && (
            <div className="move-indicator"
              style={{ width: (100 * moveLeft / maxMove) + '%' }}
            />
          )}
          {moveLeft > maxMove && (
            <div className="move-indicator-2"
              style={{ width: (100 * (moveLeft - maxMove) / maxMove) + '%' }}
            />
          )}
        </div>

        {/* Radio for selecting group is always present, so that when two heroes are in one group, the "inspect hero" icon is correctly placed. */}
        <div
          className="activate-group"
          style={(i === 0 || groupId !== armies[i - 1].groupId) ? VISIBLE_STYLE : HIDDEN_STYLE}
          onClick={() => dispatch(activateGroupAndCalculatePath(isActiveGroup ? null : groupId))}
        >
          <input type="radio" checked={isActiveGroup} readOnly />
        </div>

        {standardArmyTypes[armyTypeId].hero &&
          <div
            className={'hero btn' + (isAdditionalHeroActionAvailable ? ' flashing' : '')}
            onClick={() => dispatch(setShowingHero(id))}
          >
            <HeroPortrait imageId={heroes[id].portraitId} />
          </div>
        }

      </div>
    );
  }

  for (let i = items.length; i < maxTileCapacity; i++) {
    items.push(
      <div key={i} className="army-image-with-move">
        <ArmyImage
          armyTypeId={null}
          selectable={true}
        />
      </div>
    );
  }

  function joinAllGroups() {
    for (const { id, groupId } of armies) {
      if (groupId !== activeGroupId) {
        dispatch(transferArmyBetweenGroups(id, groupId, activeGroupId));
      }
    }

    // Path needs to be recalculated because move type of group changes.
    dispatch(activateGroupAndCalculatePath(activeGroupId));
  }

  return (
    <div className="army-group-info">
      {items}

      <ButtonWithShortcut
        shortcutCode="Space"
        disabled={armies.every((x, i) => i === 0 || armies[i].groupId === armies[i - 1].groupId)}
        onClick={joinAllGroups}
      >
        Join all
      </ButtonWithShortcut>

      <AbilitiesIcons abilities={abilities} />
    </div >
  );
}

// When you open some modal, GameScreen redraws all its children. "memo" prevents redraw of this component if it hasn't changed.
export default memo(GroupInfo);
