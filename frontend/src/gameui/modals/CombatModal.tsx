import './CombatModal.scss';
import { useEffect, useMemo, useRef, useState, type JSX } from 'react';
import { AppDispatch, RootState } from '../../model/root/configureStore';
import { Button, Modal } from 'react-bootstrap';
import ArmyImage from '../common/ArmyImage';
import { PlayerFlag } from '../common/PlayerFlag';
import { allArmyTypes } from '../../model/game/constants';
import { AbilitiesIcons } from '../common/AbilitiesIcons';
import { afterCombatAnimationEnded } from '../../model/path/actions';
import { CombatEventType } from 'ai-interface/types/combat';
import { Army } from 'ai-interface/types/army';
import { Terrain, terrainToName } from 'ai-interface/constants/terrain';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { EMPTY_ARRAY, EMPTY_MAP, getRandomItem } from '../../functions';
import { useDispatch, useSelector } from 'react-redux';

/** This many milliseconds will take one action of combat. */
// NOTE: This value is also in the definition of animation in ArmyImage.scss.
const combatAnimationDelay = 600;

/** How long it takes for the intro and outro music to fade away when interrupted. */
const musicFadeDuration = 1000;

export default function CombatModal() {
  const dispatch: AppDispatch = useDispatch();

  const timer = useRef<NodeJS.Timeout | null>(null);

  // Holds the id of the last long sound played (the start or the end of the combat; not one hit).
  // When the user closes the modal, he isn't interested is listening to it anymore, so we'll fade away.
  const soundId = useRef<number | null>(null);

  // NOTE: The animation should never access the game model, only the combatInputs and combatResult.
  // This separates combat from game state and enables for example simulating combat.
  const combatInputs = useSelector((state: RootState) => state.combat.combatInputs!);
  const combatResult = useSelector((state: RootState) => state.combat.combatResult!);
  const players = useSelector((state: RootState) => state.game.players);
  const { attackingArmies, defendingArmies, attackingPlayerId, defendingPlayerId, conditions: { fortification, terrain, city } } = combatInputs;
  const { combatEvents, attackerWon, attackingAbilities, defendingAbilities } = combatResult;

  // -1 = Before pressing Start
  // 0 = After pressing Start but before applying first event
  // 1 = event[0] was applied
  // combatEvents.length >= Combat ended
  const [combatEventsDone, setCombatEventsDone] = useState<number>(-1);
  const [damaged, setDamaged] = useState<{ [armyId: string]: boolean }>(EMPTY_MAP);
  const [dead, setDead] = useState<{ [armyId: string]: boolean }>(EMPTY_MAP);
  const [ambushed, setAmbushed] = useState<{ [armyId: string]: boolean }>(EMPTY_MAP);

  const modalCaption = useMemo(() => {
    if (city) {
      return 'Combat in City';
    } else if (terrain === Terrain.Water || terrain === Terrain.Volcanic || terrain === Terrain.Ice) {
      return 'Combat on ' + terrainToName[terrain];
    }
    return 'Combat in ' + terrainToName[terrain];
  }, EMPTY_ARRAY);

  function fadeAwaySound() {
    if (soundId.current !== null) {
      soundEffects.fade(soundEffects.volume(), 0, musicFadeDuration, soundId.current);
    }
  }

  useEffect(() => {
    const soundEffect = getRandomItem([SoundEffect.CombatStart1, SoundEffect.CombatStart2, SoundEffect.CombatStart3]);
    soundId.current = soundEffects.play(soundEffect);

    return () => {
      fadeAwaySound();

      if (timer.current !== null) {
        clearTimeout(timer.current);
      }
    };
  }, EMPTY_ARRAY);

  useEffect(() => {
    if (combatEventsDone < 0) {
      return;
    }

    if (combatEventsDone === 0) {
      fadeAwaySound();
    }

    if (combatEventsDone >= combatEvents.length) {
      const sound = attackerWon
        ? (fortification > 0 ? SoundEffect.CombatVictoryInCity : SoundEffect.CombatVictory)
        : SoundEffect.CombatLost;
      soundId.current = soundEffects.play(sound);
      return;
    }

    const { type, losingArmyId } = combatEvents[combatEventsDone];
    const losingArmyTypeId = [...attackingArmies, ...defendingArmies].find(army => army.id === losingArmyId)!.armyTypeId;
    const soundEffect = (type === CombatEventType.Damage ? 'Damage_' : 'Kill_')
      + losingArmyTypeId.replace(/[0-9]/g, ''); // In case of a hero, remove the level number.
    soundEffects.play(soundEffect);
    if (type === CombatEventType.Ambush) {
      soundEffects.play(SoundEffect.Ambush);
    }

    setDamaged({ [losingArmyId]: true });
    if (type === CombatEventType.Kill) {
      setDead({ ...dead, [losingArmyId]: true });
    } else if (type === CombatEventType.Ambush) {
      setAmbushed({ ...ambushed, [losingArmyId]: true });
    }

    timer.current = setTimeout(
      () => {
        setDamaged(EMPTY_MAP); // Clean the damaged state. Without this, second damage animation wouldn't happen, since we can't add the css class twice. 
        setCombatEventsDone(combatEventsDone + 1);
      },
      combatAnimationDelay
    );
  }, [combatEventsDone]);

  function drawArmies(armies: Army[]): JSX.Element[] {
    const { effectiveStrength } = combatResult;
    const armiesReversed = [...armies];
    armiesReversed.reverse();

    return armiesReversed.map(army => {
      const baseStr = allArmyTypes[army.armyTypeId].strength;
      const effStr = effectiveStrength[army.id];

      return (
        <div key={army.id} className="d-inline-block my-2">
          <ArmyImage
            armyTypeId={army.armyTypeId}
            damaged={damaged[army.id]}
            dead={dead[army.id]}
            ambushed={ambushed[army.id]}
            blessed={army.blessed}
            onLeftClick="INFO"
            onRightClick="INFO"
          />

          <div className={'text-center' + (effStr < baseStr ? ' str-lower' : (effStr > baseStr ? ' str-higher' : ''))}>
            {effStr}
          </div>
        </div>
      );
    });
  }

  return (
    <Modal
      className={'combat-modal overflow-hidden' + (defendingArmies.length > 16 ? ' big-combat' : '')}
      show centered backdrop="static"
    >
      <Modal.Header>
        <Modal.Title>
          {modalCaption}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="d-flex align-items-center">
          <span>Defender: {players[defendingPlayerId].name}</span>
          <span><PlayerFlag owner={defendingPlayerId} /></span>
          <span className="flex-grow-1" />
          <span><AbilitiesIcons abilities={defendingAbilities} tooltipPlacement="left" /></span>
        </div>

        <div className="defending-armies mb-4">
          {drawArmies(defendingArmies)}
        </div>

        <div className="d-flex align-items-center">
          <span>Attacker: {players[attackingPlayerId].name}</span>
          <span><PlayerFlag owner={attackingPlayerId} /></span>
          <span className="flex-grow-1" />
          <span><AbilitiesIcons abilities={attackingAbilities} tooltipPlacement="left" /></span>
        </div>

        <div className="attacking-armies">
          {drawArmies(attackingArmies)}
        </div>
      </Modal.Body>

      <Modal.Footer>
        {combatEventsDone < 0 ? (
          <Button onClick={() => setCombatEventsDone(0)}>
            Start
          </Button>
        ) : (
          <Button
            disabled={combatEventsDone < combatEvents.length}
            onClick={() => dispatch(afterCombatAnimationEnded())}
          >
            Close
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}
