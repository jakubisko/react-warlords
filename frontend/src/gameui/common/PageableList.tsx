import './PageableList.scss';
import { ReactNode, useEffect, useState } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { Spinner3 } from './Spinner3';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretLeft, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { clamp } from '../../functions';

interface PageableListProps {
  list: ReactNode[];
  working?: boolean;
  itemHeightPx: number;
  restOfHeightPx: number; // Content will take 100% of the page height minus this number (the height of headers, footers etc.)
  maxItemsPerPage: number;
  footer?: ReactNode;
}

export default function PageableList(props: PageableListProps) {
  const { list, working, itemHeightPx, restOfHeightPx, maxItemsPerPage, footer } = props;

  const [offset, setOffset] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);

  useEffect(() => {
    function handleResize() {
      const { innerHeight } = window;
      const itemsPerPage = clamp(1, maxItemsPerPage, Math.floor((innerHeight - restOfHeightPx) / itemHeightPx));
      setItemsPerPage(itemsPerPage);
      setOffset(0);
    }

    // Initial call so that the canvas has correct size and is drawn.
    handleResize();

    // And then resize and redraw the canvas whenever the window is resized.
    window.addEventListener('resize', handleResize, true);
    return () => window.removeEventListener('resize', handleResize, true);
  }, [list, itemHeightPx, maxItemsPerPage, restOfHeightPx]);

  return (
    <>
      <div
        className="position-relative"
        style={{ height: itemHeightPx * itemsPerPage }}
      >
        {working && (
          <div className="pageable-list-working">
            <div>
              <Spinner3 />
            </div>
          </div>
        )}

        {list.slice(offset, offset + itemsPerPage).map((item, i) =>
          <div key={i} style={{ height: itemHeightPx }}>
            {item}
          </div>
        )}
      </div>

      <div className="pageable-list-footer d-flex align-items-center justify-content-between">
        <ButtonGroup className={list.length > itemsPerPage ? '' : 'invisible'}>
          <Button
            disabled={offset <= 0 || working}
            onClick={() => setOffset(offset - itemsPerPage)}
          >
            <FontAwesomeIcon icon={faCaretLeft} />
          </Button>

          <Button
            disabled={offset + itemsPerPage >= list.length || working}
            onClick={() => setOffset(offset + itemsPerPage)}
          >
            <FontAwesomeIcon icon={faCaretRight} />
          </Button>
        </ButtonGroup>

        {list.length > itemsPerPage && (
          <div>
            {1 + offset}
            {(itemsPerPage > 1) ? `-${Math.min(offset + itemsPerPage, list.length)}` : ''}
            &nbsp;/ {list.length}
          </div>
        )}

        {footer}
      </div>
    </>
  );
}
