import { Button } from 'react-bootstrap';
import { setErrorMessage } from '../../model/ui/actions';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../model/root/configureStore';

interface SaveButtonProps {
  caption: string;
  disabled?: boolean;
  fileName: string;
  fileExtension: string;
  getDownloadData: () => Promise<Blob>; // Returns content of the file that will be downloaded.
}

export default function SaveButton(props: SaveButtonProps) {
  const { caption, disabled, fileName, fileExtension } = props;

  const dispatch: AppDispatch = useDispatch();

  const handleDownload = () =>
    props.getDownloadData().then((content) => {
      const url = URL.createObjectURL(content);
      const a = document.createElement('a');
      a.href = url;
      a.download = `${fileName}.${fileExtension}`;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      URL.revokeObjectURL(url);
    }).catch((err) => dispatch(setErrorMessage('Saving failed', err)));

  return (
    <>
      <Button
        disabled={disabled}
        onClick={handleDownload}
      >
        {caption}
      </Button>
    </>
  );
}
