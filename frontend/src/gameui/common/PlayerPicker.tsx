import './PlayerPicker.scss';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { PlayerFlag } from './PlayerFlag';
import { defaultPlayerNames } from 'ai-interface/constants/player';

interface PlayerPickerProps {
  value: number; // Currently picked player id 
  onChange: (playerId: number) => void;
}

export const PlayerPicker = ({ value, onChange }: PlayerPickerProps) => (
  <DropdownButton
    id="playerPicker"
    title={<PlayerFlag owner={value} />}
    className="d-inline-block"
  >
    {defaultPlayerNames.map((name, playerId) => (
      <Dropdown.Item
        key={playerId}
        onClick={() => onChange(playerId)}
      >
        <PlayerFlag owner={playerId} />
      </Dropdown.Item>
    ))}
  </DropdownButton>
);
