import './ManualModal.scss';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Modal } from 'react-bootstrap';
import { setShowingManual } from '../../model/ui/actions';
import PageableList from '../common/PageableList';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faCog, faDragon, faExpand, faForward, faHandPointRight, faHome, faHourglassHalf, faMoon, faPlay, faScroll, faSearch, faShieldAlt, faSkull, faSun, faTimes } from '@fortawesome/free-solid-svg-icons';
import FullscreenButton from '../common/FullscreenButton';
import VolumeButtons from '../common/VolumeButtons';
import { HeroPortrait } from '../common/HeroPortrait';
import { createArray } from '../../functions';
import { combatDiceSides, getChanceForAToKillB, maxStrengthOnShip, maxStrengthOverall } from 'ai-interface/constants/combat';
import { experiencePercentForAttackingCity, heroExperienceLevels } from 'ai-interface/constants/experience';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { AppDispatch } from '../../model/root/configureStore';

export default function ManualModal() {
  const dispatch: AppDispatch = useDispatch();

  const [subpage, setSubpage] = useState(0);

  const listOfControls = [
    <div key={0} className="line">
      <span>Welcome to Warlords! This quick guide will explain basic controls of the game</span>
    </div>,
    <div key={1} className="line">
      <FontAwesomeIcon size="lg" icon={faExpand} className="me-2" />
      <span>Go to fullscreen. On mobile, fullscreen is required to see all controls</span>
    </div>,
    <div key={2} className="line">
      <span>On mobile, simulate right click by a long press</span>
    </div>,
    <div key={3} className="line">
      <span>Use pinch gesture or mouse wheel to zoom the play area</span>
    </div>,
    <div key={4} className="line">
      <span>Double click the map in the corner to enlarge it</span>
    </div>,
    <div key={5} className="line">
      <FontAwesomeIcon size="lg" icon={faCog} className="me-2" />
      <span>System menu - save, load</span>
    </div>,
    <div key={6} className="line">
      <FontAwesomeIcon size="lg" icon={faDragon} className="me-2" />
      <span>Change order of your armies</span>
    </div>,
    <div key={7} className="line">
      <FontAwesomeIcon size="lg" icon={faScroll} className="me-2" />
      <span>Show reports on other players</span>
    </div>,
    <div key={8} className="line">
      <FontAwesomeIcon size="lg" icon={faShieldAlt} className="me-2" />
      <span>Show today&apos;s hero offer</span>
    </div>,
    <div key={9} className="line">
      <FontAwesomeIcon size="lg" icon={faHandPointRight} className="me-2" />
      <span>Select next group</span>
    </div>,
    <div key={10} className="line">
      <FontAwesomeIcon size="lg" icon={faTimes} className="me-2" />
      <span>(or right click) Deselect current group</span>
    </div>,
    <div key={11} className="line">
      <FontAwesomeIcon size="lg" icon={faPlay} className="me-2" />
      <span>Move selected group</span>
    </div>,
    <div key={12} className="line">
      <FontAwesomeIcon size="lg" icon={faForward} className="me-2" />
      <span>Move all groups</span>
    </div>,
    <div key={13} className="line">
      <FontAwesomeIcon size="lg" icon={faSearch} className="me-2" />
      <span>Info about place on which selected group stands</span>
    </div>,
    <div key={14} className="line">
      <FontAwesomeIcon size="lg" icon={faSkull} className="me-2" />
      <span>Dismiss selected group</span>
    </div>,
    <div key={15} className="line">
      <FontAwesomeIcon size="lg" icon={faMoon} className="me-2" />
      <FontAwesomeIcon size="lg" icon={faSun} className="me-2" />
      <FontAwesomeIcon size="lg" icon={faBell} className="me-2" />
      <span>Wait here / Wake up / Wake all <br /> Waiting group won&apos;t be selected again</span>
    </div>,
    <div key={16} className="line">
      <FontAwesomeIcon size="lg" icon={faHourglassHalf} className="me-2" />
      <span>End turn</span>
    </div>,
    <div key={17} className="line">
      <div className="path-symbols target-with-return me-2" />
      <span>Reach target and return today</span>
    </div>,
    <div key={18} className="line">
      <div className="path-symbols target-no-return me-2" />
      <span>Reach target today but without returning</span>
    </div>,
    <div key={19} className="line">
      <div className="hero">
        <HeroPortrait imageId={0} />
      </div>
      <span>Click the portrait to inspect the hero, pick artifacts and visit ruins</span>
    </div>
  ];

  const listOfShortcuts = [
    <div key={0} className="line">
      <span>Red letter in button&apos;s caption denotes shortcut</span>
    </div>,
    <div key={1} className="line">
      <span>Space bar - Join all armies into selected group</span>
    </div>,
    <div key={2} className="line">
      <span>Delete - Dismiss selected group</span>
    </div>,
    <div key={3} className="line">
      <span>Escape - Unselect current group</span>
    </div>,
  ];

  const listOfCombatMechanics = [
    <div key={0} className="line">
      <Calculator />
    </div>,
    <div key={1} className="line">
      <span>First, effective Strength, Hit points and Ambush of each army is calculated by applying all bonuses.</span>
    </div>,
    <div key={2} className="line">
      <span>These values won&apos;t change during combat even as armies die.</span>
    </div>,
    <div key={3} className="line">
      <span>Strength can never go below 1.</span>
    </div>,
    <div key={4} className="line">
      <span>Strength can never go above {maxStrengthOverall}.</span>
    </div>,
    <div key={5} className="line">
      <span>Strength of non-flying army on ship can never go above {maxStrengthOnShip}.</span>
    </div>,
    <div key={6} className="line">
      <span>Hit points can never go below 1.</span>
    </div>,
    <div key={7} className="line">
      <span>Ambush can go above 100%. That is relevant when facing another army with Ambush.</span>
    </div>,
    <div key={8} className="line">
      <span>If the first armies in both groups have Ambush, both values will be decreased by the lower value.</span>
    </div>,
    <div key={9} className="line">
      <span>Each army gets only one chance to Ambush an enemy during entire combat.</span>
    </div>,
    <div key={10} className="line">
      <span>After that, regular combat ensues.</span>
    </div>,
    <div key={11} className="line">
      <span>Both armies throw a {combatDiceSides} sided dice.</span>
    </div>,
    <div key={12} className="line">
      <span>An army succeeds if it threw a number that is less or equal to its Strength.</span>
    </div>,
    <div key={13} className="line">
      <span>If one army fails and the other succeeds, the failing one loses 1 Hit point.</span>
    </div>,
    <div key={14} className="line">
      <span>If both armies succeed or both fail, no one takes damage.</span>
    </div>,
    <div key={15} className="line">
      <span>The combat continues until one group is completely destroyed.</span>
    </div>,
    <div key={16} className="line">
      <span>Hit points of wounded but not dead army are healed after the combat.</span>
    </div>,
  ];

  const listOfExperienceLevels = [
    <div key={0} className="line">
      <span>Total experience required to reach each level:</span>
    </div>,
    ...heroExperienceLevels.slice(1).map((value, index) =>
      <div key={-1 - index} className="line">
        <span>Level {index + 1} - {value}</span>
      </div>
    ),
    <div key={1} className="line" >
      <span>Defeating an army awards experience equal to its build cost.</span>
    </div >,
    <div key={2} className="line">
      <span>Defeating a hero awards {standardArmyTypes.hro1.buildingCost} experience per level.</span>
    </div>,
    <div key={3} className="line">
      <span>Capturing a city awards {experiencePercentForAttackingCity}% experience bonus.</span>
    </div>,
    <div key={4} className="line">
      <span>Computer player receives percentual bonus as set at the start of the game.</span>
    </div>,
  ];

  const listOfHeroOffer = [
    <div key={0} className="line">
      <span>The chance of being offered a hero is 1 in N.</span>
    </div>,
    <div key={1} className="line">
      <span>Starting value of N is 2.</span>
    </div>,
    <div key={2} className="line">
      <span>Add the number of heroes you already have.</span>
    </div>,
    <div key={3} className="line">
      <span>Add 2 if you have less than 500 gold.</span>
    </div>,
    <div key={4} className="line">
      <span>Add 1 if you have between 500 and 1000 gold.</span>
    </div>,
    <div key={5} className="line">
      <span>Subtract 1 if you have no heroes and you have at most 2 cities and it&apos;s at most 2nd week.</span>
    </div>,
    <div key={6} className="line">
    </div>,
    <div key={7} className="line">
      <span>The maximum possible level of offered hero is 4.</span>
    </div>,
    <div key={8} className="line">
      <span>The chances for every possible hero level are equal.</span>
    </div>,
    <div key={9} className="line">
      <span>Levels exceeding current week won&apos;t be considered.</span>
    </div>,
    <div key={10} className="line">
      <span>Unaffordable heroes won&apos;t be considered.</span>
    </div>,
    <div key={11} className="line">
    </div>,
    <div key={12} className="line">
      <span>Hero costs 150 gold plus 100 gold for each level above the first.</span>
    </div>,
    <div key={13} className="line">
      <span>Hero costs 25 gold more for each hero you already have.</span>
    </div>,
    <div key={14} className="line">
    </div>,
    <div key={15} className="line">
      <span>Hero of level 2 or more brings with him one random army of the same level.</span>
    </div>,
    <div key={16} className="line">
      <span>The army increases the cost by 20% of its build cost.</span>
    </div>,
    <div key={17} className="line">
      <span>If that would exceed current gold, the hero is offered without the army.</span>
    </div>,
  ];

  const listOfVarious = [
    <div key={0} className="line">
      <span>The upkeep for armies is paid in the reverse ordering of your armies. The weakest will desert.</span>
    </div>,
    <div key={0} className="line">
      <span>Diagonal step costs 40% more than straight step.</span>
    </div>,
    <div key={0} className="line">
      <span>A group can move if it has positive move left. It&apos;s allowed to have negative move left after step.</span>
    </div>,
    <div key={0} className="line">
      <span>Thus a group moving in one direction may end up one step short of returning next day.</span>
    </div>,
  ];

  const titles = ['Controls', 'Keyboard shortcuts', 'Combat mechanics', 'Experience levels', 'Hero offer', 'Various'];

  const listHome =
    titles.map((title, index) =>
      <div key={index} className="line" onClick={() => setSubpage(index + 1)}>
        <span className="link">{title}</span>
      </div>
    );

  const lists = [listHome, listOfControls, listOfShortcuts, listOfCombatMechanics, listOfExperienceLevels, listOfHeroOffer, listOfVarious];

  return (
    <Modal className="manual-modal overflow-hidden" show centered backdrop="static">
      <Modal.Header>
        <Modal.Title>{subpage === 0 ? 'Manual' : titles[subpage - 1]}</Modal.Title>
        <VolumeButtons />
        <FullscreenButton />
      </Modal.Header>

      <Modal.Body>
        <PageableList
          list={lists[subpage]}
          maxItemsPerPage={10}
          itemHeightPx={55}
          restOfHeightPx={145}
          footer={<div>
            {subpage > 0 && <Button onClick={() => setSubpage(0)}><FontAwesomeIcon icon={faHome} /></Button>}
            <Button onClick={() => dispatch(setShowingManual(false))}>Close</Button>
          </div>}
        />
      </Modal.Body>
    </Modal>
  );
}

const Calculator = () => {
  const maxHitPoints = 10;
  const [strA, setStrA] = useState(1);
  const [strB, setStrB] = useState(1);
  const [hitA, setHitA] = useState(2);
  const [hitB, setHitB] = useState(2);

  const dropdown = (symbol: string, maxValue: number, value: number, setter: (value: number) => void) => (
    <select
      className="form-control d-inline"
      value={value}
      onChange={e => setter(parseInt(e.target.value))}
    >
      {createArray(maxValue, i => i).map(i =>
        <option key={i} value={i + 1}>
          {symbol} {i + 1}
        </option>
      )}
    </select>
  );

  const aWins = getChanceForAToKillB(strA, hitA, strB, hitB);

  return (
    <span className="calculator w-100 d-flex align-items-center justify-content-between">
      <form>
        {dropdown('⚔', maxStrengthOverall, strA, setStrA)}
        {dropdown('❤', maxHitPoints, hitA, setHitA)}
      </form>

      <div>{aWins.toFixed(0)}% : {(100 - aWins).toFixed(0)}%</div>

      <form>
        {dropdown('⚔', maxStrengthOverall, strB, setStrB)}
        {dropdown('❤', maxHitPoints, hitB, setHitB)}
      </form>
    </span>
  );
};
