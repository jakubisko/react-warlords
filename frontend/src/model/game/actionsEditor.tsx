import { AppDispatch, RootState } from '../root/configureStore';
import { MapData } from './types';
import { TileSprite } from '../../gameui/map/tactical-map/spriteMetaData';
import { changePlaceOwner, destroyArmy } from './actionsPlayingThunks';
import { setMapFocus, activateGroupAndCalculatePath } from '../ui/actions';
import { getEmptyMap, constructGameStateFromMapData, replaceRandomObjectsInGameState } from './saveLoad';
import { setGameState } from './actionsTurnPassing';
import { getRandomUnusedCityName } from '../../predefined-data/predefinedDataGetters';
import { getAllCityNames } from './selectorsEditor';
import { ProgramMode } from '../ui/types';
import { destroyArtifactOnGround } from './actionsHero';
import { setUndoQueue } from '../editor/actions';
import { Terrain } from 'ai-interface/constants/terrain';
import { Structure, isStructureFlaggable } from 'ai-interface/constants/structure';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { structureAllowedTerrains } from '../editor/constants';

export enum ActionTypes {
  DRAW_WITH_TERRAIN_BRUSH = '[Game] DRAW_WITH_TERRAIN_BRUSH',
  SET_TERRAIN = '[Game] SET_TERRAIN',
  SET_BACKGROUND_SPRITES = '[Game] SET_BACKGROUND_SPRITES',
  CREATE_RUIN = '[Game] CREATE_RUIN',
  CREATE_FLAGGABLE = '[Game] CREATE_FLAGGABLE',
  CREATE_CITY = '[Game] CREATE_CITY',
  PLACE_STRUCTURE = '[Game] PLACE_STRUCTURE',
  REMOVE_STRUCTURE = '[Game] REMOVE_STRUCTURE',
  SET_RUIN_LEVEL = '[Game] SET_RUIN_LEVEL',
  SET_CITY_NAME = '[Game] SET_CITY_NAME',
  SET_SIGNPOST_MESSAGE = '[Game] SET_SIGNPOST_MESSAGE'
}

export interface DrawWithTerrainBrush {
  type: ActionTypes.DRAW_WITH_TERRAIN_BRUSH;
  payload: {
    row: number;
    col: number;
    terrain: Terrain;
    brushSize: number; // use only odd numbers - 1x1, 3x3, 5x5...
  }
}

export interface SetTerrain {
  type: ActionTypes.SET_TERRAIN;
  payload: {
    terrain: Terrain[][];
  }
}

export interface SetBackgroundSprites {
  type: ActionTypes.SET_BACKGROUND_SPRITES;
  payload: {
    backgroundSprites: TileSprite[][][]
  }
}

export interface CreateRuin {
  type: ActionTypes.CREATE_RUIN;
  payload: {
    ruinId: string;
    row: number;
    col: number;
  }
}

export interface CreateFlaggable {
  type: ActionTypes.CREATE_FLAGGABLE;
  payload: {
    flaggableId: string;
    row: number;
    col: number;
  }
}

export interface CreateCity {
  type: ActionTypes.CREATE_CITY;
  payload: {
    cityId: string;
    row: number;
    col: number;
  }
}

export interface PlaceStructure {
  type: ActionTypes.PLACE_STRUCTURE;
  payload: {
    row: number;
    col: number;
    structure: Structure;
    ruinId?: string;
    flaggableId?: string;
    cityId?: string;
  }
}

export interface RemoveStructure {
  type: ActionTypes.REMOVE_STRUCTURE;
  payload: {
    row: number;
    col: number;
  }
}

export interface SetRuinLevel {
  type: ActionTypes.SET_RUIN_LEVEL;
  payload: {
    ruinId: string;
    level: number;
  }
}

export interface SetCityName {
  type: ActionTypes.SET_CITY_NAME;
  payload: {
    cityId: string;
    name: string;
  }
}

export interface SetSignpostMessage {
  type: ActionTypes.SET_SIGNPOST_MESSAGE;
  payload: {
    row: number;
    col: number;
    message?: string;
  }
}

// Action creators ////////////////////////////////////////////////////////////////////////////////

/** Sets game state to an empty map of given size. */
export const createEmptyMap = (size: number) => (dispatch: AppDispatch) => {
  const mapData = getEmptyMap(size);
  dispatch(loadMap(mapData, ProgramMode.MapEditor));
};

/**
 * Set game state to that described in map file. Program mode MapEditor will retain placeholders for random objects. Program mode Playing
 * will replace them with actual randomly chosen game objects.
 */
export const loadMap = (mapData: MapData, programMode: ProgramMode) => (dispatch: AppDispatch) => {
  const gameState = constructGameStateFromMapData(mapData);
  if (programMode === ProgramMode.Playing) {
    replaceRandomObjectsInGameState(gameState);
  }

  dispatch(activateGroupAndCalculatePath(null)); // Old activeGroupId would reference non-existing group
  dispatch(setGameState(gameState));
  dispatch(setMapFocus(gameState.size / 2, gameState.size / 2));

  if (programMode === ProgramMode.MapEditor) {
    dispatch(setUndoQueue([mapData], 0));
  }
};

/** Changes the terrain around given place. This will also erase objects if they can no longer be on the new terrain. */
export const drawWithTerrainBrushEraseIllegalTiles = (row: number, col: number, terrain: Terrain, brushSize: number) =>
  (dispatch: AppDispatch, getState: () => RootState) => {
    const { size, structures } = getState().game;

    const brushRadius = (brushSize - 1) / 2;
    for (let row2 = Math.max(row - brushRadius, 0); row2 <= Math.min(row + brushRadius, size - 1); row2++) {
      for (let col2 = Math.max(col - brushRadius, 0); col2 <= Math.min(col + brushRadius, size - 1); col2++) {
        if ((row2 - row) ** 2 + (col2 - col) ** 2 < brushSize ** 2 / 4) {
          const structure = structures[row2][col2];

          // This structure is not allowed on the terrain
          if (!structureAllowedTerrains[structure].includes(terrain)) {
            dispatch(eraseTile(row2, col2));
          }
        }
      }
    }

    dispatch(drawWithTerrainBrush(row, col, terrain, brushSize));
  };

export const drawWithTerrainBrush = (row: number, col: number, terrain: Terrain, brushSize: number): DrawWithTerrainBrush => ({
  type: ActionTypes.DRAW_WITH_TERRAIN_BRUSH,
  payload: {
    row,
    col,
    terrain,
    brushSize
  }
});

export const setTerrain = (terrain: Terrain[][]): SetTerrain => ({
  type: ActionTypes.SET_TERRAIN,
  payload: {
    terrain: terrain.map(row => [...row])
  }
});

export const setBackgroundSprites = (backgroundSprites: TileSprite[][][]): SetBackgroundSprites => ({
  type: ActionTypes.SET_BACKGROUND_SPRITES,
  payload: {
    backgroundSprites: backgroundSprites.map(row => [...row])
  }
});

/**
 * Creates and places a structure on given tile. This doesn't check whether it is legal to build the structure here! This assumes that the
 * tile doesn't contain any structures.
 * Ruins, flaggables and cities - this will add new item into the map with all ruins, flaggables and cities.
 */
export const createAndPlaceStructure = (row: number, col: number, structure: Structure) =>
  (dispatch: AppDispatch, getState: () => RootState) => {

    const ruinId = structure === Structure.Ruin ? `R-${row}-${col}` : undefined;
    const flaggableId = isStructureFlaggable[structure] ? `F-${row}-${col}` : undefined;
    const cityId = structure === Structure.City ? `C-${row}-${col}` : undefined;

    if (ruinId !== undefined) {
      dispatch(createRuin(ruinId, row, col));
    } else if (flaggableId !== undefined) {
      dispatch(createFlaggable(flaggableId, row, col));
    } else if (cityId !== undefined) {
      const cityName = getRandomUnusedCityName(getAllCityNames(getState()));
      dispatch(createCity(cityId, row, col));
      dispatch(setCityName(cityId, cityName));
      dispatch(placeStructure(row + 1, col, structure, ruinId, flaggableId, cityId));
      dispatch(placeStructure(row, col + 1, structure, ruinId, flaggableId, cityId));
      dispatch(placeStructure(row + 1, col + 1, structure, ruinId, flaggableId, cityId));
    }

    dispatch(placeStructure(row, col, structure, ruinId, flaggableId, cityId));

    // Prevent ownership of army to be different than the ownership of the city or flaggable structure
    if (flaggableId !== null || cityId !== undefined) {
      dispatch(changePlaceOwner(row, col, neutralPlayerId));
    }
  };

const createRuin = (ruinId: string, row: number, col: number): CreateRuin => ({
  type: ActionTypes.CREATE_RUIN,
  payload: {
    ruinId,
    row,
    col
  }
});

const createFlaggable = (flaggableId: string, row: number, col: number): CreateFlaggable => ({
  type: ActionTypes.CREATE_FLAGGABLE,
  payload: {
    flaggableId,
    row,
    col
  }
});

/** Creates a city. Name, income etc. will have default values and should be set later. */
const createCity = (cityId: string, row: number, col: number): CreateCity => ({
  type: ActionTypes.CREATE_CITY,
  payload: {
    cityId,
    row,
    col
  }
});

/**
 * Places a structure on given tile. This doesn't check whether it is legal to build the structure here! This assumes that the tile doesn't
 * contain any structures.
 * Ruins, flaggables and cities - this doesn't add new item into map with all ruins, flaggables and cities - just a reference to it. Create
 * appropriate object with create... action before calling this. For city, call placeStructure once for each of its tiles.
 */
const placeStructure = (row: number, col: number, structure: Structure, ruinId?: string, flaggableId?: string, cityId?: string): PlaceStructure => ({
  type: ActionTypes.PLACE_STRUCTURE,
  payload: {
    row,
    col,
    structure,
    ruinId,
    flaggableId,
    cityId
  }
});

/** Removes all armies and all structures on given tile. This includes cities, flaggables and ruins. */
export const eraseTile = (row: number, col: number) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { tiles, cities } = getState().game;
  const { armies, artifactIds, cityId } = tiles[row][col];

  // It's safe to iterate the array. Redux doesn't change the old state, each action returns new state.
  if (armies !== undefined) {
    armies.forEach(army => dispatch(destroyArmy(army.groupId, army.id)));
  }
  if (artifactIds !== undefined) {
    artifactIds.forEach(artifactId => dispatch(destroyArtifactOnGround(row, col, artifactId)));
  }

  if (cityId === undefined) {
    dispatch(removeStructure(row, col));
  } else {
    const { row, col } = cities[cityId];
    dispatch(removeStructure(row, col));
    dispatch(removeStructure(row, col + 1));
    dispatch(removeStructure(row + 1, col));
    dispatch(removeStructure(row + 1, col + 1));
  }
};

/**
 * Removes structure from given tile. For city, this will be called for each of its tiles. Thus this must handle situation that the tile
 * references already removed city!
 */
const removeStructure = (row: number, col: number): RemoveStructure => ({
  type: ActionTypes.REMOVE_STRUCTURE,
  payload: {
    row,
    col
  }
});

export const setRuinLevel = (ruinId: string, level: number): SetRuinLevel => ({
  type: ActionTypes.SET_RUIN_LEVEL,
  payload: {
    ruinId,
    level
  }
});

export const setCityName = (cityId: string, name: string): SetCityName => ({
  type: ActionTypes.SET_CITY_NAME,
  payload: {
    cityId,
    name
  }
});

export const setSignpostMessage = (row: number, col: number, message?: string): SetSignpostMessage => ({
  type: ActionTypes.SET_SIGNPOST_MESSAGE,
  payload: {
    row,
    col,
    message
  }
});

export type LandAction = DrawWithTerrainBrush
  | SetTerrain
  | SetBackgroundSprites
  | CreateRuin
  | CreateFlaggable
  | CreateCity
  | PlaceStructure
  | RemoveStructure
  | SetRuinLevel
  | SetCityName
  | SetSignpostMessage;
