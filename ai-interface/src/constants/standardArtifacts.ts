import { Artifact } from '../types/artifact';

/** All artifacts that can be found in the game. Use Artifact.id as a key in this map. */
export const standardArtifacts: { [artifactId: string]: Artifact } = Object.freeze({
  thunderMace: {
    id: 'thunderMace',
    name: `Thunder Mace`,
    rarity: 1,
    abilities: {
      StrengthBonus: 1
    }
  },
  giantFlail: {
    id: 'giantFlail',
    name: `Giant Flail`,
    rarity: 1,
    abilities: {
      StrengthBonus: 2,
      MoveBonus: -3
    }
  },
  deathbringerLance: {
    id: 'deathbringerLance',
    name: `Deathbringer Lance`,
    rarity: 1,
    abilities: {
      StrengthBonusWhenAttacking: 2
    }
  },
  defendersBuckler: {
    id: 'defendersBuckler',
    name: `Defender's Buckler`,
    rarity: 1,
    abilities: {
      StrengthBonusWhenDefending: 2
    }
  },
  murderersAxe: {
    id: 'murderersAxe',
    name: `Murderer's Axe`,
    rarity: 1,
    abilities: {
      StrengthBonus: 4,
      LeadershipBonus: -1
    }
  },
  iciclePike: {
    id: 'iciclePike',
    name: `Icicle Pike`,
    rarity: 1,
    abilities: {
      StrengthBonusOnIce: 5
    }
  },
  armoredGauntlet: {
    id: 'armoredGauntlet',
    name: `Armored Gauntlet`,
    rarity: 1,
    abilities: {
      HitPointsBonus: 1
    }
  },
  lightningRod: {
    id: 'lightningRod',
    name: `Lightning Rod`,
    rarity: 1,
    abilities: {
      AmbushBonus: 25
    }
  },
  rentedMedal: {
    id: 'rentedMedal',
    name: `Rented Medal`,
    rarity: 1,
    abilities: {
      LeadershipBonus: 1,
      IncomeBonus: -10
    }
  },
  martyrsHand: {
    id: 'martyrsHand',
    name: `Martyr's Hand`,
    rarity: 1,
    abilities: {
      LeadershipBonus: 1,
      HitPointsBonus: -1
    }
  },
  holyPendant: {
    id: 'holyPendant',
    name: `Holy Pendant`,
    rarity: 1,
    abilities: {
      Blessing: 2
    }
  },
  berserkersAmulet: {
    id: 'berserkersAmulet',
    name: `Berserker's Amulet`,
    rarity: 1,
    abilities: {
      IncreaseGroupAmbush: 15,
      ExperienceBonus: -33
    }
  },
  sharpshootersBow: {
    id: 'sharpshootersBow',
    name: `Sharpshooter's Bow`,
    rarity: 1,
    abilities: {
      AntiAirBonus: 1
    }
  },
  snakeRing: {
    id: 'snakeRing',
    name: `Snake Ring`,
    rarity: 1,
    abilities: {
      LowerEnemyAmbush: 20
    }
  },
  mobilizationDecree: {
    id: 'mobilizationDecree',
    name: `Mobilization Decree`,
    rarity: 1,
    abilities: {
      SummonsArmiesOfLevel: 1
    }
  },
  nomadBoots: {
    id: 'nomadBoots',
    name: `Nomad Boots`,
    rarity: 1,
    abilities: {
      MoveBonus: 5
    }
  },
  telescope: {
    id: 'telescope',
    name: `Telescope`,
    rarity: 1,
    abilities: {
      ViewRadiusBonus: 4
    }
  },
  tomeOfKnowledge: {
    id: 'tomeOfKnowledge',
    name: `Tome of Knowledge`,
    rarity: 1,
    abilities: {
      ExperienceBonus: 200
    }
  },
  scholarsVolume: {
    id: 'scholarsVolume',
    name: `Scholar's Volume`,
    rarity: 1,
    abilities: {
      ExperienceBonus: 300,
      StrengthBonus: -1
    }
  },
  sandsOfTime: {
    id: 'sandsOfTime',
    name: `Sands of Time`,
    rarity: 1,
    abilities: {
      DecreaseTrainingTime: 1
    }
  },
  cloverOfFortune: {
    id: 'cloverOfFortune',
    name: `Clover of Fortune`,
    rarity: 1,
    abilities: {
      CancelsArtifactPenalties: 1
    }
  },
  unwieldyOreCart: {
    id: 'unwieldyOreCart',
    name: `Unwieldy Ore Cart`,
    rarity: 1,
    abilities: {
      IncomeBonus: 20,
      MoveBonus: -7
    }
  },
  endlessPurseOfGold: {
    id: 'endlessPurseOfGold',
    name: `Endless Purse of Gold`,
    rarity: 1,
    abilities: {
      IncomeBonus: 10
    }
  },
  swordBreaker: {
    id: 'swordBreaker',
    name: `Sword Breaker`,
    rarity: 2,
    abilities: {
      StrengthBonus: 2
    }
  },
  skullMace: {
    id: 'skullMace',
    name: `Skull Mace`,
    rarity: 2,
    abilities: {
      StrengthBonus: 1,
      AmbushBonus: 30
    }
  },
  backstabber: {
    id: 'backstabber',
    name: `Backstabber`,
    rarity: 2,
    abilities: {
      StrengthBonus: 1,
      IncreaseGroupAmbush: 8
    }
  },
  spikedShield: {
    id: 'spikedShield',
    name: `Spiked Shield`,
    rarity: 2,
    abilities: {
      HitPointsBonus: 1,
      StrengthBonus: 1
    }
  },
  weightyBreastplate: {
    id: 'weightyBreastplate',
    name: `Weighty Breastplate`,
    rarity: 2,
    abilities: {
      HitPointsBonus: 2,
      MoveBonus: -5
    }
  },
  medalOfCourage: {
    id: 'medalOfCourage',
    name: `Medal of Courage`,
    rarity: 2,
    abilities: {
      LeadershipBonus: 1
    }
  },
  ringOfDeath: {
    id: 'ringOfDeath',
    name: `Ring of Death`,
    rarity: 2,
    abilities: {
      IncreaseGroupAmbush: 25,
      HitPointsBonus: -1
    }
  },
  hideousMask: {
    id: 'hideousMask',
    name: `Hideous Mask`,
    rarity: 2,
    abilities: {
      FrighteningBonus: 1
    }
  },
  heavyCrossbow: {
    id: 'heavyCrossbow',
    name: `Heavy Crossbow`,
    rarity: 2,
    abilities: {
      AntiAirBonus: 2,
      MoveBonus: -3
    }
  },
  catapultAmmo: {
    id: 'catapultAmmo',
    name: `Catapult Ammo`,
    rarity: 2,
    abilities: {
      CancelEnemyFortificationBonus: 1
    }
  },
  orbOfNegation: {
    id: 'orbOfNegation',
    name: `Orb of Negation`,
    rarity: 2,
    abilities: {
      CancelEnemyTerrainBonus: 1
    }
  },
  summonersPipe: {
    id: 'summonersPipe',
    name: `Summoner's Pipe`,
    rarity: 2,
    abilities: {
      SummonsArmiesOfLevel: 2
    }
  },
  bootsOfPolarity: {
    id: 'bootsOfPolarity',
    name: `Boots of Polarity`,
    rarity: 2,
    abilities: {
      MoveBonus: 4,
      LowerEnemyAmbush: 25
    }
  },
  prepaidStagecoach: {
    id: 'prepaidStagecoach',
    name: `Prepaid Stagecoach`,
    rarity: 2,
    abilities: {
      GroupMoveBonus: 3,
      IncomeBonus: -5
    }
  },
  allSeeingEye: {
    id: 'allSeeingEye',
    name: `All-Seeing Eye`,
    rarity: 2,
    abilities: {
      ViewRadiusBonus: 7
    }
  },
  endlessBagOfGold: {
    id: 'endlessBagOfGold',
    name: `Endless Bag of Gold`,
    rarity: 2,
    abilities: {
      IncomeBonus: 25
    }
  },
  heartPiercerJavelin: {
    id: 'heartPiercerJavelin',
    name: `Heart-Piercer Javelin`,
    rarity: 3,
    abilities: {
      StrengthBonus: 3
    }
  },
  goldenHammer: {
    id: 'goldenHammer',
    name: `Golden Hammer`,
    rarity: 3,
    abilities: {
      StrengthBonus: 2,
      IncomeBonus: 15
    }
  },
  defenderHelm: {
    id: 'defenderHelm',
    name: `Defender Helm`,
    rarity: 3,
    abilities: {
      HitPointsBonus: 2
    }
  },
  wandOfWizardry: {
    id: 'wandOfWizardry',
    name: `Wand of Wizardry`,
    rarity: 3,
    abilities: {
      AmbushBonus: 66
    }
  },
  veteransMedal: {
    id: 'veteransMedal',
    name: `Veteran's Medal`,
    rarity: 3,
    abilities: {
      LeadershipBonus: 1,
      IncomeBonus: 10
    }
  },
  peacemakersRibbon: {
    id: 'peacemakersRibbon',
    name: `Peacemaker's Ribbon`,
    rarity: 3,
    abilities: {
      LeadershipBonus: 2,
      StrengthBonus: -10
    }
  },
  cloakOfProtection: {
    id: 'cloakOfProtection',
    name: `Cloak of Protection`,
    rarity: 3,
    abilities: {
      LowerEnemyAmbush: 66
    }
  },
  conjurersBroach: {
    id: 'conjurersBroach',
    name: `Conjurer's Broach`,
    rarity: 3,
    abilities: {
      SummonsArmiesOfLevel: 3
    }
  },
  astrolabe: {
    id: 'astrolabe',
    name: `Astrolabe`,
    rarity: 3,
    abilities: {
      GroupMoveBonus: 4
    }
  },
  trueCompass: {
    id: 'trueCompass',
    name: `True Compass`,
    rarity: 3,
    abilities: {
      NoTerrainPenalty: 1
    }
  },
  endlessSackOfGold: {
    id: 'endlessSackOfGold',
    name: `Endless Sack of Gold`,
    rarity: 3,
    abilities: {
      IncomeBonus: 40
    }
  },
  titansGladius: {
    id: 'titansGladius',
    name: `Titan's Gladius`,
    rarity: 4,
    abilities: {
      StrengthBonus: 5
    }
  },
  ultimateStaff: {
    id: 'ultimateStaff',
    name: `Ultimate Staff`,
    rarity: 4,
    abilities: {
      StrengthBonus: 1,
      LeadershipBonus: 1,
      HitPointsBonus: 1
    }
  },
  crownOfEnlightenment: {
    id: 'crownOfEnlightenment',
    name: `Crown of Enlightenment`,
    rarity: 4,
    abilities: {
      LeadershipBonus: 2
    }
  },
  skeletalHelm: {
    id: 'skeletalHelm',
    name: `Skeletal Helm`,
    rarity: 4,
    abilities: {
      FrighteningBonus: 2
    }
  },
  ankh: {
    id: 'ankh',
    name: `Ankh`,
    rarity: 4,
    abilities: {
      Blessing: 8
    }
  },
  ringOfWishes: {
    id: 'ringOfWishes',
    name: `Ring of Wishes`,
    rarity: 4,
    abilities: {
      SummonsArmiesOfLevel: 4
    }
  },
  angelsFeather: {
    id: 'angelsFeather',
    name: `Angel's Feather`,
    rarity: 4,
    abilities: {
      GroupFlying: 1
    }
  },
  luckyHorseshoe: {
    id: 'luckyHorseshoe',
    name: `Lucky Horseshoe`,
    rarity: 4,
    abilities: {
      GroupMoveBonus: 6,
      CancelsArtifactPenalties: 1
    }
  },
  goldenGoose: {
    id: 'goldenGoose',
    name: `Golden Goose`,
    rarity: 4,
    abilities: {
      IncomeBonus: 75
    }
  }
});
Object.values(standardArtifacts).forEach(artifact => Object.freeze(Object.freeze(artifact).abilities));
