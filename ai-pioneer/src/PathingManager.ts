import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { Place, PlaceWithTime } from 'ai-interface/types/place';
import { ObstacleType } from 'ai-interface/types/obstacle';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { neutralPlayerId } from 'ai-interface/constants/player';
import { SearchPathArgs, MoveType, BasicAvoidingStrategy } from 'ai-interface/types/path';
import { CombatManager } from './CombatManager';
import { ThreatManager } from './ThreatManager';
import { Ability } from 'ai-interface/constants/ability';
import { straightMoveCost } from 'ai-interface/constants/path';
import { DRAW_DEBUG_INFO } from './main';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { ArtifactManager } from './ArtifactManager';
import { AggressionManager } from './AggressionManager';

/** If there are enemies blocking the direct path, this is the maximum detour we're willing to do. Otherwise, charge directly through. */
const MAX_DETOUR_IN_DAYS = 2;

export class PathingManager {

  /**
   * For given group, returns the list of places it has traveled through during this turn.
   * This is used to draw the path of the group.
   * The path is not drawn immediately because only later we'll know the style of drawing based on whether the group still exists or it has died.
   */
  private readonly pathTraveledByGroup: { [groupId: string]: Place[] } = {};

  private readonly myPlayerId: number;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly artifactManager: ArtifactManager,
    private readonly aggressionManager: AggressionManager,
    private readonly combatManager: CombatManager,
    private readonly threatManager: ThreatManager
  ) {
    this.myPlayerId = game.getMyPlayerId();
  }

  private pickPath(groupId: string, moveType: MoveType, destination: Place, expectedDays: number): PlaceWithTime[] {
    const { row: destRow, col: destCol } = destination;
    const start = this.game.getPlaceOfGroup(groupId);
    const initialMoveLeft = this.game.getMoveLeftOfGroup(groupId);

    // First try to calculate the path while avoiding enemies in the middle of the path.
    // But there may be an enemy holding a narrow path in mountains and there is no way around. In that case, go through them.
    const avoidStrategies: BasicAvoidingStrategy[] = ['GO_AROUND_ALLIES_AND_ENEMIES', 'GO_AROUND_ALLIES'];
    for (const strategy of avoidStrategies) {
      const basicAvoidingFunction = this.game.createBasicAvoidingFunction(moveType.groupSize, strategy);

      const searchArgs: SearchPathArgs = {
        start,
        isDestination: (row, col) => row === destRow && col === destCol,
        isToBeAvoided: (row, col) => basicAvoidingFunction(row, col) && (row !== destRow || col !== destCol),
        moveType,
        initialMoveLeft,
        maxDay: expectedDays + MAX_DETOUR_IN_DAYS
      };
      const timeMatrix = this.game.calculateTimeMatrix(searchArgs);

      const path = this.game.calculatePathToPlaceFromTimeMatrix(timeMatrix, destination);
      if (path.length > 0) {
        return path;
      }
    }

    return [];
  }

  /**
   * Moves the army group towards given place. Avoids all allies and enemies in the middle of the path unless the detour would be too big.
   * IMPORTANT: IT'S DELIBERATE THAT WE RECALCULATE THE PATH AGAIN.
   * Current implementation of the dijkstra can't calculate such path in one iteration.
   * Using GO_AROUND_ALLIES leads to situation where computer runs into an enemy that is on the shortest path to the target.
   * Using GO_AROUND_ALLIES_AND_ENEMIES will never lead to an attack.
   */
  public sendGroupTo(groupId: string, moveType: MoveType, place: Place, expectedDays: number): void {
    const path = this.pickPath(groupId, moveType, place, expectedDays);

    // If the group has a hero, check whether there is an artifact laying on the path. If there is, stop on it.
    const groupHasHero = this.game.getArmiesInGroup(groupId).some(army => standardArmyTypes[army.armyTypeId].hero);
    if (groupHasHero) {
      // Ignore the first and the last places on the path. We only want to ensure that we don't miss an artifact by walking over it.
      for (let i = 1; i < path.length - 1; i++) {
        if (this.game.getArtifactIdsOnGround(path[i]).length > 0) {
          path.splice(i + 1);
          place = path[i];
          break;
        }
      }
    }

    let threatOfDestination: string[] = [];
    const cityId = this.land.getCityIdAt(place);
    if (cityId !== undefined) {
      const city = this.game.getCity(cityId);
      threatOfDestination = this.threatManager.threatProducedByCity(city);
    }

    this.moveGroupAlongPath(groupId, path);

    // If we have captured an enemy city, it's production is no longer a threat.
    if (threatOfDestination.length > 0 && this.game.getOwnerOfPlace(place) === this.myPlayerId) {
      this.threatManager.spreadThreatOfArmyTypes(place, threatOfDestination, -1);
    }

    // If the group has a hero, check whether we're standing on an artifact. This could happen for example because we just killed a hero.
    if (groupHasHero
      && this.game.groupExists(groupId)
      && this.game.getArmiesInGroup(groupId).some(army => standardArmyTypes[army.armyTypeId].hero)) {
      place = this.game.getPlaceOfGroup(groupId);
      this.artifactManager.pickArtifactsIfThereAreAnyTask(groupId, place).action();
    }
  }

  private moveGroupAlongPath(groupId: string, path: PlaceWithTime[]) {
    while (path.length > 1) {
      if (DRAW_DEBUG_INFO) {
        let pathTraveled = this.pathTraveledByGroup[groupId];
        if (pathTraveled === undefined) {
          pathTraveled = [];
          this.pathTraveledByGroup[groupId] = pathTraveled;
        }
        pathTraveled.push(path[0]);
      }

      try {
        const nextPl = path[1];
        this.game.moveGroupOneStep(groupId, nextPl);
        path.shift();

        const lastCombat = this.game.getLastCombat();
        if (lastCombat !== null) {
          const { attackingArmyTypeIds, defendingArmyTypeIds } = this.combatManager.extractDestroyedArmyTypeIds(lastCombat);

          // Decrease enemy threat according to the destroyed armies.
          if (defendingArmyTypeIds.length > 0 && lastCombat.combatInputs.defendingPlayerId !== neutralPlayerId) {
            this.threatManager.spreadThreatOfArmyTypes(nextPl, defendingArmyTypeIds, -1);
          }

          // Decrease our expected upkeep according to the destroyed armies.
          this.aggressionManager.weLostArmies(attackingArmyTypeIds);

          // Stop movement after combat. Further actions for the group will be decided in next decision cycle.
          break;
        }
      } catch (e: any) {
        if (e.type !== ObstacleType.NO_MOVE_LEFT) {
          throw e;
        }
        break;
      }
    }

    // Draw the rest of the path to be traveled
    if (DRAW_DEBUG_INFO) {
      for (const place of path) {
        this.draw.drawTextWhenGroupIsSelected('' + place.day, groupId, place);
      }
    }
  }

  public calculateFlyMovesToNearestEnemy(place: Place): number {
    const maxMove = 2 * this.land.getSize() * straightMoveCost;

    const teams = this.game.getPlayersTeam();
    const myTeam = teams[this.myPlayerId];

    const moveType: MoveType = {
      abilities: { [Ability.Flies]: 1 },
      groupSize: 1,
      groupMoveOnLand: maxMove
    };

    const searchArgs: SearchPathArgs = {
      start: place,
      isDestination: (row, col) => {
        // In this case, we consider neutral as an enemy - we want to defeat him.
        const owner = this.game.getOwnerOfPlace({ row, col });
        if (owner === null || teams[owner] === myTeam) {
          return false;
        }
        // However, razed city, while neutral, is not considered an enemy, because we can't defeat it.
        const cityId = this.land.getCityIdAt({ row, col });
        if (cityId !== undefined && this.game.getCity(cityId).razed) {
          return false;
        }
        return true;
      },
      isToBeAvoided: () => false,
      moveType,
      initialMoveLeft: maxMove,
      maxDay: 0
    };
    const { destination, movesLeft } = this.game.calculateTimeMatrix(searchArgs);

    return maxMove - (destination === undefined ? 0 : movesLeft[destination.row][destination.col]);
  }

  public drawPathsTraveled() {
    for (const groupId in this.pathTraveledByGroup) {
      if (this.game.groupExists(groupId)) {
        for (const place of this.pathTraveledByGroup[groupId]) {
          this.draw.drawTextWhenGroupIsSelected('x', groupId, place);
        }
      } else {
        for (const place of this.pathTraveledByGroup[groupId]) {
          this.draw.drawTextWhenNothingIsSelected('°', place);
        }
      }
    }
  }

}
