import { AppAction } from '../root/configureStore';
import { ActionTypes } from './actions';
import { CombatResult, CombatInputs } from 'ai-interface/types/combat';

/**
 * This state contains only data that are related to combat. They are temporary, but are also used by AI.
 * None of it is saved when land or game is saved.
 */
export interface CombatState {
  readonly combatInputs: CombatInputs | null; // Initial forces of last combat. Null if the last move wasn't combat.
  readonly combatResult: CombatResult | null; // Result of last combat. Null if the last move wasn't combat.
}

export const initialState: CombatState = {
  combatInputs: null,
  combatResult: null
};

export function combatReducer(state = initialState, action: AppAction): CombatState {
  switch (action.type) {

    case ActionTypes.SET_COMBAT_INPUTS: {
      const { combatInputs } = action.payload;
      return {
        ...state,
        combatInputs
      };
    }

    case ActionTypes.SET_COMBAT_RESULT: {
      const { combatResult } = action.payload;
      return {
        ...state,
        combatResult
      };
    }

    default:
      return state;
  }
}
