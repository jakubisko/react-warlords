import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { Place } from 'ai-interface/types/place';
import { Task, NOP } from './helpers/types';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { maxHeroArtifactsCapacity, ArtifactAbility } from 'ai-interface/constants/artifactAbility';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { WinCondition } from 'ai-interface/constants/winCondition';

export class ArtifactManager {

  private readonly utilityOfArtifact: { [artifactId: string]: number } = {};
  private readonly isAbilityRedundant: Partial<{ [key in ArtifactAbility]: boolean }> = {
    GroupFlying: true,
    Blessing: true,
    NoTerrainPenalty: true,
    CancelEnemyTerrainBonus: true,
    CancelEnemyFortificationBonus: true,
    CancelsArtifactPenalties: true
  };

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi
  ) {
    this.calculateUtilityOfArtifacts();
  }

  private calculateUtilityOfArtifacts() {
    const multiplier: Partial<{ [key in ArtifactAbility]: number }> = {
      LeadershipBonus: 1.25,
      FrighteningBonus: 1.25,
      ViewRadiusBonus: 0.4
    };

    for (const artifactId in standardArtifacts) {
      const { rarity, abilities } = standardArtifacts[artifactId];
      let utility = 135 * rarity;

      for (const [ability, value] of Object.entries(abilities)) {
        if (value !== undefined && value > 0) {
          utility *= multiplier[ability as ArtifactAbility] ?? 1;
        }
      }

      if (rarity === 4 && this.game.getWinCondition() === WinCondition.ObtainALevel4Artifact) {
        utility = 1750;
      }

      this.utilityOfArtifact[artifactId] = utility;
    }
  }

  /**
   * Returns true when given artifact shares a redundant ability with an artifact from given list.
   * Adding such an artifact to the list would not increase effective abilities provided by the list.
   * Note: Since we're first adding stronger artifacts before weaker, the stronger artifact will already be in the list when weaker is considered.
   */
  private isArtifactRedundantWithList(artifactId: string, artifactIds: string[]): boolean {
    return Object.keys(standardArtifacts[artifactId].abilities)
      .some(ability => this.isAbilityRedundant[ability as ArtifactAbility]
        && artifactIds.some(artifactId2 => Object.keys(standardArtifacts[artifactId2].abilities)
          .some(ability2 => ability === ability2)));
  }

  private utilityOfEquippedArtifacts(heroIdToArtifactIds: { [heroId: string]: string[] }): number {
    let utility = 0;
    for (const heroId in heroIdToArtifactIds) {
      const artifactIds = heroIdToArtifactIds[heroId];
      artifactIds.forEach((artifactId, index) =>
        utility += this.utilityOfArtifact[artifactId]
        * (this.isArtifactRedundantWithList(artifactId, artifactIds.slice(0, index)) ? 0.2 : 1)
      );
    }
    return utility;
  }

  /** This picks/drops artifacts with a hero so that he will be equipped with the best ones. */
  public pickArtifactsIfThereAreAnyTask(groupId: string, place: Place): Task {
    let utility = 0;
    let action = NOP;

    const artifactIdsOnGround = this.game.getArtifactIdsOnGround(place);
    if (artifactIdsOnGround.length === 0) {
      return { utility, action };
    }

    // Get all heroes in the group
    const heroes = this.game.getArmiesInGroup(groupId)
      .filter(army => standardArmyTypes[army.armyTypeId].hero)
      .map(army => this.game.getHero(army.id))
      .sort((h1, h2) => h2.experience - h1.experience);

    // How are artifacts distributed before redistribution
    const heroIdToArtifactIds1: { [heroId: string]: string[] } = {};
    heroes.forEach(hero => heroIdToArtifactIds1[hero.id] = hero.equippedArtifactIds);
    const utility1 = this.utilityOfEquippedArtifacts(heroIdToArtifactIds1);

    // Create the optimal distribution of artifacts.
    // 1. Try to equip the artifacts with the highest utility first.
    // 2. Try to equip it to the hero with most experiences, then the second etc.
    // 3. If the artifact has an ability that is redundant, do not equip it to the hero who already has an artifact with such an ability.
    // 4. Equip any leftover artifacts, even if redundant.
    let availableArtifactIds = [...artifactIdsOnGround];
    heroes.forEach(hero => availableArtifactIds = [...availableArtifactIds, ...hero.equippedArtifactIds]);
    availableArtifactIds.sort((a1, a2) => this.utilityOfArtifact[a2] - this.utilityOfArtifact[a1]);

    const heroIdToArtifactIds2: { [heroId: string]: string[] } = heroes.reduce((res, { id }) => ({ ...res, [id]: [] }), {});
    const leftoverArtifactIds: string[] = [];
    for (const artifactId of availableArtifactIds) {
      let leftover = true;
      for (const { id } of heroes) {
        const artifactIds = heroIdToArtifactIds2[id];
        if (artifactIds.length < maxHeroArtifactsCapacity && !this.isArtifactRedundantWithList(artifactId, artifactIds)) {
          artifactIds.push(artifactId);
          leftover = false;
          break;
        }
      }
      if (leftover) {
        leftoverArtifactIds.push(artifactId);
      }
    }

    for (const { id } of heroes) {
      const artifactIds = heroIdToArtifactIds2[id];
      while (artifactIds.length < maxHeroArtifactsCapacity && leftoverArtifactIds.length > 0) {
        artifactIds.push(leftoverArtifactIds.shift()!);
      }
    }

    // Calculate utility of equipped artifacts after redistribution. If the utility gained by the redistribution is not positive, do nothing.
    const utility2 = this.utilityOfEquippedArtifacts(heroIdToArtifactIds2);

    utility = utility2 - utility1;
    if (utility <= 0) {
      return { utility, action };
    }

    // The easiest way to redistribute the artifacts is to simply drop off everything and pick according to the second distribution.
    action = () => {
      for (const heroId in heroIdToArtifactIds1) {
        for (const artifactId of heroIdToArtifactIds1[heroId]) {
          this.game.dropArtifactOntoGround(heroId, artifactId);
        }
      }

      for (const heroId in heroIdToArtifactIds2) {
        for (const artifactId of heroIdToArtifactIds2[heroId]) {
          this.game.pickArtifactFromGround(heroId, artifactId);
        }
      }
    };

    return { utility, action };
  }

  /**
   * Maybe one of our heroes is standing on an artifact because an enemy hero attacked us and got destroyed.
   * GroupManager would fail to find this artifact, because it ignores the field it's standing on.
   */
  public pickArtifactsWithHeroesStandingOnBags() {
    for (const { armyTypeId, groupId } of this.game.getAllMyArmies()) {
      if (standardArmyTypes[armyTypeId].hero) {
        const place = this.game.getPlaceOfGroup(groupId);
        const task = this.pickArtifactsIfThereAreAnyTask(groupId, place);
        task.action();
      }
    }
  }

}
