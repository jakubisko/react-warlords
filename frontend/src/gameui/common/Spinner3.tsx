import './Spinner3.scss';

export const Spinner3 = () => (
  <div className="spinner3">
    <div className="bounce1" />
    <div className="bounce2" />
    <div className="bounce3" />
  </div>
);
