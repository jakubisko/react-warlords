const workboxBuild = require('workbox-build');

// This is called after the normal build.
// It will generate a custom "service-worker.js", which precaches some files.
// The generated worker is called from the default "serviceWorker.js", and uses workbox.

// NOTE: This should be run *AFTER* all your assets are built
const buildSW = () => {
  // This will return a Promise
  return workboxBuild.generateSW({
    skipWaiting: true, // When new version of our app is available, don't wait until user closes all tabs; upgrade immediately
    cacheId: 'warlords',
    globDirectory: 'dist',
    globPatterns: ['favicon.ico', 'index.html', 'manifest.json', 'assets/*', 'maps/*.wmap'], // service-worker.js MUSTN'T be cached.
    swDest: 'dist/service-worker.js',
    sourcemap: false
  });
};
buildSW();
