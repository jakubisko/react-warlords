import './QuestionModal.scss';
import { ReactNode, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import ButtonWithShortcut from '../common/ButtonWithShortcut';
import { soundEffects } from '../sounds/soundEffects';
import { SoundEffect } from '../../model/sound/types';
import { EMPTY_ARRAY } from '../../functions';

interface QuestionModalProps {
  children: ReactNode;
  confirmString: string;
  rejectString: string;

  onConfirm: () => void;
  onReject: () => void;
}

export default function QuestionModal(props: QuestionModalProps) {
  useEffect(() => {
    soundEffects.play(SoundEffect.Modal);
  }, EMPTY_ARRAY);

  return (
    <Modal className="question-modal" show centered onHide={props.onReject}>
      <Modal.Header />

      <Modal.Body>
        {props.children}
      </Modal.Body>

      <Modal.Footer className="justify-content-between">
        <ButtonWithShortcut
          shortcutCode="Enter"
          allowShortcutWhileModalIsOpen={true}
          onClick={props.onConfirm}
        >
          {props.confirmString}
        </ButtonWithShortcut>

        <Button onClick={props.onReject}>{props.rejectString}</Button>
      </Modal.Footer>
    </Modal>
  );
}