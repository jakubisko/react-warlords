import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../root/configureStore';
import { maxPlayers } from 'ai-interface/constants/player';
import { createArray } from '../../functions';
import { getAllArmiesOfPlayer, getNumberOfCities } from './selectorsPlaying';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { nameOfUtopia, WinCondition } from 'ai-interface/constants/winCondition';
import { standardArtifacts } from 'ai-interface/constants/standardArtifacts';

const getGameState = (state: RootState) => state.game;

/** Returns the id of next player alive. Returns current player if all players are dead. */
export const getNextPlayer = createSelector(
  getGameState,
  ({ currentPlayerId, players }) => {
    let playerId = currentPlayerId;
    let passes = 0;
    while (passes <= maxPlayers) {
      passes++;
      playerId = (playerId + 1) % (maxPlayers + 1); // Game starts with current player being neutral. Neutral player is not alive.
      if (players[playerId].alive) {
        return playerId;
      }
    }
    return currentPlayerId;
  }
);

/** Returns the last place that was focused by current player at the end of his previous turn. */
export const getSavedMapFocusOfCurrentPlayer = createSelector(
  getGameState,
  ({ currentPlayerId, players }) => players[currentPlayerId].savedMapFocus
);

export const collectTimelineItem = createSelector(
  (state: RootState) => state,
  state => ({
    cities: createArray(maxPlayers, playerId => getNumberOfCities(state, playerId)),
    armyValue: createArray(maxPlayers, playerId => getAllArmiesOfPlayer(state, playerId)
      .map(army => standardArmyTypes[army.armyTypeId].buildingCost)
      .reduce((sum, val) => sum + val, 0))
  })
);

export const getPlayersToEliminate = createSelector(
  (state: RootState) => state,
  state => {
    const { players } = state.game;

    const teamsWithAlternativeWinCondition = players
      .filter(player => player.alive)
      .map(player => player.team)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
      .filter(team => hasTeamAlternativeWinCondition(state, team));
    const winnerTeam = teamsWithAlternativeWinCondition.length === 1 ? teamsWithAlternativeWinCondition[0] : null;

    return createArray(maxPlayers, playerId => playerId)
      .filter(playerId => players[playerId].alive)
      .filter(playerId => hasPlayerLost(state, playerId) || (winnerTeam !== null && players[playerId].team !== winnerTeam));
  }
);

const hasPlayerLost = createSelector(
  getGameState, (rootState: RootState, playerId: number) => playerId,
  ({ cities, winCondition }, playerId) => Object.values(cities)
    .every(({ razed, owner, capitol }) => (razed || owner !== playerId || (!capitol && winCondition === WinCondition.DestroyAllEnemyCapitols))
    )
);

const hasTeamAlternativeWinCondition = createSelector(
  getGameState, (rootState: RootState, teamId: number) => teamId,
  ({ cities, groups, heroes, winCondition, players }, teamId) => {

    switch (winCondition) {
      case WinCondition.Capture75PercentOfAllCities:
        return 4 * Object.values(cities)
          .filter(city => !city.razed && players[city.owner].team === teamId)
          .length
          >=
          3 * Object.values(cities)
            .filter(city => !city.razed)
            .length;

      case WinCondition.ObtainALevel4Artifact:
        return Object.values(heroes).some(hero =>
          hero.artifactIds !== undefined && hero.artifactIds.some(artifactId => standardArtifacts[artifactId].rarity === 4)
          && players[groups[hero.groupId].owner].team === teamId
        );

      case WinCondition.CaptureTheCityUtopia:
        return Object.values(cities).some(city =>
          city.name === nameOfUtopia
          && players[city.owner].team === teamId
        );
    }

    return false;
  }
);

/**
 * Returns true if at most one team is left, // Game ended in victory
 * or if all visible players are defeated. // There is nothing left to display, because there are only AIs.
 */
export const isGameOverForVisiblePlayers = createSelector(
  getGameState,
  ({ players, aiOutputVisible }) =>
    players
      .filter(player => player.alive)
      .map(player => player.team)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
      .length <= 1
    ||
    players
      .filter(({ alive, aiName }) => alive && (aiName === undefined || aiOutputVisible))
      .length === 0
);

/**
 * Call this only when the game is over.
 * Returns the text with the names of winning players. Returns null in case of defeat.
 */
export const getVictoryText = createSelector(
  getGameState,
  (rootState: RootState) => rootState.online.onlineGameId === null ? null : rootState.online.loggedInUser!.uid,
  ({ players, aiOutputVisible }, uidIfOnline) => {
    const teamsAlive = players
      .filter(player => player.alive)
      .map(player => player.team)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos);

    if (teamsAlive.length === 1 && (
      // When playing offline, victory is when the only team left contains a human or a visible AI.
      (uidIfOnline === null && (players.some(player => player.team === teamsAlive[0] && player.aiName === undefined) || aiOutputVisible)) ||
      // When playing online, victory is when the only team left contains a currently logged in user.
      (uidIfOnline !== null && players.some(player => player.team === teamsAlive[0] && player.onlineUid === uidIfOnline))
    )) {
      const winnerNames = players
        .filter(player => player.team === teamsAlive[0])
        .map(player => player.name);
      return (winnerNames.length > 1)
        ? `${winnerNames.slice(0, winnerNames.length - 1).join(', ')} and ${winnerNames.pop()} have conquered the world!`
        : `${winnerNames[0]} has conquered the world!`;
    }

    return null;
  }
);
