/** Coordinates in the land - row and column numbers. */
export interface Place {
  readonly row: number;
  readonly col: number;
}

/** A Place with additional information about the time and move spent to reach the place. */
export interface PlaceWithTime extends Place {

  /** How many days will be spent to reach this place. 0 if it can be reached today. */
  readonly day: number;

  /** Move left after reaching this place. For groups, this is the lowest value among armies in the group. */
  readonly moveLeft: number;
}
