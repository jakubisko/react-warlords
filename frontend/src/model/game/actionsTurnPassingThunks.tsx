import { AppDispatch, RootState } from '../root/configureStore';
import { getIncome, getAllArmiesOfPlayer, getViewRadiusOfGroup } from './selectorsPlaying';
import { setErrorMessage, setScreen } from '../ui/actions';
import { ScreenEnum } from '../ui/types';
import { allArmyTypes, allArtifacts, defaultArmyTypeIdToOrder } from './constants';
import { GameState } from './types';
import {
  setTurnsTrainingLeft, addGold, resetWarReports, addWarReportItem, setArmyBlessed, setGroupOwner, setFlaggableOwner, setUnexploredByTeam,
  setIncomingCaravans, setCityCapitol, setArmyMoveLeft, setSageVisitedThisWeek
} from './actionsPlaying';
import { calculateBackgroundSprites } from '../editor/tiling';
import { isGameOverForVisiblePlayers, getNextPlayer, collectTimelineItem, getPlayersToEliminate } from './selectorsTurnPassing';
import { addExperiences, levelUpHeroes, triggerArtifactsAtTheStartOfTurn } from './actionsHero';
import { SaveGameData } from './saveLoad';
import { setLastRevealedPlaces } from '../path/actions';
import { compressObject } from './serialization';
import { Structure } from 'ai-interface/constants/structure';
import { maxPlayers, neutralPlayerId } from 'ai-interface/constants/player';
import { WarReportType } from 'ai-interface/types/warReport';
import { Place } from 'ai-interface/types/place';
import { maxTileCapacity, shipMove, straightMoveCost } from 'ai-interface/constants/path';
import { cityViewRadius, flaggableViewRadius } from 'ai-interface/constants/viewRadius';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { getRandomItem, getRandom, createGrid, createArray, EMPTY_ARRAY } from '../../functions';
import { caravanDelayDays } from 'ai-interface/constants/city';
import { IncomingCaravan } from 'ai-interface/types/city';
import { createArmyAtOrAroundPlace, destroyArmy, killAndReleasePlayer, revealFogOfWar, changeCityOwner, buildTrainingFacility } from './actionsPlayingThunks';
import {
  setPlayerSavedMapFocus, setCurrentPlayer, setGameState, setHeroOffer, _resetFogOfWar, addTimelineItem, setTurnNumber, setPlayerAiState,
  resetAiOutput, setWinCondition
} from './actionsTurnPassing';
import { getHerosValueOfAbility, getHerosMove, getAllUsedHeroPortraitIds } from './selectorsHero';
import { Ability } from 'ai-interface/constants/ability';
import { isGroupShip } from '../path/MoveCostCalculatorImplUnchecked';
import { getRandomUnusedHeroPortraitId } from '../../predefined-data/predefinedDataGetters';
import { experienceForGrove } from 'ai-interface/constants/experience';
import { aiTakeTurn } from './actionsAiTakeTurn';
import { localStorageKeyAutosave } from '../root/constantsStorage';
import { nameOfUtopia, WinCondition } from 'ai-interface/constants/winCondition';
import { createAndPlaceStructure, eraseTile, setCityName, setRuinLevel } from './actionsEditor';
import { pickPlaceForAlternativeWinCondition } from '../random-map/selectors';
import { Army } from 'ai-interface/types/army';
import { createGameOnServer, updateGameOnServer } from '../online/actions';

export const saveMapFocusOfCurrentPlayer = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game: { currentPlayerId }, ui: { focusedRow, focusedCol } } = getState();
  const mapFocus: Place = { row: focusedRow, col: focusedCol };
  dispatch(setPlayerSavedMapFocus(currentPlayerId, mapFocus));
};

/** Shuffles starting positions of players at random. Colors and teams will remain the same. */
export const shuffleStartingPositions = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { cities, groups, flaggables, players } = state.game;

  // First create a random mapping between living players.
  const playerIds1 = players.map(({ alive }, playerId) => alive ? playerId : null).filter(x => x !== null);
  const playerIds2 = [...playerIds1];

  for (let i = playerIds2.length - 1; i > 0; i--) {
    const j = getRandom(i + 1);
    [playerIds2[i], playerIds2[j]] = [playerIds2[j], playerIds2[i]];
  }

  const map: { [playerId: number]: number } = {};
  playerIds1.forEach((fromId, i) => map[fromId!] = playerIds2[i]!);

  // Iterate everything that can be owned by players and change the ownership according to the mapping.
  for (const groupId in groups) {
    const { owner } = groups[groupId];
    if (owner !== neutralPlayerId) {
      dispatch(setGroupOwner(groupId, map[owner]));
    }
  }

  for (const cityId in cities) {
    const { owner } = cities[cityId];
    if (owner !== neutralPlayerId) {
      dispatch(changeCityOwner(cityId, map[owner]));
    }
  }

  for (const flaggableId in flaggables) {
    const { owner } = flaggables[flaggableId];
    if (owner !== neutralPlayerId) {
      dispatch(setFlaggableOwner(flaggableId, map[owner]));
    }
  }
};

/**
 * Starts new game. This assumes that:
 * 1. NewGameMapScreen - MapData was already loaded by calling loadMap(MapData).
 * 2. NewGamePlayersScreen - Player names, teams and AI players were initialized by calling aiCollection.initializeAiWrappers(aiData).
 * Sets up the fog of war and the unexplored map.
 * When playing offline, passes to the first player.
 * When playing online, pushes the game to server.
 */
export const startNewGame = (shuffledStartingPositions: boolean, startWithExploredMap: boolean, onlineGameTitle?: string) =>
  (dispatch: AppDispatch) => {
    if (shuffledStartingPositions) {
      dispatch(shuffleStartingPositions());
    }

    dispatch(placeWinConditionRandomlyIfItIsMissing());
    dispatch(createCapitols());
    dispatch(initializeFogOfWarAndUnexplored(startWithExploredMap));
    dispatch(setCurrentPlayer(neutralPlayerId));
    dispatch(placeArmiesToUnguardedNeutralCities()); // This has to be done during neutral player's turn.

    if (onlineGameTitle === undefined) {
      dispatch(passTurnToNextPlayer());
    } else {
      dispatch(setScreen(ScreenEnum.WaitScreen));
      dispatch(createGameOnServer(onlineGameTitle,
        () => dispatch(passTurnToNextPlayer()),
        () => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen)))
      );
    }
  };

export const loadGame = (saveGameData: SaveGameData) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { terrain, structures } = saveGameData;
  const backgroundSprites = calculateBackgroundSprites(terrain, structures);
  const gameState: GameState = { ...saveGameData, backgroundSprites };
  dispatch(setGameState(gameState));

  const { onlineGameId, onlineTimesLoaded } = getState().online;
  if (onlineGameId !== null && isGameOverForVisiblePlayers(getState())) {
    dispatch(setScreen(ScreenEnum.GameOverScreen));
  } else if (onlineGameId !== null && onlineTimesLoaded > 0) {
    dispatch(setScreen(ScreenEnum.DuplicateStartOfTurnScreen));
  } else {
    dispatch(setScreen(ScreenEnum.StartOfTurnScreen));
  }
};

/**
 * If the win condition is to obtain a level 4 artifact, but there is no level 4 ruin, this will upgrade one to it.
 * If it's not possible, this changes the win condition to the default one.
 */
const placeWinConditionRandomlyIfItIsMissing = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size, tiles, cities, ruins, winCondition } = getState().game;

  switch (winCondition) {
    case WinCondition.ObtainALevel4Artifact:
      if (Object.values(ruins).some(ruin => ruin.level === 4)
        || tiles.some(row => row.some(tile => tile.artifactIds !== undefined && tile.artifactIds.some(artifactId => allArtifacts[artifactId].rarity === 4)))) {
        return;
      }

      if (Object.values(ruins).length > 0) {
        const { row, col } = pickPlaceForAlternativeWinCondition(getState(), Object.values(ruins))!;
        const ruinId = tiles[row][col].ruinId!;
        dispatch(setRuinLevel(ruinId, 4));
      } else {
        dispatch(setWinCondition(WinCondition.CaptureAllEnemyCities));
      }

      break;

    case WinCondition.CaptureTheCityUtopia: {
      const utopia = Object.values(cities).find(city => city.name === nameOfUtopia);
      if (utopia !== undefined) {
        dispatch(setCityCapitol(utopia.id, true));
        return;
      }

      const neutralCities = Object.values(cities).filter(city => city.owner === neutralPlayerId);
      if (neutralCities.length > 0) {
        // Erase everything and create it from a scratch just to be sure.
        const { row, col } = pickPlaceForAlternativeWinCondition(getState(), neutralCities)!;
        dispatch(eraseTile(row, col));
        dispatch(eraseTile(row + 1, col));
        dispatch(eraseTile(row, col + 1));
        dispatch(eraseTile(row + 1, col + 1));

        // Defending armies have to be placed before the city. Otherwise, they would be moved to the top left corner.
        const getRandomArmyOfLevel = (level: number) => getRandomItem(Object.values(standardArmyTypes)
          .filter(armyType => !armyType.hero && armyType.strength > 2 && armyType.trainingTime === level)).id;
        const armyType1 = getRandomArmyOfLevel(size < 75 ? 3 : 4);
        const armyType2 = getRandomArmyOfLevel(Math.floor(size / (size < 75 ? 20 : 33)));
        dispatch(createArmyAtOrAroundPlace(row, col, armyType1, neutralPlayerId));
        dispatch(createArmyAtOrAroundPlace(row + 1, col + 1, armyType2, neutralPlayerId));

        // Place city and build training facilities.
        dispatch(createAndPlaceStructure(row, col, Structure.City));
        const cityId = getState().game.tiles[row][col].cityId!;
        dispatch(setCityName(cityId, nameOfUtopia));
        dispatch(setCityCapitol(cityId, true));
        dispatch(buildTrainingFacility(cityId, armyType1));
        dispatch(buildTrainingFacility(cityId, armyType2));
      } else {
        dispatch(setWinCondition(WinCondition.CaptureAllEnemyCities));
      }

      break;
    }
  }
};

const createCapitols = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities } = getState().game;

  const hasCapitol = createArray(maxPlayers, () => false);
  for (const cityId in cities) {
    const { owner, row, col } = cities[cityId];
    if (owner !== neutralPlayerId && !hasCapitol[owner]) {
      dispatch(setCityCapitol(cityId, true));
      hasCapitol[owner] = true;

      dispatch(setPlayerSavedMapFocus(owner, { row, col }));
    }
  }
};

/**
 * Covers or uncovers entire map for all players and teams.
 * If starting with explored map, updates lastSeenOwner, so that everyone sees the initial positions of players.
 */
const initializeFogOfWarAndUnexplored = (startWithExploredMap: boolean) => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size, cities, flaggables, players } = getState().game;

  const unexplored = startWithExploredMap ? null : createGrid(size, () => true);
  players
    .filter(player => player.alive)
    .map(player => player.team)
    .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
    .forEach(team => dispatch(setUnexploredByTeam(team, unexplored)));

  if (startWithExploredMap) {
    for (let playerId = 0; playerId < maxPlayers; playerId++) {
      if (players[playerId].alive) {
        dispatch(setCurrentPlayer(playerId));
        dispatch(regenerateFogOfWar());

        for (const cityId in cities) {
          const { row, col } = cities[cityId];
          dispatch(revealFogOfWar(row, col, 0));
        }

        for (const flaggableId in flaggables) {
          const { row, col } = flaggables[flaggableId];
          dispatch(revealFogOfWar(row, col, 0));
        }
      }
    }
  }
};

const makeAutoSave = () => async (dispatch: AppDispatch, getState: () => RootState) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { backgroundSprites, ...saveGameData } = getState().game;
  compressObject(saveGameData, 'game', 'string')
    .then(saveGameAsString => window.localStorage.setItem(localStorageKeyAutosave, saveGameAsString))
    .catch((err) => dispatch(setErrorMessage('Failed to create autosave', err)));
};

export const endUiTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game: { players, currentPlayerId, aiOutputVisible }, online: { onlineGameId } } = getState();

  dispatch(setScreen(ScreenEnum.WaitScreen));
  dispatch(saveMapFocusOfCurrentPlayer());

  setTimeout(() => { // Allow UI refresh.
    if (onlineGameId === null && (aiOutputVisible || players[currentPlayerId].aiName === undefined)) {
      dispatch(makeAutoSave());
    }

    dispatch(passTurnToNextPlayer());
  });
};

/** Ends turn and passes to a next player. If turn is passed to an AI player, it will take its turn and again end the turn. */
export const passTurnToNextPlayer = () => (dispatch: AppDispatch, getState: () => RootState) => {
  try {
    dispatch(setScreen(ScreenEnum.WaitScreen));

    /** Clean up after the player who's turn is ending. */
    const oldPlayerId = getState().game.currentPlayerId;
    dispatch(resetWarReports(oldPlayerId));
    dispatch(resetAiOutput());

    /** Check whether some players were eliminated. */
    for (const playerId of getPlayersToEliminate(getState())) {
      dispatch(killAndReleasePlayer(playerId, WarReportType.PlayerEliminated));
    }

    /** Check whether the game is over. */
    if (isGameOverForVisiblePlayers(getState())) {
      for (let playerId = 0; playerId < maxPlayers; playerId++) {
        dispatch(setPlayerAiState(playerId, ''));
      }
      if (getState().online.onlineGameId === null) {
        dispatch(setScreen(ScreenEnum.GameOverScreen));
      } else {
        dispatch(updateGameOnServer(
          () => dispatch(setScreen(ScreenEnum.GameOverScreen)),
          () => dispatch(setScreen(ScreenEnum.OnlineSavingFailedScreen))
        ));
      }
      return;
    }

    /** Pass to next player. */
    const playerId = getNextPlayer(getState());
    if (playerId <= oldPlayerId) {
      dispatch(lastPlayerEndedTheTurn());
    }
    dispatch(setCurrentPlayer(playerId));

    /** Execute start of turn routine - collect income, pay upkeep etc. */
    dispatch(addGold(playerId, getIncome(getState(), playerId)));
    dispatch(resetAllArmyMove());
    dispatch(triggerArtifactsAtTheStartOfTurn());
    dispatch(triggerGrovesAtTheStartOfTurn());
    dispatch(levelUpHeroes());
    dispatch(moveCaravans()); // This has to be before "trainNewArmies", otherwise timing will get mixed
    dispatch(trainNewArmies()); // This has to be after "triggerArtifactsAtTheStartOfTurn", because of "ArtifactAbility.DecreaseTrainingTime"
    dispatch(payArmyUpkeepOrDesert());
    dispatch(triggerTemplesAtTheStartOfTurn());
    dispatch(triggerEncampmentsAtTheStartOfTurn()); // This should be after "payArmyUpkeepOrDesert", otherwise it would change player's expected upkeep
    dispatch(regenerateFogOfWar());
    dispatch(calculateHeroOffer()); // This should be after "triggerEncampmentsAtTheStartOfTurn", because offer depends on the number of heroes

    /** Show next screen. */
    const { aiName, onlineUid } = getState().game.players[playerId];
    if (onlineUid !== undefined) {
      dispatch(updateGameOnServer(
        () => dispatch(setScreen(ScreenEnum.ListOfOnlineGamesScreen)),
        () => dispatch(setScreen(ScreenEnum.OnlineSavingFailedScreen))
      ));
    } else if (aiName === undefined) {
      dispatch(setScreen(ScreenEnum.StartOfTurnScreen));
    } else {
      dispatch(setScreen(ScreenEnum.AiTakingTurnScreen));
      dispatch(aiTakeTurn());
    }
  } catch (e) {
    // When passing the turn, WaitScreen is visible. Change the screen, so that the UI is no longer stuck there.
    dispatch(setScreen(ScreenEnum.FatalErrorScreen));
    // Rethrow the error so it will be written to the console. This doesn't trigger ErrorBoundary since we're called from the setTimeout().
    throw e;
  }
};

const lastPlayerEndedTheTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  if (getState().game.turnNumber % 7 === 0) {
    for (let playerId = 0; playerId < maxPlayers; playerId++) {
      dispatch(setSageVisitedThisWeek(playerId, false));
    }

    dispatch(setCurrentPlayer(neutralPlayerId)); // Otherwise newly created armies would remove fog of war for current player.
    dispatch(neutralArmyGrowth()); // Place before increase of the turn number
  }

  dispatch(setTurnNumber(getState().game.turnNumber + 1));
  const timelineItem = collectTimelineItem(getState());
  dispatch(addTimelineItem(timelineItem));
};

const placeArmiesToUnguardedNeutralCities = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { tiles, cities, turnNumber } = getState().game;

  // For neutral cities without armies at the start of the game, create some armies of the type that city could produce.
  // Before the first turn, when neutral armies grow, these will be grown to their default size.
  if (turnNumber === 0) {
    for (const cityId in cities) {
      const { owner, row, col, currentlyTrainableArmyTypeIds } = cities[cityId];
      if (owner === neutralPlayerId && !tiles[row][col].armies) {
        const armyTypeId = (currentlyTrainableArmyTypeIds ?? EMPTY_ARRAY as string[]).reduce((max, val) =>
          defaultArmyTypeIdToOrder[max] > defaultArmyTypeIdToOrder[val] ? max : val,
          'scou'
        );
        dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, neutralPlayerId));
      }
    }
  }
};

/** Neutral armies increase at the start of each week. */
const neutralArmyGrowth = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { size, tiles, groups, turnNumber } = getState().game;

  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      const { armies } = tiles[row][col];
      if (armies) {
        const { groupId, armyTypeId } = armies[0];
        if (groups[groupId].owner === neutralPlayerId &&
          !armies.some(army => standardArmyTypes[army.armyTypeId].hero) &&
          armies.length < standardArmyTypes[armyTypeId].neutralMaxSize) {

          // At the start of the game, grow neutral armies into their starting size.
          const amount = turnNumber === 0 ? (standardArmyTypes[armyTypeId].neutralStartSize - 1) : 1;

          for (let i = 0; i < Math.min(amount, maxTileCapacity - armies.length); i++) {
            dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, neutralPlayerId));
          }
        }
      }
    }
  }
};

const resetAllArmyMove = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { game, game: { tiles, groups, heroes, currentPlayerId } } = getState();

  for (const groupId in groups) {
    const { row, col, owner } = groups[groupId];
    if (owner === currentPlayerId) {
      const armies = tiles[row][col].armies!.filter(army => army.groupId === groupId);

      const isShip = isGroupShip(game, groupId);
      const groupMoveBonus = straightMoveCost * (isShip
        ? 0
        : Math.max(...armies.map(army =>
          allArmyTypes[army.armyTypeId].hero
            ? getHerosValueOfAbility(heroes[army.id], Ability.GroupMoveBonus)
            : (allArmyTypes[army.armyTypeId].abilities[Ability.GroupMoveBonus] ?? 0)
        ))
      );

      for (const { id, armyTypeId, groupId, moveLeft } of armies) {
        const movePerTurn = groupMoveBonus + (isShip
          ? shipMove
          : (standardArmyTypes[armyTypeId].hero
            ? getHerosMove(heroes[id])
            : allArmyTypes[armyTypeId].move)
        );
        const newMoveLeft = Math.min(moveLeft + movePerTurn, movePerTurn);
        dispatch(setArmyMoveLeft(groupId, id, newMoveLeft));
      }
    }
  }
};

const payArmyUpkeepOrDesert = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { currentPlayerId, players, groups } = getState().game;
  const { gold, armyTypeIdToOrder } = players[currentPlayerId];

  const myArmies = getAllArmiesOfPlayer(getState(), currentPlayerId)
    .map(army => ({
      ...army,
      order: armyTypeIdToOrder[army.armyTypeId] + Math.random()
    }));
  myArmies.sort((a, b) => b.order - a.order);

  let totalUpkeep = 0;
  for (const army of myArmies) {
    const { id, armyTypeId, groupId } = army;
    const { upkeep } = allArmyTypes[armyTypeId];
    if (upkeep > gold - totalUpkeep) {
      const { row, col } = groups[groupId];
      dispatch(addWarReportItem(currentPlayerId, {
        row,
        col,
        reportType: WarReportType.ArmiesDeserted,
        value: 1,
        playerId: currentPlayerId
      }));
      dispatch(destroyArmy(groupId, id));
    } else {
      totalUpkeep += upkeep;
    }
  }

  dispatch(addGold(currentPlayerId, -totalUpkeep));
};

const moveCaravans = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities, currentPlayerId } = getState().game;
  for (const cityId in cities) {
    const { owner, row, col, incomingCaravans } = cities[cityId];
    if (owner === currentPlayerId && incomingCaravans !== undefined) {
      let newIncomingCaravans: (IncomingCaravan[]) | undefined = undefined;
      for (const { armyTypeId, days } of incomingCaravans) {
        if (days === 1) {
          dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, owner));
        } else {
          const newItem: IncomingCaravan = {
            armyTypeId,
            days: days - 1
          };
          newIncomingCaravans = newIncomingCaravans === undefined ? [newItem] : [...newIncomingCaravans, newItem];
        }
      }
      dispatch(setIncomingCaravans(cityId, newIncomingCaravans));
    }
  }
};

const trainNewArmies = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { cities, currentPlayerId } = getState().game;
  for (const cityId in cities) {
    const { owner, row, col, currentlyTrainingArmyTypeId, turnsTrainingLeft, caravanToCityId } = cities[cityId];
    if (owner === currentPlayerId && currentlyTrainingArmyTypeId !== undefined && turnsTrainingLeft !== undefined) {
      const { id: armyTypeId, trainingTime } = allArmyTypes[currentlyTrainingArmyTypeId];

      if (turnsTrainingLeft === 1) {
        if (caravanToCityId === undefined) {
          dispatch(createArmyAtOrAroundPlace(row, col, armyTypeId, owner));
        } else {
          const newItem: IncomingCaravan = {
            armyTypeId,
            days: caravanDelayDays
          };
          const { incomingCaravans } = getState().game.cities[caravanToCityId];
          const newIncomingCaravans = (incomingCaravans === undefined) ? [newItem] : [...incomingCaravans, newItem];
          dispatch(setIncomingCaravans(caravanToCityId, newIncomingCaravans));
        }
        dispatch(setTurnsTrainingLeft(cityId, trainingTime));
      } else {
        dispatch(setTurnsTrainingLeft(cityId, turnsTrainingLeft - 1));
      }
    }
  }
};

const triggerGrovesAtTheStartOfTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { structures, groups, heroes, currentPlayerId } = getState().game;

  for (const heroId in heroes) {
    const { groupId, level } = heroes[heroId];
    if (level === 1) {
      const { row, col, owner } = groups[groupId];
      if (owner === currentPlayerId && structures[row][col] === Structure.Grove) {
        dispatch(addExperiences(heroId, experienceForGrove));
      }
    }
  }
};

const triggerTemplesAtTheStartOfTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { tiles, structures, groups, currentPlayerId } = getState().game;

  for (const groupId in groups) {
    const { owner, row, col } = groups[groupId];
    if (owner === currentPlayerId && structures[row][col] === Structure.Temple) {
      (tiles[row][col].armies ?? EMPTY_ARRAY as Army[])
        .filter(army => !army.blessed)
        .forEach(army => dispatch(setArmyBlessed(army.groupId, army.id, true)));
    }
  }
};

const triggerEncampmentsAtTheStartOfTurn = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const { structures, groups, heroes, currentPlayerId } = getState().game;

  if (Object.values(heroes).some(hero => groups[hero.groupId].owner === currentPlayerId)) {
    return;
  }

  for (const groupId in groups) {
    const { owner, row, col } = groups[groupId];
    if (owner === currentPlayerId && structures[row][col] === Structure.Encampment) {
      dispatch(createArmyAtOrAroundPlace(row, col, `hro1`, currentPlayerId));
      break;
    }
  }
};

const regenerateFogOfWar = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { size, groups, flaggables, cities, players, currentPlayerId } = state.game;
  const currentTeamId = players[currentPlayerId].team;

  dispatch(_resetFogOfWar());

  // AI - remove fog of war from entire map. However, do not remove unexplored - AI may be playing with a human in a team.
  // NOTE: This has to be placed after ResetFogOfWar. Otherwise lastSeenOwner won't be updated.
  if (players[currentPlayerId].aiName !== undefined) {
    dispatch(revealFogOfWar(0, 0, 2 * size, false));
    dispatch(setLastRevealedPlaces(EMPTY_ARRAY as Place[]));
  }

  // Armies
  for (const groupId in groups) {
    const { owner, row, col } = groups[groupId];
    if (players[owner].team === currentTeamId) {
      const viewRadius = getViewRadiusOfGroup(state, groupId);
      dispatch(revealFogOfWar(row, col, viewRadius));
    }
  }

  // Flaggables
  for (const flaggableId in flaggables) {
    const { owner, row, col } = flaggables[flaggableId];
    if (players[owner].team === currentTeamId) {
      dispatch(revealFogOfWar(row, col, flaggableViewRadius));
    }
  }

  // Cities
  for (const cityId in cities) {
    const { owner, row, col, razed } = cities[cityId];
    if (players[owner].team === currentTeamId && !razed) {
      dispatch(revealFogOfWar(row, col, cityViewRadius));
      dispatch(revealFogOfWar(row, col + 1, cityViewRadius));
      dispatch(revealFogOfWar(row + 1, col, cityViewRadius));
      dispatch(revealFogOfWar(row + 1, col + 1, cityViewRadius));
    }
  }

  dispatch(setLastRevealedPlaces(EMPTY_ARRAY as Place[]));
};

const calculateHeroOffer = () => (dispatch: AppDispatch, getState: () => RootState) => {
  const state = getState();
  const { cities, players, currentPlayerId, turnNumber, heroes, groups } = state.game;
  const { gold } = players[currentPlayerId];
  const myCityIds = Object.keys(cities).filter(cityId => !cities[cityId].razed && cities[cityId].owner === currentPlayerId);
  const cityId = getRandomItem(myCityIds);
  const noOfHeroes = Object.values(heroes).filter(({ groupId }) => groups[groupId].owner === currentPlayerId).length;
  const lostEarlyHero = turnNumber <= 14 && myCityIds.length <= 2 && noOfHeroes === 0;

  dispatch(setHeroOffer(null));

  // Chance to get a hero. Decreases when you have more heroes and increases when you have a lot of gold.
  // Remember to update the manual if you change anything here.
  if (turnNumber > 1 && 2 > getRandom(
    2
    - (lostEarlyHero ? 1 : 0) // Chance gets better if you lost a hero at the start
    + noOfHeroes // Chance gets worse if you have a lot of heroes
    + (gold < 500 ? 1 : 0) // Chance gets worse if you have too little gold
    + (gold < 1000 ? 1 : 0) // Chance gets worse if you have too little gold
  )) {
    const level = 1 + getRandom(Math.min(
      4,
      Math.ceil(turnNumber / 7),
      Math.floor((gold - 50 - 25 * noOfHeroes) / 100)
    ));
    let cost = 50 + 25 * noOfHeroes + 100 * level;
    const armyTypeIds = [`hro${level}`];
    if (level > 1) {
      const armyTypeId = getRandomItem(Object.keys(standardArmyTypes).filter(armyTypeId => standardArmyTypes[armyTypeId].trainingTime === level));
      const { buildingCost } = standardArmyTypes[armyTypeId];
      if (cost + buildingCost / 5 <= gold) {
        cost += buildingCost / 5;
        armyTypeIds.push(armyTypeId);
      }
    }

    if (cost <= gold) {
      const portraitId = getRandomUnusedHeroPortraitId(getAllUsedHeroPortraitIds(state));

      dispatch(setHeroOffer({
        cost,
        cityId,
        portraitId,
        armyTypeIds
      }));
    }
  }

};
