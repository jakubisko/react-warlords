import { Structure } from 'ai-interface/constants/structure';
import { Terrain } from 'ai-interface/constants/terrain';
import { MapData } from '../game/types';

/**
 * This state contains data that are related to map editor. They are temporary, not used by AI.
 * None of it is saved when land or game is saved.
 */
export interface EditorState {
  readonly tool: EditorTool;
  readonly terrainBrush: Terrain;
  readonly terrainBrushSize: number; // Size of terrain brush, use only odd numbers - 1x1, 3x3, 5x5... 
  readonly structure: Structure;
  readonly undoQueue: MapData[]; // Enables undo/redo actions in editor. First is the oldest state, last is the current state.
  readonly undoQueuePosition: number; // When there are no actions to redo, this points to last element of undoQueue.
  readonly lastRandomMapSettings: RandomMapSettings;
}

/** Apart from size and players, all arguments have range from 0 to 1. */
export interface RandomMapSettings {
  readonly size: number;
  readonly players: number;
  readonly water: number;
  readonly mountains: number;
  readonly heat: number;
  readonly cold: number;
  readonly cities: number;
  readonly ruins: number;
}

export enum EditorTool {
  TerrainBrush,
  StructurePlacing,
  ArmyPlacing,
  ArtifactPlacing,
  Erase,
  PlaceInfo
}

export interface StructureAtPlace {
  readonly row: number;
  readonly col: number;
  readonly structure: Structure;
  readonly border?: number;
}
