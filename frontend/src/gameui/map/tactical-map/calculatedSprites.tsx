export function calculateFogOfWarSpriteId(covered: boolean[][], row: number, col: number, size: number): number {
  if (covered[row][col]) {
    return 20;
  } else {
    const a =
      (row > 0 && covered[row - 1][col]) ||
      (col > 0 && covered[row][col - 1]) ||
      (row > 0 && col > 0 && covered[row - 1][col - 1]);

    const b =
      (row > 0 && covered[row - 1][col]) ||
      (col < size - 1 && covered[row][col + 1]) ||
      (row > 0 && col < size - 1 && covered[row - 1][col + 1]);

    const c =
      (col > 0 && covered[row][col - 1]) ||
      (row < size - 1 && covered[row + 1][col]) ||
      (row < size - 1 && col > 0 && covered[row + 1][col - 1]);

    const d =
      (row < size - 1 && covered[row + 1][col]) ||
      (col < size - 1 && covered[row][col + 1]) ||
      (row < size - 1 && col < size - 1 && covered[row + 1][col + 1]);

    return (a ? 1 : 0) + (b ? 2 : 0) + (c ? 4 : 0) + (d ? 8 : 0);
  }
}

/** Given the coordinates of a tile that is outside the land, this returns a sprite to draw the edge around the land. */
export function calculateEdgeSpriteId(col: number, row: number, size: number): number {
  if (row === -1) {
    if (col === -1) {
      return 1;
    } else if (col === size) {
      return 3;
    } else if (col >= 0 && col < size) {
      return 2;
    }
  } else if (row === size) {
    if (col === -1) {
      return 7;
    } else if (col === size) {
      return 9;
    } else if (col >= 0 && col < size) {
      return 8;
    }
  } else if (col === -1 && row >= 0 && row < size) {
    return 4;
  } else if (col === size && row >= 0 && row < size) {
    return 6;
  }
  return 5;
}
