import { GameApi } from 'ai-interface/apis/GameApi';
import { LandApi } from 'ai-interface/apis/LandApi';
import { DrawApi } from 'ai-interface/apis/DrawApi';
import { Place } from 'ai-interface/types/place';
import { ThreatManager } from './ThreatManager';
import { straightMoveCost } from 'ai-interface/constants/path';
import { standardArmyTypes } from 'ai-interface/constants/standardArmyTypes';
import { CityData } from 'ai-interface/types/city';
import { RazeManager } from './RazeManager';
import { CityManager } from './CityManager';

export class GuardManager {

  private cityManager!: CityManager;

  private preferredGuardPowerDividedByUtilityAndThreat = 0;

  constructor(
    private readonly land: LandApi,
    private readonly game: GameApi,
    private readonly draw: DrawApi,
    private readonly threatManager: ThreatManager,
    private readonly razeManager: RazeManager
  ) {
  }

  public setCityManager(cityManager: CityManager) {
    this.cityManager = cityManager;
  }

  /**
   * 1. Determine how much upkeep we're willing to pay for guarding armies. This is some fraction of our income.
   * 2. Determine the total guard power and the total upkeep of all armies under our control.
   * 3. The ratio in (2) determines the "price per 1 power of guard".
   * 4. From (1) and (3) calculate the total guard power we're willing to support.
   *    Now we need to redistribute that guard power. Cities with higher utility, and cities more threatened get the priority.
   * 5. For each city, determine the "utility multiplied by threat". Determine the sum of this value across all of your cities.
   * 6. Divide the total guard power we're willing to support (4) by the sum of "utility multiplied by threat" of your cities (5).
   * 7. Whenever you need to know the preferred guard power, multiply the (6) by the utility and the threat of given city.
   * 8. If the guard power available exceeds the threats, do not use the full guard power available.
   * 
   * Problems of the algorithm:
   * 1. Cities gained during the turn will bring the upkeep of guarding above the given amount.
   */
  public calculatePreferredGuardPowerForEachCity(maximalUpkeepForGuarding: number) {
    let totalGuardPower = 0;
    let totalUpkeep = 0;
    for (const { armyTypeId } of this.game.getAllMyArmies()) {
      totalGuardPower += this.threatManager.armyTypeIdToGuardPower[armyTypeId];
      totalUpkeep += standardArmyTypes[armyTypeId].upkeep;
    }

    let sumOfThreats = 0;
    let sumOfUtilityMultipliedByThreat = 1; // To prevent division by zero

    const myPlayerId = this.game.getMyPlayerId();
    const myCities = this.land.getAllCityIds()
      .map(cityId => this.game.getCity(cityId))
      .filter(city => !city.razed && city.owner === myPlayerId);
    for (const city of myCities) {
      const utility = this.cityManager.getCityUtility(city);
      const threat = this.threatManager.getThreatToCityInNearFuture(city.id);
      sumOfThreats += threat;
      sumOfUtilityMultipliedByThreat += utility * threat;
    }

    const guardPowerToDistribute = totalUpkeep > 0
      // If the guard power available exceeds the threats, do not use the full guard power available.
      ? Math.min(sumOfThreats, totalGuardPower * maximalUpkeepForGuarding / totalUpkeep)
      : 0;
    this.preferredGuardPowerDividedByUtilityAndThreat = guardPowerToDistribute / sumOfUtilityMultipliedByThreat;

    this.draw.drawText(`Upkeep for guarding`, { row: -3, col: 15 });
    this.draw.drawText('' + Math.round(maximalUpkeepForGuarding), { row: -2, col: 15 });
    this.draw.drawText(`Guard power to distribute`, { row: -3, col: 20 });
    this.draw.drawText('' + Math.round(guardPowerToDistribute), { row: -2, col: 20 });
    this.draw.drawText(`Power free to move`, { row: -3, col: 25 });
    this.draw.drawText('' + Math.round(totalGuardPower - guardPowerToDistribute), { row: -2, col: 25 });
  }

  public getPreferredGuardPowerForPlace(place: Place): number {
    // IMPORTANT: If you ever choose to guard places other than cities, bigger refactoring will be needed. For example:
    // - Code that attracts the guards from outside works only for cities.
    // - Code that prevents armies from leaving a threatened city works only for cities.
    const cityId = this.land.getCityIdAt(place);
    if (cityId !== undefined) {
      const city = this.game.getCity(cityId);
      this.getPreferredGuardPowerForCity(city);
    }

    return 0;
  }

  public getPreferredGuardPowerForCity(city: CityData): number {
    return Math.round(
      this.preferredGuardPowerDividedByUtilityAndThreat
      * this.cityManager.getCityUtility(city)
      * this.threatManager.getThreatToCityInNearFuture(city.id)
    );
  }

  private getTotalGuardPowerOfCity(city: CityData): number {
    const { row, col } = city;
    return [
      ...this.game.getMyArmiesAt({ row, col }),
      ...this.game.getMyArmiesAt({ row: row + 1, col }),
      ...this.game.getMyArmiesAt({ row, col: col + 1 }),
      ...this.game.getMyArmiesAt({ row: row + 1, col: col + 1 })
    ].reduce((sum, { armyTypeId }) => sum + this.threatManager.armyTypeIdToGuardPower[armyTypeId], 0);
  }

  /**
   * Given a city, returns the Power that is needed in addition to the armies already in the city to guard it.
   * Returns a negative number if there are more armies in the city than the optimum needed to guard it.
   */
  public getAdditionalGuardPowerNeededForCity(city: CityData): number {
    return this.getPreferredGuardPowerForCity(city) - this.getTotalGuardPowerOfCity(city);
  }

  /**
   * This method will transfer away from our group armies needed to guard. Armies that are still in the group are free to move away.
   * Returns true when entire group is needed to guard the threatened city.
   */
  public entireGroupIsNeededForGuardingCurrentPlace(groupId: string, place: Place): boolean {
    // We only guard cities.
    const cityId = this.land.getCityIdAt(place);
    if (cityId === undefined) {
      return false;
    }

    // Cities that are not razed.
    const city = this.game.getCity(cityId);
    if (city.razed) {
      return false;
    }

    // Check whether we need the defenders. If it looks like a hopeless city to defend, just raze it and you can leave.
    const powerFreeToLeave = -this.getAdditionalGuardPowerNeededForCity(city);
    if (powerFreeToLeave <= 0 && this.razeManager.shouldIRazeTheCity(city, this.getTotalGuardPowerOfCity(city))) {
      this.game.razeCity(city.id);
      return false;
    }

    if (powerFreeToLeave <= 0) {
      return true;
    }

    // Sometimes a group leaves a city that will be attacked next turn, but only makes some 1-2 steps. It accomplishes nothing, only makes
    // the city easier to capture. To prevent this, we forbid groups with too little movement left to leave threatened city.
    const moveLeft = this.game.getMoveLeftOfGroup(groupId);
    if (moveLeft <= 6 * straightMoveCost && this.threatManager.getThreatToCityUpToDay(cityId, 0) > 0) {
      return true;
    }

    const theseArmies = this.game.getArmiesInGroup(groupId);
    // Fast armies and heroes should leave, slow armies should stay.
    theseArmies.sort((a1, a2) => (standardArmyTypes[a1.armyTypeId].hero ? 9999 : a1.moveLeft) - (standardArmyTypes[a2.armyTypeId].hero ? 9999 : a2.moveLeft));
    const powers = theseArmies.map(army => this.threatManager.armyTypeIdToGuardPower[army.armyTypeId]);

    let armiesStaying = 0;
    while (powers.slice(armiesStaying).reduce((sum, val) => sum + val, 0) > powerFreeToLeave) {
      armiesStaying++;
    }

    if (armiesStaying > 0 && armiesStaying < theseArmies.length) {
      // We need to split the group. When we transfer armies to a new group, that group won't move this turn.
      const newGroupId = this.game.transferArmyFromGroupToNewGroup(theseArmies[0].id, groupId);
      for (let i = 1; i < armiesStaying; i++) {
        this.game.transferArmyFromGroupToOtherGroup(theseArmies[i].id, groupId, newGroupId);
      }
    }

    return armiesStaying === theseArmies.length;
  }

  public drawGuardPowerForEachCity(): void {
    const myPlayerId = this.game.getMyPlayerId();
    const cities = this.land.getAllCityIds()
      .map(cityId => this.game.getCity(cityId))
      .filter(city => !city.razed && city.owner === myPlayerId);

    for (const city of cities) {
      const optimal = this.getPreferredGuardPowerForCity(city);
      const current = this.getTotalGuardPowerOfCity(city);
      const text = `Guard optimal: ${optimal}; current: ${current}`;

      const place: Place = {
        row: city.row + 3,
        col: city.col + 0.5
      };
      this.draw.drawTextWhenNothingIsSelected(text, place);
    }
  }

}
