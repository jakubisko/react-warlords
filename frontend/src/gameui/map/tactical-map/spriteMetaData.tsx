import { heroExperienceLevels } from 'ai-interface/constants/experience';
import { FreeformSprite, Sheet, sheetResolution, TileSprite } from './spriteMetaDataTypes';

// Sprite coordinates for flags
export const flagLeft = [0, 23, 60, 115];
export const flagWidth = [22, 36, 54, 72];
export const flagHeight = 16;

export const spriteSheetFlags: Sheet<FreeformSprite> =
  [[357, 78], [336, 78], [271, 0], [124, 243], [102, 243], [428, 175], [271, 31], [293, 31], [311, 78]].reduce(
    (acc, [L, T], i) => ({ ...acc, [i]: { N: 'structures', L, T, W: 21, H: 30, X: 1, Y: 29 } }), {});

export const spriteSheetArmies = createSpriteSheet('armies', [
  ['bag', 'wolf', 'unic', 'spid', 'elf', 'scou', 'dwar', 'gian'],
  ['scor', 'wurm', 'drag', 'pike', 'pega', 'orcs', 'hro1', 'elem'],
  ['mino', 'medu', 'mamm', 'linf', 'lcav', 'devl', 'demn', 'cata'],
  ['grif', 'wiz', 'crow', 'arch', 'hinf', 'hcav', 'ghos', 'ship'],
  ['rnd0', 'rnd1', 'rnd2', 'rnd3', 'rnd4', 'dmg', 'kill', 'ambu']
]);
for (let lvl = 2; lvl < heroExperienceLevels.length; lvl++) {
  spriteSheetArmies[`hro${lvl}`] = spriteSheetArmies[`hro1`];
}

export const spriteSheetPath = createSpriteSheet('path', [
  [0, 20, 2, 4, 6, 8, 10],
  [1, -1, 3, 5, 7, 9, 11]
]);

export const spriteSheetFogOfWar = createSpriteSheet('fogOfWar', [
  [+3, +5, 12, 10, 20],
  [11, +7, 14, 13, +9],
  [+2, +8, +4, +1, +6],
  [53, 55, 62, 60, 70],
  [61, 57, 64, 63, 59],
  [52, 58, 54, 51, 56],
  [71, 72, 73, 74, 75]
]);

export const spriteSheetWater = createSpriteSheet('water', [
  [11, 12, 13, 14, 15, 16, 17, 18],
  [21, 22, 23, 24, 25, 26, 27, 28],
  [31, 32, 33, 34, 35, 36, 37, 38],
  [41, 42, 43, 44, 45, 46, 47, 48],
  [51, 52, 53, -1, 55, 56, 57, 58],
  [61, -1, 63, 64, 65, 66, 67, 68],
  [71, 72, 73, 74, 75, 76, 77, -1]
]);

export const spriteSheetDesert = createSpriteSheet('desert', [
  [11, 14, +7, 13, 15, 20, 34, 34, 34, 34],
  [+5, +9, 12, +6, +3, 21, 34, 34, 34, 34],
  [10, +8, +4, +1, +2, 22, 34, 34, 34, 34],
  [32, 32, 32, 33, 33, 33, 34, 34, 34, 34],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31],
  [19, -1, -1, -1, 16, 17, 18, -1, 60, 40],
  [41, 61, 52, 72, 62, 71, 42, 51, 50, 70],
]);

// NOTE: In original image, sprites "14" and "7" were wrongly placed 1px to the right.
export const spriteSheetForest = {
  ...createSpriteSheet('forest1', [
    [11, 14, +7, 13, 15, 20, 34, 34, 34, 34],
    [+5, +9, 12, +6, +3, 21, 34, 34, 34, 34],
    [10, +8, +4, +1, +2, 22, 34, 34, 34, 34],
    [19, 40, 50, 33, 33, 33, 34, 34, 34, 34],
    [60, 16, 17, 33, 33, 33, 30, 30, 31, 31],
    [18, 70, 99, 33, 33, 33, 30, 30, 31, 31],
  ]),
  ...createSpriteSheet('forest2', [
    [32, 32, 32],
    [32, 32, 32],
    [32, 32, 32],
  ])
};

// Forest sprite sheet doesn't have "corner" tiles, so use the same as border tiles
for (let i = 40; i < 80; i += 10) {
  spriteSheetForest[i + 1] = spriteSheetForest[i + 2] = spriteSheetForest[i];
}

export const spriteSheetHill = createSpriteSheet('hill', [
  [11, 14, +7, 13, 15, 20, 34, 34, 34, 34, 72],
  [+5, +9, 12, +6, +3, 21, 34, 34, 34, 34, 52],
  [10, +8, +4, +1, +2, 22, 34, 34, 34, 34, 62],
  [32, 32, 32, 33, 33, 33, 34, 34, 34, 34, 71],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31, 51],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31, 42],
  [19, 40, 50, 60, 16, 17, 18, 70, 61, -1, 41],
]);

export const spriteSheetVolcanic = createSpriteSheet('volcanic', [
  [11, 14, +7, 13, 15, 20, 30, 30],
  [+5, +9, 12, +6, +3, 21, 30, 30],
  [10, +8, +4, +1, +2, 22, 31, 31],
  [19, -1, -1, -1, 16, 17, 31, 31],
  [18, -1, 60, 70, 50, 32, 32, 32],
  [72, 52, 62, 71, 42, 32, 32, 32],
  [61, 41, 51, 40, -1, 32, 32, 32],
]);

export const spriteSheetMountain = createSpriteSheet('mountain', [
  [11, 14, +7, 13, 15, 20, 34, 34, 34, 34],
  [+5, +9, 12, +6, +3, 21, 34, 34, 34, 34],
  [10, +8, +4, +1, +2, 22, 34, 34, 34, 34],
  [32, 32, 32, 33, 33, 33, 34, 34, 34, 34],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31],
  [32, 32, 32, 33, 33, 33, 30, 30, 31, 31],
  [19, -1, -1, -1, 16, 17, 18, -1, 70, 60],
  [71, 52, 62, 72, 42, 51, 61, 41, 40, 50]
]);

export const spriteSheetOpen = createSpriteSheet('open', [
  [11, 14, +7, 13, 15, 20],
  [+5, +9, 12, +6, +3, 21],
  [10, +8, +4, +1, +2, 22],
  [33, 33, 33, 36, 25, 23],
  [33, 33, 33, 24, 30, 30],
  [33, 33, 33, 35, 30, 30],
  [34, 34, 34, 34, 31, 31],
  [34, 34, 34, 34, 31, 31],
  [34, 34, 34, 34, 32, 32],
  [34, 34, 34, 34, 32, 32]
]);

export const spriteSheetIce = {
  ...createSpriteSheet('ice1', [
    [11, 14, +7, 13, 15, 20, 30, 30, 31, 31],
    [+5, +9, 12, +6, +3, 21, 30, 30, 31, 31],
    [10, +8, +4, +1, +2, 22, 70, 60, 40, 50],
    [41, 61, 52, 72, 71, 62, 51, 42, -1, -1],
  ]),
  ...createSpriteSheet('ice2', [
    [19, 16, 17, 18],
  ])
};

// NOTE: In original image, sprite "52" was wrongly placed 1px to the left.
export const spriteSheetSwamp = createSpriteSheet('swamp', [
  [11, 14, +7, 13, 15, 20, 60, 70],
  [+5, +9, 12, +6, +3, 21, 40, 50],
  [10, +8, +4, +1, +2, 22, 41, 98],
  [31, 31, 31, 72, 52, 42, 61, 19],
  [31, 31, 31, 62, 71, 51, 30, 30],
  [31, 31, 31, 16, 17, 18, 30, 30],
]);

const ridgeSpritePlacement = [
  [+9, 10, -1, 21, 12, 28, +5],
  [+8, 22, 19, 26, 25, +6, +3],
  [+4, +2, 18, 20, 17, +1, 24]
];
export const spriteSheetRidgeDesert = createSpriteSheet('ridgeDesert', ridgeSpritePlacement);

export const spriteSheetRidgeIce = createSpriteSheet('ridgeIce', ridgeSpritePlacement);

export const spriteSheetRidgeOpen = createSpriteSheet('ridgeOpen', ridgeSpritePlacement);

export const spriteSheetWaterfall = createSpriteSheet('waterfall', [
  [+5, 10, 15],
  [21, 26, +0]
]);
spriteSheetWaterfall[16] = spriteSheetWaterfall[0];
spriteSheetWaterfall[31] = spriteSheetWaterfall[15];

export const spriteSheetEdge = createSpriteSheet('edge', [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9]
]);

///////////////////////////////////////////////////////////////////////////////////////////////////

function createSpriteSheet(spriteSheetName: string, spritePlacement: string[][] | number[][]): Sheet<TileSprite> {
  const result: Sheet<TileSprite> = {};
  const right = spritePlacement[0].length;
  const bottom = spritePlacement.length;

  for (let top = 0; top < bottom; top++) {
    for (let left = 0; left < right; left++) {
      const id = spritePlacement[top][left];
      if (id !== '' && id !== -1 && result[id] === undefined) {

        let W = 1;
        while (left + W < right && spritePlacement[top][left + W] === id) { W++; }

        let H = 1;
        while (top + H < bottom && spritePlacement[top + H][left] === id) { H++; }

        result[id] = {
          L: left * sheetResolution,
          T: top * sheetResolution,
          W,
          H,
          N: spriteSheetName
        };
      }
    }
  }

  return result;
}
